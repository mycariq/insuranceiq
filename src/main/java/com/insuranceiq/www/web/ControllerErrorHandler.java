package com.insuranceiq.www.web;

import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.cariq.toolkit.utils.CariqException;
import com.cariq.toolkit.utils.ResponseJson;
import com.google.common.collect.Lists;

@ControllerAdvice
public class ControllerErrorHandler {

	private static final Logger MainLogger = Logger.getLogger(ControllerErrorHandler.class);

	//Light logger is not sending emails upon failure.
	private static final Logger LightLogger = Logger.getLogger("ControllerErrorHandler.Light");

	@ExceptionHandler(CariqException.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ResponseJson handleCariqException(CariqException ex) {
		Logger logger = LightLogger;
		logger.error("\n---------INSURANCEIQ EXCEPTION---------------" + ex.getMsg() + "\n");
		logger.error(ExceptionUtils.getStackTrace(ex));
		ResponseJson responseJson = new ResponseJson();
		responseJson.setMessages(ex.getMsg());
		return responseJson;
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public List<String> handleJsonException(HttpMessageNotReadableException ex) {
		Logger logger = LightLogger;
		logger.error("\n---------JSON EXCEPTION---------------" + ex.getMessage() + "\n");
		logger.error(ExceptionUtils.getStackTrace(ex));
		List<String> list = Lists.newArrayList("Not valid request");
		return list;
	}

	@ExceptionHandler(Exception.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public List<String> handleException(Exception ex) {
		Logger logger = MainLogger;
		logger.error("\n---------GENERIC EXCEPTION---------------" + ex.getMessage() + "\n");
		logger.error(ExceptionUtils.getStackTrace(ex));
		List<String> list = Lists.newArrayList("Some internal error!");
		return list;
	}
}