package com.insuranceiq.www.web.report;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.JsonValidator;
import com.cariq.toolkit.utils.PaginationEntityJson;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.service.report.CreateReportJson;
import com.cariq.toolkit.service.report.ReportService;
import com.cariq.toolkit.serviceimpl.report.GetReportJson;

@CarIQPublicAPI(name = "Reports", description = "Everything related to Reports")
@RequestMapping("/reports")
@Controller
public class ReportController {

	@Autowired
	private ReportService reportService;

	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Get Available Types of Reports", responseClass = String.class, internal = true)
	@RequestMapping(value = "/types", method = RequestMethod.GET)
	@ResponseBody
	public List<String> getTypes() {
		try (CarIQAPI _createLog = new CarIQAPI("CARIQ_createLog")) {
			return reportService.getTypes();
		}
	}

	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Create a Report request, to be processed asynchoronously", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public ResponseJson createReport(@Valid @RequestBody CreateReportJson json, BindingResult result) {
		try (CarIQMutableAPI _createReport = new CarIQMutableAPI("CARIQ_createReport", json)) {
			JsonValidator.validate(result);
			return reportService.createReport(json.getFromDate(), json.getToDate(), json.getType(),
					json.getOwnerType(), json.getOwnerId());
		}
	}

	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Get all the reports for given owner pagewise", responseClass = GetReportJson.class, internal = true)
	@RequestMapping(value = "/{ownerType}/{ownerId}/{type}/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public PaginationEntityJson<List<GetReportJson>> getReports(
			@CarIQPublicAPIParameter(name = "ownerType", description = "type of Report owner") @PathVariable String ownerType,
			@CarIQPublicAPIParameter(name = "ownerId", description = "the Identifier for owner") @PathVariable String ownerId,
			@CarIQPublicAPIParameter(name = "type", description = "the type") @PathVariable String type,
			@CarIQPublicAPIParameter(name = "pageNo", description = "the pageNo") @PathVariable int pageNo,
			@CarIQPublicAPIParameter(name = "pageSize", description = "the pageSize") @PathVariable int pageSize) {
		try (CarIQAPI _getReports = new CarIQAPI("CARIQ_getReports")) {
			return reportService.getReports(ownerType, ownerId, type, pageNo, pageSize);
		}
	}

	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Delete a report identified by given id", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/{reportId}/{ownerType}/{ownerId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson deleteReport(@CarIQPublicAPIParameter(name = "reportId", description = "the reportId") @PathVariable long reportId,
			@CarIQPublicAPIParameter(name = "ownerType", description = "type of Report owner") @PathVariable String ownerType,
			@CarIQPublicAPIParameter(name = "ownerId", description = "the Identifier for owner") @PathVariable String ownerId) {
		try (CarIQMutableAPI _deleteReport = new CarIQMutableAPI("CARIQ_deleteReport", reportId)) {
			return reportService.deleteReport(reportId, ownerType, ownerId);
		}
	}

}
