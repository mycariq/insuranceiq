package com.insuranceiq.www.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_Out;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.service.ProxyObjectService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;


/**
 * The Class TestProxyObjectController.
 */
@CarIQPublicAPI(name = "ProxyObject", description = "Everything related to ProxyObject")
@Controller
@RequestMapping("/testproxyobject")
public class TestProxyObjectController {

	/** The domainsAccessService. */
	@Autowired
	private ProxyObjectService proxyObjectService;

	@CarIQPublicAPIMethod(description = "Create or get the ProxyObject", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/createOrGetProxy", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson createOrGetProxyObject(@RequestBody ProxyObject_In params) {
		try (CarIQMutableAPI _createOrGetProxyObject = new CarIQMutableAPI("CARIQ_createOrGetProxyObject", params)) {
			return proxyObjectService.createOrGetProxyObject(params);
		}
	}

	@CarIQPublicAPIMethod(description = "Delete proxyObject", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/removeProxy", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson deleteProxyObject(ProxyObject_In params){
		try (CarIQMutableAPI _deleteProxyObject = new CarIQMutableAPI("CARIQ_deleteProxyObject", params)) {
			return proxyObjectService.deleteProxyObject(params);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Get all proxy-objects", responseClass = ProxyObject_Out.class, internal = true)
	@RequestMapping(value = "/getAll/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<ProxyObject_Out> getAllTags(
			@CarIQPublicAPIParameter(name = "PageNo", description = "Page Number") @PathVariable int pageNo, 
			@CarIQPublicAPIParameter(name = "PageSize", description = "Page Size") @PathVariable int pageSize) {
		try (CarIQAPI _getTags = new CarIQAPI("CARIQ_getAllTags_with_pagination")) {
			return proxyObjectService.getAllProxyObjects(pageNo, pageSize);
		}
	}

	@CarIQPublicAPIMethod(description = "Get proxy-object by domain, objectId and type", responseClass = ProxyObject_Out.class, internal = true)
	@RequestMapping(value = "/getProxyByDomainObjectIdType/{domainName}/{objectId}/{objectType}", method = RequestMethod.GET)
	@ResponseBody
	public ProxyObject_Out getProxyByDomainObjectIdType(
			@CarIQPublicAPIParameter(name = "DomainName", description = "Domain Name") @PathVariable String domainName,
			@CarIQPublicAPIParameter(name = "ObjectId", description = "Object Id") @PathVariable Long objectId,
			@CarIQPublicAPIParameter(name = "ObjectType", description = "Object Type") @PathVariable String objectType) {
		try (CarIQAPI _getProxyByDomainObjectIdType = new CarIQAPI("CARIQ_getProxyByDomainObjectIdType")) {
			return proxyObjectService.getProxyByDomainObjectIdType(domainName, objectId, objectType);
		}
	}

	@CarIQPublicAPIMethod(description = "Get proxy object by domain, signature and object-type", responseClass = ProxyObject_Out.class, internal = true)
	@RequestMapping(value = "/getProxyByDomainSignatureObjectType/{domainName}/{signature}/{objectType}", method = RequestMethod.GET)
	@ResponseBody
	public ProxyObject_Out getProxyByDomainSignatureObjectType(
			@CarIQPublicAPIParameter(name = "DomainName", description = "Domain Name") @PathVariable String domainName,
			@CarIQPublicAPIParameter(name = "Signature", description = "Signature") @PathVariable String signature,
			@CarIQPublicAPIParameter(name = "ObjectType", description = "Object Type") @PathVariable String objectType) {
		try (CarIQAPI _getProxyByDomainSignatureObjectType = new CarIQAPI("CARIQ_getProxyByDomainSignatureObjectType")) {
			return proxyObjectService.getProxyByDomainSignatureObjectType(domainName, signature, objectType);
		}
	}
	 
}
