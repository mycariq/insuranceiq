package com.insuranceiq.www.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.model.User.UpdateUser_In;
import com.cariq.toolkit.model.User.User_In;
import com.cariq.toolkit.model.User.User_Out;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.service.UserService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;


/**
 * The Class UserController.
 */
@CarIQPublicAPI(name = "User", description = "Everything related to User")
@Controller
@RequestMapping("/user")
public class UserController {

	/** The userservice. */
	@Autowired
	private UserService userservice;

	/**
	 * Adds the user.
	 * @param params
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Create user", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson addUser(@RequestBody User_In params) {
		try (CarIQMutableAPI _addUser = new CarIQMutableAPI("CARIQ_addUser", params)) {
			return userservice.addUser(params);
		}
	}

	/**
	 * Gets the users with pagination.
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Get all the users", responseClass = User_Out.class, internal = true)
	@RequestMapping(value = "/getAll/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<User_Out> getUsers(
			@CarIQPublicAPIParameter(name = "PageNo", description = "Page Number") @PathVariable int pageNo, 
			@CarIQPublicAPIParameter(name = "PageSize", description = "Page Size") @PathVariable int pageSize) {
		try (CarIQAPI _getUsers = new CarIQAPI("CARIQ_getAllUsers_with_pagination")) {
			return userservice.getAllUsers(pageNo, pageSize);
		}
	}

	/**
	 * Gets the user.
	 * @param username
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Get signle user by username", responseClass = User_Out.class, internal = true)
	@RequestMapping(value = "/get/{username}", method = RequestMethod.GET)
	@ResponseBody
	public User_Out getUser(
			@CarIQPublicAPIParameter(name = "UserName", description = "User Name") @PathVariable String username) {
		try (CarIQAPI _getUser = new CarIQAPI("CARIQ_getUser")) {
			return userservice.getUser(username);
		}
	}

	/**
	 * Get all users by subscriptionName.
	 * @param username
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Get all users by subscription", responseClass = User_Out.class, internal = true)
	@RequestMapping(value = "/getBySubscription/{subscriptionName}", method = RequestMethod.GET)
	@ResponseBody
	public List<User_Out> getUsersBySubscriptionName(
			@CarIQPublicAPIParameter(name = "SubscriptionName", description = "Subscription Name") @PathVariable String subscriptionName) {
		try (CarIQAPI _getUsersBySubscriptionName = new CarIQAPI("CARIQ_getUsersBySubscriptionName")) {
			return userservice.getUsersBySubscriptionName(subscriptionName);
		}
	}
	
	/**
	 * Update user.
	 * @param params
	 * @param username
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Update user", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/update/{username}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseJson updateUser(@RequestBody UpdateUser_In params,
			@CarIQPublicAPIParameter(name = "UserName", description = "User Name") @PathVariable String username) {
		try (CarIQMutableAPI _updateUser = new CarIQMutableAPI("CARIQ_updateUser", params)) {
			return userservice.updateUser(params, username);
		}
	}

	/**
	 * Removes the user.
	 * @param username
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Remove user", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/remove/{username}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson removeUser(
			@CarIQPublicAPIParameter(name = "UserName", description = "User Name") @PathVariable String username) {
		try (CarIQMutableAPI _removeUser = new CarIQMutableAPI("CARIQ_removeUser", username)) {
			return userservice.deleteUser(username);
		}
	}
}
