package com.insuranceiq.www.web.async;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.JsonValidator;
import com.cariq.toolkit.utils.ResponseHelper;
import com.cariq.toolkit.service.async.WorkItemService;
import com.cariq.toolkit.serviceimpl.async.AddWorkItemJson;
import com.cariq.toolkit.serviceimpl.async.GetListOfWorkItem;
import com.cariq.toolkit.serviceimpl.async.GetWorkItem;

@CarIQPublicAPI(name = "WorkItems", description = "[Admin only] Everything related to WorkItems i.e. Async Work Management")
@RequestMapping("/workitems")
@Controller
public class WorkItemController {

	@Autowired
	private WorkItemService workItemService;

	/**
	 * addWorkItem
	 * 
	 * @param addWorkItemJson
	 * @param result
	 * @return Object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Add a new Work Item", responseClass = Object.class)
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public Object addWorkItem(@RequestBody @Valid AddWorkItemJson addWorkItemJson, BindingResult result) {
		JsonValidator.validate(result);
		try (CarIQAPI _workItem_addWorkItem = new CarIQAPI("CARIQ_workItem_addWorkItem")) {
			return workItemService.addWorkItem(addWorkItemJson.getWorkerClass(), addWorkItemJson.getInputJson(),
					addWorkItemJson.getType(), addWorkItemJson.getName(), addWorkItemJson.getDescriptions());
		}
	}

	/**
	 * getListOfWorkItem
	 * 
	 * @param type
	 * @param status
	 * @param pageNo
	 * @param size
	 * @param response
	 * @return Object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Get list of work items with given type and status - use 'All' for everything", responseClass = GetListOfWorkItem.class)
	@RequestMapping(value = "/{type}/{status}/{pageNo}/{size}", method = RequestMethod.GET)
	@ResponseBody
	public Object getListOfWorkItem(
			@CarIQPublicAPIParameter(name = "type", description = "the type") @PathVariable String type,
			@CarIQPublicAPIParameter(name = "status", description = "the status") @PathVariable String status,
			@CarIQPublicAPIParameter(name = "pageNo", description = "the pageNo") @PathVariable int pageNo,
			@CarIQPublicAPIParameter(name = "size", description = "the size") @PathVariable int size,
			HttpServletResponse response) {
		try (CarIQAPI _workItem_getListOfWorkItem = new CarIQAPI("CARIQ_workItem_getListOfWorkItem")) {
			if (type == null || status == null)
				return ResponseHelper.setResponse("type and status should not be null");
			if (type.equals("All") && status.equals("All"))
				return workItemService.getListOfWorkItem(pageNo, size);
			if (type.equals("All"))
				return workItemService.getListOfWorkItemByStatus(status, pageNo, size);
			if (status.equals("All"))
				return workItemService.getListOfWorkItemByType(type, pageNo, size);

			return workItemService.getListOfWorkItem(type, status, pageNo, size);
		}
	}

	/**
	 * getWorkItemByUUID
	 * 
	 * @param uuid
	 * @param response
	 * @return Object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Get WorkItem given identifier", responseClass = GetWorkItem.class)
	@RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
	@ResponseBody
	public Object getWorkItemByUUID(
			@CarIQPublicAPIParameter(name = "uuid", description = "the uuid") @PathVariable String uuid,
			HttpServletResponse response) {
		try (CarIQAPI _workItem_getWorkItemByUUID = new CarIQAPI("CARIQ_workItem_getWorkItemByUUID")) {
			return workItemService.getWorkItemByUUID(uuid);
		}
	}

	/**
	 * getListOfWorkItemType
	 * 
	 * @param response
	 * @return
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Get List of Work Item Types", responseClass = String.class)
	@RequestMapping(value = "/getListOfWorkItemType", method = RequestMethod.GET)
	@ResponseBody
	public Object getListOfWorkItemType(HttpServletResponse response) {
		try (CarIQAPI _workItem_getListOfWorkItemType = new CarIQAPI("CARIQ_workItem_getListOfWorkItemType")) {
			return workItemService.getListOfWorkItemType();
		}
	}

	/**
	 * getListOfWorkItemStatus
	 * 
	 * @param response
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Get List of Work Item Statuses", responseClass = String.class)
	@RequestMapping(value = "/getListOfWorkItemStatus", method = RequestMethod.GET)
	@ResponseBody
	public Object getListOfWorkItemStatus(HttpServletResponse response) {
		try (CarIQAPI _workItem_getListOfWorkItemStatus = new CarIQAPI("CARIQ_workItem_getListOfWorkItemStatus")) {
			return workItemService.getListOfWorkItemStatus();
		}
	}
}
