package com.insuranceiq.www.web.async;

import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.ResponseHelper;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.model.async.CarIqJob;
import com.cariq.toolkit.model.async.WorkItem;
import com.cariq.toolkit.service.async.GetJobJson;
import com.cariq.toolkit.service.async.JobService;


@CarIQPublicAPI(name = "Jobs", description = "[Admin Only] Everything related to Jobs - i.e. the Scheduled Tasks")
@RequestMapping("/jobs")
@Controller
public class JobController {

	/** The car service. */
	@Autowired
	private JobService jobService;

	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Get Information of job identified by given identifier", responseClass = GetJobJson.class, internal = true)
	@RequestMapping(value = "/getJob/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public Object getJob(@CarIQPublicAPIParameter(name = "id", description = "the identifier") @PathVariable Long id,
			HttpServletResponse response) {
		try (CarIQAPI _getJob = new CarIQAPI("CARIQ_getJob")) {
			response.setHeader("Access-Control-Allow-Origin", "*");
			CarIqJob carIqJob = CarIqJob.findCarIqJob(id);
			GetJobJson jobJson = new GetJobJson(carIqJob, WorkItem.getByDescription(carIqJob.getJobName()));
			return jobJson;
		}
	}

	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "get list of jobs with paging", responseClass = GetJobJson.class, internal = true)
	@RequestMapping(value = "/getJobs/{start}/{pageSize}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public Object getJobs(
			@CarIQPublicAPIParameter(name = "start", description = "index of first record") @PathVariable int start,
			@CarIQPublicAPIParameter(name = "pageSize", description = "the pageSize") @PathVariable int pageSize,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		try (CarIQAPI _getJobs = new CarIQAPI("CARIQ_getJobs")) {
			return jobService.getJobs(start, pageSize);
		}
	}

	@RequestMapping(value = "/removeJob/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public Object removeJob(@PathVariable Long id, HttpServletResponse response) {
		try (CarIQAPI _removeJob = new CarIQAPI("CARIQ_removeJob")) {
			response.setHeader("Access-Control-Allow-Origin", "*");
			CarIqJob carIqJob = CarIqJob.findCarIqJob(id);
			carIqJob.remove();
			ResponseJson responseJson = ResponseHelper.setResponse("Job deleted successfully");
			return responseJson;
		}
	}
}
