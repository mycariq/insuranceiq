package com.insuranceiq.www.web;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.publicapi.ApiDocService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.Utils;

/**
 * Root Controller that responds in the form of REST API documentation Standard (Swagger2)
 * It gives
 */
@RequestMapping("/apidoc/**")
@Controller
public class APIDocController {
	@Resource(name = "configProperties")
	private Properties configProperties;

	@Autowired
	ApiDocService apiDocService;


	@RequestMapping(value = "/test/{securityToken}", method = RequestMethod.GET)
	@ResponseBody
	@Transactional
	public Object test(@PathVariable("securityToken") String securityToken,	HttpServletResponse response) {
		try (CarIQAPI download = new CarIQAPI("CARIQ_API_download")) {
			try {
				if (!configProperties.getProperty(Utils.SECURITY_TOKEN).equals(securityToken))
					Utils.handleException("Invalid Security Token");

				return apiDocService.testResponse(); // to check if SwaggerUI works
			} catch (Exception e) {
				Utils.handleException(e);
			}

			return null;
		}
	}


	@RequestMapping(value = "/download/{securityToken}", method = RequestMethod.GET)
	@ResponseBody
	@Transactional
	public Object download(@PathVariable("securityToken") String securityToken,	HttpServletResponse response) {
		try (CarIQAPI download = new CarIQAPI("CARIQ_API_download")) {
			try {
				if (!configProperties.getProperty(Utils.SECURITY_TOKEN).equals(securityToken))
					Utils.handleException("Invalid Security Token");

				return apiDocService.download(); // to check if SwaggerUI works
			} catch (Exception e) {
				Utils.handleException(e);
			}

			return null;
		}
	}

	@RequestMapping(value = "/generate/{securityToken}", method = RequestMethod.GET)
	@ResponseBody
	@Transactional
	public Object generate(@PathVariable("securityToken") String securityToken,	HttpServletResponse response) {
		try (CarIQAPI download = new CarIQAPI("CARIQ_API_generate")) {
			try {
				if (!configProperties.getProperty(Utils.SECURITY_TOKEN).equals(securityToken))
					Utils.handleException("Invalid Security Token");

				return apiDocService.generate(false); // only public API - Do it through reflection
			} catch (Exception e) {
				Utils.handleException(e);
			}

			return null;
		}
	}

	@RequestMapping(value = "/generate/{securityToken}/{internal}", method = RequestMethod.GET)
	@ResponseBody
	@Transactional
	public Object generateAPIDocumentation(@PathVariable("securityToken") String securityToken, @PathVariable("internal") String internal, HttpServletResponse response) {
		try (CarIQAPI download = new CarIQAPI("CARIQ_API_generate")) {
			try {
				if (!configProperties.getProperty(Utils.SECURITY_TOKEN).equals(securityToken))
					Utils.handleException("Invalid Security Token");

				return apiDocService.generate(internal.equalsIgnoreCase("internal")); // Do it through reflection
			} catch (Exception e) {
				Utils.handleException(e);
			}

			return null;
		}
	}	
}
