package com.insuranceiq.www.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.model.Subscription.Subscription_In;
import com.cariq.toolkit.model.Subscription.Subscription_Out;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.service.SubscriptionService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;


/**
 * The Class SubscriptionController.
 */
@CarIQPublicAPI(name = "Subscription", description = "Everything related to Subscription")
@Controller
@RequestMapping("/subscription")
public class SubscriptionController {

	/** The subscriptionService. */
	@Autowired
	private SubscriptionService subscriptionService;

	/**
	 * Adds the subscription.
	 * @param params
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Create subscription", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson addSubscription(@RequestBody Subscription_In params) {
		try (CarIQMutableAPI _addSubscription = new CarIQMutableAPI("CARIQ_addSubscription", params)) {
			return subscriptionService.addSubscription(params);
		}
	}


	/** Gets the subscriptions with pagination.
	 *
	 * @return the subscriptions
	 */
	@CarIQPublicAPIMethod(description = "Get all the subscriptions", responseClass = Subscription_Out.class, internal = true)
	@RequestMapping(value = "/getAll/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<Subscription_Out> getAllSubscriptions(
			@CarIQPublicAPIParameter(name = "PageNo", description = "Page Number") @PathVariable int pageNo, 
			@CarIQPublicAPIParameter(name = "PageSize", description = "Page Size") @PathVariable int pageSize) {
		try (CarIQAPI _getAllSubscriptions = new CarIQAPI("CARIQ_getAllSubscriptions_with_pagination")) {
			return subscriptionService.getAllSubscriptions(pageNo, pageSize);
		}
	}
	
	/**
	 * Gets the subscription.
	 * @param subscriptionName
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Get signle subscription by subscriptionname", responseClass = Subscription_Out.class, internal = true)
	@RequestMapping(value = "/get/{subscriptionName}", method = RequestMethod.GET)
	@ResponseBody
	public Subscription_Out getSubscription(
			@CarIQPublicAPIParameter(name = "SubscriptionName", description = "Subscription Name") @PathVariable String subscriptionName) {
		try (CarIQAPI _getSubscription = new CarIQAPI("CARIQ_getSubscription")) {
			return subscriptionService.getSubscription(subscriptionName);
		}
	}

	/**
	 * Update subscription.
	 * @param params
	 * @param subscriptionName
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Update subscription", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/update/{subscriptionName}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseJson updateSubscription(@RequestBody Subscription_In params,
			@CarIQPublicAPIParameter(name = "SubscriptionName", description = "Subscription Name") @PathVariable String subscriptionName) {
		try (CarIQMutableAPI _updateSubscription = new CarIQMutableAPI("CARIQ_updateSubscription", params)) {
			return subscriptionService.updateSubscription(params, subscriptionName);
		}
	}

	/**
	 * Removes the subscription.
	 * @param subscriptionName
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Remove subscription", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/remove/{subscriptionName}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson removeSubscription(
			@CarIQPublicAPIParameter(name = "SubscriptionName", description = "Subscription Name") @PathVariable String subscriptionName) {
		try (CarIQMutableAPI _removeSubscription = new CarIQMutableAPI("CARIQ_removeSubscription", subscriptionName)) {
			return subscriptionService.deleteSubscription(subscriptionName);
		}
	}
}
