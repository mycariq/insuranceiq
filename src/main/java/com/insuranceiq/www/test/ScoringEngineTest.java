/**
 * 
 */
package com.insuranceiq.www.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.model.ScoreDefinition;
import com.cariq.toolkit.utils.scoring.Scorer;

/**
 * The Class DriverScoringEngineTest.
 *
 * @author amita
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
public class ScoringEngineTest {

	/**
	 * Test driving score.
	 */
	@Test
	public void testDrivingScore() {
	
			// Get the scoreDef for "DriverScore"
			ScoreDefinition scoreDef = ScoreDefinition.getByName("drivingscore");
			System.out.println("scoreDef: " + scoreDef.getName() +
					", maxValue = " + scoreDef.getMaxValue() + ", minValue =");
			
			// Configure scorer
			Scorer scorer = new Scorer();
			scorer.configure(scoreDef);
			System.out.println("Scorer configuration done");
			
			scorer.addParameter("RashBraking", 10.036);
			scorer.addParameter("IDling", 10.00);
			scorer.addParameter("RashAcceleration", 10.00);
			scorer.addParameter("OverSpeedingPercent", 20.00);
			scorer.addParameter("nightdriving", 20.00);
			scorer.addParameter("totaldriving", 200.00);
	
			double carScore = scorer.evaluate();
			System.out.println("Driving Score = " + carScore);

	}
	
	/**
	 * Test driver score.
	 */
	@Test
	public void testDriverScore() {
	
			// Get the scoreDef for "DriverScore"
			ScoreDefinition scoreDef = ScoreDefinition.getByName("driverscore");
			System.out.println("scoreDef: " + scoreDef.getName() +
					", maxValue = " + scoreDef.getMaxValue() + ", minValue =");
			
			// Configure scorer
			Scorer scorer = new Scorer();
			scorer.configure(scoreDef);
			System.out.println("Scorer configuration done");
			
			scorer.addParameter("RashBraking", 0.036);
			scorer.addParameter("Idling", 40.00);
			scorer.addParameter("RashAcceleration", 6.00);
			scorer.addParameter("OverSpeeding", 4.00);
			//scorer.addParameter("nightdriving", 20.00);
	
			double carScore = scorer.evaluate();
			System.out.println("Driver Score = " + carScore);

	}
}
