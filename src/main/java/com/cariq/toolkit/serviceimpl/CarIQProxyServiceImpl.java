package com.cariq.toolkit.serviceimpl;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cariq.toolkit.utils.APIHelper;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.service.CariqProxyService;

@Service
public class CarIQProxyServiceImpl implements CariqProxyService {

	CarIQLogger logger = APIHelper.logger.getLogger("CariqProxyService");

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.fleetiq.www.service.CariqProxyService#call(com.fleetiq.www.model.
	 * Admin, com.cariq.toolkit.utils.GenericJSON, java.lang.String,
	 * org.springframework.http.HttpMethod)
	 */
	public Object call(User user, GenericJSON json, String apiName, HttpMethod method) {
		try (ProfilePoint _call_CarIQ_API_ = ProfilePoint.profileAction("ProfAction_call_CarIQ_API_" + apiName)) {
			//TODO: Fix this at cariq/toolkit level
			//String url = new ErrorHelper().makeUrl(apiName);
			String url = "";
			logger.debug("Calling " + method + " API : " + url + " and input json : " + json);
			ResponseEntity<Object> response = new RestTemplate(Utils.getRequestFactory()).exchange(url, method,
					new HttpEntity<GenericJSON>(json,
							Utils.setHeaders(Utils.getSecurityToken(user.getUsername(), user.getPassword()))),
					Object.class);
			return response.getBody();
		}
	}

}
