package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_Out;
import com.cariq.toolkit.service.ProxyObjectService;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;


/**
 * The Class ProxyObjectServiceImpl.
 */
@Service
public class ProxyObjectServiceImpl implements ProxyObjectService {

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;

	@Override
	@Transactional
	public ResponseWithIdJson createOrGetProxyObject(ProxyObject_In params) {
		ProxyObject proxyObject = null;
		try {
			if (null == params.getObjectId() || params.getObjectId() <= 0) { 
				Utils.handleException("ObjectId " + params.getObjectId() + " is invalid");
			}
			
			if (null == params.getObjectType()) { 
				Utils.handleException("ObjectType is invalid");
			}
			
			Domain domain = Domain.getByName(params.getDomainName());
			if (null == domain) {
				Utils.handleException("Domain " + params.getDomainName() + " is invalid");
			}
			
			proxyObject = ProxyObject.createOrGet(domain, params.getObjectId(), params.getObjectType(), params.getSignature());
				
		} catch (Exception e) {
			Utils.handleException("Failed to create or get proxyObject, Exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(proxyObject.getId().toString(), "ProxyObject : " 
			+ params.getDomainName() + "_" + params.getObjectId() + "_" + params.getObjectType() 
			+ " created/obtained successfully.");
	}

	@Override
	public List<ProxyObject_Out> getAllProxyObjects(int pageNo, int pageSize) {
		List<ProxyObject_Out> resultList = new ArrayList<>();
		try {
			List<ProxyObject> proxyObjects = ProxyObject.getAllObjects(pageNo, pageSize);
			
			for (ProxyObject proxyObject : proxyObjects) {
				resultList.add(proxyObject.toJSON());
			}
		} catch (Exception e) {
			Utils.handleException("Failed to get the proxyObjects, Exception: " + e.getMessage());
		}
		return resultList;
	}

	@Override
	@Transactional
	public ResponseJson deleteProxyObject(ProxyObject_In params) {
		ProxyObject proxyObject = null;
		try {
			if (null == params.getObjectId() || params.getObjectId() <= 0) { 
				Utils.handleException("ObjectId " + params.getObjectId() + " is invalid");
			}
			
			if (null == params.getObjectType()) { 
				Utils.handleException("ObjectType is invalid");
			}
			
			Domain domain = Domain.getByName(params.getDomainName());
			if (null == domain) {
				Utils.handleException("Domain " + params.getDomainName() + " is invalid");
			}
			
			// Check if proxy-object exists
			proxyObject = ProxyObject.getByDomainObjectIdType(domain, params.getObjectId(), params.getObjectType());
			if (proxyObject != null) {
				//TODO: Proxy objects might be used in tags. How to take care of FK.
				proxyObject.remove();
				return new ResponseJson("ProxyObject : " + proxyObject.getId() + " deleted successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to remove proxyObject: " 
					+ params.getDomainName() + "_" + params.getObjectId() + "_" + params.getObjectType() 
					+ ", exception: " + e.getMessage());
		}
		return new ResponseJson("ProxyObject : " 
				+ params.getDomainName() + "_" + params.getObjectId() + "_" + params.getObjectType() 
				+ " does not exist!");
	}
	
	@Override
	public ProxyObject_Out getProxyByDomainObjectIdType(String domainName, Long objectId, String objectType) {
		ProxyObject_Out proxyObjectOut = null;
		try {
			if (null == objectId || objectId <= 0) { 
				Utils.handleException("ObjectId " + objectId + " is invalid");
			}
			
			if (null == objectType) { 
				Utils.handleException("ObjectType is invalid");
			}
			
			Domain domain = Domain.getByName(domainName);
			if (domain == null) {
				Utils.handleException("Domain " + domainName + " is invalid");
			}
			
			ProxyObject proxyObject = ProxyObject.getByDomainObjectIdType(domain, objectId, objectType);
			if (null != proxyObject) {
				proxyObjectOut = proxyObject.toJSON();
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get proxyObject, exception: " + e.getMessage());
		}
		return proxyObjectOut;
	}
	
	@Override
	public ProxyObject_Out getProxyByDomainSignatureObjectType(String domainName, String signature, String objectType) {
		ProxyObject_Out proxyObjectOut = null;
		try {
			if (null == objectType) { 
				Utils.handleException("ObjectType is invalid");
			}
			
			Domain domain = Domain.getByName(domainName);
			if (domain == null) {
				Utils.handleException("Domain " + domainName + " is invalid");
			}
			
			ProxyObject proxyObject = ProxyObject.getByDomainSignatureObjectType(domain, signature, objectType);
			if (null != proxyObject) {
				proxyObjectOut = proxyObject.toJSON();
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get proxyObject, exception: " + e.getMessage());
		}
		return proxyObjectOut;
		
	}
}