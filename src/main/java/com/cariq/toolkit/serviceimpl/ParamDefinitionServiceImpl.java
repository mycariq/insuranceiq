package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.model.ParamDefinition;
import com.cariq.toolkit.model.ParamDefinition.ParamDefinition_In;
import com.cariq.toolkit.model.ParamDefinition.ParamDefinition_Out;
import com.cariq.toolkit.service.ParamDefinitionService;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;


/**
 * The Class ParamDefinitionServiceImpl.
 */
@Service
public class ParamDefinitionServiceImpl implements ParamDefinitionService {

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;

	@Override
	@Transactional
	public ResponseWithIdJson createOrGetParamDefinition(ParamDefinition_In params) {
		ParamDefinition paramDef = null;
		try {
			
			paramDef = ParamDefinition.createOrGet(params.getName(), params.getDescription(), 
						params.getWeight(), params.getMinValue(), params.getMaxValue(), 
						params.getDefaultValue());
			
		} catch (Exception e) {
			Utils.handleException("Failed to get or create paramDefinition, Exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(paramDef.getId().toString(), "ParamDefinition : " + params.getName() + " created/obtained successfully.");
	}

	@Override
	public List<ParamDefinition_Out> getAllParamDefinitions(int pageNo, int pageSize) {
		List<ParamDefinition_Out> resultList = new ArrayList<>();
		try {
			List<ParamDefinition> paramDefinitions = ParamDefinition.getAllObjects(pageNo, pageSize);
			
			for (ParamDefinition paramDefinition : paramDefinitions) {
				resultList.add(paramDefinition.toJSON());
			}
		} catch (Exception e) {
			Utils.handleException("Failed to get the paramDefinitions, Exception: " + e.getMessage());
		}
		return resultList;
	}

	@Override
	public List<ParamDefinition_Out> getParamDefinitionsByName(String paramDefinitionName) {
		List<ParamDefinition_Out> resultList = new ArrayList<>();
		try {
			List<ParamDefinition> paramDefs = ParamDefinition.getByName(paramDefinitionName);
			for (ParamDefinition paramDef : paramDefs) {
				resultList.add(paramDef.toJSON());
			}
		}catch (Exception e) {
			Utils.handleException("Failed to get paramDefinitions: " + paramDefinitionName + ", Exception: " + e.getMessage());
		}
		
		return resultList;
	}

	@Override
	@Transactional
	public ResponseJson deleteParamDefinition(ParamDefinition_In params) {
		try {
			ParamDefinition paramDefinition = ParamDefinition.getByUniqueValues(params.getName(), params.getWeight(), 
					params.getMinValue(), params.getMaxValue(), params.getDefaultValue());
			if (paramDefinition != null) {
				// ScoreParam - FK constraint
				paramDefinition.remove();
				return new ResponseJson("ParamDefinition : " + params.getName() + " deleted successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to remove paramDefinition: " + params.getName() + ", exception: " + e.getMessage());
		}
		return new ResponseJson("ParamDefinition : " + params.getName() + " does not exist!");
	}
}