package com.cariq.toolkit.serviceimpl.async;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

import com.cariq.toolkit.utils.CSVLogger;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.service.async.WorkItemUpdater;

public class WIHelper {
	public static CarIQLogger logger = new CSVLogger(Logger.getLogger("WorkItem"), "WORKITEM");

	public static final String carLogCDN = "cariqcarlogstore";

	// @TODO use configuration
	public static final String azureBlobPath = "https://dal05.objectstorage.softlayer.net/v1/AUTH_781e325b-15cb-49a4-ae5e-2a4408bac801"; // @TMPL public static final String azureBlobPath = "[% ROOT.servers.fileServer.endPoint %]"; @TMPL

	public static CarIQLogger getLogger(String innerContext) {
		return logger.getLogger(innerContext);
	}

	public static CarIQLogger getLogger(Class<?> clazz) {
		return getLogger(clazz.getCanonicalName());
	}

	public static WorkItemUpdater getUpdater(JobExecutionContext context) {
		if (null == context)
			return null;

		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		if (null == jobDataMap)
			return null;

		WorkItemUpdater updater = (WorkItemUpdater) jobDataMap.get(CarIQJobExecutor.UPDATER);
		if (null == updater)
			return null;
		return updater;
	}

	public static String getCarLogPath(String logName) {
		return azureBlobPath + "/" + carLogCDN + "/" + logName;
	}
}
