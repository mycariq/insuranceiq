package com.cariq.toolkit.serviceimpl.async;

public class GetListOfType {

	private String type;

	public GetListOfType(String type) {
		super();
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
}
