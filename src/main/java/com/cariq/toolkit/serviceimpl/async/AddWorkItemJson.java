package com.cariq.toolkit.serviceimpl.async;

import org.hibernate.validator.constraints.NotEmpty;

public class AddWorkItemJson {
	@NotEmpty(message = "Name should not be empty")
	private String name;
	@NotEmpty(message = "Descriptions should not be empty")
	private String descriptions;
	@NotEmpty(message = "WorkerClass should not be empty")
	private String workerClass;
	@NotEmpty(message = "InputJson should not be empty")
	private String inputJson;
	@NotEmpty(message = "Type should not be empty")
	private String type;

	public AddWorkItemJson() {
	}

	public AddWorkItemJson(String workerClass, String inputJson, String type,
			String name, String descriptions) {
		super();
		this.workerClass = workerClass;
		this.inputJson = inputJson;
		this.type = type;
		this.name = name;
		this.descriptions = descriptions;
	}

	public String getWorkerClass() {
		return workerClass;
	}

	public String getInputJson() {
		return inputJson;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getDescriptions() {
		return descriptions;
	}
}
