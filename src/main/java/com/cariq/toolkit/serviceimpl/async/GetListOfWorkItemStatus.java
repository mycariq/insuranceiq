package com.cariq.toolkit.serviceimpl.async;

public class GetListOfWorkItemStatus {

	private String status;

	public GetListOfWorkItemStatus(String status) {
		super();
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

}
