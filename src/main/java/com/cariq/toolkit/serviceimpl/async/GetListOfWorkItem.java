package com.cariq.toolkit.serviceimpl.async;

import java.util.Date;

import com.cariq.toolkit.model.async.WorkItemStatus;


public class GetListOfWorkItem {
	private String type;
	private Integer progress;
	private String startTime;
	private String endTime;
	private String uuid;
	private String name;
	private String descriptions;
	private WorkItemStatus status;

	public GetListOfWorkItem(String type, Integer progress, Date startTime,
			Date endTime, String uuid, String name, String descriptions,
			WorkItemStatus status) {
		super();
		this.type = type != null ? type : null;
		this.progress = progress != null ? progress : null;
		this.startTime = startTime != null ? startTime.toString() : null;
		this.endTime = endTime != null ? endTime.toString() : null;
		this.uuid = uuid != null ? uuid : null;
		this.name = name;
		this.descriptions = descriptions != null ? descriptions : null;
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public Integer getProgress() {
		return progress;
	}

	public String getStartTime() {
		return startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public String getUuid() {
		return uuid;
	}

	public String getName() {
		return name;
	}

	public String getDescriptions() {
		return descriptions;
	}

	public WorkItemStatus getStatus() {
		return status;
	}

}
