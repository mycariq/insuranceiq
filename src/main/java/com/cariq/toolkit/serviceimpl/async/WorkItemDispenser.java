package com.cariq.toolkit.serviceimpl.async;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.model.async.WorkItem;

@Transactional(propagation=Propagation.REQUIRES_NEW)
public class WorkItemDispenser implements Iterable<WorkItem> {
	private int rowNum = 0;
	public static final int BATCH_SIZE = 20;
	private List<WorkItem> workItems = new ArrayList<WorkItem>();

	private void loadWorkItems() {
		// WIHelper.logger.debug("Loading Work Items from row = " +  rowNum);
		List<WorkItem> items = WorkItem.findUnCompletedWorkItem(rowNum, BATCH_SIZE);
		rowNum += items.size();
		workItems.addAll(items);
	}
	
	public WorkItemDispenser() {
		loadWorkItems();
	}

	@Override
	public Iterator<WorkItem> iterator() {
		return new WorkItemIterator();
	}

	private class WorkItemIterator implements Iterator<WorkItem> {

		@Override
		public boolean hasNext() {
			if (rowNum < BATCH_SIZE - 1)
				return workItems.size() > 0;
				
			loadWorkItems();
			return workItems.size() > 0;
		}

		@Override
		public WorkItem next() {
			WorkItem workItem = workItems.remove(0);
//			workItem.setStatus(WorkItemStatus.RUNNING);
//			workItem = workItem.merge();
//			APIHelper.logger.debug("================NEXT============="
//					+ workItem.getId());
			return workItem;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Unsupported method: WorkItemDispenser.remove");
		}

	}
}
