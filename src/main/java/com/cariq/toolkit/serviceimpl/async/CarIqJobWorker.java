package com.cariq.toolkit.serviceimpl.async;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.service.async.WorkItemUpdater;

/**
 * Main base class for all the Crawlers/Spiders/Healers or other async
 * activities This is invoked via async workitem mechanism
 * 
 * @author hrishi
 *
 */
public abstract class CarIqJobWorker {
	CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIqJobWorker");

	protected final void updateStatus(JobExecutionContext context, int percentComplete) {
		if (null == context)
			return;

		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		if (null == jobDataMap)
			return;

		WorkItemUpdater updater = (WorkItemUpdater) jobDataMap.get(CarIQJobExecutor.UPDATER);
		if (null == updater)
			return;
		try {
			updater.setPercentComplete(percentComplete);
			jobDataMap.put(CarIQJobExecutor.UPDATER, updater); // is this
																// needed?
		} catch (Exception e) {
			Utils.logException(logger, e, "Update Percentage for WorkItem: " + updater.getItem().getDescription());
		}
	}

	protected abstract void executeInternal(JobExecutionContext context) throws Exception;

	// @Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try (ProfilePoint _jobExecute = ProfilePoint.profileActivity("ProfActivity_jobExecute")) {
			try {
				updateStatus(context, 2); // Set percent complete to 2 at the beginning - for TEST					
				executeInternal(context);
				
			} catch (Exception e) {
				Utils.logException(logger, e, "Executing Job: " + this.getClass().getName());
				throw new JobExecutionException(e);
			}
		}
	}
}
