package com.cariq.toolkit.serviceimpl.async;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.model.async.CarIqJob;
import com.cariq.toolkit.model.async.WorkItem;
import com.cariq.toolkit.service.async.GetJobJson;
import com.cariq.toolkit.service.async.JobService;

@Service
public class JobServiceImpl implements JobService {
	@Override
	@Transactional
	public List<GetJobJson> getJobs(int start, int pageSize) {
		List<GetJobJson> jobJsons = new ArrayList<GetJobJson>();
		int pageNum = Utils.validatePagination(pageSize, start);
		List<CarIqJob> carIqJobs = CarIqJob.findCarIqJobEntries(pageNum,
				pageSize);
		for (CarIqJob job : carIqJobs) {
			GetJobJson jobJson = new GetJobJson(job,
					WorkItem.getByDescription(job.getJobName()));
			jobJsons.add(jobJson);
		}
		return jobJsons;
	}

}
