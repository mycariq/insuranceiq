package com.cariq.toolkit.serviceimpl.async;

import java.util.Date;

import com.cariq.toolkit.model.async.WorkItemStatus;

public class GetWorkItem {
	private String type;
	private Integer progress;
	private String startTime;
	private String endTime;
	private String uuid;
	private String workerClass;
	private String inputJson;
	private String outputJson;
	private WorkItemStatus status;
	private String name;
	private String descriptions;
	private String host;
	private String failureCause;

	public GetWorkItem(String type, Integer progress, Date startTime,
			Date endTime, String uuid, String workerClass, String inputJson,
			String outputJson, WorkItemStatus status, String name,
			String descriptions, String failureCause,String host) {
		super();
		this.type = type != null ? type : null;
		this.progress = progress;
		this.startTime = startTime != null ? startTime.toString() : null;
		this.endTime = endTime != null ? endTime.toString() : null;
		this.uuid = uuid != null ? uuid : null;
		this.workerClass = workerClass != null ? workerClass : null;
		this.inputJson = inputJson != null ? inputJson : null;
		this.outputJson = outputJson != null ? outputJson : null;
		this.status = status;
		this.name = name;
		this.descriptions = descriptions != null ? descriptions : null;
		this.failureCause = failureCause;
		this.host=host;
	}

	public String getType() {
		return type;
	}

	public Integer getProgress() {
		return progress;
	}

	public String getStartTime() {
		return startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public String getUuid() {
		return uuid;
	}

	public String getWorkerClass() {
		return workerClass;
	}

	public String getInputJson() {
		return inputJson;
	}

	public String getOutputJson() {
		return outputJson;
	}

	public WorkItemStatus getStatus() {
		return status;
	}

	public String getName() {
		return name;
	}

	public String getDescriptions() {
		return descriptions;
	}
	
	public String getFailureCause() {
		return failureCause;
	}

	public String getHost() {
		return host;
	}
	
}
