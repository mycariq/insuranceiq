package com.cariq.toolkit.serviceimpl.async;

public class PaginationJson<T> {

	private int pageNum;
	private Long noOfRecords;
	private int totalPages;
	private T rows;
	
	public PaginationJson(){
		
	}
	
	public PaginationJson(int pageNum, Long noOfRecords, int totalPages, T rows) {
		super();
		this.pageNum = pageNum;
		this.noOfRecords = noOfRecords;
		this.totalPages = totalPages;
		this.rows = rows;
	}


	public int getPageNum() {
		return pageNum;
	}


	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}


	public Long getNoOfRecords() {
		return noOfRecords;
	}


	public void setNoOfRecords(Long noOfRecords) {
		this.noOfRecords = noOfRecords;
	}


	public int getTotalPages() {
		return totalPages;
	}


	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}


	public T getRows() {
		return rows;
	}


	public void setRows(T rows) {
		this.rows = rows;
	}
		
}
