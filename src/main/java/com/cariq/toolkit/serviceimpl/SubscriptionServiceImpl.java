package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.model.ActionLog;
import com.cariq.toolkit.model.Subscription;
import com.cariq.toolkit.model.Subscription.Subscription_In;
import com.cariq.toolkit.model.Subscription.Subscription_Out;
import com.cariq.toolkit.service.SubscriptionService;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;


/**
 * The Class SubscriptionServiceImpl.
 */
@Service
public class SubscriptionServiceImpl implements SubscriptionService {

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;

	@Override
	@Transactional
	public ResponseWithIdJson addSubscription(Subscription_In params) {
		Subscription subscription = null;
		try {
			subscription = new Subscription();
			subscription.fromJSON(params);
			subscription.setCreatedOn(new Date());
			subscription.setModifiedOn(new Date());
			subscription.persist();
		} catch (Exception e) {
			Utils.handleException("Failed to add subscription, Error: " + e.getMessage());
		}
		return new ResponseWithIdJson(subscription.getId().toString(), "Subscription : " + params.getName() + " created successfully.");
	}

	@Override
	public List<Subscription_Out> getAllSubscriptions(int pageNo, int pageSize) {
		List<Subscription_Out> resultList = new ArrayList<>();
		try {
			List<Subscription> subscriptions = Subscription.getAllObjects(pageNo, pageSize);
			
			for (Subscription subscription : subscriptions) {
				Subscription_Out subscriptionOut = subscription.toJSON();
				resultList.add(subscriptionOut);
			}
		} catch (Exception e) {
			Utils.handleException("Failed to get subscriptions");
		}
		return resultList;
	}

	@Override
	public Subscription_Out getSubscription(String subscriptionName) {
		Subscription_Out subscriptionOut = null;
		try {
			Subscription subscription = Subscription.getByName(subscriptionName);
			if (null != subscription) {
				subscriptionOut = subscription.toJSON();
			}
		}catch (Exception e) {
			Utils.handleException("Failed to get subscription: " + subscriptionName + ", Exception: " + e.getMessage());
		}
		
		return subscriptionOut;
	}

	@Override
	@Transactional
	public ResponseJson updateSubscription(Subscription_In params, String subscriptionName) {
		try {
			Subscription subscription = Subscription.getByName(subscriptionName);
			if (subscription == null) {
				Utils.handleException("Subscription " + subscriptionName + " is invalid");
			}
			subscription.updateSubscription(params.getName(), params.getStartDate(), params.getEndDate(), 
					params.getProfile(), params.getContractInfo());
			subscription.merge();
			ActionLog.log(Utils.createContext("SubscriptionServiceImpl.updateSubscription"), "Update", "subscription",
					"Subscription id : " + subscription.getId() + " subscriptionname : " + subscription.getName(),
					"Old info :" + Utils.getJSonString(subscription.toJSON()));
		} catch (Exception e) {
			Utils.handleException("Failed to update subscription: " + subscriptionName + ", Exception: " + e.getMessage());
		}

		return new ResponseJson("Subscription : " + subscriptionName + " updated successfully.");
	}

	@Override
	@Transactional
	public ResponseJson deleteSubscription(String subscriptionName) {
		try {
			Subscription subscription = Subscription.getByName(subscriptionName);
			if (subscription != null) {
				// domainaccess - FK constraint
				subscription.remove();
				return new ResponseJson("Subscription : " + subscriptionName + " deleted successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to remove subscription: " + subscriptionName + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("Subscription : " + subscriptionName + " does not exist!");
	}
}