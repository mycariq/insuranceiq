package com.cariq.toolkit.serviceimpl.report;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.utils.PaginationEntityJson;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.service.FileHandlerService;
import com.cariq.toolkit.model.report.CarIqReport;
import com.cariq.toolkit.service.async.WorkItemService;
import com.cariq.toolkit.service.report.ReportService;
import com.cariq.toolkit.service.report.ReportType;
import com.cariq.toolkit.serviceimpl.async.WIHelper;

@Service
public class ReportServiceImpl implements ReportService {

	@Autowired
	private WorkItemService workItemService;

	@Autowired
	private FileHandlerService fileHandlerService;

	@Override
	public List<String> getTypes() {
		List<String> types = new ArrayList<String>();
		for (ReportType type : ReportType.values())
			types.add(type.toString());
		return types;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.interfaces.CarService#createLog(long,
	 * java.lang.String, java.lang.String, com.cariq.www.model.CarIqUser)
	 */
	@Transactional
	@SuppressWarnings("unchecked")
	@Override
	public ResponseJson createReport(String fromDate, String toDate, String type, String ownerType, String ownerId) {
		try (ProfilePoint _createLog = ProfilePoint.profileAction("ProfAction_createLog")) {
			Utils.parseDateFormat(fromDate);
			Utils.parseDateFormat(toDate);

			JSONObject inputJson = new JSONObject();
			inputJson.put("class", ReportGenerator.getReportWorker(ReportHelper.validateReportType(type)));
			JSONObject input = new JSONObject();
			input.put("fromDate", fromDate);
			input.put("toDate", toDate);
			input.put("ownerType", ownerType);
			input.put("ownerId", ownerId);
			inputJson.put("input", input);

			String worker = ReportGenerator.class.getCanonicalName();
			return workItemService.addWorkItem(worker, inputJson.toString(), type, worker,
					"To generate report between two dates");
		} catch (Exception e) {
			Utils.handleException(e);
		}
		
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.interfaces.CarService#deleteCarLog(long,
	 * com.cariq.www.model.CarIqUser)
	 */
	@Override
	public ResponseJson deleteReport(long reportId, String ownerType, String ownerId) {
		try (ProfilePoint _deleteCarLog = ProfilePoint.profileAction("ProfAction_deleteCarLog")) {
			CarIqReport report = CarIqReport.findCarIqReport(reportId);
			fileHandlerService.deleteFile(report.getName(), WIHelper.carLogCDN);
			report.remove();
			return new ResponseJson("Report deleted successfully.");
		} catch (Exception e) {
			Utils.handleException(e.getMessage());
		}
		
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.interfaces.CarService#getCarLogs(long, int, int,
	 * com.cariq.www.model.CarIqUser)
	 */
	@Override
	public PaginationEntityJson<List<GetReportJson>> getReports(String ownerType, String ownerId, String type,
			int pageNo, int pageSize) {		
		try (ProfilePoint _getReports = ProfilePoint.profileAction("ProfAction_getReports")) {
			int firstResult = Utils.validatePagination(pageSize, pageNo);
			List<CarIqReport> reports;

			long totalCount;

			if (type == null || type.isEmpty() || type.equalsIgnoreCase("all")) {
				totalCount = CarIqReport.countReports(ownerType, ownerId);
				reports = CarIqReport.findReports(ownerType, ownerId, firstResult, pageSize);
			} else {
				ReportType reportType = ReportHelper.validateReportType(type);
				totalCount = CarIqReport.countReports(ownerType, ownerId, reportType);
				reports = CarIqReport.findReports(ownerType, ownerId, reportType, firstResult, pageSize);
			}

			List<GetReportJson> detailJson = new ArrayList<GetReportJson>();
			for (CarIqReport report : reports)
				detailJson.add(new GetReportJson(report.getId(), report.getGeneratedOn(), report.getFromDate(),
						report.getToDate(), WIHelper.getCarLogPath(report.getName()), report.getSizeBytes() + " bytes",
						report.getType()));

			return new PaginationEntityJson<List<GetReportJson>>(totalCount, pageNo, detailJson,
					Utils.totalPageCount(totalCount, pageSize));
		}
	}
}
