package com.cariq.toolkit.serviceimpl.report;

import org.quartz.JobExecutionContext;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.service.async.WorkItemUpdater;
import com.cariq.toolkit.service.async.WorkerBase;
import com.cariq.toolkit.service.report.ReportType;
import com.cariq.toolkit.service.report.ReportWorker;

// TODO: Auto-generated Javadoc
/**
 * The Class ReportGenerator.
 */
public class ReportGenerator extends WorkerBase {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("ReportGenerator");

	public static String getReportWorker(ReportType type) {
		if (type == ReportType.DEMO)
			return DemoReportWorker.class.getCanonicalName();
		
//		if (type == ReportType.ANALYTICS)
//			return DemoReportWorker.class.getCanonicalName();
//		CarIqValidator.handleException("Invalid Report Type");
		return null;
	}

	/* (non-Javadoc)
	 * @see com.fleetiq.www.service.toolkit.async.WorkerBase#executeInternal(org.quartz.JobExecutionContext, com.fleetiq.www.service.toolkit.async.WorkItemUpdater, com.cariq.toolkit.utils.GenericJSON)
	 */
	@Override
	protected void executeInternal(JobExecutionContext context, WorkItemUpdater updater, GenericJSON inputJSON)
			throws Exception {
		ReportWorker worker = ReportWorker.getWorker(updater);
		worker.execute();
	}
}
