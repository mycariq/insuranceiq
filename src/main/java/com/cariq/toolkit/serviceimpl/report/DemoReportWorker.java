package com.cariq.toolkit.serviceimpl.report;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.cariq.toolkit.coreiq.exporter.CarIQExporterService;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.CompositeReport;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.model.report.CarIqReport;
import com.cariq.toolkit.service.async.WorkItemUpdater;
import com.cariq.toolkit.service.report.ReportWorker;
import com.cariq.toolkit.serviceimpl.async.WIHelper;

//@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Configurable
public class DemoReportWorker extends ReportWorker {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("DemoReportWorker");

	/** The Constant PAGE_SIZE. */
	private static final int PAGE_SIZE = 1000;

	@Autowired
	CarIQExporterService exporterService;
		
	/** The car log. */
	private CarIqReport report;

	/**
	 * Instantiates a new car log report worker.
	 *
	 * @param inputJson
	 *            the input json
	 * @param updater
	 *            the updater
	 */
	public DemoReportWorker(GenericJSON inputJson, WorkItemUpdater updater) {
		super(inputJson, updater);
		logger.debug("[CALLED],[DemoReportWorker],\"CarAnalyticsReportWorker(inputJson=" + inputJson
				+ ",updater=" + updater + ")\"");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.worker.ReportWorker#getNotificationType()
	 */
	@Override
	protected String getNotificationType() {
		logger.debug("[CALLED],[getNotificationType],\"getNotificationType()\"");
		return "DummyReportCompletion"; 
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.worker.ReportWorker#executeInternal()
	 */
	@Override
	protected CompositeReport executeInternal() throws Exception {
		logger.debug("[CALLED],[executeInternal],\"executeInternal()\"");
		String workId = updater.getItem().getUuId();
		report= CarIqReport.getReport(workId, inputJson, "Demo", "DemoUser", "User123");


		try (ProfilePoint _executeInternal = ProfilePoint.profileActivity("ProfActivity_AnalyticsReport")) {
			// Here the actual report should be generated from worker
//			// Create Analytics worker to do series of Analytics multi-pass job
//			AnalyticsWorker worker = new DriverAnalysisNew(workId,
//					"Do Deep analytics of all packets to infer driving patterns ", null, carLog.getItsCar(),
//					carLog.getFromDate(), carLog.getToDate());
//			worker.execute();
//
//			// Create a zip file and add report file to it.
			String tempFile = CarIQFileUtils.getTempFile(workId + ".html");
			CarIQFileUtils.saveToFile(tempFile, "<b>Hello World!!</b>");
			CompositeReport compositeReport = new CompositeReport(report.getName());
			compositeReport.addFile(tempFile, "DemoReport.html");
//			compositeReport.addFiles(worker.getReportFiles()).build();

			report = report.updateName(compositeReport.getName());
			logger.debug("** carLog Name = " + report.getName());

			return compositeReport;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.worker.ReportWorker#getModelMap()
	 */
	@Override
	protected Map<String, Object> getModelMap() {
		logger.debug("[CALLED],[getModelMap],\"getModelMap()\"");

		try (ProfilePoint _getModelMap = ProfilePoint.profileAction("ProfAction_getModelMap")) {
			Map<String, Object> modelMap = new HashMap<String, Object>();
//			CarIqUser user = carLog.getGeneratedBy();
			modelMap.put("userName", "DemoUser");
			modelMap.put("logPath", WIHelper.getCarLogPath(report.getName()));
			modelMap.put("generatedOn", Utils.convertDateForMailing(report.getGeneratedOn()));
			modelMap.put("fromDate", Utils.convertDateForMailing(report.getFromDate()));
			modelMap.put("toDate", Utils.convertDateForMailing(report.getToDate()));
			modelMap.put("size", report.getSizeBytes() + " bytes");
//			modelMap.put("car",
//					report.getItsCar().getNickName() + " * " + report.getItsCar().getRegisterNo());
			return modelMap;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.worker.ReportWorker#getContainer()
	 */
	@Override
	protected String getContainer() {
		logger.debug("[CALLED],[getContainer],\"getContainer()\"");
		return WIHelper.carLogCDN;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.worker.ReportWorker#updateSize(long)
	 */
	@Override
	protected void updateSize(long size) {
		logger.debug("[CALLED],[updateSize],\"updateSize(size=" + size + ")\"");
		try (ProfilePoint _updateSize = ProfilePoint.profileAction("ProfAction_updateSize")) {
			report = report.updateSize(size);
		}
	}

	@Override
	protected String getEmailTo() {
		return ReportHelper.getEmailAddress(report.getOwnerType(), report.getOwnerId());
	}

	@Override
	protected String getEmailSubject() {
		return "Your car's Driving Report is now ready";
	}

}
