package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.model.ActionLog;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.DomainAccess;
import com.cariq.toolkit.model.Subscription;
import com.cariq.toolkit.model.Subscription.Subscription_Out;
import com.cariq.toolkit.service.DomainAccessService;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;


/**
 * The Class DomainServiceImpl.
 */
@Service
public class DomainAccessServiceImpl implements DomainAccessService {

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;

	@Override
	@Transactional
	public ResponseJson grantDomainAccessToSubscription(String subscriptionName, String domainName) {
		try {
			Subscription subscription = Subscription.getByName(subscriptionName);
			if (null == subscription) {
				Utils.handleException("Subscription " + subscriptionName + " is invalid");
			}
			
			Domain domain = Domain.getByName(domainName);
			if (null == domain) {
				Utils.handleException("Domain " + domainName + " is invalid");
			}
			
			DomainAccess domainAccess = new DomainAccess(domain, subscription, new Date(), new Date());
			
			domainAccess.persist();
			
			ActionLog.log(Utils.createContext("DomainAccessServiceImpl.updateDomain"), "grantAccess", "domain",
					"Domain : " + domain.getName() + " Subscription : " + subscription.getName(),
					"No access earlier");
			
		} catch (Exception e) {
			Utils.handleException("Failed to grant access to domain: " + domainName 
				+ " for subscription: " + subscriptionName + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("Subscription " + subscriptionName + " granted access to Domain " + domainName + " successfully.");
	}
	
	@Override
	@Transactional
	public ResponseJson removeDomainAccessForSubscription(String subscriptionname, String domainName) {
		try {
			Subscription subscription = Subscription.getByName(subscriptionname);
			if (subscription == null) {
				Utils.handleException("Subscription " + subscriptionname + " is invalid");
			}
			
			Domain domain = Domain.getByName(domainName);
			if (domain == null) {
				Utils.handleException("Domain " + domainName + " is invalid");
			}
			
			DomainAccess domainAccess = DomainAccess.getObjectByDomainUSubscription(domain, subscription);
			if (null != domainAccess) {
				domainAccess.remove();
				ActionLog.log(Utils.createContext("DomainAccessServiceImpl.removeDomainAccessForSubscription"), "removeAccess", "domain",
						"Domain : " + domain.getName() + " Subscription : " + subscription.getName(),
						"Had access earlier");
				return new ResponseJson("Subscription " + subscriptionname + " is blocked to access the Domain " + domainName);
			}
		} catch (Exception e) {
			Utils.handleException("Failed to block access to domain: " + domainName 
				+ " for subscription: " + subscriptionname + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("Subscription " + subscriptionname + " already does not have to access the Domain " + domainName);
	}
	
	@Override
	public List<Domain_Out> getAccessibleDomainsForSubscription(String subscriptionName) {
		List<Domain_Out> resultList = new ArrayList<>();
		try {
			Subscription subscription = Subscription.getByName(subscriptionName);
			if (subscription == null) {
				Utils.handleException("Subscription " + subscriptionName + " is invalid");
			}
			
			List<DomainAccess> domainAccesslist = DomainAccess.getAllObjectsBySubscription(subscription);
			for (DomainAccess domainAccess : domainAccesslist) {
				Domain domain = domainAccess.getDomain();
				if (null != domain) {
					Domain_Out domainOut = domain.toJSON();
					resultList.add(domainOut);
				}
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get accessible domains for subscription: " 
				+ subscriptionName + ", Exception: " + e.getMessage());
		}
		return resultList;
		
	}
	
	@Override
	public List<Subscription_Out> getPrivilegedSubscriptionsForDomain(String domainName) {
		List<Subscription_Out> resultList = new ArrayList<>();
		try {
			Domain domain = Domain.getByName(domainName);
			if (domain == null) {
				Utils.handleException("domain " + domainName + " is invalid");
			}
			
			List<DomainAccess> domainAccesslist = DomainAccess.getAllObjectsByDomain(domain);
			for (DomainAccess domainAccess : domainAccesslist) {
				Subscription subscription = domainAccess.getSubscription();
				if (null != subscription) {
					Subscription_Out subscriptionOut = subscription.toJSON();
					resultList.add(subscriptionOut);
				}
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get privileged subscriptions for domain: " 
					+ domainName + ", Exception: " + e.getMessage());
		}
		return resultList;
	}
}