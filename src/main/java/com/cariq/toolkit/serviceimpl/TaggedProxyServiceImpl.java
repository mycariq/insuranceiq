package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.model.Tag.Tag_In;
import com.cariq.toolkit.model.Tag.Tag_Out;
import com.cariq.toolkit.model.TaggedProxy.TagAndProxyDetails_In;
import com.cariq.toolkit.model.TaggedProxy.TaggedProxy_In;
import com.cariq.toolkit.model.Tag;
import com.cariq.toolkit.model.TaggedProxy;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_Out;
import com.cariq.toolkit.service.ProxyObjectService;
import com.cariq.toolkit.service.TagService;
import com.cariq.toolkit.service.TaggedProxyService;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;


/**
 * The Class TagServiceImpl.
 */
@Service
public class TaggedProxyServiceImpl implements TaggedProxyService {

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;
	
	@Autowired
	ProxyObjectService proxyService;
	
	@Autowired
	TagService tagService;

	@Override
	@Transactional
	public ResponseWithIdJson tagProxy(TaggedProxy_In params) {
		TaggedProxy taggedProxy = null;
		ProxyObject proxyObject = null;
		ResponseWithIdJson idJson = null;
		Tag tag = null;
		
		try {
			// Obtain the tag
			Tag_In tagIn = new Tag_In(params.getTagname(), null);
			idJson = tagService.createOrGetTag(tagIn);
			if (null == idJson.getId() || 
				(null == (tag = Tag.findObjectById(Long.parseLong(idJson.getId()))))) {
				Utils.handleException("Failed to create or get tag: " + params.getTagname()
					+ ", exception: " + idJson.getMessages());
			}
			
			// Validate the proxy-object
			proxyObject = ProxyObject.findObjectById(params.getProxyId());
			if (null == proxyObject) {
				Utils.handleException("Invalid proxyId: " + params.getProxyId());
			}
								
			taggedProxy = new TaggedProxy(tag, proxyObject, new Date(), new Date());
			taggedProxy.persist();
		} catch (Exception e) {
			Utils.handleException("Failed to tag proxyObject, exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(taggedProxy.getId().toString(), "ProxyObject " + params.getProxyId() + " tagged with " + params.getTagname() + " successfully.");
	}
	
	@Override
	@Transactional
	public ResponseWithIdJson createAndTagProxy(TagAndProxyDetails_In params) {
		TaggedProxy taggedProxy = null;
		ProxyObject proxyObject = null;
		ResponseWithIdJson idJson = null;
		Tag tag = null;
		
		try {
			// Obtain the tag
			Tag_In tagIn = new Tag_In(params.getTagname(), null);
			idJson = tagService.createOrGetTag(tagIn);
			if (null == idJson.getId() || 
				(null == (tag = Tag.findObjectById(Long.parseLong(idJson.getId()))))) {
				Utils.handleException("Failed to create or get tag: " + params.getTagname()
					+ ", exception: " + idJson.getMessages());
			}
			
			// Obtain the proxy-object
			ProxyObject_In proxyObjectIn = new ProxyObject_In(params.getDomainName(), params.getObjectId(),
					params.getObjectType(), params.getSignature());
			if (null == proxyObjectIn.getObjectType()) { 
				Utils.handleException("proxyObjectIn.ObjectType is invalid here!!! from params: " + params.getObjectType());
			}
			
			idJson = proxyService.createOrGetProxyObject(proxyObjectIn);
			if (null == idJson.getId() || 
				(null == (proxyObject = ProxyObject.findObjectById(Long.parseLong(idJson.getId()))))) {
				Utils.handleException("Failed to create or get proxyObject: " 
					+ params.getDomainName() + "_" + params.getObjectId() + "_" + params.getObjectType() + "_" + params.getSignature()
					+ ", exception: " + idJson.getMessages());
			}
								
			taggedProxy = new TaggedProxy(tag, proxyObject, new Date(), new Date());
			taggedProxy.persist();
		} catch (Exception e) {
			Utils.handleException("Failed to create and tag proxyObject, exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(taggedProxy.getId().toString(), "ObjectId " + params.getObjectId() + " tagged with " + params.getTagname() + " successfully.");
	}
	
	@Override
	@Transactional
	public ResponseJson untagProxy(TaggedProxy_In params) {
		try {
			// Validate tag
			Tag tag = Tag.getByName(params.getTagname());
			if (tag == null) {
				Utils.handleException("Tag " + params.getTagname() + " is invalid");
			}
			
			// Validate proxy object
			ProxyObject proxyObject = ProxyObject.findObjectById(params.getProxyId());
			if (null == proxyObject) {
				Utils.handleException("Invalid proxyId: " + params.getProxyId());
			}
			
			// Get taggedProxy
			TaggedProxy taggedProxy = TaggedProxy.getObjectByTagProxy(tag, proxyObject);
			if (null != taggedProxy) {
				taggedProxy.remove();
				return new ResponseJson("Tag " + params.getTagname() + " is removed from proxyObject " + params.getProxyId());
			}
		} catch (Exception e) {
			Utils.handleException("Failed to untag the proxyObject, exception " + e.getMessage());
		}
		return new ResponseJson("ProxyObject " + params.getProxyId() + " is not tagged with " + params.getTagname() + " already!");
	}
	
	@Override
	@Transactional
	public ResponseJson untagProxyByProxyDetails(TagAndProxyDetails_In params) {
		try {
			// Validate tag
			Tag tag = Tag.getByName(params.getTagname());
			if (tag == null) {
				Utils.handleException("Tag " + params.getTagname() + " is invalid");
			}
			
			// Validate Domain
			Domain domain = Domain.getByName(params.getDomainName());
			if (domain == null) {
				Utils.handleException("Domain " + params.getDomainName() + " is invalid");
			}
			
			// Validate proxy object
			ProxyObject proxyObject = ProxyObject.getByDomainObjectIdType(domain, params.getObjectId(), params.getObjectType());
			if (null == proxyObject) {
				Utils.handleException("Invalid proxyDetails: " + domain.getName() + "_" + params.getObjectId() + "_" + params.getObjectType());
			}
			
			// Get taggedProxy
			TaggedProxy taggedProxy = TaggedProxy.getObjectByTagProxy(tag, proxyObject);
			if (null != taggedProxy) {
				taggedProxy.remove();
				return new ResponseJson("Tag " + params.getTagname() + " is removed from proxyObject " + proxyObject.getId());
			}
		} catch (Exception e) {
			Utils.handleException("Failed to untag the proxyObject, exception " + e.getMessage());
		}
		return new ResponseJson("ProxyObject is not tagged with " + params.getTagname() + " already!");
	}
	
	@Override
	public List<Tag_Out> getTagsByProxyObject(String domainName, Long objectId, String objectType) {
		List<Tag_Out> resultList = new ArrayList<>();
		try {
			// Validate Domain
			Domain domain = Domain.getByName(domainName);
			if (domain == null) {
				Utils.handleException("Domain " + domainName + " is invalid");
			}
			
			// Validate proxy object
			ProxyObject proxyObject = ProxyObject.getByDomainObjectIdType(domain, objectId, objectType);
			if (null == proxyObject) {
				Utils.handleException("Invalid proxyDetails: " + domain.getName() + "_" +objectId + "_" + objectType);
			}

			List<TaggedProxy> taggedProxyList = TaggedProxy.getAllObjectsByProxy(proxyObject);
			for (TaggedProxy taggedProxy : taggedProxyList) {
				Tag tag = taggedProxy.getTag();
				if (null != tag) {
					resultList.add(tag.toJSON());
				}
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get tags assigned to proxyObject, exception: " + e.getMessage());
		}
		return resultList;	
	}
	
	@Override
	public List<ProxyObject_Out> getProxyObjectsByTagname(String tagname) {
		List<ProxyObject_Out> resultList = new ArrayList<>();
		try {	
			// Validate tag
			Tag tag = Tag.getByName(tagname);
			if (null == tag) {
				Utils.handleException("Tag " + tagname + " is invalid");
			}
			
			List<TaggedProxy> taggedProxyList = TaggedProxy.getAllObjectsByTag(tag);
			for (TaggedProxy taggedProxy : taggedProxyList) {
				ProxyObject proxyObject = taggedProxy.getProxyObject();
				if (null != proxyObject) {
					resultList.add(proxyObject.toJSON());
				}
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get proxy objects tagged with " + tagname + ", exception: " + e.getMessage());
		}
		return resultList;
	}
}