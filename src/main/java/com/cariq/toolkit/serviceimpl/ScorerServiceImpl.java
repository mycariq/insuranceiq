package com.cariq.toolkit.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cariq.toolkit.model.ScoreDefinition;
import com.cariq.toolkit.service.ScorerService;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.scoring.Scorer;
import com.cariq.toolkit.utils.service.CarIQRestClientService;


/**
 * The Class ScoreDefinitionServiceImpl.
 */
@Service
public class ScorerServiceImpl implements ScorerService {

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;

	@Override
	public Double evaluateScore(String scoreDefName, GenericJSON paramValues){
		ScoreDefinition scoreDefinition = null;
		Double finalScore = null;
		try {
				scoreDefinition = ScoreDefinition.getByName(scoreDefName);
				if (null == scoreDefinition) {
					Utils.handleException("Invalid scoreDefintion: " + scoreDefName);
				}
				
				Scorer scorer = new Scorer();
				scorer.configure(scoreDefinition);
				
				for (String key : paramValues.keySet()) {
					scorer.addParameter(key, Double.parseDouble(paramValues.get(key).toString())); 
				}
				
				finalScore = scorer.evaluate();
				
		} catch (Exception e) {
			Utils.handleException("Failed to evaluate " + scoreDefName + ", Exception: " + e.getMessage());
		}
		return finalScore;
	}
}