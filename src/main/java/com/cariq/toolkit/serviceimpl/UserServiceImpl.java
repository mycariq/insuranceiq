package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.model.ActionLog;
import com.cariq.toolkit.model.Subscription;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.model.User.UpdateUser_In;
import com.cariq.toolkit.model.User.User_In;
import com.cariq.toolkit.model.User.User_Out;
import com.cariq.toolkit.service.UserService;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;


/**
 * The Class UserServiceImpl.
 */
@Service
public class UserServiceImpl implements UserService {

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;

	@Override
	@Transactional
	public ResponseWithIdJson addUser(User_In params) {
		User user = null;
		try {
			user = new User();
			user.fromJSON(params);
			user.setPassword(Utils.getMD5(params.getPassword()));
			user.setCreatedOn(new Date());
			user.setModifiedOn(new Date());
			user.persist();
			
		} catch (Exception e) {
			Utils.handleException("Failed to add user, exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(user.getId().toString(), "User : " + params.getUsername() + " created successfully.");
	}

	@Override
	public List<User_Out> getAllUsers(int pageNo, int pageSize) {
		List<User_Out> resultList = new ArrayList<>();
		try {
			List<User> users = User.getAllObjects(pageNo, pageSize);
			
			for (User user : users) {
				User_Out userOut = user.toJSON();
				resultList.add(userOut);
			}
		} catch (Exception e) {
			Utils.handleException("Failed to get users, exception: " + e.getMessage());
		}
		return resultList;
	}

	@Override
	public User_Out getUser(String username) {
		User_Out userOut = null;
		try {
			User user = User.getByUsername(username);
			if (null != user) {
				userOut = user.toJSON();
			}
		}catch (Exception e) {
			Utils.handleException("Failed to get user: " + username + ", exception: " + e.getMessage());
		}
		
		return userOut;
	}

	@Override
	public List<User_Out> getUsersBySubscriptionName(String subscriptionName) {
		List<User_Out> resultList = new ArrayList<>();
		try {
			Subscription subscritpion = Subscription.getByName(subscriptionName);
			if (null == subscritpion) {
				Utils.handleException("Subscription " + subscriptionName + " is invalid");
			}
			List<User> users = User.getBySubscription(subscritpion);
			for (User user : users) {
				User_Out userOut = user.toJSON();
				resultList.add(userOut);
			}
		}catch (Exception e) {
			Utils.handleException("Failed to get users for subscription: " + subscriptionName + ", exception: " + e.getMessage());
		}
		
		return resultList;
	}
	
	@Override
	@Transactional
	public ResponseJson updateUser(UpdateUser_In params, String username) {
		try {
			User user = User.getByUsername(username);
			if (user == null) {
				Utils.handleException("User " + username + " is invalid");
			}
			if (null != params.getSubscriptionName()) {
				 if (null == Subscription.getByName(params.getSubscriptionName()))
					 Utils.handleException("Subscription " + params.getSubscriptionName() + " is invalid");
			}
			user.updateUser(params.getUsername(), params.getSubscriptionName(), params.getRole(), 
					params.getFirstName(), params.getLastName(), params.getCellNumber(), 
					params.getEmail(), params.getCountryCode(), params.getTimeZoneId());
			user.merge();
			ActionLog.log(Utils.createContext("UserServiceImpl.updateUser"), "Update", "user",
					"User id : " + user.getId() + " username : " + user.getUsername(),
					"Old info :" + Utils.getJSonString(user.toJSON()));
		} catch (Exception e) {
			Utils.handleException("Failed to update user: " + username + ", exception: " + e.getMessage());
		}

		return new ResponseJson("User : " + username + " updated successfully.");
	}

	@Override
	@Transactional
	public ResponseJson deleteUser(String username) {
		try {
			User user = User.getByUsername(username);
			if (user != null) {
				// domainaccess - FK constraint
				user.remove();
				return new ResponseJson("User : " + username + " deleted successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to remove user: " + username + ", exception: " + e.getMessage());
		}
		return new ResponseJson("User : " + username + " does not exist!");
	}
}