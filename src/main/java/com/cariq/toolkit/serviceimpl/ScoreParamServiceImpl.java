package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.model.ScoreDefinition;
import com.cariq.toolkit.model.ScoreDefinition.ScoreDefinition_Out;
import com.cariq.toolkit.model.ScoreParam.ScoreParamDefinition_In;
import com.cariq.toolkit.model.ScoreParam.ScoreParam_In;
import com.cariq.toolkit.model.ScoreParam;
import com.cariq.toolkit.model.ParamDefinition;
import com.cariq.toolkit.model.ParamDefinition.ParamDefinition_In;
import com.cariq.toolkit.model.ParamDefinition.ParamDefinition_Out;
import com.cariq.toolkit.service.ParamDefinitionService;
import com.cariq.toolkit.service.ScoreParamService;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;


/**
 * The Class ScoreDefinitionServiceImpl.
 */
@Service
public class ScoreParamServiceImpl implements ScoreParamService {

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;
	
	@Autowired
	ParamDefinitionService paramService;

	@Override
	@Transactional
	public ResponseJson cloneScoreDefinitionWithParams(String existingScoreDefName, String newScoreDefName) {
		ScoreDefinition oldScoreDef = null, newScoreDef = null;
		try {
			// Get scoreDef
			oldScoreDef = ScoreDefinition.getByName(existingScoreDefName);
			if (null == oldScoreDef) {
				Utils.handleException("ScoreDefinition " + existingScoreDefName + " is invalid");
			}
			
			// Create new scoreDefinition with same specifications
			newScoreDef = new ScoreDefinition(newScoreDefName, oldScoreDef.getDescription(), oldScoreDef.getMinValue(), oldScoreDef.getMaxValue(), new Date(), new Date());
			newScoreDef.persist();
			System.out.println("Created ScoreDefinition: " + newScoreDefName);
			
			// Get list of paramDefs for existing ScoreDef and attach to the new ones
			ScoreParam newScoreParam = null;
			List<ScoreParam> scoreParamList = ScoreParam.getAllObjectsByScoreDefinition(oldScoreDef);
			for (ScoreParam scoreParam : scoreParamList) {
				ParamDefinition paramDef = scoreParam.getParamDef();
				newScoreParam = new ScoreParam(newScoreDef, paramDef, new Date(), new Date());
				newScoreParam.persist();	
				System.out.println("Attached ParamDefinition: " + paramDef.getName());
			}
			
		} catch(Exception e) {
			Utils.handleException("Failed to clone scoreDefinition, Exception: " + e.getMessage());
		}
		
		return new ResponseJson("Cloned scoreDefinition " + existingScoreDefName + " to " 
				+ newScoreDefName + "and attached the same paramDefintions successfully");
	}
	
	@Override
	@Transactional
	public ResponseWithIdJson attachParamToScoreDef(ScoreParamDefinition_In params) {
		ParamDefinition paramDef = null;
		ScoreParam scoreParam = null;
		try {
			// Get scoreDef
			ScoreDefinition scoreDefinition = ScoreDefinition.getByName(params.getScoreDefName());
			if (null == scoreDefinition) {
				Utils.handleException("ScoreDefinition " + params.getScoreDefName() + " is invalid");
			}
			
			// create or get paramDef
			ParamDefinition_In paramDefIn = new ParamDefinition_In(params.getParamDefName(), null, params.getWeight(),
					params.getMinValue(), params.getMaxValue(), params.getDefaultValue());
			ResponseWithIdJson idJson = paramService.createOrGetParamDefinition(paramDefIn); 
			if (null == idJson.getId() ||
					null == (paramDef = ParamDefinition.findObjectById(Long.parseLong(idJson.getId())))) {
				Utils.handleException("Failed to create or get paramDefinition " + params.getParamDefName());
			}
			
			// Associate
			scoreParam = new ScoreParam(scoreDefinition, paramDef, new Date(), new Date());
			scoreParam.persist();		
		} catch (Exception e) {
			Utils.handleException("Failed to associate paramDefinition: " + params.getParamDefName() 
				+ " to scoreDefinition: " + params.getScoreDefName() + ", Exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(scoreParam.getId().toString(), "Associated ParamDefinition to ScoreDefinition " + params.getScoreDefName() + " successfully.");
	}
	
	@Override
	@Transactional
	public ResponseWithIdJson attachParamToScoreDefByIDs(ScoreParam_In params) {
		ParamDefinition paramDef = null;
		ScoreParam scoreDefinitionAccess = null;
		try {
			// Get scoreDef
			ScoreDefinition scoreDefinition = ScoreDefinition.getByName(params.getScoreDefName());
			if (null == scoreDefinition) {
				Utils.handleException("ScoreDefinition " + params.getScoreDefName() + " is invalid");
			}
			
			// Get paramDef
			if (null == (paramDef = ParamDefinition.findObjectById(params.getParamDefId()))) {
				Utils.handleException("Invalid paramDefinition " + params.getParamDefId());
			}
			
			// Associate
			scoreDefinitionAccess = new ScoreParam(scoreDefinition, paramDef, new Date(), new Date());
			scoreDefinitionAccess.persist();		
		} catch (Exception e) {
			Utils.handleException("Failed to associate paramDefinition: " + params.getParamDefId()
				+ " to scoreDefinition: " + params.getScoreDefName() + ", Exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(scoreDefinitionAccess.getId().toString(), "Associated paramDefinition to ScoreDefinition " + params.getScoreDefName() + " successfully.");
	}
	
	@Override
	@Transactional
	public ResponseJson detachParamFromScoreDef(ScoreParamDefinition_In params) {
		ParamDefinition paramDef = null;
		try {
			// Get scoreDef
			ScoreDefinition scoreDefinition = ScoreDefinition.getByName(params.getScoreDefName());
			if (null == scoreDefinition) {
				Utils.handleException("ScoreDefinition " + params.getScoreDefName() + " is invalid");
			}
			
			// Get paramDef
			paramDef = ParamDefinition.getByUniqueValues(params.getParamDefName(), params.getWeight(),
					params.getMinValue(), params.getMaxValue(), params.getDefaultValue());
			if (null == paramDef) {
				Utils.handleException("Invalid paramDefinition " + params.getParamDefName());
			}
			
			ScoreParam scoreParam = ScoreParam.getObjectByScoreDefParamDef(scoreDefinition, paramDef);
			if (null != scoreParam) {
				scoreParam.remove();
				return new ResponseJson("ParamDefinition " + params.getParamDefName() + " is dissociated from ScoreDefinition " + params.getScoreDefName());
			}
		} catch (Exception e) {
			Utils.handleException("Failed to dissociate paramDefinition: " +  params.getParamDefName()
				+ " from scoreDefinition: " + params.getScoreDefName() + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("ParamDefinition " + params.getParamDefName() + " already not associated with ScoreDefinition " + params.getScoreDefName());
	}
	
	@Override
	@Transactional
	public ResponseJson detachParamFromScoreDefByIDs(ScoreParam_In params) {
		ParamDefinition paramDef = null;
		try {
			// Get scoreDef
			ScoreDefinition scoreDefinition = ScoreDefinition.getByName(params.getScoreDefName());
			if (null == scoreDefinition) {
				Utils.handleException("ScoreDefinition " + params.getScoreDefName() + " is invalid");
			}
			
			// Get paramDef
			paramDef = ParamDefinition.findObjectById(params.getParamDefId());
			if (null == paramDef) {
				Utils.handleException("Invalid paramDefinition " + params.getParamDefId());
			}
			
			ScoreParam scoreParam = ScoreParam.getObjectByScoreDefParamDef(scoreDefinition, paramDef);
			if (null != scoreParam) {
				scoreParam.remove();
				return new ResponseJson("ParamDefinition " + params.getParamDefId() + " is dissociated from ScoreDefinition " + params.getScoreDefName());
			}
		} catch (Exception e) {
			Utils.handleException("Failed to dissociate paramDefinition: " +  params.getParamDefId()
				+ " from scoreDefinition: " + params.getScoreDefName() + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("ParamDefinition " + params.getParamDefId() + " already not associated with ScoreDefinition " + params.getScoreDefName());
	}
	
	@Override
	public List<ScoreDefinition_Out> getScoresForParamDefId(Long paramDefId) {
		List<ScoreDefinition_Out> resultList = new ArrayList<>();
		try {
			ParamDefinition paramDefinition = ParamDefinition.findObjectById(paramDefId);
			if (paramDefinition == null) {
				Utils.handleException("ParamDefinition " + paramDefId + " is invalid");
			}
			
			List<ScoreParam> scoreParamList = ScoreParam.getAllObjectsByParamDefinition(paramDefinition);
			for (ScoreParam scoreParam : scoreParamList) {
				ScoreDefinition scoreDefinition = scoreParam.getScoreDef();
				if (null != scoreDefinition) {
					ScoreDefinition_Out scoreDefinitionOut = scoreDefinition.toJSON();
					resultList.add(scoreDefinitionOut);
				}
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get scoreDefinitions for paramDefinition: " 
				+ paramDefId + ", Exception: " + e.getMessage());
		}
		return resultList;
		
	}
	
	@Override
	public List<ParamDefinition_Out> geParamDefsForScoreDef(String scoreDefinitionName) {
		List<ParamDefinition_Out> resultList = new ArrayList<>();
		try {
			ScoreDefinition scoreDefinition = ScoreDefinition.getByName(scoreDefinitionName);
			if (scoreDefinition == null) {
				Utils.handleException("scoreDefinition " + scoreDefinitionName + " is invalid");
			}
			
			List<ScoreParam> scoreParamList = ScoreParam.getAllObjectsByScoreDefinition(scoreDefinition);
			for (ScoreParam scoreParam : scoreParamList) {
				ParamDefinition paramDefinition = scoreParam.getParamDef();
				if (null != paramDefinition) {
					ParamDefinition_Out paramDefinitionOut = paramDefinition.toJSON();
					resultList.add(paramDefinitionOut);
				}
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get paramDefinitions for scoreDefinition: " 
					+ scoreDefinitionName + ", Exception: " + e.getMessage());
		}
		return resultList;
	}
}