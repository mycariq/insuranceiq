package com.cariq.toolkit.serviceimpl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cariq.toolkit.coreiq.exporter.ExportedData;
import com.cariq.toolkit.coreiq.exporter.ExportedData.ExporterDataJSON_Out;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.service.ExporterDataService;

@Service
public class ExporterDataServiceImpl implements ExporterDataService {

	@Override
	public List<ExporterDataJSON_Out> getExportedDataList(String option, int pageNo, int pageSize) {
		try (ProfilePoint _getExportedDataList = ProfilePoint.profileAction("ProfAction_getExportedDataList")) {
			List<ExportedData> exportedDataList;
			if (option.equalsIgnoreCase(ExportedData.ALL)) {
				exportedDataList = ExportedData.findExportedData(pageNo, pageSize);
			} else {
				User user = User.getLoggedInUser();
				exportedDataList = ExportedData.findExportedData(user, pageNo, pageSize);
			}
			return Utils.convertList(exportedDataList, ExporterDataJSON_Out.class);
		}
	}

}
