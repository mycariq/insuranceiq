package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.model.ScoreDefinition;
import com.cariq.toolkit.model.ScoreDefinition.ScoreDefinition_In;
import com.cariq.toolkit.model.ScoreDefinition.ScoreDefinition_Out;
import com.cariq.toolkit.service.ScoreDefinitionService;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;


/**
 * The Class ScoreDefinitionServiceImpl.
 */
@Service
public class ScoreDefinitionServiceImpl implements ScoreDefinitionService {

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;

	@Override
	@Transactional
	public ResponseWithIdJson addScoreDefinition(ScoreDefinition_In params) {
		ScoreDefinition scoreDefinition = null;
		try {
				scoreDefinition = new ScoreDefinition(params.getName(), params.getDescription(), params.getMinValue(), params.getMaxValue(), new Date(), new Date());
				scoreDefinition.persist();
				
		} catch (Exception e) {
			Utils.handleException("Failed to add ScoreDefinition, Exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(scoreDefinition.getId().toString(), "ScoreDefinition : " + params.getName() + " created successfully.");
	}

	@Override
	public List<ScoreDefinition_Out> getAllScoreDefinitions(int pageNo, int pageSize) {
		List<ScoreDefinition_Out> resultList = new ArrayList<>();
		try {
			List<ScoreDefinition> scoreDefinitions = ScoreDefinition.getAllObjects(pageNo, pageSize);
			
			for (ScoreDefinition scoreDefinition : scoreDefinitions) {
				resultList.add(scoreDefinition.toJSON());
			}
		} catch (Exception e) {
			Utils.handleException("Failed to get the scoreDefinitions, Exception: " + e.getMessage());
		}
		return resultList;
	}

	@Override
	public ScoreDefinition_Out getScoreDefinition(String scoreDefinitionName) {
		ScoreDefinition_Out scoreDefinitionOut = null;
		try {
			ScoreDefinition scoreDefinition = ScoreDefinition.getByName(scoreDefinitionName);
			if (null != scoreDefinition) {
				scoreDefinitionOut = scoreDefinition.toJSON();
			}
		}catch (Exception e) {
			Utils.handleException("Failed to get scoreDefinition: " + scoreDefinitionName + ", Exception: " + e.getMessage());
		}
		
		return scoreDefinitionOut;
	}

	@Override
	@Transactional
	public ResponseJson updateScoreDefinition(ScoreDefinition_In params, String scoreDefinitionName) {
		try {
			ScoreDefinition scoreDefinition = ScoreDefinition.getByName(scoreDefinitionName);
			if (scoreDefinition == null) {
				Utils.handleException("ScoreDefinition " + scoreDefinitionName + " is invalid");
			}
			scoreDefinition.updateScoreDefinition(params.getName(), params.getDescription(), params.getMinValue(), params.getMinValue());
			scoreDefinition.merge();
		} catch (Exception e) {
			Utils.handleException("Failed to update scoreDefinition: " + scoreDefinitionName + ", exception: " + e.getMessage());
		}

		return new ResponseJson("ScoreDefinition : " + scoreDefinitionName + " updated successfully.");
	}

	@Override
	@Transactional
	public ResponseJson deleteScoreDefinition(String scoreDefinitionName) {
		try {
			ScoreDefinition scoreDefinition = ScoreDefinition.getByName(scoreDefinitionName);
			if (scoreDefinition != null) {
				// ScoreParam - FK constraint
				scoreDefinition.remove();
				return new ResponseJson("ScoreDefinition : " + scoreDefinitionName + " deleted successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to remove scoreDefinition: " + scoreDefinitionName + ", exception: " + e.getMessage());
		}
		return new ResponseJson("ScoreDefinition : " + scoreDefinitionName + " does not exist!");
	}
}