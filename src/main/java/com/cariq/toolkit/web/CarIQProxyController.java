package com.cariq.toolkit.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.model.User;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.service.CariqProxyService;

@CarIQPublicAPI(name = "CarIQProxy", description = "Everything related to CarIQ API calling from fleet")
@Controller
@RequestMapping("/cariqProxy")
public class CarIQProxyController {

	@Autowired
	CariqProxyService proxyService;

	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Call get cariq api", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "/get/{apiName}", method = RequestMethod.GET)
	@ResponseBody
	public Object get(
			@CarIQPublicAPIParameter(name = "apiName", description = "the apiName") @PathVariable String apiName) {
		try (CarIQAPI _get_CarIQ = new CarIQAPI("CARIQ_get_CarIQ")) {
			return proxyService.call(User.getLoggedInUser(), null, Utils.makeApi(apiName),
					HttpMethod.GET);
		}
	}

	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Call post cariq api", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "/post/{apiName}", method = RequestMethod.POST)
	@ResponseBody
	public Object post(@RequestBody GenericJSON json,
			@CarIQPublicAPIParameter(name = "apiName", description = "the apiName") @PathVariable String apiName) {
		try (CarIQMutableAPI _post_CarIQ = new CarIQMutableAPI("CARIQ_post_CarIQ", json)) {
			return proxyService.call(User.getLoggedInUser(), json, Utils.makeApi(apiName),
					HttpMethod.POST);
		}
	}

	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Call put cariq api", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "/put/{apiName}", method = RequestMethod.PUT)
	@ResponseBody
	public Object put(@RequestBody GenericJSON json,
			@CarIQPublicAPIParameter(name = "apiName", description = "the apiName") @PathVariable String apiName) {
		try (CarIQMutableAPI _put_CarIQ = new CarIQMutableAPI("CARIQ_put_CarIQ", json)) {
			return proxyService.call(User.getLoggedInUser(), json, Utils.makeApi(apiName),
					HttpMethod.PUT);
		}
	}

	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Call delete cariq api ", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "/delete/{apiName}", method = RequestMethod.DELETE)
	@ResponseBody
	public Object delete(
			@CarIQPublicAPIParameter(name = "apiName", description = "the apiName") @PathVariable String apiName) {
		try (CarIQMutableAPI _delete_CarIQ = new CarIQMutableAPI("CARIQ_delete_CarIQ", apiName)) {
			return proxyService.call(User.getLoggedInUser(), null, Utils.makeApi(apiName),
					HttpMethod.DELETE);
		}
	}
}
