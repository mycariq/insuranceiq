package com.cariq.toolkit.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.coreiq.exporter.ExportedData.ExporterDataJSON_Out;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.service.ExporterDataService;

@CarIQPublicAPI(name = "ExportedData", description = "Cache of async downloaded Exported Data")
@RequestMapping("/exportedData")
@Controller
public class ExportedDataController {

	@Autowired
	ExporterDataService exporterDataService;
	
	/**
	 * Get All ExporterData in decending order with paging
	 *
	 * @param param
	 *            the raw json
	 * @param response
	 *            the response
	 * @return the object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Enlist ExporterData", responseClass = ExporterDataJSON_Out.class, internal = true)
	@RequestMapping(value = "/enlist/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<ExporterDataJSON_Out> enlistExporterData(
			@CarIQPublicAPIParameter(name = "pageNo", description = "the pageNo") @PathVariable int pageNo,
			@CarIQPublicAPIParameter(name = "pageSize", description = "the pageSize") @PathVariable int pageSize) {
		try (CarIQAPI _getResource = new CarIQAPI("FleetIQ_getExporterData")) {
			return exporterDataService.getExportedDataList("user", pageNo, pageSize);
		}
	}
	
}
