package com.cariq.toolkit.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.model.ParamDefinition.ParamDefinition_In;
import com.cariq.toolkit.model.ParamDefinition.ParamDefinition_Out;
import com.cariq.toolkit.model.ScoreDefinition.ScoreDefinition_In;
import com.cariq.toolkit.model.ScoreDefinition.ScoreDefinition_Out;
import com.cariq.toolkit.model.ScoreParam.ScoreParamDefinition_In;
import com.cariq.toolkit.model.ScoreParam.ScoreParam_In;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.service.ParamDefinitionService;
import com.cariq.toolkit.service.ScoreDefinitionService;
import com.cariq.toolkit.service.ScoreParamService;
import com.cariq.toolkit.service.ScorerService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;


/**
 * The Class ScoringEngineController.
 */
@CarIQPublicAPI(name = "ScoringEngine", description = "Everything related to ScoringEngine")
@Controller
@RequestMapping("/scoringengine")
public class ScoringEngineController {

	/** The scoreDefinitionservice. */
	@Autowired
	private ScoreDefinitionService scoreDefinitionservice;

	/** The ParamDefinitionService. */
	@Autowired
	private ParamDefinitionService paramDefinitionservice;
	
	/** The ScoreParamService. */
	@Autowired
	private ScoreParamService scoreParamService;
	
	@Autowired
	private ScorerService scorerService;

	/** 
	 * APIs for ScoreDefintion 
	 */
	
	/**
	 * Adds the ScoreDefinition.
	 * @param params
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Create new ScoreDefinition", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/addScoreDef", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson addScoreDefinition(@RequestBody ScoreDefinition_In params) {
		try (CarIQMutableAPI _addScoreDefinition = new CarIQMutableAPI("CARIQ_addScoreDefinition", params)) {
			return scoreDefinitionservice.addScoreDefinition(params);
		}
	}

	/**
	 * Gets the scoreDefinitions with pagination.
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Get all ScoreDefinitions", responseClass = ScoreDefinition_Out.class, internal = true)
	@RequestMapping(value = "/getAllScoreDefs/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<ScoreDefinition_Out> getAllScoreDefinitions(
			@CarIQPublicAPIParameter(name = "PageNo", description = "Page Number") @PathVariable int pageNo, 
			@CarIQPublicAPIParameter(name = "PageSize", description = "Page Size") @PathVariable int pageSize) {
		try (CarIQAPI _getScoreDefinitions = new CarIQAPI("CARIQ_getAllScoreDefinitions_with_pagination")) {
			return scoreDefinitionservice.getAllScoreDefinitions(pageNo, pageSize);
		}
	}
	
	/**
	 * Gets the ScoreDefinition.
	 * @param scoreDefinitionName
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Get ScoreDefinition by Name", responseClass = ScoreDefinition_Out.class, internal = true)
	@RequestMapping(value = "/getScoreDef/{scoreDefinitionName}", method = RequestMethod.GET)
	@ResponseBody
	public ScoreDefinition_Out getScoreDefinition(
			@CarIQPublicAPIParameter(name = "ScoreDefinitionName", description = "ScoreDefinition Name") @PathVariable String scoreDefinitionName) {
		try (CarIQAPI _getScoreDefinition = new CarIQAPI("CARIQ_getScoreDefinition")) {
			return scoreDefinitionservice.getScoreDefinition(scoreDefinitionName);
		}
	}

	 /**
	  * Update scoreDefinition.
	  * @param params
	  * @param scoreDefinitionName
	  * @return
	  */
	@CarIQPublicAPIMethod(description = "Update ScoreDefinition", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/updateScoreDef/{scoreDefinitionName}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseJson updateScoreDefinition(@RequestBody ScoreDefinition_In params,
			@CarIQPublicAPIParameter(name = "ScoreDefinitionName", description = "ScoreDefinition Name") @PathVariable String scoreDefinitionName) {
		try (CarIQMutableAPI _updateScoreDefinition = new CarIQMutableAPI("CARIQ_updateScoreDefinition", params)) {
			return scoreDefinitionservice.updateScoreDefinition(params, scoreDefinitionName);
		}
	}

	/**
	 * Removes the scoreDefinition.
	 * @param scoreDefinitionName
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Remove ScoreDefinition", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/removeScoreDef/{scoreDefinitionName}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson removeScoreDefinition(
			@CarIQPublicAPIParameter(name = "ScoreDefinitionName", description = "ScoreDefinition Name") @PathVariable String scoreDefinitionName) {
		try (CarIQMutableAPI _removeScoreDefinition = new CarIQMutableAPI("CARIQ_removeScoreDefinition", scoreDefinitionName)) {
			return scoreDefinitionservice.deleteScoreDefinition(scoreDefinitionName);
		}
	}
	
	/** 
	 * APIs for ParamDefintion 
	 */
	
	/**
	 * Adds the ParamDefinition.
	 * @param params
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Create new ParamDefinition", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/addParamDef", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson addParamDefinition(@RequestBody ParamDefinition_In params) {
		try (CarIQMutableAPI _addParamDefinition = new CarIQMutableAPI("CARIQ_addParamDefinition", params)) {
			return paramDefinitionservice.createOrGetParamDefinition(params);
		}
	}

	/**
	 * Gets the paramDefinitions with pagination.
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Get all ParamDefinitions", responseClass = ParamDefinition_Out.class, internal = true)
	@RequestMapping(value = "/getAllParamDefs/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<ParamDefinition_Out> getAllParamDefinitions(
			@CarIQPublicAPIParameter(name = "PageNo", description = "Page Number") @PathVariable int pageNo, 
			@CarIQPublicAPIParameter(name = "PageSize", description = "Page Size") @PathVariable int pageSize) {
		try (CarIQAPI _getParamDefinitions = new CarIQAPI("CARIQ_getAllParamDefinitions_with_pagination")) {
			return paramDefinitionservice.getAllParamDefinitions(pageNo, pageSize);
		}
	}
	
	/**
	 * Get ParamDefinitions by name.
	 * @param paramDefinitionName
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Get ParamDefinitions by Name", responseClass = ParamDefinition_Out.class, internal = true)
	@RequestMapping(value = "/getParamDef/{paramDefinitionName}", method = RequestMethod.GET)
	@ResponseBody
	public List<ParamDefinition_Out> getParamDefinition(
			@CarIQPublicAPIParameter(name = "ParamDefinitionName", description = "ParamDefinition Name") @PathVariable String paramDefinitionName) {
		try (CarIQAPI _getParamDefinition = new CarIQAPI("CARIQ_getParamDefinition")) {
			return paramDefinitionservice.getParamDefinitionsByName(paramDefinitionName);
		}
	}

	/**
	 * Remove ParamDefinition.
	 * @param params
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Remove ParamDefinition", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/removeParamDef", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson removeParamDefinition(@RequestBody ParamDefinition_In params) {
		try (CarIQMutableAPI _removeParamDefinition = new CarIQMutableAPI("CARIQ_removeParamDefinition", params)) {
			return paramDefinitionservice.deleteParamDefinition(params);
		}
	}
	
	
	/**
	 * APIs for ScoreParam Service
	 */

	@CarIQPublicAPIMethod(description = "Clone existing scoreDefinition with same paramDefintions", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/clone/{existingScoreDefName}/{newScoreDefName}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseJson cloneScoreDefinitionWithParams(
		@CarIQPublicAPIParameter(name = "ExistingScoreDefName", description = "Existing ScoreDefinition Name") @PathVariable String existingScoreDefName,
		@CarIQPublicAPIParameter(name = "NewScoreDefName", description = "New ScoreDefinition Name") @PathVariable String newScoreDefName){
		try (CarIQMutableAPI _cloneScoreDefinitionWithParams = new CarIQMutableAPI("CARIQ_cloneScoreDefinitionWithParams", existingScoreDefName + "_" + newScoreDefName)) {
			return scoreParamService.cloneScoreDefinitionWithParams(existingScoreDefName, newScoreDefName);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Associate ParamDefinition with ScoreDefinition", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/attachParamToScoreDef", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson attachParamToScoreDef(@RequestBody ScoreParamDefinition_In params) {
		try (CarIQMutableAPI _associateParamToScoreDefinition = new CarIQMutableAPI("CARIQ_associateParamToScoreDefinition", params)) {
			return scoreParamService.attachParamToScoreDef(params);
		}
	}

	@CarIQPublicAPIMethod(description = "Associate ParamDefinition with ScoreDefinition by IDs", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/attachParamToScoreDefByIDs", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson attachParamToScoreDefByIDs(@RequestBody ScoreParam_In params) {
		try (CarIQMutableAPI _associateParamToScoreDefinition = new CarIQMutableAPI("CARIQ_associateParamToScoreDefinition", params)) {
			return scoreParamService.attachParamToScoreDefByIDs(params);
		}
	}

	@CarIQPublicAPIMethod(description = "Dissociate ParamDefinition from ScoreDefinition by IDs", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/detachParamFromScoreDefByIDs", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson detachParamFromScoreDefByIDs(@RequestBody ScoreParam_In params) {
		try (CarIQMutableAPI _detachParamFromScoreDefByIDs = new CarIQMutableAPI("CARIQ_detachParamFromScoreDefByIDs", params)) {
			return scoreParamService.detachParamFromScoreDefByIDs(params);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Get all scoreDefs associated with given paramDef", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/getScoresForParamDefId/{paramDefinitionId}", method = RequestMethod.GET)
	@ResponseBody
	public List<ScoreDefinition_Out> getScoresForParamDefId(
			@CarIQPublicAPIParameter(name = "ParamDefinitionId", description = "ParamDefinition Id") @PathVariable Long paramDefinitionId) {
		try (CarIQAPI _getScoreDefinitionsForParamDefinition = new CarIQAPI("CARIQ_getScoreDefinitionsForParamDefinition")) {
			return scoreParamService.getScoresForParamDefId(paramDefinitionId);
		}
	}

	@CarIQPublicAPIMethod(description = "Get all paramDefs associated with given scoreDef", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/geParamsForScore/{scoreDefinitionName}", method = RequestMethod.GET)
	@ResponseBody
	public List<ParamDefinition_Out> geParamDefinitionsForScoreDefinition(
			@CarIQPublicAPIParameter(name = "ScoreDefinitionName", description = "ScoreDefinition Name") @PathVariable String scoreDefinitionName) {
		try (CarIQAPI _geParamDefinitionsForScoreDefinition = new CarIQAPI("CARIQ_geParamDefinitionsForScoreDefinition")) {
			return scoreParamService.geParamDefsForScoreDef(scoreDefinitionName);
		}
	}
	
	/*
	 * APIs for Scorer Service
	 */
	
	@CarIQPublicAPIMethod(description = "Calculate the score for given parameter values", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/evaluateScore/{scoreDefName}", method = RequestMethod.POST)
	@ResponseBody
	public Double evaluateScore(@RequestBody GenericJSON paramValues,
			@CarIQPublicAPIParameter(name = "scoreDefName", description = "ScoreDefinition Name") @PathVariable String scoreDefName) {
		try (CarIQAPI _evaluateScore = new CarIQAPI("CARIQ_evaluateScore")) {
			return scorerService.evaluateScore(scoreDefName, paramValues);
		}
	}
}
