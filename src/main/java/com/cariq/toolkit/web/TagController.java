package com.cariq.toolkit.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.model.Tag;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_Out;
import com.cariq.toolkit.model.Tag.Tag_In;
import com.cariq.toolkit.model.Tag.Tag_Out;
import com.cariq.toolkit.model.TaggedObject.TaggedObject_In;
import com.cariq.toolkit.model.TaggedObject.TaggedObject_Out;
import com.cariq.toolkit.model.TaggedProxy.TagAndProxyDetails_In;
import com.cariq.toolkit.model.TaggedProxy.TaggedProxy_In;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.service.TagService;
import com.cariq.toolkit.service.TaggedObjectService;
import com.cariq.toolkit.service.TaggedProxyService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;


/**
 * The Class TagController.
 */
@CarIQPublicAPI(name = "Tag", description = "Everything related to Tagging")
@Controller
@RequestMapping("/tag")
public class TagController {

	/** The tagservice. */
	@Autowired
	private TagService tagservice;
	
	/** The taggedObjectService. */
	@Autowired
	private TaggedObjectService taggedObjectService;
	
	/** The taggedProxyService. */
	@Autowired
	private TaggedProxyService taggedProxyService;
	
	
	/*
	 * APIs using tagservice
	 */
	
	@CarIQPublicAPIMethod(description = "Check if tag exists OR create a new one", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/createOrGetTag", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson createOrGetTag(@RequestBody Tag_In params) {
		try (CarIQMutableAPI _createOrGetTag = new CarIQMutableAPI("CARIQ_createOrGetTag", params)) {
			return tagservice.createOrGetTag(params);
		}
	}

	@CarIQPublicAPIMethod(description = "Get all tags", responseClass = Tag_Out.class, internal = true)
	@RequestMapping(value = "/getAll/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<Tag_Out> getAllTags(
			@CarIQPublicAPIParameter(name = "PageNo", description = "Page Number") @PathVariable int pageNo, 
			@CarIQPublicAPIParameter(name = "PageSize", description = "Page Size") @PathVariable int pageSize) {
		try (CarIQAPI _getTags = new CarIQAPI("CARIQ_getAllTags_with_pagination")) {
			return tagservice.getAllTags(pageNo, pageSize);
		}
	}

	@CarIQPublicAPIMethod(description = "Get signle tag by Tag Name", responseClass = Tag_Out.class, internal = true)
	@RequestMapping(value = "/get/{tagName}", method = RequestMethod.GET)
	@ResponseBody
	public Tag_Out getTag(
			@CarIQPublicAPIParameter(name = "TagName", description = "Tag Name") @PathVariable String tagName) {
		try (CarIQAPI _getTag = new CarIQAPI("CARIQ_getTag")) {
			return tagservice.getTag(tagName);
		}
	}

	@CarIQPublicAPIMethod(description = "Update tag", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/update/{tagName}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseJson updateTag(@RequestBody Tag_In params,
			@CarIQPublicAPIParameter(name = "TagName", description = "Tag Name") @PathVariable String tagName) {
		try (CarIQMutableAPI _updateTag = new CarIQMutableAPI("CARIQ_updateTag", params)) {
			return tagservice.updateTag(params, tagName);
		}
	}

	@CarIQPublicAPIMethod(description = "Remove tag", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/remove/{tagName}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson removeTag(
			@CarIQPublicAPIParameter(name = "TagName", description = "Tag Name") @PathVariable String tagName) {
		try (CarIQMutableAPI _removeTag = new CarIQMutableAPI("CARIQ_removeTag", tagName)) {
			return tagservice.deleteTag(tagName);
		}
	}
	
	/*
	 * APIs using taggedObjectService
	 */
	
	@CarIQPublicAPIMethod(description = "Tag the specific object", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/tagObject", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson tagObject(@RequestBody TaggedObject_In params) {
		try (CarIQMutableAPI _tagObject = new CarIQMutableAPI("CARIQ_tagObject", params)) {
			return taggedObjectService.tagObject(params);
		}
	}

	@CarIQPublicAPIMethod(description = "Remove tag from tagged object", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/untagObject/{tagname}/{objectId}/{fqdnClass}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson untagObject (@RequestBody TaggedObject_In params) {
		try (CarIQMutableAPI _untagObject = new CarIQMutableAPI("CARIQ_untagObject", params)) {
			return taggedObjectService.untagObject(params);
		}
	}

	@CarIQPublicAPIMethod(description = "Get all tags assigned to specific object", responseClass = Tag_Out.class, internal = true)
	@RequestMapping(value = "/getTagsByFqdnObjectId/{fqdnClass}/{objectId}", method = RequestMethod.GET)
	@ResponseBody
	public List<Tag_Out> getTagsByFqdnObjectId(
			@CarIQPublicAPIParameter(name = "FqdnClass", description = "Fully Qualified Class Name") @PathVariable String fqdnClass,
			@CarIQPublicAPIParameter(name = "ObjectId", description = "Object Id") @PathVariable Long objectId) {
		try (CarIQAPI _getTagsByFqdnObjectId = new CarIQAPI("CARIQ_getTagsByFqdnObjectId")) {
			return taggedObjectService.getTagsByFqdnObjectId(fqdnClass, objectId);
		}
	}

	@CarIQPublicAPIMethod(description = "Get all tags used for specific class", responseClass = Tag_Out.class, internal = true)
	@RequestMapping(value = "/getTagsByFqdnClass/{fqdnClass}", method = RequestMethod.GET)
	@ResponseBody
	public List<Tag_Out> getTagsByFqdnClass (
			@CarIQPublicAPIParameter(name = "FqdnClass", description = "Fully Qualified Class Name") @PathVariable String fqdnClass) {
		try (CarIQAPI _getTagsByFqdnClass = new CarIQAPI("CARIQ_getTagsByFqdnClass")) {
			return taggedObjectService.getTagsByFqdnClass(fqdnClass);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Get all objects of same class tagged with a specific tag", responseClass = Long.class, internal = true)
	@RequestMapping(value = "/getObjectIDsByFqdnTagname/{fqdnClass}/{tagname}", method = RequestMethod.GET)
	@ResponseBody
	public List<Long> getObjectIDsByFqdnTagname(
			@CarIQPublicAPIParameter(name = "FqdnClass", description = "Fully Qualified Class Name") @PathVariable String fqdnClass,
			@CarIQPublicAPIParameter(name = "TagName", description = "Tag Name") @PathVariable String tagname) {
		try (CarIQAPI _getObjectIDsByFqdnTagname = new CarIQAPI("CARIQ_getObjectIDsByFqdnTagname")) {
			return taggedObjectService.getObjectIDsByFqdnTagname(fqdnClass, tagname);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Get all objects tagged with a specific tag", responseClass = TaggedObject_Out.class, internal = true)
	@RequestMapping(value = "/getAllObjectsByTagname/{tagname}", method = RequestMethod.GET)
	@ResponseBody
	public List<TaggedObject_Out> getAllObjectsByTagname(
			@CarIQPublicAPIParameter(name = "TagName", description = "Tag Name") @PathVariable String tagname) {
		try (CarIQAPI _getObjectsByTagId = new CarIQAPI("CARIQ_getObjectsByTagname")) {
			List<TaggedObject_Out> allObjectList = new ArrayList<>();
			List<ProxyObject_Out> proxyList = new ArrayList<>();
			
			// Get the direct taggedObject list
			allObjectList = taggedObjectService.getObjectsByTagname(tagname);
			
			// Get taggedProxy list and convert them to taggedObject_Out objects on the fly. Merge the list.
			proxyList = taggedProxyService.getProxyObjectsByTagname(tagname);
			for (ProxyObject_Out proxy : proxyList) {
				TaggedObject_Out object = new TaggedObject_Out(tagname, proxy.getObjectId(), "proxy", 
						proxy.getCreatedOn(), proxy.getModifiedOn(), proxy.getId(), (Tag_Out)(Tag.getByName(tagname)).toJSON());
				allObjectList.add(object);
			}
			
			return allObjectList;
		}
	}
	
	/*
	 * APIs using taggedProxyService
	 */
	
	@CarIQPublicAPIMethod(description = "Tag the proxyObject", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/tagproxy", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson tagProxy(@RequestBody TaggedProxy_In params){
		try (CarIQMutableAPI _tagProxy = new CarIQMutableAPI("CARIQ_tagProxy", params)) {
			return taggedProxyService.tagProxy(params);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Create proxy-object and tag", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/createAndTagProxy", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson createAndTagProxy(@RequestBody TagAndProxyDetails_In params){
		try (CarIQMutableAPI _createAndTagProxy = new CarIQMutableAPI("CARIQ_createAndTagProxy", params)) {
			return taggedProxyService.createAndTagProxy(params);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Remove tag from proxy-object", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/untagProxy", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson untagProxy(@RequestBody TaggedProxy_In params){
		try (CarIQMutableAPI _untagProxy = new CarIQMutableAPI("CARIQ_untagProxy", params)) {
			return taggedProxyService.untagProxy(params);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Remove the tag from proxy-object identified by proxy-details.", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/untagProxyByProxyDetails", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson untagProxyByProxyDetails(@RequestBody TagAndProxyDetails_In params){
		try (CarIQMutableAPI _untagProxyByProxyDetails = new CarIQMutableAPI("CARIQ_untagProxyByProxyDetails", params)) {
			return taggedProxyService.untagProxyByProxyDetails(params);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Get all tags assigned to proxy-object", responseClass = Tag_Out.class, internal = true)
	@RequestMapping(value = "/getTagsByProxyObject/{domainName}/{objectId}/{objectType}", method = RequestMethod.GET)
	@ResponseBody
	public List<Tag_Out> getTagsByProxyObject(
			@CarIQPublicAPIParameter(name = "DomainName", description = "Domai NName") @PathVariable String domainName,
			@CarIQPublicAPIParameter(name = "ObjectId", description = "Object Id") @PathVariable Long objectId,
			@CarIQPublicAPIParameter(name = "ObjectType", description = "Object Type") @PathVariable String objectType) {
		try (CarIQAPI _getTagsByProxyObject = new CarIQAPI("CARIQ_getTagsByProxyObject")) {
			return taggedProxyService.getTagsByProxyObject(domainName, objectId, objectType);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Get all proxy-objects tagged with a specific tag", responseClass = ProxyObject_Out.class, internal = true)
	@RequestMapping(value = "/getProxyObjectsByTagname/{tagname}", method = RequestMethod.GET)
	@ResponseBody
	public List<ProxyObject_Out> getProxyObjectsByTagname(
			@CarIQPublicAPIParameter(name = "TagName", description = "Tag Name") @PathVariable String tagname) {
		try (CarIQAPI _getProxyObjectsByTagname = new CarIQAPI("CARIQ_getProxyObjectsByTagname")) {
			return taggedProxyService.getProxyObjectsByTagname(tagname);
		}
	}
}
