package com.cariq.toolkit.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.model.Domain.CreateDomain_In;
import com.cariq.toolkit.model.Domain.Domain_In;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.Domain.UpdateDomain_In;
import com.cariq.toolkit.model.Subscription.Subscription_Out;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.service.DomainAccessService;
import com.cariq.toolkit.service.DomainService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;


/**
 * The Class DomainController.
 */
@CarIQPublicAPI(name = "Domain", description = "Everything related to Domain")
@Controller
@RequestMapping("/domain")
public class DomainController {

	/** The domainservice. */
	@Autowired
	private DomainService domainservice;

	/** The domainsAccessService. */
	@Autowired
	private DomainAccessService domainAccessService;
	
	/*
	 * APIs using domainService
	 */

	@CarIQPublicAPIMethod(description = "Create new domain", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson addDomain(@RequestBody CreateDomain_In params) {
		try (CarIQMutableAPI _addDomain = new CarIQMutableAPI("CARIQ_addDomain", params)) {
			String authString = Utils.createEncodedText(params.getUsername(), params.getPassword());
			String authType = Utils.AUTH_BASIC;
			Domain_In domainIn = new Domain_In(params.getName(), params.getUrl(), authString, authType);
			return domainservice.addDomain(domainIn);
		}
	}

	@CarIQPublicAPIMethod(description = "Get all domains", responseClass = Domain_Out.class, internal = true)
	@RequestMapping(value = "/getAll/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<Domain_Out> getAllDomains(
			@CarIQPublicAPIParameter(name = "PageNo", description = "Page Number") @PathVariable int pageNo, 
			@CarIQPublicAPIParameter(name = "PageSize", description = "Page Size") @PathVariable int pageSize) {
		try (CarIQAPI _getDomains = new CarIQAPI("CARIQ_getAllDomains_with_pagination")) {
			return domainservice.getAllDomains(pageNo, pageSize);
		}
	}

	@CarIQPublicAPIMethod(description = "Get signle domain by Domain Name", responseClass = Domain_Out.class, internal = true)
	@RequestMapping(value = "/get/{domainName}", method = RequestMethod.GET)
	@ResponseBody
	public Domain_Out getDomain(
			@CarIQPublicAPIParameter(name = "DomainName", description = "Domain Name") @PathVariable String domainName) {
		try (CarIQAPI _getDomain = new CarIQAPI("CARIQ_getDomain")) {
			return domainservice.getDomain(domainName);
		}
	}

	@CarIQPublicAPIMethod(description = "Update domain", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/update/{domainName}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseJson updateDomain(@RequestBody UpdateDomain_In params,
			@CarIQPublicAPIParameter(name = "DomainName", description = "Domain Name") @PathVariable String domainName) {
		try (CarIQMutableAPI _updateDomain = new CarIQMutableAPI("CARIQ_updateDomain", params)) {
			return domainservice.updateDomain(params, domainName);
		}
	}

	@CarIQPublicAPIMethod(description = "Remove domain", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/remove/{domainName}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson removeDomain(
			@CarIQPublicAPIParameter(name = "DomainName", description = "Domain Name") @PathVariable String domainName) {
		try (CarIQMutableAPI _removeDomain = new CarIQMutableAPI("CARIQ_removeDomain", domainName)) {
			return domainservice.deleteDomain(domainName);
		}
	}
	
	/*
	 * APIs using domainAccessService
	 */
	
	@CarIQPublicAPIMethod(description = "Grant DomainAccess to Subscription", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/grantDomainAccessToSubscription/{subscriptionName}/{domainName}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseJson grantDomainAccessToSubscription(
			@CarIQPublicAPIParameter(name = "SubscriptionName", description = "Subscription Name") @PathVariable String subscriptionName, 
			@CarIQPublicAPIParameter(name = "DomainName", description = "Domain Name") @PathVariable String domainName) {
		try (CarIQMutableAPI _grantDomainAccessToSubscription = new CarIQMutableAPI("CARIQ_grantDomainAccessToSubscription", subscriptionName)) {
			return domainAccessService.grantDomainAccessToSubscription(subscriptionName, domainName);
		}
	}

	@CarIQPublicAPIMethod(description = "Block DomainAccess for Subscription", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/removeDomainAccessForSubscription/{subscriptionName}/{domainName}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson removeDomainAccessForSubscription(			
			@CarIQPublicAPIParameter(name = "SubscriptionName", description = "Subscription Name") @PathVariable String subscriptionName, 
			@CarIQPublicAPIParameter(name = "DomainName", description = "Domain Name") @PathVariable String domainName) {
		try (CarIQMutableAPI _removeDomainAccessForSubscription = new CarIQMutableAPI("CARIQ_removeDomainAccessForSubscription", subscriptionName)) {
			return domainAccessService.removeDomainAccessForSubscription(subscriptionName, domainName);
		}
	}

	@CarIQPublicAPIMethod(description = "Get Accessible Domains for Subscription", responseClass = Domain_Out.class, internal = true)
	@RequestMapping(value = "/getAccessibleDomainsForSubscription/{subscriptionName}", method = RequestMethod.GET)
	@ResponseBody
	public List<Domain_Out> getAccessibleDomainsForSubscription(
			@CarIQPublicAPIParameter(name = "SubscriptionName", description = "Subscription Name") @PathVariable String subscriptionName) {
		try (CarIQAPI _getAccessibleDomainsForSubscription = new CarIQAPI("CARIQ_getAccessibleDomainsForSubscription")) {
			return domainAccessService.getAccessibleDomainsForSubscription(subscriptionName);
		}
	}

	@CarIQPublicAPIMethod(description = "Get Privileged Subscriptions for Domain", responseClass = Subscription_Out.class, internal = true)
	@RequestMapping(value = "/getPrivilegedSubscriptionsForDomain/{domainName}", method = RequestMethod.GET)
	@ResponseBody
	public List<Subscription_Out> getPrivilegedSubscriptionsForDomain(
			@CarIQPublicAPIParameter(name = "DomainName", description = "Domain Name") @PathVariable String domainName) {
		try (CarIQAPI _getPrivilegedSubscriptionsForDomain = new CarIQAPI("CARIQ_getPrivilegedSubscriptionsForDomain")) {
			return domainAccessService.getPrivilegedSubscriptionsForDomain(domainName);
		}
	}
}
