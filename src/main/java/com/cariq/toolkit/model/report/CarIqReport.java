package com.cariq.toolkit.model.report;
import java.util.Date;
import java.util.List;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.service.report.ReportType;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class CarIqReport {

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date generatedOn;

    /**
     */
    private String type;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date fromDate;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date toDate;

    /**
     */
    private String name;

    /**
     */
    private String token;

    /**
     */
    private long sizeBytes;

    /**
     */
    private String ownerType;

    /**
     */
    private String ownerId;
    

    /**
     * Mandatory default constructor
     */
    public CarIqReport() {
    	super();
    }

    /**
     * 
     * @param token
     * @param generatedOn
     * @param fromDate
     * @param toDate
     * @param name
     * @param ownerType
     * @param ownerId
     * @param type
     */
    public CarIqReport(String token, Date generatedOn, Date fromDate, Date toDate, String name, String ownerType, String ownerId,
			String type) {
		this.token = token;
		this.generatedOn = generatedOn;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.name = name;
		this.ownerType = ownerType;
		this.ownerId = ownerId;
		this.type = type.toString();
	}

	/**
	 * Construct the report from the token and input json and type.
	 *
	 * @param inputJson
	 *            the input json
	 * @return the car log
	 */
	public static CarIqReport getReport(String token, GenericJSON inputJson, String type, String ownerType, String ownerId) {
		try {
			String logName = token + ".zip";
			return new CarIqReport(token, new Date(), Utils.getValue(Date.class, inputJson, "fromDate"),
					Utils.getValue(Date.class, inputJson, "toDate"), logName, ownerType, ownerId, type);
		} catch (Exception e) {
			Utils.handleException(e);
		}
		return null;
	}

	/**
	 * Find car report.
	 *
	 * @param logId
	 *            the log id
	 * @param itsUser
	 *            the its user
	 * @return the car log
	 */
	public static CarIqReport findReport(long logId) {
		TypedQuery<CarIqReport> query = CarIqReport.entityManager()
				.createQuery("SELECT c FROM CarIqReport AS c WHERE c.id=:id", CarIqReport.class);
		query.setParameter("id", logId);

		if (query.getResultList().isEmpty())
			return null;
		return query.getSingleResult();
	}

	/**
	 * Find car logs.
	 *
	 * @param itsUser
	 *            the its user
	 * @param itsCar
	 *            the its car
	 * @param firstResult
	 *            the first result
	 * @param pageSize
	 *            the page size
	 * @return the list
	 */
	public static List<CarIqReport> findReports(String ownerType, String ownerId, int firstResult, int pageSize) {
		TypedQuery<CarIqReport> query = CarIqReport.entityManager().createQuery(
				"SELECT c FROM CarIqReport AS c WHERE c.ownerType=:ownerType AND c.ownerId=:ownerId ORDER BY c.ownerType DESC",
				CarIqReport.class);
		query.setFirstResult(firstResult);
		query.setMaxResults(pageSize);
		query.setParameter("ownerType", ownerType);
		query.setParameter("ownerId", ownerId);
		return query.getResultList();
	}

	/**
	 * Count car logs.
	 *
	 * @param itsUser
	 *            the its user
	 * @param itsCar
	 *            the its car
	 * @return the long
	 */
	public static long countReports(String ownerType, String ownerId) {
		TypedQuery<Long> query = CarIqReport.entityManager().createQuery(
				"SELECT count(c) FROM CarIqReport AS c WHERE c.ownerType=:ownerType AND c.ownerId=:ownerId", Long.class);
		query.setParameter("ownerType", ownerType);
		query.setParameter("ownerId", ownerId);
		if (query.getResultList().isEmpty())
			return 0;
		
		return query.getSingleResult();
	}

	public CarIqReport updateSize(long size) {
		this.sizeBytes = size;
		return merge();
	}

	public static long countReports(String ownerType, String ownerId, ReportType type) {
		TypedQuery<Long> query = CarIqReport.entityManager().createQuery(
				"SELECT count(c) FROM CarIqReport AS c WHERE c.ownerType=:ownerType AND c.ownerId=:ownerId AND c.type=:type",
				Long.class);
		query.setParameter("ownerId", ownerId);
		query.setParameter("ownerType", ownerType);
		query.setParameter("type", type.toString());
		if (query.getResultList().isEmpty())
			return 0;
		return query.getSingleResult();
	}

	public static List<CarIqReport> findReports(String ownerType, String ownerId, ReportType type, int firstResult,
			int pageSize) {
		TypedQuery<CarIqReport> query = CarIqReport.entityManager().createQuery(
				"SELECT c FROM CarIqReport AS c WHERE c.ownerType=:ownerType AND c.ownerId=:ownerId AND c.type=:type ORDER BY c.generatedOn DESC",
				CarIqReport.class);
		query.setFirstResult(firstResult);
		query.setMaxResults(pageSize);
		query.setParameter("ownerId", ownerId);
		query.setParameter("ownerType", ownerType);
		query.setParameter("type", type.toString());
		return query.getResultList();
	}

	public CarIqReport updateName(String newName) {
		if (getName().equals(newName))
			return this;
		
		setName(newName);
		return merge();		
	}

//	/**
//	 * @param startDate
//	 * @param fromDate
//	 * @param pageSize
//	 * @param pageNo
//	 * @return
//	 */
//	public static List<UserActivityCount> getLogCountInDateRange(Date fromDate, Date toDate, int pageNo, int pageSize) {
//		int startPage = Utils.validatePagination(pageSize, pageNo);
//		TypedQuery<UserActivityCount> query = entityManager().createQuery(
//				"SELECT NEW com.cariq.www.cqbadges.helper.UserActivityCount(c.owner, count(o)) FROM CarIqReport as o JOIN o.itsCar as c WHERE date(o.generatedOn) BETWEEN :fromDate and :toDate group by c.owner",
//				UserActivityCount.class);
//		query.setParameter("fromDate", fromDate);
//		query.setParameter("toDate", toDate);
//		query.setFirstResult(startPage);
//		query.setMaxResults(pageSize);
//		return query.getResultList();
//	}
}
