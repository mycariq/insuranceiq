// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.cariq.toolkit.model.report;

import com.cariq.toolkit.model.report.CarIqReport;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect CarIqReport_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager CarIqReport.entityManager;
    
    public static final List<String> CarIqReport.fieldNames4OrderClauseFilter = java.util.Arrays.asList("generatedOn", "type", "fromDate", "toDate", "name", "token", "sizeBytes", "ownerType", "ownerId");
    
    public static final EntityManager CarIqReport.entityManager() {
        EntityManager em = new CarIqReport().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long CarIqReport.countCarIqReports() {
        return entityManager().createQuery("SELECT COUNT(o) FROM CarIqReport o", Long.class).getSingleResult();
    }
    
    public static List<CarIqReport> CarIqReport.findAllCarIqReports() {
        return entityManager().createQuery("SELECT o FROM CarIqReport o", CarIqReport.class).getResultList();
    }
    
    public static List<CarIqReport> CarIqReport.findAllCarIqReports(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM CarIqReport o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, CarIqReport.class).getResultList();
    }
    
    public static CarIqReport CarIqReport.findCarIqReport(Long id) {
        if (id == null) return null;
        return entityManager().find(CarIqReport.class, id);
    }
    
    public static List<CarIqReport> CarIqReport.findCarIqReportEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM CarIqReport o", CarIqReport.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<CarIqReport> CarIqReport.findCarIqReportEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM CarIqReport o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, CarIqReport.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void CarIqReport.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void CarIqReport.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            CarIqReport attached = CarIqReport.findCarIqReport(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void CarIqReport.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void CarIqReport.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public CarIqReport CarIqReport.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        CarIqReport merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
