package com.cariq.toolkit.model.async;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.quartz.Job;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

	
@RooToString
@RooJpaActiveRecord
public class WorkItem {

	/**
	 */
	@NotNull(message = "Worker class should not be null")
	private String workerClass;

	/**
	 */
	private String inputJson;

	/**
	 */
	private String type;

	/**
	 */
	private int progress;

	private String host;

	/**
	 */
	@ManyToOne
	private User user;

	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date startTime;

	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date endTime;

	/**
	 */
	private String outputJson;

	/**
	 */
	@NotNull(message = "Status should not be null")
	private String status = WorkItemStatus.READY.toString();

	/**
	 *
	 */
	@NotEmpty(message = "UUID should not be empty")
	@Column(unique = true)
	private String uuId;

	/**
	 *
	 */
	@NotNull(message = "name should not be null")
	private String name;

	/**
	 *
	 */
	private String descriptions;

	public WorkItem() {
	}
	
	public WorkItem(String inputJson, String type, String workerClass, String uuId, String name,
			String descriptions) {
		this(inputJson, type, workerClass, uuId, null, name, descriptions);
	}
	
	public WorkItem(GenericJSON inputJson, String type, String workerClass, String uuId, String name,
			String descriptions) {
		this(Utils.getJSonString(inputJson), type, workerClass, uuId, name, descriptions);
	}
	
	// Getters and Setters
	public WorkItem(String inputJson, String type, String workerClass, String uuId, User user, String name,
			String descriptions) {
		this.inputJson = inputJson;
		this.type = type;
		this.workerClass = workerClass;
		this.uuId = uuId;
		this.name = name;
		this.descriptions = descriptions;
		this.user = user;
	}

	public String getWorkerClass() {
		return workerClass;
	}

	public void setWorkerClass(String workerClass) {
		this.workerClass = workerClass;
	}

	public String getInputJson() {
		return inputJson;
	}

	public void setInputJson(String inputJson) {
		this.inputJson = inputJson;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getOutputJson() {
		return outputJson;
	}

	public void setOutputJson(String outputJson) {
		this.outputJson = outputJson;
	}

	public String getUuId() {
		return uuId;
	}

	public void setUuId(String uuId) {
		this.uuId = uuId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return descriptions;
	}

	public void setDescription(String descriptions) {
		this.descriptions = descriptions;
	}

	public String getFailureCause() {
		return failureCause;
	}

	public void setFailureCause(String failureCause) {
		this.failureCause = failureCause;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public static List<WorkItem> findUnCompletedWorkItem(int rowNumber, int size) {
		TypedQuery<WorkItem> query = WorkItem.entityManager()
				.createQuery("SELECT w from WorkItem AS w WHERE w.status=:status order by id desc", WorkItem.class);
		query.setParameter("status", WorkItemStatus.READY.toString());
		query.setFirstResult(rowNumber);
		query.setMaxResults(size);
		return query.getResultList();
	}

	public static long countUnCompletedWorkItems() {
		TypedQuery<Long> query = WorkItem.entityManager()
				.createQuery("SELECT count(w) from WorkItem AS w WHERE w.status!=:status", Long.class);
		query.setParameter("status", WorkItemStatus.READY.toString());
		return query.getSingleResult();
	}

	public WorkItemStatus getStatus() {
		return WorkItemStatus.valueOf(status);
	}

	public void setStatus(WorkItemStatus status) {
		this.status = status.toString();
	}

	/**
	 * getWorkItemByTypeAndStatus
	 *
	 * @param type
	 * @param status
	 * @param startPosition
	 * @param maxResult
	 * @return List<WorkItem>
	 */
	public static List<WorkItem> getWorkItemByTypeAndStatus(String type, String status, int startPosition,
			int maxResult) {
		TypedQuery<WorkItem> query = WorkItem.entityManager()
				.createQuery(" SELECT o FROM WorkItem o WHERE O.type = :type AND o.status = :status ", WorkItem.class);
		query.setFirstResult(startPosition);
		query.setMaxResults(maxResult);
		query.setParameter("type", type);
		query.setParameter("status", status);
		return query.getResultList();
	}

	/**
	 * getWorkItemByUUID
	 *
	 * @param uuid
	 * @return WorkItem
	 */
	public static WorkItem getWorkItemByUUID(String uuid) {
		TypedQuery<WorkItem> query = WorkItem.entityManager()
				.createQuery(" SELECT o FROM WorkItem o WHERE o.uuId = :uuid ", WorkItem.class);
		query.setParameter("uuid", uuid);
		if (!query.getResultList().isEmpty())
			return query.getSingleResult();
		return null;
	}

	/**
	 * getWorkItemByItsUser
	 *
	 * @param startPosition
	 * @param maxResult
	 * @return List<WorkItem>
	 */
	public static List<WorkItem> getWorkItems(int startPosition, int maxResult) {
		TypedQuery<WorkItem> query = WorkItem.entityManager()
				.createQuery("SELECT o FROM WorkItem o  order by startTime desc ", WorkItem.class);
		query.setFirstResult(startPosition);
		query.setMaxResults(maxResult);
		return query.getResultList();
	}

	/**
	 * getWorkItemByStatus
	 *
	 * @param status
	 * @param startPosition
	 * @param maxResult
	 * @return List<WorkItem>
	 */
	public static List<WorkItem> getWorkItemByStatus(String status, int startPosition, int maxResult) {
		TypedQuery<WorkItem> query = WorkItem.entityManager().createQuery(
				" SELECT o FROM WorkItem o WHERE o.status = :status  order by startTime desc ", WorkItem.class);
		query.setFirstResult(startPosition);
		query.setMaxResults(maxResult);
		query.setParameter("status", status);
		return query.getResultList();
	}

	/**
	 * getWorkItemByType
	 *
	 * @param type
	 * @param startPosition
	 * @param maxResult
	 * @return List<WorkItem>
	 */
	public static List<WorkItem> getWorkItemByType(String type, int startPosition, int maxResult) {
		TypedQuery<WorkItem> query = WorkItem.entityManager()
				.createQuery("SELECT o FROM WorkItem o WHERE o.type = :type order by startTime desc ", WorkItem.class);
		query.setFirstResult(startPosition);
		query.setMaxResults(maxResult);
		query.setParameter("type", type);
		return query.getResultList();
	}

	/**
	 * countWorkItemsIfStatusEquals
	 *
	 * @param status
	 * @return
	 */
	public static Long countWorkItemsIfStatusEquals(String status) {
		TypedQuery<Long> query = WorkItem.entityManager().createQuery(
				"SELECT COUNT(o) FROM WorkItem o WHERE o.status = :status  order by startTime desc ", Long.class);
		query.setParameter("status", status);
		if (query.getSingleResult() != null)
			return query.getSingleResult();
		return (long) 0;
	}

	/**
	 * countWorkItemsIfTypeEquals
	 *
	 * @param type
	 * @return
	 */
	public static Long countWorkItemsIfTypeEquals(String type) {
		TypedQuery<Long> query = WorkItem.entityManager().createQuery(
				"SELECT COUNT(o) FROM WorkItem o WHERE o.type = :type order by startTime desc ", Long.class);
		query.setParameter("type", type);
		if (query.getSingleResult() != null)
			return query.getSingleResult();
		return (long) 0;
	}

	/**
	 * countWorkItemsIfStatusEqualsAndStatusEquals
	 *
	 * @param type
	 * @param status
	 * @return
	 */
	public static Long countWorkItemsIfStatusEqualsAndStatusEquals(String type, String status) {
		TypedQuery<Long> query = WorkItem.entityManager().createQuery(
				"SELECT COUNT(o) FROM WorkItem o WHERE o.type = :type AND o.status = :status  order by startTime desc ",
				Long.class);
		query.setParameter("type", type);
		query.setParameter("status", status);
		if (query.getSingleResult() != null)
			return query.getSingleResult();
		return (long) 0;
	}

	// Autocommit function - not transactional - requires some annotation?
	public WorkItem start() {
		if (getStatus().equals(WorkItemStatus.READY)) {
			setStatus(WorkItemStatus.RUNNING);
			setStartTime(new Date());
			this.host = Utils.getHostName();
			return merge();
		}
		throw new RuntimeException("WorkItem.start(): Invalid status to start the WorkItem: " + getStatus());
	}

	public WorkItem fail(String cause) {
		if (getStatus().equals(WorkItemStatus.RUNNING)) {
			setStatus(WorkItemStatus.FAILED);
			setEndTime(new Date());
			this.failureCause = cause;
			return merge();
		}
		throw new RuntimeException("WorkItem.fail(): Invalid status to start the WorkItem: " + getStatus());
	}

	// Autocommit function - not transactional - requires some annotation?
	public WorkItem complete() {
		if (getStatus().equals(WorkItemStatus.RUNNING)) {
			setStatus(WorkItemStatus.COMPLETED);
			setEndTime(new Date());
			return merge();
		}
		throw new RuntimeException("WorkItem.complete(): Invalid status to start the WorkItem: " + getStatus());
	}

	// Autocommit function - not transactional - requires some annotation?
	public WorkItem setPercentComplete(int percentComplete) {
		setProgress(percentComplete);
		return merge();
	}

	@SuppressWarnings("unchecked")
	public Class<? extends Job> getWorker()
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		return (Class<? extends Job>) Class.forName(workerClass);
	}

	/**
	 * Get Latest Work Item with given JobName - it may be running, or, in
	 * Progress
	 * 
	 * @param jobName
	 * @return
	 */
	public static WorkItem getByDescription(String jobName) {
		String qry = "SELECT o FROM WorkItem o WHERE o.descriptions = :description order by id desc ";
		System.out.println("jobName = " + jobName);
		TypedQuery<WorkItem> query = WorkItem.entityManager().createQuery(qry, WorkItem.class);
		query.setParameter("description", jobName);
		query.setMaxResults(1);
		if (!query.getResultList().isEmpty())
			return query.getSingleResult();
		return null;
	}

	public static List<String> getAllTypes() {
		String qry = "SELECT DISTINCT w.type from WorkItem w ORDER by w.type";
		TypedQuery<String> query = WorkItem.entityManager().createQuery(qry, String.class);
		return query.getResultList();
	}

	/**
	 */
	@Size(max = 5000)
	private String failureCause;

	public WorkItem refresh() {
		if (WorkItem.entityManager().contains(this)) {
			WorkItem.entityManager().refresh(this);
		}
		return this;
	}
}
