/**
 * 
 */
package com.cariq.toolkit.model.async;

/**
 * @author hrishi
 *
 */
public enum WorkItemStatus {
	READY, RUNNING, FAILED, COMPLETED, EXPIRED;
}