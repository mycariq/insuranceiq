// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.fleetiq.www.model.toolkit.async;

import com.cariq.toolkit.model.async.CarIqJob;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect CarIqJob_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager CarIqJob.entityManager;
    
    public static final List<String> CarIqJob.fieldNames4OrderClauseFilter = java.util.Arrays.asList("logger", "JOB_FREQUENCY_NONE", "JOB_FREQUENCY_DAILY", "JOB_FREQUENCY_WEEKLY", "JOB_FREQUENCY_MONTHLY", "jobName", "lastRunDate", "frequencyUnit", "frequency", "defaultFrequency", "scheduledDay", "scheduledTime", "jobType");
    
    public static final EntityManager CarIqJob.entityManager() {
        EntityManager em = new CarIqJob().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long CarIqJob.countCarIqJobs() {
        return entityManager().createQuery("SELECT COUNT(o) FROM CarIqJob o", Long.class).getSingleResult();
    }
    
    public static List<CarIqJob> CarIqJob.findAllCarIqJobs() {
        return entityManager().createQuery("SELECT o FROM CarIqJob o", CarIqJob.class).getResultList();
    }
    
    public static List<CarIqJob> CarIqJob.findAllCarIqJobs(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM CarIqJob o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, CarIqJob.class).getResultList();
    }
    
    public static CarIqJob CarIqJob.findCarIqJob(Long id) {
        if (id == null) return null;
        return entityManager().find(CarIqJob.class, id);
    }
    
    public static List<CarIqJob> CarIqJob.findCarIqJobEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM CarIqJob o", CarIqJob.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<CarIqJob> CarIqJob.findCarIqJobEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM CarIqJob o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, CarIqJob.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void CarIqJob.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void CarIqJob.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            CarIqJob attached = CarIqJob.findCarIqJob(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void CarIqJob.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void CarIqJob.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public CarIqJob CarIqJob.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        CarIqJob merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
