package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.Tag.Tag_Out;
import com.cariq.toolkit.model.TaggedObject.TaggedObject_In;
import com.cariq.toolkit.model.TaggedObject.TaggedObject_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;

/**
 * This class implements "tag-object association" concept. Any object can be
 * tagged (many-to-many relation).
 * 
 * @author amita
 *
 */

@Entity
@Configurable
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "tag", "objectId", "fqdnClass" }))
public class TaggedObject extends PersistentObject<TaggedObject>
		implements JSONable<TaggedObject_In, TaggedObject_Out, IdJSON> {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new TaggedObject().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static TaggedObject find(Long id) {
		return entityManager().find(TaggedObject.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public TaggedObject findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull
	@ManyToOne
	Tag tag;

	// This is any object in the database
	@NotNull
	Long objectId;

	@NotNull
	@Column(length = 512)
	String fqdnClass;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public TaggedObject() {
		super();
	}

	/**
	 * Parameterized constructor
	 * 
	 * @param tag
	 * @param objectId
	 * @param createdOn
	 * @param modifiedOn
	 */
	public TaggedObject(Tag tag, Long objectId, String fqdnClass, Date createdOn, Date modifiedOn) {
		super();
		this.tag = tag;
		this.objectId = objectId;
		this.fqdnClass = fqdnClass;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Getters Setters
	 */
	public Tag getTag() {
		return tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public String getFqdnClass() {
		return fqdnClass;
	}

	public void setFqdnClass(String fqdnClass) {
		this.fqdnClass = fqdnClass;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "TaggedObject [id=" + id + ", version=" + version + ", tagId=" + tag.getId() + ", objectIdId=" + objectId
				+ ", fqdnClass=" + fqdnClass + ", createdOn=" + createdOn.toString() + ", modifiedOn="
				+ modifiedOn.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objectId == null) ? 0 : objectId.hashCode());
		result = prime * result + ((fqdnClass == null) ? 0 : fqdnClass.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaggedObject other = (TaggedObject) obj;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.getId().equals(other.tag.getId()))
			return false;
		if (objectId == null) {
			if (other.objectId != null)
				return false;
		} else if (!objectId.equals(other.objectId))
			return false;
		if (fqdnClass == null) {
			if (other.fqdnClass != null)
				return false;
		} else if (!fqdnClass.equals(other.fqdnClass))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	// In Class (for construction)
	public static class TaggedObject_In {
		String tagname; //Tagname is unique and easily available as input while creating taggedObject.
		Long objectId;
		String fqdnClass;

		public TaggedObject_In() {
			super();
		}

		/**
		 * @param tagname
		 * @param objectId
		 * @param fqdnClass
		 */
		public TaggedObject_In(String tagname, Long objectId, String fqdnClass) {
			this.tagname = tagname;
			this.objectId = objectId;
			this.fqdnClass = fqdnClass;
		}

		public String getTagName() {
			return tagname;
		}

		public void setTagName(String tagname) {
			this.tagname = tagname;
		}

		public Long getObjectId() {
			return objectId;
		}

		public void setObjectId(Long objectIdId) {
			this.objectId = objectIdId;
		}

		public String getFqdnClass() {
			return fqdnClass;
		}

		public void setFqdnClass(String fqdnClass) {
			this.fqdnClass = fqdnClass;
		}
	}

	// Out class for output
	public static class TaggedObject_Out extends TaggedObject_In {
		Long id;
		Tag_Out tagJSON;
		String createdOn;
		String modifiedOn;

		public TaggedObject_Out() {
			super();
		}

		/**
		 * /**
		 * 
		 * @param tagId
		 * @param objectId
		 * @param fqdnClass
		 * @param createdOn
		 * @param modifiedOn
		 */
		public TaggedObject_Out(String tagname, Long objectId, String fqdnClass, String createdOn, String modifiedOn,
				Long id, Tag_Out tagJSON) {
			super(tagname, objectId, fqdnClass);
			this.id = id;
			this.tagJSON = tagJSON;
			this.modifiedOn = modifiedOn;
			this.createdOn = createdOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Tag_Out getTagJSON() {
			return tagJSON;
		}

		public void setTagJSON(Tag_Out tagJSON) {
			this.tagJSON = tagJSON;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public TaggedObject_Out toJSON() {
		return new TaggedObject_Out(tag.getName(), objectId, fqdnClass, createdOn.toString(), modifiedOn.toString(), id,
				tag.toJSON());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(TaggedObject_In json) {
		this.tag = json.tagname == null ? null : Tag.getByName(json.tagname);
		this.objectId = json.getObjectId();
		this.fqdnClass = json.getFqdnClass();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static TaggedObject findObjectById(Long id) {
		return find(id);
	}

	public static List<TaggedObject> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<TaggedObject> qry = new CarIQSimpleQuery<TaggedObject>("getAllObjects", entityManager(),
				TaggedObject.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	/**
	 * @param tag
	 * @return
	 */
	public static List<TaggedObject> getObjectsByTag(Tag tag) {
		CarIQSimpleQuery<TaggedObject> qry = new CarIQSimpleQuery<TaggedObject>("getObjectsByTag", entityManager(),
				TaggedObject.class);
		qry.addCondition("tag", "=", tag);

		return qry.getResultList();
	}

	/**
	 * 
	 * @param fqdnClass
	 * @return
	 */
	public static List<Tag> getTagsByFqdn(String fqdnClass) {
		String qryStr = "SELECT DISTINCT to.tag from TaggedObject to where to.fqdnClass='" + fqdnClass + "'";
		TypedQuery<Tag> qry = TaggedObject.entityManager().createQuery(qryStr, Tag.class);
		return qry.getResultList();
	}

	/**
	 * 
	 * @param fqdnClass
	 * @param objectId
	 * @return
	 */
	public static List<TaggedObject> getTagsByFqdnObjectId(String fqdnClass, Long objectId) {
		CarIQSimpleQuery<TaggedObject> qry = new CarIQSimpleQuery<TaggedObject>("getTagsByFqdnObjectId",
				entityManager(), TaggedObject.class);
		qry.addCondition("fqdnClass", "=", fqdnClass);
		qry.addCondition("objectId", "=", objectId);

		return qry.getResultList();
	}

	/**
	 * 
	 * @param fqdnClass
	 * @param tag
	 * @return
	 */
	public static List<TaggedObject> getObjectsByFqdnTag(String fqdnClass, Tag tag) {
		CarIQSimpleQuery<TaggedObject> qry = new CarIQSimpleQuery<TaggedObject>("getObjectsByFqdnTagId",
				entityManager(), TaggedObject.class);
		qry.addCondition("fqdnClass", "=", fqdnClass);
		qry.addCondition("tag", "=", tag);

		return qry.getResultList();
	}

	public static TaggedObject getObject(Tag tag, Long objectId, String fqdnClass) {
		CarIQSimpleQuery<TaggedObject> qry = new CarIQSimpleQuery<TaggedObject>("getObject", entityManager(),
				TaggedObject.class);
		qry.addCondition("tag", "=", tag);
		qry.addCondition("objectId", "=", objectId);
		qry.addCondition("fqdnClass", "=", fqdnClass);

		return qry.getSingleResult();
	}

}
