package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.ScoreDefinition.ScoreDefinition_In;
import com.cariq.toolkit.model.ScoreDefinition.ScoreDefinition_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;

/**
 * This class implements "ScoreDefinition" concept. 
 * We need to come up with different scores by processing the stored information. 
 * ScoringEngine can instantiate scoring-model using ScoreDefinition and associated ParamDefinitions. 
 * Scorer can actually evaluate the score by processing parameter values.
 * ScoreDefinition(name, minvalue, maxvalue)
 * ParamDefinition(name, weight, minvalue, maxvalue, defaultvalue)
 * ScoreDefinitions can be associated with multiple ParamDefinitions to define the scoring-model (many-to-many)
 * 
 * @author amita
 *
 */

@Entity
@Configurable
public class ScoreDefinition extends PersistentObject<ScoreDefinition> implements JSONable<ScoreDefinition_In, ScoreDefinition_Out, IdJSON> {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new ScoreDefinition().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static ScoreDefinition find(Long id) {
		return entityManager().find(ScoreDefinition.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public ScoreDefinition findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull(message = "@NotNull ScoreDefinition Name")
	@Column(unique = true)
	String name;
	
	@Column(length = 1024)
	String description;
	
	@NotNull(message = "@NotNull minvalue")
	Double minValue;
	
	@NotNull(message = "@NotNull maxvalue")
	Double maxValue;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public ScoreDefinition() {
		super();
	}

	public ScoreDefinition(String name, String description, Double minvalue, Double maxvalue, 
			Date createdOn,	Date modifiedOn) {
		super();
		this.name = name;
		this.description = description;
		this.minValue = minvalue;
		this.maxValue = maxvalue;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getMinValue() {
		return minValue;
	}

	public void setMinValue(Double minvalue) {
		this.minValue = minvalue;
	}

	public Double getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Double maxvalue) {
		this.maxValue = maxvalue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "ScoreDefinition [id=" + id + ", version=" + version + ", name=" + name + ", description=" + description 
				+ ", minValue=" + minValue + ", maxValue=" + maxValue  
				+ ", createdOn=" + createdOn.toString() + ", modifiedOn=" + modifiedOn.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((minValue == null) ? 0 : minValue.hashCode());
		result = prime * result + ((maxValue == null) ? 0 : maxValue.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScoreDefinition other = (ScoreDefinition) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;		
		if (minValue == null) {
			if (other.minValue != null)
				return false;
		} else if (!minValue.equals(other.minValue))
			return false;		
		if (maxValue == null) {
			if (other.maxValue != null)
				return false;
		} else if (!maxValue.equals(other.maxValue))
			return false;			
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	// In Class (for construction)
	public static class ScoreDefinition_In {
		String name;
		String description;
		Double minValue;
		Double maxValue;

		public ScoreDefinition_In() {
			super();
		}

		public ScoreDefinition_In(String name, String description, Double minValue, Double maxValue) {
			super();
			this.name = name;
			this.description = description;
			this.minValue = minValue;
			this.maxValue = maxValue;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public Double getMinValue() {
			return minValue;
		}

		public void setMinValue(Double minValue) {
			this.minValue = minValue;
		}

		public Double getMaxValue() {
			return maxValue;
		}

		public void setMaxValue(Double maxValue) {
			this.maxValue = maxValue;
		}
	}

	// Out class for output
	public static class ScoreDefinition_Out extends ScoreDefinition_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public ScoreDefinition_Out() {
			super();
		}
		
		public ScoreDefinition_Out(String name, String description, Double minValue, Double maxValue, String createdOn, String modifiedOn,
				Long id) {
			super(name, description, minValue, maxValue);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public ScoreDefinition_Out toJSON() {
		return new ScoreDefinition_Out(name, description, minValue, maxValue, createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(ScoreDefinition_In json) {
		this.name = json.name;
		this.description = json.description;
		this.minValue = json.minValue;
		this.maxValue = json.maxValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static ScoreDefinition findObjectById(Long id) {
		return find(id);
	}
	
	public static List<ScoreDefinition> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<ScoreDefinition> qry = new CarIQSimpleQuery<ScoreDefinition>("getAllObjects", entityManager(), ScoreDefinition.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static ScoreDefinition getByName(String name) {
		CarIQSimpleQuery<ScoreDefinition> qry = new CarIQSimpleQuery<ScoreDefinition>("getByName", entityManager(), ScoreDefinition.class);
		qry.addCondition("name", "=", name);

		return qry.getSingleResult();
	}

	public void updateScoreDefinition(String name, String description, Double minValue, Double maxValue) {
		if (name != null)
			this.name = name;
		if (description != null)
			this.description = description;
		if (minValue != null)
			this.minValue = minValue;
		if (maxValue != null)
			this.maxValue = maxValue;

		this.modifiedOn = new Date();
	}
}
