package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.ParamDefinition.ParamDefinition_In;
import com.cariq.toolkit.model.ParamDefinition.ParamDefinition_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;

/**
 * This class implements "ParamDefinition" concept. 
 * We need to come up with different scores by processing the stored information. 
 * ScoringEngine can instantiate scoring-model using ScoreDefinition and associated ParamDefinitions. 
 * Scorer can actually evaluate the score by processing parameter values.
 * ScoreDefinition(name, minvalue, maxvalue)
 * ParamDefinition(name, weight, minvalue, maxvalue, defaultvalue)
 * ScoreDefinitions can be associated with multiple ParamDefinitions to define the scoring-model (many-to-many)
 * 
 * @author amita
 *
 */

@Entity
@Configurable
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "name", "weight", "minValue", "maxValue", "defaultValue"}))
public class ParamDefinition extends PersistentObject<ParamDefinition> implements JSONable<ParamDefinition_In, ParamDefinition_Out, IdJSON> {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new ParamDefinition().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static ParamDefinition find(Long id) {
		return entityManager().find(ParamDefinition.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public ParamDefinition findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull(message = "@NotNull ParamDefinition Name")
	String name;

	@Column(length = 1024)
	String description;

	@NotNull(message = "@NotNull weightValue")
	Double weight;
	
	@NotNull(message = "@NotNull minValue")
	Double minValue;
	
	@NotNull(message = "@NotNull maxValue")
	Double maxValue;
	
	@NotNull(message = "@NotNull defaultValue")
	Double defaultValue;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public ParamDefinition() {
		super();
	}


	public ParamDefinition(String name, String description, Double weight, Double minValue, Double maxValue,
			Double defaultValue, Date createdOn, Date modifiedOn) {
		super();
		this.name = name;
		this.description = description;
		this.weight = weight;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.defaultValue = defaultValue;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getMinValue() {
		return minValue;
	}

	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}

	public Double getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Double maxValue) {
		this.maxValue = maxValue;
	}

	public Double getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(Double defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "ParamDefinition [id=" + id + ", version=" + version + ", name=" + name + ", description=" + description 
				+ ", weight=" + weight + ", minValue=" + minValue + ", maxValue=" + maxValue + ", defaultValue=" + defaultValue +
				", createdOn=" + createdOn.toString() + ", modifiedOn=" + modifiedOn.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((weight == null) ? 0 : weight.hashCode());
		result = prime * result + ((minValue == null) ? 0 : minValue.hashCode());
		result = prime * result + ((maxValue == null) ? 0 : maxValue.hashCode());
		result = prime * result + ((defaultValue == null) ? 0 : defaultValue.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParamDefinition other = (ParamDefinition) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (weight == null) {
			if (other.weight != null)
				return false;
		} else if (!weight.equals(other.weight))
			return false;
		if (minValue == null) {
			if (other.minValue != null)
				return false;
		} else if (!minValue.equals(other.minValue))
			return false;
		if (maxValue == null) {
			if (other.maxValue != null)
				return false;
		} else if (!maxValue.equals(other.maxValue))
			return false;
		if (defaultValue == null) {
			if (other.defaultValue != null)
				return false;
		} else if (!defaultValue.equals(other.defaultValue))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}
	
	public double validateValue(double value) {
		if (value < minValue)
			return minValue;
		
		if (value > maxValue)
			return maxValue;

		return value;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	// In Class (for construction)
	public static class ParamDefinition_In {
		String name;
		String description;
		Double weight;
		Double minValue;
		Double maxValue;
		Double defaultValue;

		public ParamDefinition_In() {
			super();
		}

		public ParamDefinition_In(String name, String description, Double weight, Double minValue, Double maxValue, Double defaultValue) {
			this.name = name;
			this.description = description;
			this.weight = weight;
			this.minValue = minValue;
			this.maxValue = maxValue;
			this.defaultValue = defaultValue;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public Double getWeight() {
			return weight;
		}

		public void setWeight(Double weight) {
			this.weight = weight;
		}

		public Double getMinValue() {
			return minValue;
		}

		public void setMinValue(Double minValue) {
			this.minValue = minValue;
		}

		public Double getMaxValue() {
			return maxValue;
		}

		public void setMaxValue(Double maxValue) {
			this.maxValue = maxValue;
		}

		public Double getDefaultValue() {
			return defaultValue;
		}

		public void setDefaultValue(Double defaultValue) {
			this.defaultValue = defaultValue;
		}
	}

	// Out class for output
	public static class ParamDefinition_Out extends ParamDefinition_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public ParamDefinition_Out() {
			super();
		}
		
		public ParamDefinition_Out(String name, String description, Double weight, Double minValue, Double maxValue, 
				Double defaultValue, String createdOn, String modifiedOn, Long id) {
			super(name, description, weight, minValue, maxValue, defaultValue);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public ParamDefinition_Out toJSON() {
		return new ParamDefinition_Out(name, description, weight, minValue, maxValue, defaultValue, createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(ParamDefinition_In json) {
		this.name = json.name;
		this.description = json.description;
		this.weight = json.weight;
		this.minValue = json.minValue;
		this.maxValue = json.maxValue;
		this.defaultValue = json.defaultValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static ParamDefinition findObjectById(Long id) {
		return find(id);
	}
	
	public static List<ParamDefinition> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<ParamDefinition> qry = new CarIQSimpleQuery<ParamDefinition>("getAllObjects", entityManager(), ParamDefinition.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static ParamDefinition createOrGet(String name, String description, Double weight, 
			Double minValue, Double maxValue, Double defaultValue) {
		ParamDefinition paramDef = null;
		// Check if paramDefinition already exists
		paramDef = ParamDefinition.getByUniqueValues(name, weight, minValue, maxValue, defaultValue);
		if (null != paramDef) {
			// Update description if not present
			if (null == paramDef.description && null != description) {
				paramDef.setDescription(description);
				paramDef.merge();
			}
			return paramDef;
		}
		
		// Need to create a new paramDefinition
		paramDef = new ParamDefinition(name, description, weight, minValue, maxValue, defaultValue, new Date(), new Date());
		paramDef.persist();
		
		return paramDef;
	}
	
	// Get ParamDefs with same name
	public static List<ParamDefinition> getByName(String name) {
		CarIQSimpleQuery<ParamDefinition> qry = new CarIQSimpleQuery<ParamDefinition>("getByName", entityManager(), ParamDefinition.class);
		qry.addCondition("name", "=", name);

		return qry.getResultList();
	}
	
	// Get specific paramDef
	public static ParamDefinition getByUniqueValues(String name, Double weight, Double minValue, Double maxValue, Double defaultValue) {
		CarIQSimpleQuery<ParamDefinition> qry = new CarIQSimpleQuery<ParamDefinition>("getByUniqueValues", entityManager(), ParamDefinition.class);
		qry.addCondition("name", "=", name);
		qry.addCondition("weight", "=", weight);
		qry.addCondition("minValue", "=", minValue);
		qry.addCondition("maxValue", "=", maxValue);
		qry.addCondition("defaultValue", "=", defaultValue);

		return qry.getSingleResult();
	}


	public void updateParamDefinition(String name, String description, Double weight, 
			Double minValue, Double maxValue, Double defaultValue) {
		if (name != null)
			this.name = name;
		if (description != null)
			this.description = description;
		if (weight != null)
			this.weight = weight;
		if (minValue != null)
			this.minValue = minValue;
		if (maxValue != null)
			this.maxValue = maxValue;
		if (defaultValue != null)
			this.defaultValue = defaultValue;

		this.modifiedOn = new Date();
	}
}
