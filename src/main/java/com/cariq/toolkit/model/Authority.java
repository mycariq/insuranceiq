package com.cariq.toolkit.model;

import org.springframework.security.core.GrantedAuthority;

/**
 * The Class Authority.
 */
public class Authority implements GrantedAuthority {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The role. */
	private String role;

	/**
	 * Instantiates a new authority.
	 *
	 * @param role
	 *            the role
	 */
	public Authority(final String role) {
		this.role = role;
	}

	/**
	 * Gets the authority.
	 *
	 * @return the authority
	 */
	@Override
	public final String getAuthority() {
		return this.role;
	}
}
