package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;

/**
 * This class implements "ProxyObject" concept. We use proxy calls to obtain objects from "remote" services (e.g calls to CarIQ/W4 API layers). 
 * These objects can be retained in the current service/layer using their lighter version(just identification). 
 * These proxyobjects can be used to enhance functionality or to decorate in the current service.
 * 
 * @author amita
 *
 */

@Entity
@Configurable
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "objectId", "objectType", "domain"}))
public class ProxyObject extends PersistentObject<ProxyObject> implements JSONable<ProxyObject_In, ProxyObject_Out, IdJSON> {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new ProxyObject().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static ProxyObject find(Long id) {
		return entityManager().find(ProxyObject.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public ProxyObject findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */

	@NotNull
	@ManyToOne
	Domain domain;
	
	@NotNull
	Long objectId;
	
	@NotNull
	String objectType;
	
	String signature;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public ProxyObject() {
		super();
	}

	/**
	 * Parameterized constructor
	 * 
	 * @param domain
	 * @param objectId
	 * @param signature
	 * @param objectType
	 * @param createdOn
	 * @param modifiedOn
	 */
	public ProxyObject(Domain domain, Long objectId, String objectType, String signature, Date createdOn, Date modifiedOn) {
		super();
		this.domain = domain;
		this.objectId = objectId;
		this.objectType = objectType;
		this.signature = signature;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Getters Setters
	 */
	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "ProxyObject [id=" + id + ", version=" + version + ", domain=" + domain.getId() + 
				", objectId=" + objectId + ", signature=" + signature + ", objectType=" + objectType + 
				", createdOn=" + createdOn.toString() + ", modifiedOn=" + modifiedOn.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((domain == null) ? 0 : domain.getId().hashCode());
		result = prime * result + ((objectId == null) ? 0 : objectId.hashCode());
		result = prime * result + ((signature == null) ? 0 : signature.hashCode());
		result = prime * result + ((objectType == null) ? 0 : objectType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProxyObject other = (ProxyObject) obj;
		if (domain == null) {
			if (other.domain != null)
				return false;
		} else if (!domain.getId().equals(other.domain.getId()))
			return false;
		if (objectId == null) {
			if (other.objectId != null)
				return false;
		} else if (!objectId.equals(other.objectId))
			return false;
		if (signature == null) {
			if (other.signature != null)
				return false;
		} else if (!signature.equals(other.signature))
			return false;
		if (objectType == null) {
			if (other.objectType != null)
				return false;
		} else if (!objectType.equals(other.objectType))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	// In Class (for construction)
	public static class ProxyObject_In {
		String domainName;
		Long objectId;
		String signature;
		String objectType;

		public ProxyObject_In() {
			super();
		}

		/**
		 * @param domainId
		 * @param objectId
		 * @param objectType
		 * @param signature
		 */
		public ProxyObject_In(String domainName, Long objectId, String objectType, String signature) {
			this.domainName = domainName;
			this.objectId = objectId;
			this.objectType = objectType;
			this.signature = signature;
		}

		public String getDomainName() {
			return domainName;
		}

		public void setDomainName(String domainName) {
			this.domainName = domainName;
		}

		public Long getObjectId() {
			return objectId;
		}

		public void setObjectId(Long objectId) {
			this.objectId = objectId;
		}

		public String getObjectType() {
			return objectType;
		}

		public void setObjectType(String objectType) {
			this.objectType = objectType;
		}
		
		public String getSignature() {
			return signature;
		}

		public void setSignature(String signature) {
			this.signature = signature;
		}
	}

	// Out class for output
	public static class ProxyObject_Out extends ProxyObject_In {
		Long id;
		Domain_Out domainJSON;
		String createdOn;
		String modifiedOn;

		public ProxyObject_Out() {
			super();
		}
		
		/**
		 * 
		 * @param domainName
		 * @param objectId
		 * @param signature
		 * @param objectType
		 * @param createdOn
		 * @param modifiedOn
		 * @param id
		 * @param domainJSON
		 */
		public ProxyObject_Out(String domainName, Long objectId, String signature, String objectType, 
				String createdOn, String modifiedOn, Long id, Domain_Out domainJSON) {
			super(domainName, objectId, signature, objectType);
			this.domainJSON = domainJSON;
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Domain_Out getDomainJSON() {
			return domainJSON;
		}

		public void setDomainJSON(Domain_Out domainJSON) {
			this.domainJSON = domainJSON;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public ProxyObject_Out toJSON() {
		return new ProxyObject_Out(domain.getName(), objectId, signature, objectType, createdOn.toString(), 
				modifiedOn.toString(), id, domain.toJSON());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(ProxyObject_In json) {
		this.domain = json.domainName == null ? null : Domain.getByName(json.domainName);
		this.objectId = json.getObjectId();
		this.signature = json.getSignature();
		this.objectType = json.getObjectType();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static ProxyObject findObjectById(Long id) {
		return find(id);
	}
	
	public static List<ProxyObject> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<ProxyObject> qry = new CarIQSimpleQuery<ProxyObject>("getAllObjects", entityManager(), ProxyObject.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static ProxyObject createOrGet(Domain domain, Long objectId, String objectType, String signature) {
		ProxyObject proxy = null;
		
		// Check if proxy-object exists
		proxy = ProxyObject.getByDomainObjectIdType(domain, objectId, objectType);
		if (null != proxy) {
			// Update signature if required
			if (null != proxy.signature && null != signature) {
				proxy.setSignature(signature);
				proxy.merge();
			}
			return proxy;
		}
		
		// Need to create new proxy-object
		proxy = new ProxyObject(domain, objectId, objectType, signature, new Date(), new Date());
		proxy.persist();
		
		return proxy;
	}
	
	public static ProxyObject getByDomainObjectIdType(Domain domain, Long objectId, String objectType) {
		CarIQSimpleQuery<ProxyObject> qry = new CarIQSimpleQuery<ProxyObject>("getByName", entityManager(), ProxyObject.class);
		qry.addCondition("domain", "=", domain);
		qry.addCondition("objectId", "=", objectId);
		qry.addCondition("objectType", "=", objectType);

		return qry.getSingleResult();
	}
	
	public static ProxyObject getByDomainSignatureObjectType(Domain domain, String signature, String objectType) {
		CarIQSimpleQuery<ProxyObject> qry = new CarIQSimpleQuery<ProxyObject>("getByName", entityManager(), ProxyObject.class);
		qry.addCondition("domain", "=", domain);
		qry.addCondition("signature", "=", signature);
		qry.addCondition("objectType", "=", objectType);

		return qry.getSingleResult();
	}
}
