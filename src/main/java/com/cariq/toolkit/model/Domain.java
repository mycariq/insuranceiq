package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.Domain.Domain_In;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;

/**
 * This class implements "domain" concept in the producer context. Domain
 * restricts the access to its data for specific users.
 * 
 * @author amita
 *
 */

@Entity
@Configurable
public class Domain extends PersistentObject<Domain> implements JSONable<Domain_In, Domain_Out, IdJSON> {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new Domain().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static Domain find(Long id) {
		return entityManager().find(Domain.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public Domain findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull(message = "@NotNull domain Name")
	@Column(unique = true)
	String name;

	@NotNull(message = "@NotNull domain URL")
	@Column(unique = true)
	String url;

	@NotNull(message = "@NotNull domain authString")
	String authString;

	@NotNull(message = "@NotNull domain authType")
	String authType;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public Domain() {
		super();
	}

	/**
	 * Parameterized constructor
	 * 
	 * @param name
	 * @param url
	 * @param authString
	 * @param authType
	 * @param createdOn
	 * @param modifiedOn
	 */
	public Domain(String name, String url, String authString, String authType, Date createdOn, Date modifiedOn) {
		super();
		this.name = name;
		this.url = url;
		this.authString = authString;
		this.authType = authType;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Getters Setters
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAuthString() {
		return authString;
	}

	public void setAuthString(String authString) {
		this.authString = authString;
	}

	public String getAuthType() {
		return authType;
	}

	public void setAuthType(String authType) {
		this.authType = authType;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "Domain [id=" + id + ", version=" + version + ", name=" + name + ", url=" + url + ", authString="
				+ authString + ", authType=" + authType + ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		result = prime * result + ((authString == null) ? 0 : authString.hashCode());
		result = prime * result + ((authType == null) ? 0 : authType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Domain other = (Domain) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (authString == null) {
			if (other.authString != null)
				return false;
		} else if (!authString.equals(other.authString))
			return false;
		if (authType == null) {
			if (other.authType != null)
				return false;
		} else if (!authType.equals(other.authType))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	

	// Special Input class while creating domain. Username password should be accepted.
	public static class CreateDomain_In {
		String username;
		String password;
		String name;
		String url;
		
		public CreateDomain_In(String username, String password, String name, String url) {
			super();
			this.username = username;
			this.password = password;
			this.name = name;
			this.url = url;
		}

		public CreateDomain_In() {
			super();
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}
	
	}
	
	// Special input class while updating domain. Only name, url can be updated.
	public static class UpdateDomain_In {
		String name;
		String url;
		
		public UpdateDomain_In(String name, String url) {
			super();
			this.name = name;
			this.url = url;
		}

		public UpdateDomain_In() {
			super();
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}
	
	// In Class (for construction)
	public static class Domain_In {
		String name;
		String url;
		String authString;
		String authType;

		public Domain_In() {
			super();
		}

		/**
		 * @param name
		 * @param url
		 * @param authString
		 * @param authType
		 */
		public Domain_In(String name, String url, String authString, String authType) {
			this.name = name;
			this.url = url;
			this.authString = authString;
			this.authType = authType;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getAuthString() {
			return authString;
		}

		public void setAuthString(String authString) {
			this.authString = authString;
		}

		public String getAuthType() {
			return authType;
		}

		public void setAuthType(String authType) {
			this.authType = authType;
		}
	}

	// Out class for output
	public static class Domain_Out extends Domain_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public Domain_Out() {
			super();
		}
		
		/**
		 * @param name
		 * @param url
		 * @param authString
		 * @param authType
		 * @param createdOn
		 * @param modifiedOn
		 */
		public Domain_Out(String name, String url, String authString, String authType, String createdOn, String modifiedOn,
				Long id) {
			super(name, url, authString, authType);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public Domain_Out toJSON() {
		// authstring and authtype should not be out to JSON
		return new Domain_Out(name, url, null, null, createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(Domain_In json) {
		this.name = json.name;
		this.url = json.url;
		this.authString = json.authString;
		this.authType = json.authType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static Domain findObjectById(Long id) {
		return find(id);
	}
	
	public static List<Domain> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<Domain> qry = new CarIQSimpleQuery<Domain>("getAllObjects", entityManager(), Domain.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static Domain getByName(String name) {
		CarIQSimpleQuery<Domain> qry = new CarIQSimpleQuery<Domain>("getByName", entityManager(), Domain.class);
		qry.addCondition("name", "=", name);

		if (qry.getResultList().isEmpty())
			return null;
		return qry.getSingleResult();
	}

	public void updateDomain(String name, String url) {
		if (name != null)
			this.name = name;
		if (url != null)
			this.url = url;

		this.modifiedOn = new Date();
	}
}
