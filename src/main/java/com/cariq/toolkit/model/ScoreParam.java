package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.ScoreDefinition.ScoreDefinition_Out;
import com.cariq.toolkit.model.ScoreParam.ScoreParam_In;
import com.cariq.toolkit.model.ScoreParam.ScoreParam_Out;
import com.cariq.toolkit.model.ParamDefinition.ParamDefinition_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;

/**
 * This class implements "Score Parameter association" concept. 
 * We need to come up with different scores by processing the stored information. 
 * ScoringEngine can instantiate scoring-model using ScoreDefinition and associated ParamDefinitions. 
 * Scorer can actually evaluate the score by processing parameter values.
 * ScoreDefinition(name, minvalue, maxvalue)
 * ParamDefinition(name, weight, minvalue, maxvalue, defaultvalue)
 * ScoreDefinitions can be associated with multiple ParamDefinitions to define the scoring-model (many-to-many)
 * 
 * @author amita
 *
 */

@Entity
@Configurable
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"score_def", "param_def"}))
public class ScoreParam extends PersistentObject<ScoreParam> implements JSONable<ScoreParam_In, ScoreParam_Out, IdJSON>{

	// SECTION 1 -Code for Persistence
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    
    @Version
    Integer version;
    
    @Override
	public Long getId() {
		return id;
	}
    @Override
	public void setId(Long id) {
		this.id = id;
	}
    @Override
	public Integer getVersion() {
		return version;
	}
    @Override
	public void setVersion(Integer version) {
		this.version = version;
	}
	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new ScoreParam().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null) this.entityManager = entityManager();
		return entityManager;
	}
	
    /**
	 * @param id
	 * @return
	 */
	protected static ScoreParam find(Long id) {
		return entityManager().find(ScoreParam.class, id);
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public ScoreParam findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull
	@ManyToOne
	ScoreDefinition scoreDef;
	
	@NotNull
	@ManyToOne
	ParamDefinition paramDef;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public ScoreParam() {
		super();
	}
	
	public ScoreParam(ScoreDefinition scoreDef, ParamDefinition paramDef, Date createdOn, Date modifiedOn) {
		super();
		this.scoreDef = scoreDef;
		this.paramDef = paramDef;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	public ScoreDefinition getScoreDef() {
		return scoreDef;
	}
	public void setScoreDef(ScoreDefinition scoreDef) {
		this.scoreDef = scoreDef;
	}
	public ParamDefinition getParamDef() {
		return paramDef;
	}
	public void setParamDef(ParamDefinition paramDef) {
		this.paramDef = paramDef;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all Model classes
	@Override
	public String toString() {
		return "ScoreParam [id=" + id + ", version=" + version + ", scoreDefId=" + scoreDef.getId() + ", paramDefId=" + paramDef.getId()
				+ ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((scoreDef == null) ? 0 : scoreDef.getId().hashCode());
		result = prime * result + ((paramDef == null) ? 0 : paramDef.getId().hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScoreParam other = (ScoreParam) obj;
		if (scoreDef == null) {
			if (other.scoreDef != null)
				return false;
		} else if (!scoreDef.getId().equals(other.scoreDef.getId()))
			return false;
		if (paramDef == null) {
			if (other.paramDef != null)
				return false;
		} else if (!paramDef.getId().equals(other.paramDef.getId()))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}
	

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API interaction
	// In Class (for construction)
	
	// Special JSON for associating param definition to score definition
	public static class ScoreParamDefinition_In {
		String scoreDefName;
		String paramDefName;
		Double weight, minValue, maxValue, defaultValue;
		
		public ScoreParamDefinition_In () {
			super();
		}

		public ScoreParamDefinition_In(String scoreDefName, String paramDefName, Double weight, Double minValue,
				Double maxValue, Double defaultValue) {
			super();
			this.scoreDefName = scoreDefName;
			this.paramDefName = paramDefName;
			this.weight = weight;
			this.minValue = minValue;
			this.maxValue = maxValue;
			this.defaultValue = defaultValue;
		}

		public String getScoreDefName() {
			return scoreDefName;
		}

		public void setScoreDefName(String scoreDefName) {
			this.scoreDefName = scoreDefName;
		}

		public String getParamDefName() {
			return paramDefName;
		}

		public void setParamDefName(String paramDefName) {
			this.paramDefName = paramDefName;
		}

		public Double getWeight() {
			return weight;
		}

		public void setWeight(Double weight) {
			this.weight = weight;
		}

		public Double getMinValue() {
			return minValue;
		}

		public void setMinValue(Double minValue) {
			this.minValue = minValue;
		}

		public Double getMaxValue() {
			return maxValue;
		}

		public void setMaxValue(Double maxValue) {
			this.maxValue = maxValue;
		}

		public Double getDefaultValue() {
			return defaultValue;
		}

		public void setDefaultValue(Double defaultValue) {
			this.defaultValue = defaultValue;
		}
	}
	
	public static class ScoreParam_In {
		 String scoreDefName; // ScoreDef name is unique and better user input
		 Long paramDefId;
		 
		public ScoreParam_In () {
			super();
		}
		
		/**
		 * @param scoreDef
		 * @param paramDef
		 * @param createdOn
		 * @param modifiedOn
		 */
		public ScoreParam_In(String scoreDefName, Long paramDefId) {
			this.scoreDefName = scoreDefName;
			this.paramDefId = paramDefId;
		}
		
		public String getScoreDefName() {
			return scoreDefName;
		}

		public void setScoreDefName(String scoreDefName) {
			this.scoreDefName = scoreDefName;
		}

		public Long getParamDefId() {
			return paramDefId;
		}

		public void setParamDefId(Long paramDefId) {
			this.paramDefId = paramDefId;
		}	
	}
	
	// Out class for output
	public static class ScoreParam_Out extends ScoreParam_In {
		Long id;
		ScoreDefinition_Out scoreDefJSON;
		ParamDefinition_Out paramDefJSON;
		String createdOn;
		String modifiedOn;
		
		public ScoreParam_Out () {
			super();
		}
		
		/**
		 * 
		 * @param scoreDefId
		 * @param paramDefId
		 * @param createdOn
		 * @param modifiedOn
		 * @param id
		 * @param scoreDefJSON
		 * @param paramDefJSON
		 */
		public ScoreParam_Out(String scoreDefName, Long paramDefId, String createdOn, String modifiedOn, Long id, 
					ScoreDefinition_Out scoreDefJSON, ParamDefinition_Out paramDefJSON) {
			super(scoreDefName, paramDefId);
			this.id = id;
			this.scoreDefJSON = scoreDefJSON;
			this.paramDefJSON = paramDefJSON;
			this.modifiedOn = modifiedOn;
			this.createdOn = createdOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public ScoreDefinition_Out getScoreDefJSON() {
			return scoreDefJSON;
		}

		public void setScoreDefJSON(ScoreDefinition_Out scoreDefJSON) {
			this.scoreDefJSON = scoreDefJSON;
		}

		public ParamDefinition_Out getParamDefJSON() {
			return paramDefJSON;
		}

		public void setParamDefJSON(ParamDefinition_Out paramDefJSON) {
			this.paramDefJSON = paramDefJSON;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}	
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public ScoreParam_Out toJSON() {
		return new ScoreParam_Out(scoreDef.getName(), paramDef.getId(), createdOn.toString(), 
				modifiedOn.toString(), id, scoreDef.toJSON(), paramDef.toJSON());
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(ScoreParam_In json) {
		this.scoreDef = json.getScoreDefName() == null ? null : ScoreDefinition.getByName(json.getScoreDefName());
		this.paramDef = json.getParamDefId() == null ? null : ParamDefinition.find(json.getParamDefId());         	
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}
		
	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static ScoreParam findObjectById(Long id) {
		return find(id);
	}
	
	public static List<ScoreParam> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<ScoreParam> qry = new CarIQSimpleQuery<ScoreParam>("getAllObjects", entityManager(), ScoreParam.class);
		qry.setPaging(pageNo, pageSize);
		
		return qry.getResultList();
	}
	
	/**
	 * @param scoreDef
	 * @return
	 */
	public static List<ScoreParam> getAllObjectsByScoreDefinition(ScoreDefinition scoreDef) {
		CarIQSimpleQuery<ScoreParam> qry = new CarIQSimpleQuery<ScoreParam>("getAllObjectsByScoreDefinition", entityManager(), ScoreParam.class);
		qry.addCondition("scoreDef", "=", scoreDef);
		
		return qry.getResultList();
	}
	
	/**
	 * @param paramDef
	 * @return
	 */
	public static List<ScoreParam> getAllObjectsByParamDefinition(ParamDefinition paramDef) {
		CarIQSimpleQuery<ScoreParam> qry = new CarIQSimpleQuery<ScoreParam>("getAllObjectsByParamDefinition", entityManager(), ScoreParam.class);
		qry.addCondition("paramDef", "=", paramDef);
		
		return qry.getResultList();
	}
	
	public static ScoreParam getObjectByScoreDefParamDef(ScoreDefinition scoreDef, ParamDefinition paramDef) {
		CarIQSimpleQuery<ScoreParam> qry = new CarIQSimpleQuery<ScoreParam>("getObjectByScoreDefinitionUParamDefinition", entityManager(), ScoreParam.class);
		qry.addCondition("scoreDef", "=", scoreDef);
		qry.addCondition("paramDef", "=", paramDef);
		
		return qry.getSingleResult();
	}
	
}

