package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;


import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.Subscription.Subscription_In;
import com.cariq.toolkit.model.Subscription.Subscription_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;

/**
 * This class implements "subscription" in the producer context. Subscription is approved contract between CARIQ and producer.
 * Multiple users can be created for any subscription. Subscriptions and underneath users are provided access to specific domains. 
 * Specific rules/behavior can be defined for any subscription.
 * @author amita
 *
 */

@Entity
@Configurable
public class Subscription extends PersistentObject<Subscription> implements JSONable<Subscription_In, Subscription_Out, IdJSON>{

	// ---------------------------------------------
	// SECTION 1 - Code for Persistence
	// ---------------------------------------------
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    
    @Version
    Integer version;
    
    @Override
	public Long getId() {
		return id;
	}
    @Override
	public void setId(Long id) {
		this.id = id;
	}
    @Override
	public Integer getVersion() {
		return version;
	}
    @Override
	public void setVersion(Integer version) {
		this.version = version;
	}
	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new Subscription().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null) this.entityManager = entityManager();
		return entityManager;
	}
	
    /**
	 * @param id
	 * @return
	 */
	protected static Subscription find(Long id) {
		return entityManager().find(Subscription.class, id);
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public Subscription findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */

	@NotNull(message = "@NotNull Subscription Name")
	@Column(unique = true)
	@Size(max = 50, min = 2)
	String name;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date endDate;
	
	@Size(max = 4000, message = "Address, contact numbers, emails, GST/PAN details etc.")
	String profile;
	
	@Size(max = 4000, message = "Terms/SLAs etc.")
	String contractInfo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public Subscription() {
		super();
	}
	

	public Subscription(String name, Date startDate, Date endDate, String profile, 
			String contractInfo, Date createdOn, Date modifiedOn) {
		super();
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
		this.profile = profile;
		this.contractInfo = contractInfo;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public String getContractInfo() {
		return contractInfo;
	}
	public void setContractInfo(String contractInfo) {
		this.contractInfo = contractInfo;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	
	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------


	// Implementation of toString and hashCode and equals are advisable for all Model classes
	@Override
	public String toString() {
		return "Subscription [id=" + id + ", version=" + version + ", name=" + name 
				+ ", startDate=" + startDate + ", endDate=" + endDate
				+ ", profile=" + profile + ", contractInfo=" + contractInfo 
				+ ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((profile == null) ? 0 : profile.hashCode());
		result = prime * result + ((contractInfo == null) ? 0 : contractInfo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Subscription other = (Subscription) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (startDate== null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (profile == null) {
			if (other.profile != null)
				return false;
		} else if (!profile.equals(other.profile))
			return false;
		if (contractInfo == null) {
			if (other.contractInfo != null)
				return false;
		} else if (!contractInfo.equals(other.contractInfo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API interaction
	
	// In Class (for construction)
	public static class Subscription_In {
		String name;
		String startDate;
		String endDate;
		String profile;
		String contractInfo;

		public Subscription_In() {
			super();
		}

		public Subscription_In(String name, String startDate, String endDate, String profile, String contractInfo) {
			super();
			this.name = name;
			this.startDate = startDate;
			this.endDate = endDate;
			this.profile = profile;
			this.contractInfo = contractInfo;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getStartDate() {
			return startDate;
		}

		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}

		public String getEndDate() {
			return endDate;
		}

		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}

		public String getProfile() {
			return profile;
		}

		public void setProfile(String profile) {
			this.profile = profile;
		}

		public String getContractInfo() {
			return contractInfo;
		}

		public void setContractInfo(String contractInfo) {
			this.contractInfo = contractInfo;
		}
		
	}
	
	// Out class for output
	public static class Subscription_Out extends Subscription_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public Subscription_Out() {
			super();
		}

		public Subscription_Out(String name, String startDate, String endDate, String profile, String contractInfo, 
				Long id, String createdOn, String modifiedOn) {
			super(name, startDate, endDate, profile, contractInfo);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public Subscription_Out toJSON() {
		return new Subscription_Out(name, startDate.toString(), endDate.toString(), profile, 
				contractInfo, id, createdOn.toString(), modifiedOn.toString());
	}
	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(Subscription_In json) {
		this.name = json.name;
		this.startDate = Utils.parseDateFormat(json.startDate);
		this.endDate = Utils.parseDateFormat(json.endDate);
		this.profile = json.profile;
		this.contractInfo = json.contractInfo;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}
		
	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static Subscription findObjectById(Long id) {
		return find(id);
	}
	
	public static List<Subscription> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<Subscription> qry = new CarIQSimpleQuery<Subscription>("getAllObjects", entityManager(), Subscription.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}
	
	public static Subscription getByName(String name) {
		CarIQSimpleQuery<Subscription> qry = new CarIQSimpleQuery<Subscription>("getBySubscriptionByName", entityManager(), Subscription.class);
		qry.addCondition("name", "=", name);

		return qry.getSingleResult();
	}
	
	public static List<Subscription> getByStartDate(Date startDate) {
		CarIQSimpleQuery<Subscription> qry = new CarIQSimpleQuery<Subscription>("getBySubscriptionsByStartDate", entityManager(), Subscription.class);
		qry.addCondition("startDate", "=", startDate);
		
		if (qry.getResultList().isEmpty())
			return null;
		return qry.getResultList();
	}
	
	public static List<Subscription> getByEndDate(Date endDate) {
		CarIQSimpleQuery<Subscription> qry = new CarIQSimpleQuery<Subscription>("getBySubscriptionsByEndDate", entityManager(), Subscription.class);
		qry.addCondition("endDate", "=", endDate);
		
		if (qry.getResultList().isEmpty())
			return null;
		return qry.getResultList();
	}
	
	public void updateSubscription(String name, String startDate, String endDate, String profile, String contractInfo) {
		if (null != name)
			this.name = name;
		if (null != startDate)
			this.startDate = Utils.parseDateFormat(startDate);
		if (null != endDate)
			this.endDate = Utils.parseDateFormat(endDate);
		if (null != profile)
			this.profile = profile;
		if (null != contractInfo)
			this.contractInfo = contractInfo;
		this.modifiedOn = new Date();
	}
}

