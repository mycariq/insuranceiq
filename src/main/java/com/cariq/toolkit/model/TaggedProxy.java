package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.Tag.Tag_Out;
import com.cariq.toolkit.model.TaggedProxy.TaggedProxy_In;
import com.cariq.toolkit.model.TaggedProxy.TaggedProxy_Out;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;

/**
 * This class implements "tag-proxyObject" association. Any ProxyObject can be tagged with any tag (many-to-many relation).
 * @author amita
 *
 */

@Entity
@Configurable
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "tag", "proxy"}))
public class TaggedProxy extends PersistentObject<TaggedProxy> implements JSONable<TaggedProxy_In, TaggedProxy_Out, IdJSON>{

	// SECTION 1 -Code for Persistence
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    
    @Version
    Integer version;
    
    @Override
	public Long getId() {
		return id;
	}
    @Override
	public void setId(Long id) {
		this.id = id;
	}
    @Override
	public Integer getVersion() {
		return version;
	}
    @Override
	public void setVersion(Integer version) {
		this.version = version;
	}
	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new TaggedProxy().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null) this.entityManager = entityManager();
		return entityManager;
	}
	
    /**
	 * @param id
	 * @return
	 */
	protected static TaggedProxy find(Long id) {
		return entityManager().find(TaggedProxy.class, id);
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public TaggedProxy findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull
	@ManyToOne
	Tag tag;
	
	@NotNull
	@ManyToOne
	ProxyObject proxy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public TaggedProxy() {
		super();
	}
	
	/**
	 * Parameterized constructor
	 * @param tag
	 * @param proxy
	 * @param createdOn
	 * @param modifiedOn
	 */
	public TaggedProxy(Tag tag, ProxyObject proxy, Date createdOn, Date modifiedOn) {
		super();
		this.tag = tag;
		this.proxy = proxy;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Getters Setters
	 */
	public Tag getTag() {
		return tag;
	}
	public void setTag(Tag tag) {
		this.tag = tag;
	}
	public ProxyObject getProxyObject() {
		return proxy;
	}
	public void setProxyObject(ProxyObject proxy) {
		this.proxy = proxy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}


	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all Model classes
	@Override
	public String toString() {
		return "TaggedProxy [id=" + id + ", version=" + version + ", tagId=" + tag.getId() + ", proxyId=" + proxy.getId()
				+ ", createdOn=" + createdOn.toString() + ", modifiedOn=" + modifiedOn.toString() + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((tag == null) ? 0 : tag.getId().hashCode());
		result = prime * result + ((proxy == null) ? 0 : proxy.getId().hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaggedProxy other = (TaggedProxy) obj;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.getId().equals(other.tag.getId()))
			return false;
		if (proxy == null) {
			if (other.proxy != null)
				return false;
		} else if (!proxy.getId().equals(other.proxy.getId()))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}
	

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API interaction
	// In Class (for construction)
	
	// This is special input JSON to tag/untag proxy objects
	public static class TagAndProxyDetails_In {
		String tagname; //Tagname is unique and easily available as input while creating taggedProxy.
		String domainName; //Domain name is unique and easily available as input.
		Long objectId;
		String objectType;
		String signature;
		
		 public TagAndProxyDetails_In() {
			 super();
		 }
		 
		public TagAndProxyDetails_In(String tagname, String domainName, Long objectId, String objectType,
				String signature) {
			super();
			this.tagname = tagname;
			this.domainName = domainName;
			this.objectId = objectId;
			this.objectType = objectType;
			this.signature = signature;
		}

		public String getTagname() {
			return tagname;
		}

		public void setTagname(String tagname) {
			this.tagname = tagname;
		}

		public String getDomainName() {
			return domainName;
		}

		public void setDomainName(String domainName) {
			this.domainName = domainName;
		}

		public Long getObjectId() {
			return objectId;
		}

		public void setObjectId(Long objectId) {
			this.objectId = objectId;
		}

		public String getObjectType() {
			return objectType;
		}

		public void setObjectType(String objectType) {
			this.objectType = objectType;
		}

		public String getSignature() {
			return signature;
		}

		public void setSignature(String signature) {
			this.signature = signature;
		}
	}
	
	public static class TaggedProxy_In {
		 String tagname; //Tagname is unique and easily available as input while creating taggedProxy.
		 Long proxyId;
		 
		 public TaggedProxy_In() {
			 super();
		 }
		 
		/**
		 * @param tag
		 * @param proxy
		 * @param createdOn
		 * @param modifiedOn
		 */
		public TaggedProxy_In(String tagname, Long proxyId) {
			this.tagname = tagname;
			this.proxyId = proxyId;
		}
		
		public String getTagname() {
			return tagname;
		}

		public void setTagname(String tagname) {
			this.tagname = tagname;
		}

		public Long getProxyId() {
			return proxyId;
		}

		public void setProxyId(Long proxyId) {
			this.proxyId = proxyId;
		}	
	}
	
	// Out class for output
	public static class TaggedProxy_Out extends TaggedProxy_In {
		Long id;
		Tag_Out tagJSON;
		ProxyObject_Out proxyJSON;
		String createdOn;
		String modifiedOn;
		
		 public TaggedProxy_Out() {
			 super();
		 }
		 
		/**
		 * @param tagId
		 * @param proxyId
		 * @param createdOn
		 * @param modifiedOn
		 */
		public TaggedProxy_Out(String tagname, Long proxyId, String createdOn, String modifiedOn, Long id, 
					Tag_Out tagJSON, ProxyObject_Out proxyJSON) {
			super(tagname, proxyId);
			this.id = id;
			this.tagJSON = tagJSON;
			this.proxyJSON = proxyJSON;
			this.modifiedOn = modifiedOn;
			this.createdOn = createdOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}
		
		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public TaggedProxy_Out toJSON() {
		return new TaggedProxy_Out(tag.getName(), proxy.getId(), createdOn.toString(), modifiedOn.toString(), id, tag.toJSON(), proxy.toJSON());
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(TaggedProxy_In json) {
		this.tag = json.getTagname() == null ? null : Tag.getByName(json.getTagname());
		this.proxy = json.getProxyId() == null ? null : ProxyObject.find(json.getProxyId());         	
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}
		
	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static TaggedProxy findObjectById(Long id) {
		return find(id);
	}
	
	public static List<TaggedProxy> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<TaggedProxy> qry = new CarIQSimpleQuery<TaggedProxy>("getAllObjects", entityManager(), TaggedProxy.class);
		qry.setPaging(pageNo, pageSize);
		
		return qry.getResultList();
	}
	
	/**
	 * @param tag
	 * @return
	 */
	public static List<TaggedProxy> getAllObjectsByTag(Tag tag) {
		CarIQSimpleQuery<TaggedProxy> qry = new CarIQSimpleQuery<TaggedProxy>("getAllObjectsByTag", entityManager(), TaggedProxy.class);
		qry.addCondition("tag", "=", tag);
		
		return qry.getResultList();
	}
	
	/**
	 * @param proxy
	 * @return
	 */
	public static List<TaggedProxy> getAllObjectsByProxy(ProxyObject proxy) {
		CarIQSimpleQuery<TaggedProxy> qry = new CarIQSimpleQuery<TaggedProxy>("getAllObjectsByProxy", entityManager(), TaggedProxy.class);
		qry.addCondition("proxy", "=", proxy);
		
		return qry.getResultList();
	}
	
	public static TaggedProxy getObjectByTagProxy(Tag tag, ProxyObject proxy) {
		CarIQSimpleQuery<TaggedProxy> qry = new CarIQSimpleQuery<TaggedProxy>("getObjectByTagProxy", entityManager(), TaggedProxy.class);
		qry.addCondition("tag", "=", tag);
		qry.addCondition("proxy", "=", proxy);
		
		return qry.getSingleResult();
	}
	
}