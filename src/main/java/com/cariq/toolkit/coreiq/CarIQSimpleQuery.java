package com.cariq.toolkit.coreiq;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Simple Query mechanism using Entity Manager by specifying Where clause and
 * order
 * 
 * @author Abhijit
 *
 * @param <T>
 */
public class CarIQSimpleQuery<T> {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIQSimpleQuery");
	String name = "SimpleQuery";

	static class Parameter {
		String key;
		Object value;

		public Parameter(String key, Object value) {
			super();
			this.key = key;
			this.value = value;
		}
	}

	String condition = "";
	String orderBy = "";
	Class<?> objectType;
	Class<?> returnType;
	List<Parameter> parameters = new ArrayList<Parameter>();
	EntityManager entityManager;
	int paramId;
	int rowNum = -1, pageSize = -1;

	public CarIQSimpleQuery(String name, EntityManager entityManager, Class<T> clazz) {
		this(name, entityManager, clazz, clazz);
	}

	public CarIQSimpleQuery(String name, EntityManager entityManager, Class<?> objectType, Class<T> returnType) {
		this.name = name;
		this.objectType = objectType;
		this.returnType = returnType;
		this.entityManager = entityManager;
	}

	
	public Class<?> getObjectType() {
		return objectType;
	}

	public Class<?> getReturnType() {
		return returnType;
	}

	public CarIQSimpleQuery<T> addCondition(String parameter, String booleanOperation, Object value) {
		if (condition.isEmpty())
			condition = " where ";
		else
			condition = condition + " and ";

		String paramName = parameter + paramId++;
		condition = condition + "obj." + parameter + " " + booleanOperation + " :" + paramName;

		parameters.add(new Parameter(paramName, value));
		// logger.debug("Query Condition: " + parameter + " " + booleanOperation
		// + " " + value.toString());
		return this;
	}

	public CarIQSimpleQuery<T> addNotNullCondition(String parameter) {
		if (condition.isEmpty())
			condition = " where ";
		else
			condition = condition + " and ";

		condition = condition + "obj." + parameter + " is not null ";

		return this;
	}

	
	public void orderBy(String parameter, String order) {
		if (orderBy.isEmpty())
			orderBy = "obj." + parameter + " " + order;
		else
			orderBy = ", obj." + parameter + " " + order;
	}

	public void setPaging(int pageNo, int pageSize) {
		this.rowNum = Utils.validatePagination(pageSize, pageNo);
		this.pageSize = pageSize;
	}

	protected TypedQuery<T> getQuery() {
		TypedQuery<T> typedQuery = (TypedQuery<T>) entityManager.createQuery(constructQueryString(), returnType);

		for (Parameter parameter : parameters) {
			typedQuery.setParameter(parameter.key, parameter.value);
		}

		if (rowNum >= 0)
			typedQuery.setFirstResult(this.rowNum);
		if (pageSize >= 1)
			typedQuery.setMaxResults(this.pageSize);

		return typedQuery;
	}

	// Select the Object - override in the Aggregation query
	protected String getSelectClause() {
		return " obj ";
	}

	private String constructQueryString() {
		// build select clause polymorphically
		StringBuilder builder = new StringBuilder(
				"select " + getSelectClause() + " from " + objectType.getSimpleName() + " obj ");

		if (!condition.isEmpty())
			builder.append(condition);

		if (!orderBy.isEmpty())
			builder.append(" order by ").append(orderBy);

		String retval = builder.toString();
		System.out.println("[CarIQSimpleQuery - " + name + "] " + retval);
		return retval;
	}

	public CarIQSimpleQuery<T> addRawCondition(String parameter, String rawCondition) {
		if (condition.isEmpty())
			condition = " where ";
		else
			condition = condition + " and ";

		condition = condition + "obj" + "." + parameter + " " + rawCondition;

		return this;
	}

	public CarIQSimpleQuery<T> addRawCondition(String rawCondition) {
		if (condition.isEmpty())
			condition = " where ";
		else
			condition = condition + " and ";

		condition = condition + rawCondition;

		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return constructQueryString();
		// StringBuilder bldr = new StringBuilder(constructQueryString());
		// bldr.append("; parameters: ");
		// for (Parameter parameter : parameters) {
		// bldr.append(parameter.key).append(" = ").append(parameter.value);
		// }
		//
		// return bldr.toString();
	}

	public T getSingleResult() {
		try (ProfilePoint _CarIQSimpleQuery_getSingleResult = ProfilePoint.profileAction("ProfAction_CarIQSimpleQuery_getSingleResult-" + name)) {
			TypedQuery<T> q = getQuery();
			return getSingleResult(q, "Executing SimpleQuery");
		}
	}
	
	public List<T> getResultList() {
		try (ProfilePoint _CarIQSimpleQuery_getResultList = ProfilePoint.profileAction("ProfAction_CarIQSimpleQuery_getResultList-" + name)) {
			TypedQuery<T> q = getQuery();
			return q.getResultList();
		}
	}
	
	public static <T> T getSingleResult(TypedQuery<T> query, String description) {
		 List<T> objects = query.getResultList();
		 if (objects.isEmpty())
			return null;
		 
		if (objects.size() > 1)
		 logger.warn("Expected single results, but multiple found while  " + description);
		
		 return objects.get(0);
	}

	public CarIQSimpleQuery<T> addNullCondition(String parameter) {
		if (condition.isEmpty())
			condition = " where ";
		else
			condition = condition + " and ";

		condition = condition + "obj." + parameter + " is null ";

		return this;
	}


}
