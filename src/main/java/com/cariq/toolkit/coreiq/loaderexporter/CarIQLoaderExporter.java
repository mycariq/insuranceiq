/**
 * 
 */
package com.cariq.toolkit.coreiq.loaderexporter;

import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.coreiq.loader.CarIQLoader;

/**
 * @author hrishi
 *
 */
public interface CarIQLoaderExporter extends CarIQLoader, CarIQExporter {

}
