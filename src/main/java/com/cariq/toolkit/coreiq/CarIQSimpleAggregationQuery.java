package com.cariq.toolkit.coreiq;

import javax.persistence.EntityManager;

public class CarIQSimpleAggregationQuery<T> extends CarIQSimpleQuery<T> {
	public static enum AggregationType {
		Count,
		Max,
		Min,
		Sum,
		Average,
		Stddev,
		Raw,
		Unknown
	}
	
	AggregationType aggregationType = AggregationType.Unknown;
	String aggregationParameter = null;
	String rawSelectQuery = null; //only if raw selecting is asked

	// Select the Object - override in the Aggregation query based on Aggregation Type
	protected String getSelectClause() {
		switch (aggregationType) {
		case Count:
			return aggregationParameter == null ? "COUNT(*)" : "COUNT(" + aggregationParameter + ")";
		case Max:
			return "MAX(" + aggregationParameter + ")";
		case Min:
			return "MIN(" + aggregationParameter + ")";
		case Sum:
			return "SUM(" + aggregationParameter + ")";
		case Average:
			return "AVG(" + aggregationParameter + ")";
		case Stddev:
			return "STDDEV(" + aggregationParameter + ")";
		case Raw:
			return rawSelectQuery;

		default:
			break;
		}
		
		return null;
	}
	
	public CarIQSimpleAggregationQuery(String name, EntityManager entityManager, Class<?> objectType, Class<T> returnType, AggregationType type, String aggregationParameter) {
		super(name, entityManager, objectType, returnType);
		this.aggregationParameter = aggregationParameter;
		aggregationType = type;
		
		if (aggregationType.equals(AggregationType.Raw))
			rawSelectQuery = aggregationParameter;
	}
}
