/**
 * 
 */
package com.cariq.toolkit.coreiq.model;

import javax.persistence.EntityManager;

import org.springframework.transaction.annotation.Transactional;

/**
 * Base class of all models generated without Roo.
 * Derive from this and implement the 5 sections to get it rolling
 * @author hrishi
 *
 */

public abstract class PersistentObject <T> {
	public abstract Long getId();
	public abstract void setId(Long id);
	public abstract Integer getVersion();
	public abstract void setVersion(Integer version);
	public abstract T findById(Long id);
	public abstract EntityManager getEntityManager();
	
	
    @Transactional
    public void persist() {
        getEntityManager().persist(this);
    }
    
    @Transactional
    public void remove() {
        if (getEntityManager().contains(this)) {
        	getEntityManager().remove(this);
        } else {
            T attached = findById(getId());
            getEntityManager().remove(attached);
        }
    }
 
	@Transactional
    public void flush() {
        getEntityManager().flush();
    }
    
    @Transactional
    public void clear() {
        getEntityManager().clear();
    }
    
    @Transactional
    public T merge() {
        T merged = (T) getEntityManager().merge(this);
        getEntityManager().flush();
        return merged;
    }
}
