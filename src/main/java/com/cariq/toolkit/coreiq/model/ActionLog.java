package com.cariq.toolkit.coreiq.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cariq.toolkit.utils.profile.ProfilePoint;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class ActionLog {
	static final int MAX_INFO_SIZE = 3995;

	/**
	   */
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date itsTimeStamp;

	/**
	 */
	private String actionName;

	/**
	 */
	private String objectType;

	/**
	 */
	private String objectId;

	/**
	*/
	@Column(length = 4000)
	private String additionalInfo;

	/**
	*
	*/
	public ActionLog() {
		super();
	}

	/**
	 * @param itsTimeStamp
	 * @param actionName
	 * @param objectType
	 * @param objectId
	 * @param additionalInfo
	 */
	public ActionLog(String context, Date itsTimeStamp, String actionName, String objectType, String objectId,
			String additionalInfo) {
		this.context = context;
		this.itsTimeStamp = itsTimeStamp;
		this.actionName = actionName;
		this.objectType = objectType;
		this.objectId = objectId;
		this.additionalInfo = additionalInfo;
	}

	/**
	 * Log the operation as part of the Transaction
	 * 
	 * @param actionName
	 * @param objectType
	 * @param objectId
	 * @param additionalInfo
	 */
	public static void log(String context, String actionName, String objectType, Object objectId,
			Object additionalInfoObj) {
		try (ProfilePoint _ActionLogPersist = ProfilePoint.profileAction("ProfAction_ActionLogPersist")) {
			// manage additionalInfo
			String additionalInfo = additionalInfoObj == null ? null : additionalInfoObj.toString();
			if (additionalInfo != null && additionalInfo.length() > MAX_INFO_SIZE)
				additionalInfo = additionalInfo.substring(0, MAX_INFO_SIZE);
			ActionLog log = new ActionLog(context, new Date(), actionName, objectType,
					objectId == null ? null : objectId.toString(), additionalInfo);
			log.persist();
		} catch (Exception e) {
			// It's ok to fail. Don't fail the operation, don't rollback
			// transaction
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	*/
	private String context;
}
