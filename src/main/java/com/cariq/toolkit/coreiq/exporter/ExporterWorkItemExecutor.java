package com.cariq.toolkit.coreiq.exporter;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.cariq.toolkit.utils.APIHelper;
import com.cariq.toolkit.utils.CSVHtmlReportModel;
import com.cariq.toolkit.utils.CarIQFileServer;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQZipFile;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ObjectFeeder;
import com.cariq.toolkit.utils.PagingBatchFeeder;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.core.AnalyticsHelper;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.service.NotificationService;
import com.cariq.toolkit.utils.service.NotificationService.CarIQNotificationType;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.service.async.WorkItemUpdater;
import com.cariq.toolkit.serviceimpl.async.WIHelper;

@Configurable
public class ExporterWorkItemExecutor implements Job {

	static CarIQLogger logger = APIHelper.logger.getLogger("ExporterWorkItemExecutor");

	/** The file server. */
	@Autowired
	private CarIQFileServer fileServer;

	/** The notification service. */
	@Autowired
	private NotificationService notificationService;

	@Autowired
	private VelocityEngine velocityEngine;

	@Override
	@Transactional
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try (ProfilePoint _ExporterWorkItemExecutor_execute = ProfilePoint
				.profileAction("ProfAction_ExporterWorkItemExecutor_execute")) {
			// Do under profilepoint Action (hopefully it is wrapped in the Action
			logger.debug("[CALLED],[execute],\"execute(context=" + context + ")\"");
			WorkItemUpdater updater = WIHelper.getUpdater(context);

			// get input json from the workitem
			String inputJson = updater.getItem().getInputJson();
			logger.debug("Input Json," + inputJson);

			// construct ExporterRequest object from the json.
			ExporterRequest exporterRequest = Utils.getJSonObject(inputJson, ExporterRequest.class);
			updater.setPercentComplete(5);

			// Find ExportedData by report id
			ExportedData report = ExportedData.findByReportId(exporterRequest.getReportId());
			try {
				// Execute Executor and upload file and return public url
				String fileUrl = executeExporterAndUploadFile(exporterRequest);

				report.setFileUrl(fileUrl);
				report.setStatus(ExportedData.COMPLETE);
				report.merge();

				updater.setPercentComplete(87);

				updater.setOutputJson("{\"success\" : true}");
				updater.setPercentComplete(100);

				updater.complete();
			} catch (Exception e) {
				Utils.logException(logger, e, "While executing ExporterWorkItemExecutor");
				updater.fail(Utils.getExceptionMessage(e));
				if (report != null) {
					report.setStatus(ExportedData.FAILED);
					report = report.merge();
				}
				throw new JobExecutionException(e);
			}
		}
	}

	/**
	 * @param exporterRequest
	 * @return
	 */

	private String executeExporterAndUploadFile(ExporterRequest exporterRequest) {
		try (ProfilePoint _executeExporterAndUploadFile = ProfilePoint
				.profileAction("ProfAction_executeExporterAndUploadFile")) {
			// prepare the filepaths - filename = exportername+datecode
			String outputFileName = exporterRequest.getOutputFileName();
			outputFileName = outputFileName.replace(" ", "_");
			String fileExtension = exporterRequest.getFileFormat() == null ? "csv"
					: exporterRequest.getFileFormat().toLowerCase();
			String filename = outputFileName + "." + fileExtension;
			String zipFileName = filename + ".zip";
			String exportFile = CarIQFileUtils.getTempFile(filename);
			String zipExportFile = CarIQFileUtils.getTempFile(zipFileName);

			String zipServerFilePath = "exporter/" + zipFileName;
			logger.debug("Exporting : " + exporterRequest);
			// Get Exporter and export into the file
			export(exporterRequest.getExporterName(), exporterRequest.getQueryType(), exporterRequest.getInputJSON(),
					exportFile, exporterRequest.getFileFormat(), false);
			// Add the file to zip file
			CarIQZipFile zipFile = new CarIQZipFile(zipExportFile, zipServerFilePath);
			zipFile.add(filename, exportFile);
			zipFile = zipFile.build();

			fileServer.upload(zipFile, WIHelper.carLogCDN);

			String fileUrl = WIHelper.getCarLogPath(zipServerFilePath);
			notifyByMail(exporterRequest, fileUrl, zipFileName, CarIQFileUtils.getFileSize(zipExportFile));
			return fileUrl;
		} catch (Exception e) {
			Utils.handleException(e);
		}

		return null;
	}

	public static final String CSV = "csv";
	public static final String HTML = "htm";
	public static final String ZIP = "zip"; // zip file (containing csv file(s))
	public static final String ZIP_CSV = "zip-csv"; // zip file (containing csv
													// file(s))
	public static final String ZIP_HTML = "zip-htm"; // zip file (containing
														// html file(s))

	public static final String SLASH = "/";

	private void export(String exporterName, final String queryType, final GenericJSON json, String outputFile,
			String fileFormat, boolean checkAuth) throws Exception {
		// if HTML output, process CSV to render in HTML format using generic
		// velocity template

		if (fileFormat.toUpperCase().contains(CarIQFileUtils.HTML.toUpperCase())) {
			String csvExportFile = outputFile + ".csv";
			final CarIQExporter exporter = CarIQExporterRegistry.getExporter(exporterName);

			exportCSV(exporterName, queryType, json, csvExportFile, checkAuth);

			// make generic json map from the csv
			String text = VelocityEngineUtils
					.mergeTemplateIntoString(velocityEngine, CSVHtmlReportModel.VelocityTemplate, "UTF-8",
							new CSVHtmlReportModel(
									exporter.getName(), exporter.getDescription()
											+ ", report Generated on the basis of QueryType: " + queryType,
									new Date(), csvExportFile, exporter.getHtmlRenderer()));

			// save to html fileExporterAsyncCompletion
			CarIQFileUtils.saveToFile(outputFile, text);

		} else {
			exportCSV(exporterName, queryType, json, outputFile, checkAuth);
		}
	}

	private void exportCSV(String exporterName, final String queryType, final GenericJSON json, String outputFile,
			boolean checkAuth) {
		try {
			final CarIQExporter exporter = CarIQExporterRegistry.getExporter(exporterName);
			Utils.checkNotNull(exporter, "Exporter with name: " + exporterName + " not Found!!");

			if (checkAuth && !exporter.isAuthorised())
				Utils.handleException("User is not authorized to run Exporter: " + exporterName);

			// TEMP @TODO - revert it back to 500 when done.
			final int BATCH_SIZE = 2*100*1000;
			AnalyticsHelper.writeToCSVFile(outputFile,
					new ObjectFeeder<GenericJSON>(new PagingBatchFeeder<GenericJSON>(BATCH_SIZE) {
						@Override
						protected List<GenericJSON> getList(int pageNo, int pageSize) {
							return exporter.export(queryType, json, pageNo, pageSize);
						}
					}));
		} catch (Exception e) {
			Utils.handleException(e);
		}
	}

	/**
	 * Notify by mail.
	 */
	private void notifyByMail(ExporterRequest exporterRequest, String fileUrl, String fileName, long fileSize) {
		logger.debug("[CALLED],[notifyByMail],\"notifyByMail()\"");
		try (ProfilePoint _notifyByMail = ProfilePoint.profileAction("ProfAction_notifyByMail")) {
			// To uncomment later
			User user = User.findObjectById(exporterRequest.getUserId());

			// For Test
			// Long rajeshUserId = 1126L;
			// CarIqUser user = CarIqUser.getById(1126L);

			if (user == null) // no email
				return;

			notificationService.sendNotificationEmail(user.getEmail(), "Report ready for download",
					CarIQNotificationType.ExporterAsyncCompletion,
					getModelMap(user, exporterRequest, fileUrl, fileName, fileSize));
		} catch (Exception e) {
			Utils.logException(logger, e, "While sending mail to user");
		}
	}

	private Map<String, Object> getModelMap(User user, ExporterRequest exporterRequest, String fileUrl,
			String fileName, long fileSize) {
		if (user == null)
			return null;

		logger.debug("[CALLED],[getModelMap],\"getModelMap()\"");

		try (ProfilePoint _getModelMap = ProfilePoint.profileAction("ProfAction_getModelMap")) {
			Map<String, Object> modelMap = new HashMap<String, Object>();
			modelMap.put("userName", user.getUsername());
			modelMap.put("fileUrl", fileUrl);
			modelMap.put("generatedOn", exporterRequest.getCreatedOn());
			modelMap.put("exporterName", exporterRequest.getExporterName());
			modelMap.put("fileName", fileName);
			modelMap.put("fileSize", fileSize + " bytes");
			return modelMap;
		}
	}
}