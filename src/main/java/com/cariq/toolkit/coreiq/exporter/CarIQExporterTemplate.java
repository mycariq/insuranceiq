package com.cariq.toolkit.coreiq.exporter;

import java.util.List;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.HtmlValueIdentityRenderer;
import com.cariq.toolkit.utils.HtmlValueRenderer;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

public abstract class CarIQExporterTemplate implements CarIQExporter {
	protected static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIQExporter");
	CarIQExporterDefinition definition;

	public CarIQExporterTemplate(String name, String description) {
		super();
		definition = new CarIQExporterDefinition(name, description);
	}

	@Override
	public String getName() {
		return definition.getName();
	}

	@Override
	public String getDescription() {
		return definition.getDescription();
	}

	@Override
	public CarIQExporterDefinition getExporterDefinition() {
		return definition;
	}

	protected void addQueryType(String name, String description, String[] attrList) {
		definition.add(CarIQExporterDefinition.QUERY_DEF, new CarIQQueryDefinition(name, description, attrList));
	}

	protected void addQueryType(String name, String description, List<QueryDefinitionAttribute> attrList) {
		definition.add(CarIQExporterDefinition.QUERY_DEF, new CarIQQueryDefinition(name, description, attrList));
	}

	protected void addExportAttribute(String exportAttributeName) {
		definition.add(CarIQExporterDefinition.EXPORT_PARAM, exportAttributeName);
	}

	protected void addExportAttributes(List<String> exportAttributeNames) {
		for (String name : exportAttributeNames) {
			addExportAttribute(name);
		}
	}

	protected void addSupportedRole(String supportedRole) {
		definition.add(CarIQExporterDefinition.ROLE, supportedRole);
	}

	protected void addSupportedRoles(List<String> supportedRoles) {
		for (String name : supportedRoles) {
			addSupportedRole(name);
		}
	}

	@Override
	public boolean isAuthorised() {
		return getExporterDefinition().isAuthorized();
	}

	@Override
	public List<GenericJSON> export(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _exporter_export = ProfilePoint
				.profileActivity("ProfActivity_exporter_export_" + this.getClass().getName())) {
			json = Utils.trimStringValues(json);
			List<GenericJSON> list = doExport(queryType, json, pageNo, pageSize);
			return getEnhancer().enhance(list);
			//return doExport(queryType, json, pageNo, pageSize);
		}
	}

	private ExportDataEnhancer getEnhancer() {
		return new UnityExportDataEnhancer();
	}

	@Override
	public HtmlValueRenderer getHtmlRenderer() {
		return new HtmlValueIdentityRenderer();
	}

	protected abstract List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize);
}
