package com.cariq.toolkit.coreiq.exporter;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.CSVHtmlReportModel;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ObjectFeeder;
import com.cariq.toolkit.utils.PagingBatchFeeder;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.core.AnalyticsHelper;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.service.async.WorkItemService;
import com.cariq.toolkit.serviceimpl.async.WIHelper;

@Service
public class CarIQExporterServiceImpl implements CarIQExporterService {
	static CarIQLogger apiLogger = CarIQToolkitHelper.logger.getLogger("CarIQExporterServiceImpl");

	@Autowired
	private VelocityEngine velocityEngine;

	@Autowired
	private WorkItemService workItemService;

	@Override
	public List<CarIQExporterDefinition> getExporterDefinitions() {
		Collection<CarIQExporter> exporters = CarIQExporterRegistry.getAllExporters();
		List<CarIQExporterDefinition> returnList = new ArrayList<CarIQExporterDefinition>();
		for (CarIQExporter exporter : exporters) {
			if (exporter.getExporterDefinition().isAuthorized())
				returnList.add(exporter.getExporterDefinition());
		}

		Collections.sort(returnList);
		return returnList;
	}

	@Override
	public CarIQExporterDefinition getExporterDefinition(String exporterName) {
		CarIQExporter exporter = CarIQExporterRegistry.getExporter(exporterName);
		Utils.checkNotNull(exporter, "Exporter with name: " + exporterName + " not Found!!");
		return exporter.getExporterDefinition();
	}

	@Override
	public GenericJSON exportSingle(String exporterName, String queryType, GenericJSON json) {
		CarIQExporter exporter = CarIQExporterRegistry.getExporter(exporterName);
		Utils.checkNotNull(exporter, "Exporter with name: " + exporterName + " not Found!!");

		// get first item from the first page
		return exporter.export(queryType, json, 1, 1).get(0); // return the first entity from the page
	}

	/**
	 * Export to a CSV file using batched File Stream Writer
	 */
	@Override
	public void export(String exporterName, final String queryType, final GenericJSON json, String outputFile)
			throws Exception {
		export(exporterName, queryType, json, outputFile, true); // check auth by default
	}

	/**
	 * Export to a CSV file using batched File Stream Writer
	 */
	@Override
	public void export(String exporterName, final String queryType, final GenericJSON json, String outputFile,
			boolean checkAuth) throws Exception {
		try (ProfilePoint _export_using_batch = ProfilePoint.profileAction("ProfAction_export_using_batch")) {
			final CarIQExporter exporter = CarIQExporterRegistry.getExporter(exporterName);
			Utils.checkNotNull(exporter, "Exporter with name: " + exporterName + " not Found!!");
			if (checkAuth && !exporter.isAuthorised())
				Utils.handleException("User is not authorized to run Exporter: " + exporterName);
			// TEMP @TODO - revert it back to 5000 when done.
			int BATCH_SIZE = 2 * 100 * 1000;
			AnalyticsHelper.writeToCSVFile(outputFile,
					new ObjectFeeder<GenericJSON>(new PagingBatchFeeder<GenericJSON>(BATCH_SIZE) {
						@Override
						protected List<GenericJSON> getList(int pageNo, int pageSize) {
							return exporter.export(queryType, json, pageNo, pageSize);
						}
					}));
		}
	}

	@Override
	public String export(String exporterName, final String queryType, final GenericJSON json,
			HttpServletResponse response) {
		try {
			String tmpExportFile = CarIQFileUtils
					.getTempFile(exporterName + "-" + queryType + "-" + Utils.createDateId() + ".csv");
			export(exporterName, queryType, json, tmpExportFile, false);
			return AnalyticsHelper.StringifyCSV(tmpExportFile);
		} catch (Exception e) {
			Utils.handleException(e.getMessage());
		}
		return null;
	}

	@Override
	public void pupulateFile(String exporterName, String queryType, File file, HttpServletResponse response)
			throws Exception {
		// Not implemented yet

	}

	/**
	 * Export Multiple records based on query type in right order pageNumber and
	 * pageSize
	 */
	@Override
	public List<GenericJSON> exportMultiple(String exporterName, String queryType, GenericJSON json, int pageNumber,
			int pageSize) {
		try (ProfilePoint _exportMultiple = ProfilePoint
				.profileActivity("ProfActivity_exportMultiple_" + this.getClass().getCanonicalName())) {
			CarIQExporter exporter = CarIQExporterRegistry.getExporter(exporterName);
			Utils.checkNotNull(exporter, "Exporter with name: " + exporterName + " not Found!!");
			// get first item from the first page
			return exporter.export(queryType, json, pageNumber, pageSize);
		}
	}

	@Override
	public void export(String exporterName, String queryType, GenericJSON json, String fileType,
			HttpServletResponse response) {
		try (ProfilePoint _export_file = ProfilePoint
				.profileActivity("ProfActivity_export_file_" + this.getClass().getCanonicalName())) {
			try {
				String csvExportFile = CarIQFileUtils
						.getTempFile(exporterName + "-" + queryType + "-" + Utils.createDateId() + ".csv");
				// First export to CSV file
				export(exporterName, queryType, json, csvExportFile);

				// if HTML output, process CSV to render in HTML format using
				// generic velocity template
				if (fileType.toUpperCase().contains("HTM")) {
					try (ProfilePoint _processCSVToHTML = ProfilePoint.profileAction("ProfAction_processCSVToHTML")) {
						final CarIQExporter exporter = CarIQExporterRegistry.getExporter(exporterName);
						String htmlFile = CarIQFileUtils
								.getTempFile(exporterName + "-" + queryType + "-" + Utils.createDateId() + ".html");
						// make generic json map from the csv
						String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
								CSVHtmlReportModel.VelocityTemplate, "UTF-8",
								new CSVHtmlReportModel(exporter.getName(),
										exporter.getDescription() + ", report Generated on the basis of QueryType: "
												+ queryType,
										Utils.getUTCToLocalTimeStamp(new Date(), null), csvExportFile,
										exporter.getHtmlRenderer()));
						// save to html file
						CarIQFileUtils.saveToFile(htmlFile, text);
						// send file as an attachment in the response
						CarIQFileUtils.attachHTMLFileToResponse(response, htmlFile);
					}
				} else {
					// send file as an attachment in the response
					CarIQFileUtils.attachCSVFileToResponse(response, csvExportFile);
				}
			} catch (Exception e) {
				Utils.handleException(e.getMessage());
			}
		}
	}

	@Override
	public List<CarIQExporterDefinition> getReporterDefinitions() {
		Collection<CarIQExporter> exporters = CarIQExporterRegistry.getAllReporters();
		List<CarIQExporterDefinition> returnList = new ArrayList<CarIQExporterDefinition>();
		for (CarIQExporter exporter : exporters) {
			if (exporter.getExporterDefinition().isAuthorized())
				returnList.add(exporter.getExporterDefinition());
		}
		Collections.sort(returnList);
		return returnList;
	}

	@Transactional
	@Override
	public ResponseJson downloadAsync(String exporter, String queryType, String fileType, GenericJSON json) {
		try (ProfilePoint _downloadAsync = ProfilePoint.profileAction("ProfAction_downloadAsync")) {
			apiLogger.debug("Exporter : " + exporter + " queryType : " + queryType + " fileType : " + fileType
					+ " Input json : " + json);
			User currentUser = User.getLoggedInUser();
			String reportId = exporter + "-" + Utils.createDateId(true);
			String outputFile = reportId;

			// create a work request object
			ExporterRequest request = new ExporterRequest(exporter, queryType, outputFile, fileType, json,
					currentUser == null ? null : currentUser.getId(), reportId);

			String queryString = Utils.getJSonString(request);
			WIHelper.logger.debug("InputJson," + queryString);

			// Create a work item and register
			String worker = ExporterWorkItemExecutor.class.getCanonicalName();
			workItemService.addWorkItem(worker, queryString.toString(), "ExporterWorker", exporter,
					"Async worker for downloading exporter file", null);

			// Create ExportData object
			ExportedData exportData = new ExportedData(request);
			exportData.setInputJSON(filterJson(request.getInputJSON()));
			exportData.persist();

			// respond back to user. Think of a better response after some discussion
			return new ResponseJson("Download Request is successfully registered with reference Id: " + reportId
					+ ". We'll notify you on completion of this task.");
		}
	}

	private String filterJson(GenericJSON inputJSON) {
		GenericJSON json = inputJSON;
		if (json != null && json.containsKey(ExportedData.USERNAME))
			;
		json.remove(ExportedData.USERNAME);
		return json.toString();
	}
}
