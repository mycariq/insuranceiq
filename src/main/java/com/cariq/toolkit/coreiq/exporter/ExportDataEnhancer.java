/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;

/**
 * The Interface ExportDataEnhancer.
 *
 * @author santosh
 */
public interface ExportDataEnhancer {

	/**
	 * Enhance.
	 *
	 * @param list
	 *            the list
	 * @return the list
	 */
	public List<GenericJSON> enhance(List<GenericJSON> list);
}
