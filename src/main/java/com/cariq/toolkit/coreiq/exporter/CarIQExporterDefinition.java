package com.cariq.toolkit.coreiq.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.ObjectBuilder;
import com.cariq.toolkit.utils.Utils;

/**
 * Exporter requires set of attributes as inputs, based on conditions or population
 * There are different ways to invoke the Exporter.
 * There are set of conditions (Strings) the Exporter can support.
 * For Each Condition, there is a list of AttributeDef
 * So, Exporter Definition is nothing but a list of Condition Definitions
 * And it also declares the Output attribute Definitions
 * @author hrishi
 *
 */
public class CarIQExporterDefinition implements ObjectBuilder, Comparable<CarIQExporterDefinition> {
	// different type of objects that can be added to this
	public static String ROLE = "ROLE", QUERY_DEF = "QUERY_DEF", EXPORT_PARAM = "EXPORT_PARAM";
	String name, description;
	List<CarIQQueryDefinition> queryDefinitions = new ArrayList<CarIQQueryDefinition>();
//	List<CarIQAttrDefinition> exportAttrDefinition = new ArrayList<CarIQAttrDefinition>();
	List<String> exportParameters = new ArrayList<String>();
	List<String> supportedRoles = new ArrayList<String>();

	
	public CarIQExporterDefinition(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public CarIQExporterDefinition(String name, String description, List<CarIQQueryDefinition> queryDefinitions,
			List<String> exportParameters, List<String> supportedRoles) {
		super();
		this.name = name;
		this.description = description;
		this.queryDefinitions = queryDefinitions;
		this.exportParameters = exportParameters;
		this.supportedRoles = supportedRoles;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<CarIQQueryDefinition> getQueryDefinition() {
		if (queryDefinitions == null || queryDefinitions.isEmpty())
			return Arrays.asList(new CarIQQueryDefinition("All", "Everything"));

		return queryDefinitions;
	}

	public List<String> getExportAttrDefinition() {
		return exportParameters;
	}

	public void setExportAttrDefinition(List<String> exportParameters) {
		this.exportParameters = exportParameters;
	}
	
	// Builder Methods
	public List<String> getSupportedRoles() {
		return supportedRoles;
	}

	public void setSupportedRoles(List<String> supportedRoles) {
		this.supportedRoles = supportedRoles;
	}

	public void setQueryDefinitions(List<CarIQQueryDefinition> queryDefinitions) {
		this.queryDefinitions = queryDefinitions;
	}

	public void setExportParameters(List<String> exportParameters) {
		this.exportParameters = exportParameters;
	}

	public List<CarIQQueryDefinition> getQueryDefinitions() {
		return queryDefinitions;
	}

	public List<String> getExportParameters() {
		return exportParameters;
	}

	@Override
	public CarIQExporterDefinition configure(String setting, Object value) {
		if (setting.equals("NAME"))
			name = (String) value;
		else if (setting.equals("DESCRIPTION"))
			description = (String) value;
		
		return this;
	}

	@Override
	public CarIQExporterDefinition add(Object value) {
//		if (value instanceof CarIQQueryDefinition) {
//			CarIQQueryDefinition qryDef = (CarIQQueryDefinition) value;
//			queryDefinitions.add(qryDef);
//		}
//		else if (value instanceof String)
//			exportParameters.add((String) value);
		Utils.handleException("add without Parameter not supported in CarIQExporterDefinition");

		return this;
	}
	

	@Override
	public CarIQExporterDefinition add(String parameter, Object value) {
		if (parameter.equals(ROLE))
			Utils.addToList(supportedRoles, value.toString());
		else if (parameter.equals(EXPORT_PARAM))
			Utils.addToList(exportParameters, value.toString());
		else if (parameter.equals(QUERY_DEF))
			Utils.addToList(queryDefinitions, (CarIQQueryDefinition) value);
		else
			Utils.handleException("Unknown Object added to CarIQExporter: " + parameter);

		return this;
	}

	@Override
	public CarIQExporterDefinition build() {
		return this;
	}

	@Override
	public int compareTo(CarIQExporterDefinition other) {
		return getName().compareTo(other.getName());
	}

	/**
	 * @return
	 */
	public boolean isAuthorized() {
		// if roles is not supported, return error
		// @TODO <7-June-2017> this is tricky - revisit this - we are not having any "ROLE" concept in FleetIQ yet - Role is imposed by CarIQ currently
//		return true;  
		List<String> currentUserRoles = new ArrayList<String>();
		// @TODO this is tricky - must be checked how to do
		 currentUserRoles.add(User.getCurrentUserRole());
		// this is temp
		List<String> definitionRoles = new ArrayList<String>();
		definitionRoles.addAll(getSupportedRoles());
		
		definitionRoles.retainAll(currentUserRoles);
		return definitionRoles.isEmpty() == false; // if intersection exist, it is authorized!
	}	
}
