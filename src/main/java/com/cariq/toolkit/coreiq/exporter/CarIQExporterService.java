package com.cariq.toolkit.coreiq.exporter;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ResponseJson;

public interface CarIQExporterService {

	List<CarIQExporterDefinition> getExporterDefinitions();

	// Export single record - mostly the query type is of that kind
	GenericJSON exportSingle(String exporter, String queryType, GenericJSON json);
	
	// Export multiple records - with paging
	List<GenericJSON> exportMultiple(String exporter, String queryType, GenericJSON json, int pageNumber, int pageSize);

	// Write the output in the form of CSV string
	Object export(String exporter, String queryType, GenericJSON json, HttpServletResponse response);

	// Export all records to a file and attach to response handle
	void export(String exporter, String queryType, GenericJSON json, String fileType, HttpServletResponse response);

	// Export all records in a file
	void export(String exporterName, final String queryType, final GenericJSON json, String outputFile) throws Exception;

	// Export with or without Checking Authentication
	void export(String exporterName, final String queryType, final GenericJSON json, String outputFile, boolean checkAuth) throws Exception;

	// Extension of export from the input file to the output file - keeping the old entries intact
	void pupulateFile(String exporterName, String queryType, File file, HttpServletResponse response) throws Exception;

	// Get Definition of the given Exporter
	CarIQExporterDefinition getExporterDefinition(String exporter);

	// Get reporter definition
	List<CarIQExporterDefinition> getReporterDefinitions();

	// This needs to be fixed at the generic level (cariq/toolkit)
	// List<GenericJSON> getReportData(ReporterJson json, String exporterName, String userName, int pageNo, int pageSize);

	/**
	 * Download report
	 * @param exporter
	 * @param queryType
	 * @param fileType
	 * @param json
	 * @return
	 */
	ResponseJson downloadAsync(String exporter, String queryType, String fileType, GenericJSON json);

}
