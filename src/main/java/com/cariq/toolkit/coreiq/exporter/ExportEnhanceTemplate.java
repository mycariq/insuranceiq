/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.List;

import com.cariq.toolkit.utils.APIHelper;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class ExportEnhanceTemplate.
 *
 * @author santosh
 */
public abstract class ExportEnhanceTemplate implements ExportDataEnhancer {

	private CarIQLogger logger = APIHelper.logger.getLogger("Export Enhance Template");

	/**
	 * Do enhance.
	 *
	 * @param list
	 *            the list
	 * @return the list
	 */
	protected abstract List<GenericJSON> doEnhance(List<GenericJSON> list);

	@Override
	public List<GenericJSON> enhance(List<GenericJSON> list) {
		try (ProfilePoint _enhance = ProfilePoint.profileAction("ProfAction_enhance")) {
			return doEnhance(list);
		} catch (Exception e) {
			logger.debug("Exception Generated in Enhancer : " + e);
			return list;
		}
	}
}
