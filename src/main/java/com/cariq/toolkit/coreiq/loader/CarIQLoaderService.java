package com.cariq.toolkit.coreiq.loader;

import java.io.File;
import java.util.List;

import com.cariq.toolkit.utils.CarIQAttrDefinition;
import com.cariq.toolkit.utils.GenericJSON;

public interface CarIQLoaderService {

	List<CarIQLoaderDefinition> getLoaderDefinition();

	List<CarIQAttrDefinition> getLoaderAttributes(String loader);

	void loadSingle(String loader, GenericJSON json, CarIQLoaderContext context);

	int load(String loaderName, File file, CarIQLoaderContext context) throws Exception;

	CarIQLoaderDefinition getLoaderDefinition(String loaderName);

}
