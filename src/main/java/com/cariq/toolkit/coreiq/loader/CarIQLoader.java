package com.cariq.toolkit.coreiq.loader;
 
import java.util.List;

import com.cariq.toolkit.utils.CarIQAttrDefinition;
import com.cariq.toolkit.utils.GenericJSON;

/**
 * Base Interface of CarIQ Loader. Loader is used to perform loading action into
 * the platform Loader infrastructure accepts CSV file to load into the system
 * Loader Definition informs the caller about name, description and in what
 * format Loader expects the information
 * 
 * @author hrishi
 *
 */
public interface CarIQLoader {
	String getName();

	String getDescription();

	List<CarIQAttrDefinition> getAttributes();

	void load(GenericJSON json, CarIQLoaderContext context);

	CarIQLoaderDefinition getLoaderDefinition();

}
