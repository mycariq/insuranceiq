package com.cariq.toolkit.coreiq.loader;

import com.cariq.toolkit.utils.CarIQAttrDefinition;
import com.cariq.toolkit.utils.GenericJSON;

public class DemoLoader extends CarIQLoaderTemplate {
	/**
	 * Argumentless constructor, must pass name and description to the baseclass constructor
	 */
	public DemoLoader() {
		super("DemoLoader", "Loader to Demonstrate Loader Framework");
		addAttribute("person.name", "Name of the person", false);
		addAttribute(new CarIQAttrDefinition("person.gender", false));
		addAttribute(new CarIQAttrDefinition("person.age", "Age between 0 and 100"));
		addAttribute(new CarIQAttrDefinition("person.hobby", "Hobby of person", true)); // optional
	}

	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context) {
		for (String key : json.keySet()) {
			System.out.println("DemoLoader loading: " + key + " = " + json.get(key));
		}		
	}

}
