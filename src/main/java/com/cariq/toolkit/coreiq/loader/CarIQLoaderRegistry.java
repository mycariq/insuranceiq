package com.cariq.toolkit.coreiq.loader;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CarIQLoaderRegistry {
	static Map<String, CarIQLoader> map = new HashMap<String, CarIQLoader>();

	static void add(CarIQLoader loader) {
		map.put(loader.getName().toUpperCase(), loader);
	}

	static {
		// Enlist all the loaders to be made available to Admin through REST call

		//		add(new DeviceConfigLoader());
		//		add(new OnboardCarLoader());
		//		add(new PolicyCarLoader());
		//		add(new UserPolicyLinkLoader());
		//		add(new PidLoader());
		//		add(new PolicyDeactivateLoader());
		//		add(new PolicyRenewalLoader());
		//		add(new AWBLoader());
		//		add(new UpdateShippedPolicyOrderLoader());
		//		add(new UnregisterPolicyLoader());
		//		add(new UpdatePolicyDetailsLoader());
		//		add(new UpsertShipmentDeliveryStatusLoader());
		//		add(new PacketLoader());
		//		add(new UploadCarMakeModelLoader());
		//		add(new UploadErrorCodesLoader());
		//		add(new LogisticsEmailBroadcaster());
		//		add(new DevicelessPolicyCarUserLoader());
		//		add(new PolicyUpdateLoader());

	}

	static CarIQLoader getLoader(String loaderName) {
		return map.get(loaderName.toUpperCase());
	}

	static Collection<CarIQLoader> getAllLoaders() {
		return map.values();
	}
}
