package com.cariq.toolkit.coreiq.loader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CarIQLoaderContext extends HashMap<String, Object>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String MESSAGE_QUEUE = "MESSAGE_QUEUE";

	public void addMessage(String msg) {
		putInList(MESSAGE_QUEUE, msg);
	}

	private <T> void putInList(String key, T msg) {
		List<T> msgList = (List<T>) get(key);
		if (msgList == null) {
			msgList = new ArrayList<T>();
			put(key,msgList);
		}
		msgList.add(msg);		
	}
	
	public List<String> getMessages() {
		return (List<String>) get(MESSAGE_QUEUE);
	}

}
