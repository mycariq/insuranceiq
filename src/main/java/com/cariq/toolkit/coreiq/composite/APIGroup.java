package com.cariq.toolkit.coreiq.composite;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.cariq.toolkit.utils.API;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * API Group responsible for 
 * @author hrishi
 *
 */
public class APIGroup {
	private static CarIQLogger apiGroupLogger = CarIQToolkitHelper.logger.getLogger("APIGroup");
	 
	// default constructor needed for JSON to initialize
	public APIGroup() {}

	public APIGroup(Map<String, API> apis) {
		super();
		this.apis = apis;
	}


	public Map<String, API> getApis() {
		return apis;
	}


	public void setApis(Map<String, API> apis) {
		this.apis = apis;
	}


	Map<String, API> apis = new HashMap<String, API> ();

	static final int MAX_THREAD_COUNT = 10;
	
	public GenericJSON execute(String auth) {
		try (ProfilePoint _APIGroup_execute = ProfilePoint.profileAction("ProfAction_APIGroup_execute")) {
			ExecutorService executorService = Executors.newFixedThreadPool(Math.min(apis.size(), MAX_THREAD_COUNT));
			Map<String, Future<Object>> futures = new HashMap<String, Future<Object>>();
			// Iterate over APIs and submit the job
			for (String api : apis.keySet())
				futures.put(api, executorService.submit(apis.get(api).setAuth(auth)));
			// resolve all futures and return the values
			GenericJSON retval = resolveFutures(futures);
			executorService.shutdown();
			return retval;
		}
	}


	private GenericJSON resolveFutures(Map<String, Future<Object>> apiFutures) {
		GenericJSON retval = new GenericJSON();
		
		for (String api : apiFutures.keySet()) {
			try {
				retval.put(api, apiFutures.get(api).get());
			} catch (InterruptedException | ExecutionException e) {
				Utils.logException(apiGroupLogger, e, "Calling API: api");
				retval.put(api, null);
			}	
		}
		
		return retval;
	}
}
