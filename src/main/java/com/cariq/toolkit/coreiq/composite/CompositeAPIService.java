package com.cariq.toolkit.coreiq.composite;

public interface CompositeAPIService {
	/**
	 * Invoke set of APIs in parallel
	 * @param json
	 * @param auth 
	 */
	Object call(APIGroup json, String auth);
}
