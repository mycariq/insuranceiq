package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.model.ParamDefinition.ParamDefinition_In;
import com.cariq.toolkit.model.ParamDefinition.ParamDefinition_Out;

/**
 * The Interface ParamDefinitionService.
 */
public interface ParamDefinitionService {

	/**
	 * Adds the ParamDefinition.
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson createOrGetParamDefinition(ParamDefinition_In params);

	/**
	 * Gets the ParamDefinition.
	 * @param paramDefinitionName
	 * @return
	 */
	public List<ParamDefinition_Out> getParamDefinitionsByName(String paramDefinitionName);

	/**
	 * Delete ParamDefinition.
	 * @param params
	 * @return
	 */
	public ResponseJson deleteParamDefinition(ParamDefinition_In params);

	/**
	 * Get all paramDefinitions
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<ParamDefinition_Out> getAllParamDefinitions(int pageNo, int pageSize);
}