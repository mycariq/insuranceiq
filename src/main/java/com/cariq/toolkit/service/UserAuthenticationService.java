package com.cariq.toolkit.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.cariq.toolkit.model.User;


public class UserAuthenticationService implements UserDetailsService {

	/**
	 * * Instantiates a new authentication service.
	 */
	public UserAuthenticationService() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetailsService#
	 * loadUserByUsername(java.lang.String)
	 */
	@Override
	public final UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		return User.getByUsername(username);
	}

	/**
	 * Gets the user.
	 *
	 * @param username
	 *            the username
	 * @param password
	 *            the password
	 * @return the user
	 *
	public UserDetails getAdminUser(String username, String password) {
		return User.getUserAuth(username, password);
	}
	*/
}
