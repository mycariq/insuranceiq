package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.model.ScoreDefinition.ScoreDefinition_Out;
import com.cariq.toolkit.model.ScoreParam.ScoreParamDefinition_In;
import com.cariq.toolkit.model.ScoreParam.ScoreParam_In;
import com.cariq.toolkit.model.ParamDefinition.ParamDefinition_Out;

/**
 * The Interface ScoreParamService.
 */
public interface ScoreParamService {
	
	/**
	 * Clone existing score definition and attach the same params
	 * @param scoreDefName
	 * @return
	 */
	public ResponseJson cloneScoreDefinitionWithParams(String existingScoreDefName, String newScoreDefName);

	/**
	 * Associate ParamDefinition with ScoreDefinition
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson attachParamToScoreDef(ScoreParamDefinition_In params);

	/**
	 * Associate ParamDefinition with ScoreDefinition by IDs
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson attachParamToScoreDefByIDs(ScoreParam_In params);

	/**
	 * Dissociate ParamDefinition from ScoreDefinition
	 * @param params
	 * @return
	 */
	public ResponseJson detachParamFromScoreDef(ScoreParamDefinition_In params);

	/**
	 * Dissociate ParamDefinitionId from ScoreDefinition by IDs
	 * @param params
	 * @return
	 */
	public ResponseJson detachParamFromScoreDefByIDs(ScoreParam_In params);
	
	/**
	 * Lists scoreDefinitions associated with given paramDefinition Id
	 * @param paramDefinitionName
	 * @return
	 */
	public List<ScoreDefinition_Out> getScoresForParamDefId(Long paramDefId);
	
	/**
	 * Lists paramDefinitions associated with given scoreDefinition 
	 * @param scoreDefinitionName
	 * @return
	 */
	public List<ParamDefinition_Out> geParamDefsForScoreDef(String scoreDefinitionName);
}