package com.cariq.toolkit.service;

import org.springframework.http.HttpMethod;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.model.User;

public interface CariqProxyService {

	/**
	 * Call Http method 
	 * 
	 * @param user
	 * @param json
	 * @param apiName
	 * @param method
	 * @return
	 */
	public Object call(User user, GenericJSON json, String apiName, HttpMethod method);
}
