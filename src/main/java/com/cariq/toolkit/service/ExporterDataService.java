package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.coreiq.exporter.ExportedData.ExporterDataJSON_Out;


public interface ExporterDataService {

	/**
	 * Get exported data list
	 * @param string
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	List<ExporterDataJSON_Out> getExportedDataList(String string, int pageNo, int pageSize);

}
