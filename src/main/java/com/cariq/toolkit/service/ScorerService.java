package com.cariq.toolkit.service;


import com.cariq.toolkit.utils.GenericJSON;

public interface ScorerService {

	/**
	 * Calculate the score for given parameter values
	 * @param scoreDefName
	 * @param json
	 * @return
	 */
	public Double evaluateScore(String scoreDefName, GenericJSON paramValues);
}
