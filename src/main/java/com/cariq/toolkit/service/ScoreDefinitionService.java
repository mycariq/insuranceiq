package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.model.ScoreDefinition.ScoreDefinition_In;
import com.cariq.toolkit.model.ScoreDefinition.ScoreDefinition_Out;

/**
 * The Interface ScoreDefinitionService.
 */
public interface ScoreDefinitionService {

	/**
	 * Adds the ScoreDefinition.
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson addScoreDefinition(ScoreDefinition_In params);

	/**
	 * Gets the scoreDefinition.
	 * @param scoreDefinitionName
	 * @return
	 */
	public ScoreDefinition_Out getScoreDefinition(String scoreDefinitionName);

	/**
	 * Update scoreDefinition.
	 * @param params
	 * @param scoreDefinitionname
	 * @return
	 */
	public ResponseJson updateScoreDefinition(ScoreDefinition_In params, String scoreDefinitionName);

	/**
	 * Delete scoreDefinition.
	 * @param scoreDefinitionname
	 * @return
	 */
	public ResponseJson deleteScoreDefinition(String scoreDefinitionName);

	/**
	 * Get all scoreDefinitions
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<ScoreDefinition_Out> getAllScoreDefinitions(int pageNo, int pageSize);
}