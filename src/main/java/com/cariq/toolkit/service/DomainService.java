package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.model.Domain.Domain_In;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.Domain.UpdateDomain_In;

/**
 * The Interface DomainService.
 */
public interface DomainService {

	/**
	 * Adds the domain.
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson addDomain(Domain_In params);

	/**
	 * Gets the domain.
	 * @param domainName
	 * @return
	 */
	public Domain_Out getDomain(String domainName);

	/**
	 * Update domain.
	 * @param params
	 * @param domainname
	 * @return
	 */
	public ResponseJson updateDomain(UpdateDomain_In params, String domainname);

	/**
	 * Delete domain.
	 * @param domainname
	 * @return
	 */
	public ResponseJson deleteDomain(String domainname);

	/**
	 * Get All domains.
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<Domain_Out> getAllDomains(int pageNo, int pageSize);
}