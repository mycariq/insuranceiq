package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.Subscription.Subscription_Out;

/**
 * The Interface DomainService.
 */
public interface DomainAccessService {

	/**
	 * Associates Subscription with Domain
	 * @param subscriptionName
	 * @param domainName
	 * @return
	 */
	public ResponseJson grantDomainAccessToSubscription(String subscriptionName, String domainName);
	
	/**
	 * Removes Domain-subscription association
	 * @param subscriptionName
	 * @param domainName
	 * @return
	 */
	public ResponseJson removeDomainAccessForSubscription(String subscriptionName, String domainName);
	
	/**
	 * Lists domains accessible for given subscription
	 * @param subscriptionName
	 * @return
	 */
	public List<Domain_Out> getAccessibleDomainsForSubscription(String subscriptionName);
	
	/**
	 * Lists subscriptions allowed to access the given domain 
	 * @param domainName
	 * @return
	 */
	public List<Subscription_Out> getPrivilegedSubscriptionsForDomain(String domainName);
}