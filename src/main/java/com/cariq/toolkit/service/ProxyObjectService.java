package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_Out;

/**
 * The Interface ProxyObjectService.
 */
public interface ProxyObjectService {

	/**
	 * Create or get the ProxyObject.
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson createOrGetProxyObject(ProxyObject_In params);

	/**
	 * Delete proxyObject.
	 *
	 * @param id
	 * @return the response json
	 */
	public ResponseJson deleteProxyObject(ProxyObject_In params);

	/**
	 * Get all proxyObjects
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<ProxyObject_Out> getAllProxyObjects(int pageNo, int pageSize);

	/**
	 * Get proxy-object by domain, objectId and type
	 * @param objectId
	 * @param objectType
	 * @return
	 */
	public ProxyObject_Out getProxyByDomainObjectIdType(String domainName, Long objectId, String objectType);
	
	/**
	 * Get proxy object by domain, signature and object-type. 
	 * @param signature
	 * @param objectType
	 * @return
	 */
	public ProxyObject_Out getProxyByDomainSignatureObjectType(String domainName, String signature, String objectType);
}