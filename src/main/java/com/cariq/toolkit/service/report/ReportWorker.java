package com.cariq.toolkit.service.report;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import org.hibernate.StaleStateException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.utils.CarIQFileServer;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.CompositeReport;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.service.async.WorkItemUpdater;

// TODO: Auto-generated Javadoc
/**
 * The Class ReportWorker.
 */
public abstract class ReportWorker {

	/** The input json. */
	protected GenericJSON inputJson;

	/** The updater. */
	protected WorkItemUpdater updater;

	/** The file server. */
	@Autowired
	private CarIQFileServer fileServer;

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("ReportWorker");
//
//	/** The notification service. */
//	@Autowired
//	private NotificationService notificationService;

	/**
	 * Instantiates a new report worker.
	 *
	 * @param inputJson
	 *            the input json
	 * @param updater
	 *            the updater
	 */
	public ReportWorker(GenericJSON inputJson, WorkItemUpdater updater) {
		logger.debug("[CALLED],[ReportWorker],\"ReportWorker(inputJson=" + inputJson + ",updator=updator)\"");
		this.inputJson = inputJson;
		this.updater = updater;
	}

	/**
	 * Execute.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Transactional(noRollbackFor = StaleStateException.class)
	public final void execute() throws Exception {
		logger.debug("[CALLED],[execute],\"execute()\"");
		try (ProfilePoint _executeReportWorker = ProfilePoint.profileActivity("ProfAction_executeReportWorker")) {
			CompositeReport report = executeInternal();
			logger.debug("Report Size:" + report.getSize());
			updateSize(report.getSize());

			fileServer.upload(report, getContainer());
			updater.setPercentComplete(99);
			notifyByMail();
			report.deleteLocalFile(); // change to cleanup in the worker
			updater.setPercentComplete(100);
			updater.complete();
		}
	}

	/**
	 * Update size.
	 *
	 * @param size
	 *            the size
	 */
	protected abstract void updateSize(long sizeBytes);

	/**
	 * Notify by mail.
	 */
	private void notifyByMail() {
		logger.debug("[CALLED],[notifyByMail],\"notifyByMail()\"");
		try (ProfilePoint _notifyByMail = ProfilePoint.profileAction("ProfAction_notifyByMail")) {
//			notificationService.sendNotificationEmail(getEmailTo(), getEmailSubject(), getNotificationType(),
//					getModelMap());
		} catch (Exception e) {
			Utils.logException(logger, e, "While sending mail to user");
		}
	}

	/**
	 * Gets the notification type.
	 *
	 * @return the notification type
	 */
	protected abstract String getNotificationType();

	/**
	 * Gets the email to.
	 *
	 * @return the email to
	 */
	protected abstract String getEmailTo();

	/**
	 * Gets the email subject.
	 *
	 * @return the email subject
	 */
	protected abstract String getEmailSubject();

	/**
	 * Execute internal.
	 *
	 * @return the composite report
	 * @throws Exception
	 *             the exception
	 */
	protected abstract CompositeReport executeInternal() throws Exception;

	/**
	 * Gets the model map.
	 *
	 * @return the model map
	 */
	protected abstract Map<String, Object> getModelMap();

	/**
	 * Gets the container.
	 *
	 * @return the container
	 */
	protected abstract String getContainer();

	/**
	 * Gets the worker.
	 *
	 * @param updater
	 *            the updater
	 * @return the worker
	 * @throws JSONException
	 *             the JSON exception
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws InstantiationException
	 *             the instantiation exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws NoSuchMethodException
	 *             the no such method exception
	 * @throws SecurityException
	 *             the security exception
	 */
	public static ReportWorker getWorker(WorkItemUpdater updater)
			throws JSONException, ClassNotFoundException, InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		logger.debug("[CALLED],[getWorker],\"getWorker(updater=" + updater + ")\"");
		try (ProfilePoint _getWorker = ProfilePoint.profileAction("ProfAction_getWorker")) {
			JSONObject inputJson = new JSONObject(updater.getItem().getInputJson());
			@SuppressWarnings("unchecked")
			Class<? extends ReportWorker> worker = (Class<? extends ReportWorker>) Class
					.forName(inputJson.getString("class"));
			return worker.getDeclaredConstructor(GenericJSON.class, WorkItemUpdater.class)
					.newInstance(Utils.getJSonObject(inputJson.getString("input"), GenericJSON.class), updater);
		}
	}
}
