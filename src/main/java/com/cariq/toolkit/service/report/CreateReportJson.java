package com.cariq.toolkit.service.report;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateReportJson.
 */
public class CreateReportJson {

	/** The from date. */
	@NotEmpty(message = "From date should not be empty")
	private String fromDate;

	/** The to date. */
	@NotEmpty(message = "To date should not be empty")
	private String toDate;

	/** The type. */
	@NotEmpty(message = "Type should not be empty")
	private String type;

	@NotNull(message = "Owner Type should not be null")
	private String ownerType;

	@NotNull(message = "Owner Id should not be null")
	private String ownerId;

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * @param fromDate
	 * @param toDate
	 * @param type
	 * @param ownerType
	 * @param ownerId
	 */
	public CreateReportJson(String fromDate, String toDate, String type, String ownerType, String ownerId) {
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.type = type;
		this.ownerType = ownerType;
		this.ownerId = ownerId;
	}

	/**
	 * 
	 */
	public CreateReportJson() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	// Default Constructor
	
}
