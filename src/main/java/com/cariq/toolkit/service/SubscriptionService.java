package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.model.Subscription.Subscription_In;
import com.cariq.toolkit.model.Subscription.Subscription_Out;

/**
 * The Interface SubscriptionService.
 */
public interface SubscriptionService {

	/**
	 * Adds the subscription.
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson addSubscription(Subscription_In params);

	/**
	 * Gets the subscription.
	 * @param subscriptionName
	 * @return
	 */
	public Subscription_Out getSubscription(String subscriptionName);

	/**
	 * Update subscription.
	 * @param params
	 * @param subscriptionName
	 * @return
	 */
	public ResponseJson updateSubscription(Subscription_In params, String subscriptionName);

	/**
	 * Delete subscription.
	 * @param subscriptionName
	 * @return
	 */
	public ResponseJson deleteSubscription(String subscriptionName);

	/**
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<Subscription_Out> getAllSubscriptions(int pageNo, int pageSize);
}