/**
 * 
 */
package com.cariq.toolkit.service.async;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.serviceimpl.async.WIHelper;

/**
 * @author hrishi
 *
 */
public abstract class WorkerBase implements Job {
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("WorkerBase");

	protected abstract void executeInternal(JobExecutionContext context, WorkItemUpdater updater, GenericJSON inputJSON) throws Exception ;
	
	/* (non-Javadoc)
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.debug("[CALLED],[execute],\"execute(context=" + context + ")\"");
		WorkItemUpdater updater = WIHelper.getUpdater(context);
		try {
			String inputJson = updater.getItem().getInputJson();
			GenericJSON json = null;
			if (inputJson != null) {
				logger.debug("Input Json," + inputJson);
				json = Utils.getJSonObject(inputJson, GenericJSON.class);
			}
			
			executeInternal(context, updater, json);

		} catch (Exception e) {
			Utils.logException(logger, e, "While executing WorkItem: " + this.getClass().getCanonicalName());
			updater.fail(Utils.getExceptionMessage(e));
			throw new JobExecutionException(e);
		}
		
	}

}
