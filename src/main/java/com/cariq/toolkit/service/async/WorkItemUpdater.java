package com.cariq.toolkit.service.async;

import com.cariq.toolkit.model.async.WorkItem;

/**
 * Communication back to the Executor
 * 
 * @author HVN
 *
 */
public interface WorkItemUpdater {
	WorkItem getItem();

	void start();

	void fail(String message);

	void complete();

	void setPercentComplete(int percentComplete);

	void refresh();

	void setOutputJson(String string);
}
