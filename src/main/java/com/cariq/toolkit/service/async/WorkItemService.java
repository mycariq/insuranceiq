package com.cariq.toolkit.service.async;

import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ResponseJson;

public interface WorkItemService {

	public ResponseJson addWorkItem(String workerClass, String inputJson, String type, String name,
			String descriptions);

	public ResponseJson addWorkItem(String workerClass, GenericJSON inputJson, String type, String name,
			String descriptions);

	public Object getListOfWorkItem(String type, String status, int pageNo, int size);

	public Object getWorkItemByUUID(String uuid);

	public Object getListOfWorkItemByType(String type, int pageNo, int size);

	public Object getListOfWorkItemByStatus(String status, int pageNo, int size);

	public Object getListOfWorkItem(int pageNo, int size);

	public Object getListOfWorkItemType();

	public Object getListOfWorkItemStatus();

	public ResponseJson addWorkItem(String workerClass, String inputJson, String type, String name, String descriptions,
			User user);

}
