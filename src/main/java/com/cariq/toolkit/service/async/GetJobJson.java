package com.cariq.toolkit.service.async;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;

import com.cariq.toolkit.utils.PeriodicEvent;
import com.cariq.toolkit.model.async.CarIqJob;
import com.cariq.toolkit.model.async.WorkItem;

@RooJavaBean
public class GetJobJson {

    /* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GetJobJson [jobName=" + jobName + ", lastRunDate="
				+ lastRunDate + ", frequency=" + frequency + ", scheduledTime="
				+ scheduledTime + ", id=" + id + ", frequencyUnit="
				+ frequencyUnit + ", nextScheduledTime=" + nextScheduledTime
				+ ", lastRunStatus=" + lastRunStatus
				+ ", lastRunDurationSecond=" + lastRunDurationSecond
				+ ", progress=" + progress + "]";
	}

	/**
     */
    private String jobName;

    /**
     */
    private String lastRunDate;

    /**
     */
    private Integer frequency;

    /**
     */
    private String scheduledTime;

    /* *
     */
    private Long id;

    public GetJobJson() {
    }

    /**
     */
    private String frequencyUnit;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date nextScheduledTime;

    /**
     */
    private String lastRunStatus;

    /**
     */
    private int lastRunDurationSecond;
    
    /**
     */
    private int progress;

	public GetJobJson(CarIqJob carIqJob, WorkItem workItem) {
		this.id = carIqJob.getId();
		this.jobName = carIqJob.getJobName();
		this.frequency = carIqJob.getFrequency();
		if (carIqJob.getLastRunDate() != null) {
			this.lastRunDate = carIqJob.getLastRunDate().toString();
		}
		this.scheduledTime = carIqJob.getScheduledTime().toString();
		this.frequencyUnit = PeriodicEvent.translateFrequencyUnit(carIqJob
				.getFrequencyUnit());
		this.nextScheduledTime = carIqJob.getNextRunTime();
		if (workItem != null) {
			this.lastRunStatus = workItem.getStatus().toString();
			this.progress = workItem.getProgress();
			Date startTime = workItem.getStartTime(), endTime = workItem
					.getEndTime();
			if (startTime != null && endTime != null)
				this.lastRunDurationSecond = (int) ((endTime.getTime() - startTime
						.getTime()) / 1000);
		}
	}

 }
