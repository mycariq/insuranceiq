package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.model.Tag.Tag_Out;
import com.cariq.toolkit.model.TaggedProxy.TagAndProxyDetails_In;
import com.cariq.toolkit.model.TaggedProxy.TaggedProxy_In;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_Out;

/**
 * The Interface TagService.
 */
public interface TaggedProxyService {

	/**
	 * Tag the specific proxy-object
	 * @param proxyName
	 * @param tagName
	 * @return
	 */
	public ResponseWithIdJson tagProxy(TaggedProxy_In params);
	
	/**
	 * Create proxy-object and tag with given name
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson createAndTagProxy(TagAndProxyDetails_In params);
	
	/**
	 * Remove tag from tagged proxy-object
	 * @param proxyName
	 * @param tagName
	 * @return
	 */
	public ResponseJson untagProxy(TaggedProxy_In params);
	
	/**
	 * Remove the tag from proxy-object identified by proxy-details.
	 * @param params
	 * @return
	 */
	public ResponseJson untagProxyByProxyDetails(TagAndProxyDetails_In params);
	
	/**
	 * Get all tags assigned to specific proxy-object
	 * @param proxyName
	 * @return
	 */
	public List<Tag_Out> getTagsByProxyObject(String domainName, Long objectId, String objectType);
	
	/**
	 * Get all proxy-objects tagged with a specific tag
	 * @param tagName
	 * @return
	 */
	public List<ProxyObject_Out> getProxyObjectsByTagname(String tagname);
}