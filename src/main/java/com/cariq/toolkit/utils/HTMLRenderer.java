package com.cariq.toolkit.utils;

import java.io.IOException;
import java.util.List;
import java.util.zip.ZipOutputStream;

// TODO: Auto-generated Javadoc
/**
 * The Class HTMLRenderer.
 */
public class HTMLRenderer implements ReportRenderer {

	/** The writer. */
	private ZipOutputStream writer;

	/**
	 * Instantiates a new HTML renderer.
	 *
	 * @param writer the writer
	 */
	public HTMLRenderer(ZipOutputStream writer) {
		this.writer = writer;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.workitem.ReportRenderer#writeHeader(java.util.List)
	 */
	@Override
	public void writeHeader(List<String> header) throws IOException {
		throw new RuntimeException("Yet to implement");
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.workitem.ReportRenderer#writeData(java.util.List)
	 */
	@Override
	public void writeData(List<String> data) throws IOException {
		throw new RuntimeException("Yet to implement");
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.workitem.ReportRenderer#open()
	 */
	@Override
	public void open() throws IOException {
		throw new RuntimeException("Yet to implement");
	}

	/* (non-Javadoc)
	 * @see java.lang.AutoCloseable#close()
	 */
	@Override
	public void close() throws IOException {
		throw new RuntimeException("Yet to implement");
	}
}
