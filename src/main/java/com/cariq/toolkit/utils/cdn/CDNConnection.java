package com.cariq.toolkit.utils.cdn;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Configurable;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;

/**
 * Similar to JDBCConnection - to be used for doing operations on the CDN
 * 
 * @author hrishi
 *
 */


@Configurable
public class CDNConnection implements AutoCloseable {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CDNConnection");

	@Resource(name = "configProperties")
	private Properties configProperties;

	CDNDriver driver;

	/**
	 * Private Constructor - can't be instantiated from outside
	 * @return 
	 */
	private void initialize(String driverClass, String url, String userName, String password) {
		try {
			logger.debug("BEGIN: Initializing Driver:" + driverClass);
			Class<CDNDriver> driverClazz = (Class<CDNDriver>) Class.forName(driverClass);
			driver = driverClazz.newInstance();
			driver.initialize(url, userName, password);
			logger.debug("END: Initializing Driver:" + driverClass);

		} catch (Exception e) {
			Utils.handleException(e);
		}
	}
	
	 @PostConstruct
	private void initialize() {
		initialize(configProperties.getProperty("cdn.driverClassName"), configProperties.getProperty("cdn.url"),
					configProperties.getProperty("cdn.username"), configProperties.getProperty("cdn.password"));
	}
	
	public CDNConnection() {
	}

	public CDNConnection(String driverClass, String url, String userName, String password) {
		initialize(driverClass, url, userName, password);
	}

	/**
	 * Create a CDNConnection using the Driver and Connection Specs
	 * 
	 * @param driverClass
	 * @param url
	 * @return
	 */
	static CDNConnection open(String driverClass, String url, String userName, String password) {
		CDNConnection con = new CDNConnection(driverClass, url, userName, password);
		return con;
	}

	/**
	 * Open CDNConnection from CDN Connection String
	 * 
	 * @return
	 */
	public static CDNConnection open() {
		return new CDNConnection();
	}

	/**
	 * Upload file to the given container from local filesystem
	 * 
	 * @param containerName
	 * @param fileName
	 * @param localFilePath
	 * @param contentType
	 * @throws Exception
	 */
	public void uploadFile(String containerName, String fileName, String localFilePath, String contentType)
			throws Exception {
		logger.debug("BEGIN: Uploading File, Container: " + containerName + "File Name: " + fileName);

		File localFile = new File(localFilePath);
		InputStream inputStream = new FileInputStream(localFile);
		uploadFile(containerName, fileName, inputStream, contentType, localFile.length());
		
		logger.debug("END: Uploading File, Container: " + containerName + "File Name: " + fileName);
	}

	/**
	 * Raw upload Function to work with Stream
	 * 
	 * @param containerName
	 * @param fileName
	 * @param fileStream
	 * @param contentType
	 * @param fileSize
	 * @throws FileNotFoundException
	 */
	public void uploadFile(String containerName, String fileName, InputStream fileStream, String contentType,
			long fileSize) throws Exception {
		String publicUrl = "publicUrl";
		
		if (configProperties != null)
			publicUrl = configProperties.getProperty("cdn.publicUrl");

		// logger.debug("BEGIN: Uploading File, Container: " + containerName + "File Name: " + fileName);
		logger.debug("Uploading File to PUBLIC URL: " + publicUrl + "/" + containerName + "/" + fileName);

//		try {
//			driver.deleteFile(containerName, fileName);
//			Thread.sleep(100); // wait for some time till operation is complete any offline work
//		} catch (Exception e) {
//			//logger.info("Unable to delete file: " + containerName + "/" + fileName);
//			Utils.logException(logger, e, "Deleting File from CDN: " + containerName + "/" + fileName);
//		}
		driver.uploadFile(containerName, fileName, fileStream, contentType, fileSize);
		logger.debug("Uploaded File to PUBLIC URL: " + publicUrl + "/" + containerName + "/" + fileName);
	}

	public void deleteFile(String containerName, String fileName) throws Exception {
		String publicUrl = "publicUrl";
	
		if (configProperties != null)
			publicUrl = configProperties.getProperty("cdn.publicUrl");

		logger.debug("BEGIN: Deleting File:" + publicUrl + "/" + containerName + "/" + fileName);
		driver.deleteFile(containerName, fileName);
		logger.debug("Deleted File from PUBLIC URL: " + publicUrl + "/" + containerName + "/" + fileName);
	}

	// More to follow

	@Override
	public void close() {
		try {
			driver.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
