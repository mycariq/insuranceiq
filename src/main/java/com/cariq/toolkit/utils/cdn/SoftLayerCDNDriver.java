package com.cariq.toolkit.utils.cdn;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.cariq.toolkit.utils.API;
import com.cariq.toolkit.utils.CarIQFileUtils.CarIQFileHolder;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;

public class SoftLayerCDNDriver implements CDNDriver {
	static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("SoftlayerCDNDriver");

	String url;
	String username;
	String password;
	
	// specific to Softlayer - to get the url for operation
	String publicUrl;
	String token;
	
	@Override
	public void initialize(String url, String username, String password) throws Exception {
		this.url = url;
		this.username = username;
		this.password = password;
				
		
		// Parse string to get publicUrl and token and store
		getToken();
	}

	private void getToken() {
		API api = new API("GET", url, null);
		api.addHeader("X-Auth-User", username);
		api.addHeader("X-Auth-Key", password);
		
		try {
			ResponseEntity<Object> response = api.invoke();
			HttpHeaders headers =  response.getHeaders();
			token = headers.get("X-Auth-Token").get(0);
			publicUrl = headers.get("X-Storage-Url").get(0);
			logger.debug("Obtained Token: " + token + " and url: " + publicUrl);
		} catch (Exception e) {
			Utils.handleException(e);
		}
	}

	@Override
	public String getUrl() {
		return url;
	}
	
	@Override
	public String getUserName() {
		return username;
	}
	
	@Override
	public String getPassword() {
		return password;
	}


	@Override
	public void uploadFile(String containerName, String fileName, InputStream inputStream, String contentType,
			long length) throws Exception {
		// store in a local file and upload and delete the local file :(
		try (CarIQFileHolder fileHolder = new CarIQFileHolder(inputStream)) {
			uploadFile(containerName, fileName, fileHolder.getFile(), contentType);
		}
	}

	@Override
	public void deleteFile(String containerName, String fileName) throws Exception {
		String fileUrl = publicUrl + "/" + containerName + "/" + fileName;
		logger.debug("Deleting file : " + fileUrl); // Should be 204
		API deleteAPI = new API("DELETE", fileUrl, null);
		deleteAPI.addHeader("X-Auth-Token", token);
		
		ResponseEntity<Object> response = deleteAPI.invoke();
		// Request is lazily fired whenever you need to obtain information about response.
		HttpStatus responseCode = response.getStatusCode();

		logger.debug("File Delete Response Code (expected 204): " + responseCode); // Should be 204
		logger.debug("File Delete Response Message (expected No Content): " +  response);
	}

	@Override
	public void uploadFile(String containerName, String fileName, String localFilePath, String contentType)
			throws Exception {
		String fileUrl = publicUrl + "/" + containerName + "/" + fileName;
		uploadUsingRawHTTPUrlConnection(fileUrl, token, localFilePath, contentType);
	}
	
	/**
	 * Remember, it is not a multi-part file - simply push binary data onto the url
	 * Too simple to be true
	 * @param url
	 * @param localFilePath
	 * @param contentType
	 * @throws Exception
	 */
	private static void uploadUsingRawHTTPUrlConnection(String url, String authToken, String localFilePath, String contentType) throws Exception{
		String charset = "UTF-8";
		File binaryFile = new File(localFilePath);
		
		logger.debug("Uploading local file " + localFilePath + " size("+ binaryFile.length() +") to url: " + url);


		HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod("PUT");
		connection.setRequestProperty("X-Auth-Token", authToken);
		try (
		    OutputStream output = connection.getOutputStream();
		    PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);
		) {
		    Files.copy(binaryFile.toPath(), output);
		    output.flush(); // Important before continuing with writer!
		}

		// Request is lazily fired whenever you need to obtain information about response.
		int responseCode = connection.getResponseCode();

		logger.debug("File Upload Response Code (expected 201): " + responseCode); // Should be 201
		logger.debug("File Upload Response Message:" +  connection.getResponseMessage());
	}
	
	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub

	}

}
