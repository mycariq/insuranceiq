package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

public class JsonValidator {
	@SuppressWarnings("serial")
	public static void validate(BindingResult result) {
		// if does not contains error
		if (!result.hasErrors())
			return;
		final List<String> errorMessages = new ArrayList<String>();
		// get error messages
		for (ObjectError error : result.getFieldErrors()) {
			errorMessages.add(error.getDefaultMessage());
		}
		// return response with errors
		throw new CariqException() {

			@Override
			public List<String> getMsg() {
				return errorMessages;
			}
		};
	}
}
