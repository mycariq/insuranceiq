package com.cariq.toolkit.utils;

import java.io.IOException;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Interface ReportRenderer.
 */
public interface ReportRenderer extends AutoCloseable {
	
	/**
	 * Write header.
	 *
	 * @param header the header
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void writeHeader(List<String> header) throws IOException;

	/**
	 * Write data.
	 *
	 * @param data the data
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void writeData(List<String> data) throws IOException;

	/**
	 * Open.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void open() throws IOException;

}
