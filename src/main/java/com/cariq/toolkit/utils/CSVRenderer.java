package com.cariq.toolkit.utils;

import java.io.IOException;
import java.util.List;
import java.util.zip.ZipOutputStream;

// TODO: Auto-generated Javadoc
/**
 * The Class CSVRenderer.
 */
public class CSVRenderer implements ReportRenderer {

	/** The writer. */
	private ZipOutputStream writer;
	
	/** The Constant logger. */
	private static final CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CSVRenderer");

	/**
	 * Instantiates a new CSV renderer.
	 *
	 * @param writer the writer
	 */
	public CSVRenderer(ZipOutputStream writer) {
		this.writer = writer;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.workitem.ReportRenderer#writeHeader(java.util.List)
	 */
	@Override
	public void writeHeader(List<String> header) throws IOException {
		String csvHeader = "";
		for (String colName : header) {
			if (colName == null)
				colName = "";
			csvHeader += ",\"" + colName + "\"";
		}
		writer.write((csvHeader.replaceFirst(",", "") + "\n").getBytes());
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.workitem.ReportRenderer#writeData(java.util.List)
	 */
	@Override
	public void writeData(List<String> row) throws IOException {
		String csvRow = "";
		for (String cell : row) {
			csvRow += ",\"" + cell + "\"";
		}
		writer.write((csvRow.replaceFirst(",", "") + "\n").getBytes());
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.workitem.ReportRenderer#open()
	 */
	@Override
	public void open() throws IOException {

	}

	/* (non-Javadoc)
	 * @see java.lang.AutoCloseable#close()
	 */
	@Override
	public void close() throws IOException {
		writer.closeEntry();
	}

}
