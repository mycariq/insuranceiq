package com.cariq.toolkit.utils;

public interface CarIQLogger {
	void fatal(String message);

	void error(String message);

	void warn(String message);

	void info(String message);

	void debug(String message);

	CarIQLogger getLogger(String innerContext);
}
