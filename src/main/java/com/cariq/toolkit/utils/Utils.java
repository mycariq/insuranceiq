package com.cariq.toolkit.utils;

import static org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.LogManager;
import org.apache.velocity.app.VelocityEngine;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.LocalDateTime;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.client.RestTemplate;

import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.wwwservice.core.GeoTile;
import com.cariq.toolkit.utils.wwwservice.core.Region;
import com.google.common.base.Strings;

/**
 * The Class Utils.
 */
public class Utils {

	/*
	 * Image path
	 */
	/** The root path. */
	public static String rootPath = "https://ciqbspseastrgdef.blob.core.windows.net/cdn/"; //@TMPL public static String rootPath = "[% ROOT.servers.fileServer.endPoint %]/cdn/"; @TMPL

	/** The next error sending interval. */
	public static int nextErrorSendingInterval = 10080;

	/** The next battery alert sending interval. */
	public static int nextBatteryAlertSendingInterval = 1440;

	/** The base url */
	public static String baseUrl = "http://app.mycariq.com"; // @TMPL public static String baseUrl = "[% ROOT.servers.webapp.url %]"; @TMPL

	public static String companyName = "CarIQ";//@TMPL public static String companyName="[% ROOT.environment.companyName %]"; @TMPL

	public static String companyLogo = "http://mycariq.com/images/logo_dark.png";//@TMPL public static String companyLogo="[% ROOT.environment.companyLogo %]"; @TMPL

	//	/** The base url on ecu. */
	//	public static String baseUrlOnEcu = "http://app.mycariq.com";

	/** The admin user. */
	public static String adminUser = "cariq_archived";

	// Zero Voltage is a wrong packet - no alert for that!!
	public static Double BATTERY_VOLTAGE_ZERO = 0.01;

	// radius of earth
	/** The Constant RADIUS. */
	public static final double RADIUS = 6371.0;

	// distance less than 2 meters is considered coincident point
	/** The Constant EPSILON_DISTANCE. */
	public static final double EPSILON_DISTANCE = 0.002;

	// distance not calculated is UNDEFINED_DISTANCE
	/** The Constant UNDEFINED_DISTANCE. */
	public static final double UNDEFINED_DISTANCE = -65535.00;

	// A packet that is older than 5 minutes will be considered backlog
	/** The Constant BACKLOG_DURATION_THRESHOLD_MINUTES. */
	public static final int BACKLOG_DURATION_THRESHOLD_MINUTES = 10;

	/** The Constant BATTERY_ALERT_VOLTAGE. */
	public static final double BATTERY_ALERT_VOLTAGE = 11.0;

	/**
	 * The Constant BATTERY_LOWER_NO_ALERT_VOLTAGE - below this, ALERT is not to
	 * be given.
	 */
	public static final double BATTERY_LOWER_NO_ALERT_VOLTAGE = 1.0;

	// Both are use for email which is used in Notification.
	/** The Constant USER_EMAIL. */
	public static final String USER_EMAIL = "userEmail";

	/** The Constant EMAIL_SUBJECT. */
	public static final String EMAIL_SUBJECT = "emailSubject";

	/** The Constant mailToSupport. */
	public static final String mailToSupport = "eagles@mycariq.com";// @TMPL public static final String mailToSupport = "[% ROOT.servers.email.egales %]"; @TMPL

	// 0.5 km per hour
	/** The Constant EPSILON_SPEED. */
	public static final double EPSILON_SPEED = 0.5;

	// speed higher than 255 not supported
	/** The Constant MAX_SPEED. */
	public static final double MAX_SPEED = 255.0;

	// battery voltage decrease by 0.2
	/** The Constant BATTERY_VOLTAGE_DECREASE. */
	public static final double BATTERY_VOLTAGE_DECREASE = 0.2;

	// India time zone.
	/** The Constant INDIA_TIME_ZONE. */
	public static final String INDIA_TIME_ZONE = "Asia/Kolkata";
	public static final String UTC = "UTC";

	private static final String VRMbaseUrl = "https://vrm.mycariq.com"; // @TMPL private static final String VRMbaseUrl = "[% ROOT.servers.vrmwebapp.url %]"; @TMPL

	//	private static final String VRMbaseUrlOnEcu = "https://vrm.mycariq.com";

	public static final Double MAX_INCREMENTAL_DISTANCE = 100.00; // km

	public static final double MIN_DISTANCE_TOLERANCE = 5.0; // km

	public static final int LOW_RESOLUTION_POINTS = 100;

	public static final int MEDIUM_RESOLUTION_POINTS = 6 * LOW_RESOLUTION_POINTS;

	public static final int HIGH_RESOLUTION_POINTS = 6 * MEDIUM_RESOLUTION_POINTS;

	public static final int ONE_MB_IN_BYTES = 1048576;

	public static final int MAX_PREVIOUS_DAYS = 3;

	public static final String V1DEVICE = "V1DEVICE";

	public static final String V3DEVICE = "V3DEVICE";

	public static final String PROXIMITY_THRESHOLD = "PROXIMITY_THRESHOLD";

	private static final String TEST_CAR_IDS = "cariqTestCars";

	/** RESOLUTION_HIGH for web UI. */
	public static final String RESOLUTION_HIGH = "HIGH";

	public static final double MAX_TRAVEL_HOURS = 24.0; // Travel time in hours.

	public static final String SINOCASTLE = "SINOCASTLE";

	public static final double MIN_RPM_THRESHOLD = 100.0; // less than 100 rpm
	// means IC Engine
	// not running

	public static final int MIN_SPEED_THRESHOLD = 5; // less than 5km per hour
	// is considered idle

	public static final int MIN_ECONOMY_SPEED = 35;
	public static final int MAX_ECONOMY_SPEED = 75;

	public static final String ECONOMY_SCORE = "EconomyScore";

	public static final String WRONG_GEAR_PERCENT = "WrongGearPercent";
	public static final String IDLING_PERCENT = "IdlingPercent";
	public static final String NON_ECONOMIC_DRIVE_PERCENT = "NonEconomicDrivePercent";
	public static final String SPEED_STDDEV = "SpeedStandardDeviation";

	public static final double DEFAULT_ECONOMY_SCORE = 6.0;

	public static final String FUEL_COST = "FuelCost";
	public static final String COST_SAVING = "CostSaving";

	public static final Region PuneRegion = new Region(new GeoTile(18.24, 73.3), new GeoTile(18.45, 74.0));
	public static final Region MaharashtraRegion = new Region(new GeoTile(15.5, 72.5), new GeoTile(22, 80.5));
	public static final Region IndiaRegion = new Region(new GeoTile(7.53, 71.8), new GeoTile(31.2, 85.1));

	public static final Integer FRONT_TYRE_PRESSURE = 32;

	public static final Integer REAR_TYRE_PRESSURE = 32;

	public static final double MILAGE = 15.0; // default value

	public static final double FUEL_PRICE = 70.0; // original default value

	public static final long PACKET_THROUGHPUT_THRESHOLD = 5000;

	public static final double MIN_DAILY_DISTANCE_THRESHOLD = 1.0;
	public static final double MIN_DAILY_AVERAGE_SPEED_THRESHOLD = 5;

	public static final long MIN_DAILY_MOVING_PACKETS = 5L;

	public static final String DESC = "desc";
	public static final String DUMMY_REG_NO = "UNKNOWN_";

	public static final double EPSILON_RECORDABLE_DISTANCE = 0.05; // 50 meters
	public static final String NA = "Not Available";

	public static final String SECURITY_TOKEN = "SecurityToken";

	public static final String LAST_DAY = "LAST_DAY", LAST_WEEK = "LAST_WEEK", LAST_MONTH = "LAST_MONTH",
			LAST_YEAR = "LAST_YEAR";

	// Constant Times
	public static final int dayHours = 24, weekHours = dayHours * 7, monthHours = dayHours * 30,
			yearHours = dayHours * 365;
	public static final int hourMinutes = 60, dayMinutes = 24 * hourMinutes, weekMinutes = dayMinutes * 7,
			monthMinutes = dayMinutes * 30, yearMinutes = dayMinutes * 365;
	public static final int minuteSeconds = 60, hourSeconds = hourMinutes * 60, daySeconds = dayMinutes * 60,
			weekSeconds = weekMinutes * 60, monthSeconds = monthMinutes * 60, yearSeconds = yearMinutes * 60;
	public static final int minuteMiliSeconds = minuteSeconds * 1000, hourMiliSeconds = hourSeconds * 1000,
			dayMiliSeconds = daySeconds * 1000, weekMiliSeconds = weekSeconds * 1000;

	public static final String PARAM = "Parameter";
	public static final String DIAGNOSIS = "Diagnosis";
	public static final String PARAMETER = "Parameter";
	public static final String DESCRIPTION = "Description";
	public static final String VALUE = "Value";

	// Color Coded Diagnostics
	public static final String DIAG_GOOD = "GOOD";
	public static final String DIAG_OK = "OK";
	public static final String DIAG_BAD = "BAD";

	public static final String DATE = "Date";

	// Authentication type
	public static final String AUTH_BASIC = "BASIC_AUTH";
	
	// Special class name (fqdnClass) for tagged_proxy
	public static final String TAGGEDPROXY_FQDNCLASS = "TAGGED_PROXY";
	
	// Role names
	public static final String ADMIN_ROLE = "ROLE_ADMIN";
	public static final String USER_ROLE = "ROLE_USER";
	public static final String SUPPORT_ADMIN_ROLE = "ROLE_SUPPORT_ADMIN";
	public static final String BAGIC_ADMIN_ROLE = "ROLE_BAGIC_ADMIN";
	public static final String FLEET_ADMIN_ROLE = "ROLE_FLEET_ADMIN";

	public static final String XPRESSBEE_URL_KEY = "xpressBeesTrackingUrl";
	public static final String INDIA_POST_URL_KEY = "indiaPostTrackingUrl";

	public static final Integer TRUE = 1;

	public static final double DEFAULT_TANK_CAPACITY = 45.42;

	public static final Integer DEFAULT_GEAR_COUNT = 5; // In litre

	//W4IQ Query type
	public static String OWNER_BASED = "OwnerBased";

	private static final String INFINITY = "infinity";

	private static final Double MAX_LAT_THRESHOLD = 180.0;

	private static final Double MAX_LONG_THRESHOLD = 180.0;

	private static final Double MIN_VOLTAGE = 1.0;

	private static final Double MAX_VOLTAGE = 50.0;

	public static final Long DAYS_COUNT = 24 * 60 * 60 * 1000L;

	public static final Double DEFAULT_BATTERY_VOLTAGE = 14.0;

	public static final String ROLE_FLEET_ADMIN = "ROLE_FLEET_ADMIN";

	public static final String ROLE_GROUP_ADMIN = "ROLE_GROUP_ADMIN";

	public static final String CarId_Query = "CarId_Query";

	public static final String CarId = "CarId";

	public static final String ALL = "All";

	public static final String GENERIC = "generic";

	/** The Constant operatorRole. */
	public static final String ROLE_GROUP_OPERATOR = "ROLE_GROUP_OPERATOR";

	public static final String USER_FAMILY = "user";
	
	public static final String SYSTEM_FAMILY = "system";

	public static final String All_FLEET_CARS = "All fleet cars";
	
	public static final String ACTIVE = "Active";
	
	public static final String INACTIVE = "In-Active";

	/** The con properties. */
	@Resource(name = "configProperties")
	private Properties conProperties;
	
	/**
	 * Parses the date.
	 *
	 * @param dateStr
	 *            the date str
	 * @return the date
	 */
	public static Date parseDate(String dateStr) {
		return fromTimeString(dateStr, UTC, new String[] { "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd" });
		//
		//		try {
		//			if (dateStr.equalsIgnoreCase("NOW"))
		//				return new Date();
		//
		//			String[] datePatterns = new String[] { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss" };
		//			return DateUtils.parseDate(dateStr, datePatterns);
		//		} catch (ParseException e) {
		//			Utils.handleException("Invalid date format");
		//		}
		//		return null;
	}

	/**
	 * Change date format.
	 *
	 * @param date
	 *            the date
	 * @return the string
	 */
	public static String changeDateFormat(Date date) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("dd MMM yyyy hh:mm a");
		DateTime dateTime = new DateTime(date);
		String dateString = fmt.print(dateTime);
		return dateString;
	}

	/**
	 * Format date in user timezone
	 * 
	 * @param date
	 * @param timeZone
	 * @return
	 */
	public static String convertTo_dd_MMM_yyyy_hh_mm_a(Date date, String timeZone) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm a");
		if (timeZone == null) {
			sdf.setTimeZone(TimeZone.getTimeZone(INDIA_TIME_ZONE));
		} else {
			sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		}
		String dateString = sdf.format(date);
		return dateString;
	}

	/**
	 * Change date format optimize.
	 *
	 * @param date
	 *            the date
	 * @return the string
	 */
	public static String changeDateFormatOptimize(Date date) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
		DateTime dateTime = new DateTime(date);
		String dateString = fmt.print(dateTime);
		return dateString;
	}

	/**
	 * Convert toyyyymmdd format.
	 *
	 * @param date
	 *            the date
	 * @return the string
	 */
	public static String convertToyyyymmdd_hhmmssFormat(Date date) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		DateTime dateTime = new DateTime(date);
		String dateString = fmt.print(dateTime);
		return dateString;
	}

	/**
	 * Convert date for mailing.
	 *
	 * @param date
	 *            the date
	 * @return the string
	 */
	public static String convertDateForMailing(Date date) {
		return convertDateForMailing(date, null);
	}

	/*
	 * convert date for sms
	 */
	/**
	 * Convert date for mailing.
	 *
	 * @param date
	 *            the date
	 * @return the string
	 */
	public static String convertDateForMailing(Date date, String timezone) {
		if (timezone == null)
			timezone = INDIA_TIME_ZONE;

		return toTimeString(date, timezone, "EEE, dd MMM yyyy ") + "at " + toTimeString(date, timezone, "hh:mm a");
	}

	/**
	 * Parses the date format.
	 *
	 * @param dateStr
	 *            the date str
	 * @return the date
	 */
	public static Date parseDateFormatWithMiliSecond(String dateStr) {
		return fromTimeString(dateStr, UTC,
				new String[] { "yyyy-MM-dd HH:mm:ss.S", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd" });
		//
		//		// support for now
		//		if (dateStr.equalsIgnoreCase("NOW"))
		//			return new Date();
		//
		//		// Is Joda faster?
		//		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.S");
		//		DateTime dt = fmt.parseDateTime(dateStr);
		//		return dt.toDate();
	}

	/**
	 * Parses the date format.
	 *
	 * @param dateStr
	 *            the date str
	 * @return the date
	 */
	public static Date parseDateFormat(String dateStr) {
		return parseDateFormat(dateStr, null);
	}

	/**
	 * Parses the date format.
	 *
	 * @param dateStr
	 *            the date str
	 * @return the date
	 */
	public static Date parseDateFormat(String dateStr, String timeZone) {
		return fromTimeString(dateStr, dateStr, new String[] { "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd" });
	}

	/**
	 * Parsing DateTime String without throwing exception
	 * 
	 * @param dateTimeString
	 * @return
	 */
	public static Date parseDateTime(String dateTimeString) {
		return fromTimeString(dateTimeString, UTC, new String[] { "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd" });
		//
		//		if (dateTimeString == null)
		//			return null;
		//
		//		try {
		//			if (dateTimeString.equalsIgnoreCase("NOW"))
		//				return new Date();
		//
		//			String[] datePatterns = new String[] { "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd" };
		//			// <HVN> if it is a pure date, still it should work with yyyy-MM-dd
		//			return DateUtils.parseDate(dateTimeString, datePatterns);
		//		} catch (ParseException e) {
		//			Utils.handleException("Date Parsing", e);
		//		}
		//
		//		return null;
	}

	/**
	 * Validate battery.
	 *
	 * @param voltage
	 *            the voltage
	 * @return String
	 */
	public static String validateBattery(Double voltage) {
		if (voltage == null) {
			return "Good"; // <HVN> be optimistic. Say Good when we don't know
		} else if (voltage >= 12.6) {
			return "Good";
		} else if (voltage < 12.6 && voltage >= 12) {
			return "OK";
		} else {
			return "Bad";
		}

	}

	/**
	 * Get battery status based on voltage value.
	 * 
	 * @param voltage
	 * @return
	 */
	public static String validateBattery(String voltage) {
		Double batteryVoltage = voltage != null ? Utils.downCast(Double.class, voltage) : null;
		return validateBattery(batteryVoltage);
	}

	/**
	 * Parses the time format.
	 *
	 * @param dateStr
	 *            the date str
	 * @return the date
	 */
	public static Date parseTimeFormat(String dateStr) {
		Date date = null;
		try {
			// support for now
			if (dateStr.equalsIgnoreCase("NOW"))
				return new Date();

			String[] datePatterns = new String[] { "HH:mm:ss" };
			date = DateUtils.parseDate(dateStr, datePatterns);
		} catch (ParseException e) {
			Utils.handleException("Invalid date format");
		}
		return date;
	}

	/*
	 * 
	 */
	/**
	 * Gets the class name.
	 *
	 * @param className
	 *            the class name
	 * @return the class name
	 */
	public static String getClassName(String className) {
		String onlyClassName = className.split("\\.")[5];
		return onlyClassName;
	}

	/*
	 * Pattern matching code
	 */
	/**
	 * Pattern matching.
	 *
	 * @param patternString
	 *            the pattern string
	 * @param validatingString
	 *            the validating string
	 * @return true, if successful
	 */
	public static boolean patternMatching(String patternString, String validatingString) {
		Pattern pattern;
		Matcher matcher;
		pattern = Pattern.compile(patternString);
		matcher = pattern.matcher(validatingString);
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Creates the sticker number.
	 *
	 * @return the string
	 */
	public static String createStickerNumber() {
		String stickerNumber = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
		return stickerNumber;
	}

	/**
	 * Creates the device number.
	 *
	 * @return the string
	 */
	public static String createDeviceNumber() {
		String deviceNumber = "G" + RandomStringUtils.randomAlphanumeric(9).toUpperCase();
		return deviceNumber;
	}

	/**
	 * Creates the bue tooth device number.
	 *
	 * @return the string
	 */
	public static String createBueToothDeviceNumber() {
		String deviceNumber = "B" + RandomStringUtils.randomAlphanumeric(9).toUpperCase();
		return deviceNumber;
	}

	/**
	 * Creates the blue tooth sticker number.
	 *
	 * @return the string
	 */
	public static String createBlueToothStickerNumber() {
		String stickerNumber = "CBT" + RandomStringUtils.randomAlphanumeric(7);
		return stickerNumber;
	}

	/**
	 * Total page count.
	 *
	 * @param noOfRecords
	 *            the no of records
	 * @param pageSize
	 *            the page size
	 * @return the int
	 */
	public static int totalPageCount(double noOfRecords, double pageSize) {
		double tpage = (double) noOfRecords / pageSize;
		int totalPages = (int) Math.ceil(tpage);
		return totalPages;
	}

	/**
	 * Gets the diff in months.
	 *
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the diff in months
	 */
	public static int getDiffInMonths(Date fromDate, Date toDate) {
		DateTime ToDate = new DateTime(toDate);
		DateTime FromDate = new DateTime(fromDate);
		int diffInMonths = Months.monthsBetween(FromDate, ToDate).getMonths();
		return diffInMonths;
	}

	/**
	 * Gets the diff in days.
	 *
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the diff in days
	 */
	public static int getDiffInDays(Date fromDate, Date toDate) {
		DateTime ToDate = new DateTime(toDate);
		DateTime FromDate = new DateTime(fromDate);
		int diffInDays = 0;
		if (fromDate.before(toDate) || fromDate.equals(toDate)) {
			diffInDays = Days.daysBetween(FromDate, ToDate).getDays();
		}
		return diffInDays;
	}

	/**
	 * Gets the diff in Minutes.
	 *
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the diff in Minutes - can be negative
	 */
	public static int getDiffInMinutes(Date after, Date before) {
		return Minutes.minutesBetween(new DateTime(before), new DateTime(after)).getMinutes();
	}

	/**
	 * Gets the diff in Hours.
	 *
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the diff in Hours - can be negative
	 */
	public static int getDiffInHours(Date after, Date before) {
		return Hours.hoursBetween(new DateTime(before), new DateTime(after)).getHours();
	}

	/**
	 * Find diference in days.
	 *
	 * @param fromdate
	 *            the fromdate
	 * @param toDate
	 *            the to date
	 * @return the int
	 */
	public static int findDiferenceInDays(Date fromdate, Date toDate) {
		int noOfDays = Days.daysBetween(new LocalDateTime(fromdate), new LocalDateTime(toDate)).getDays();
		return noOfDays;
	}

	/**
	 * Date by minute back.
	 *
	 * @param currentDate
	 *            the current date
	 * @param minutes
	 *            the minutes
	 * @return the date
	 */
	public static Date DateByMinuteBack(Date currentDate, int minutes) {
		DateTime date = new DateTime(currentDate);
		DateTime rangeDate = date.minusMinutes(minutes);
		return rangeDate.toDate();
	}

	/**
	 * Date by minute back.
	 *
	 * @param currentDate
	 *            the current date
	 * @param minutes
	 *            the minutes
	 * @return the date
	 */
	public static Date DateByHoursBack(Date currentDate, int hours) {
		DateTime date = new DateTime(currentDate);
		DateTime rangeDate = date.minusHours(hours);
		return rangeDate.toDate();
	}

	/**
	 * Trim space.
	 *
	 * @param registrationNumberForDisplay
	 *            the registration number for display
	 * @return the string
	 */
	public static String trimSpace(String registrationNumberForDisplay) {
		String value = registrationNumberForDisplay.replaceAll("\\s", "");
		return value;
	}

	/**
	 * Gets the message time.
	 *
	 * @param itsTimestamp
	 *            the its timestamp
	 * @param timezone
	 *            the timezone
	 * @return the message time
	 */
	public static String getMessageTime(Date itsTimestamp, String timezone) {
		Date date = getLocalTime(itsTimestamp, timezone);
		String returnDate = convertDateForMailing(date);
		return returnDate;
	}

	/**
	 * Gets the local time.
	 *
	 * @param itsTimeStamp
	 *            the its time stamp
	 * @param timezone
	 *            the timezone
	 * @return the local time
	 */
	public static Date getLocalTime(Date itsTimeStamp, String timezone) {
		DateTime date = new DateTime(itsTimeStamp);
		String sign = String.valueOf(timezone.charAt(0));
		String[] offset = timezone.substring(1).split(":");
		DateTime localDate = date;
		if (sign.equalsIgnoreCase("+")) {
			localDate = date.plusHours(Integer.parseInt(offset[0]));
			localDate = localDate.plusMinutes(Integer.parseInt(offset[1]));
		} else if (sign.equalsIgnoreCase("-")) {
			localDate = date.minusHours(Integer.parseInt(offset[0]));
			localDate = localDate.minusMinutes(Integer.parseInt(offset[1]));
		}
		return localDate.toDate();
	}

	/**
	 * Gets the startof day.
	 *
	 * @param itsTimeZone
	 *            the its time zone
	 * @return the startof day
	 */
	public static Date getStartofDay(String itsTimeZone) {
		DateTime now;
		if (itsTimeZone == null) {
			now = new DateTime(DateTimeZone.forID(INDIA_TIME_ZONE));
			return now.withTimeAtStartOfDay().toDate();
		}
		now = new DateTime(DateTimeZone.forID(itsTimeZone));
		return now.withTimeAtStartOfDay().toDate();
	}

	/**
	 * Gets the startof day.
	 *
	 * @param itsTimeZone
	 *            the its time zone
	 * @return the startof day
	 */
	public static Date getStartofDay(Date utcTimeStamp, String itsTimeZone) {
		DateTime retval;
		if (itsTimeZone == null)
			retval = new DateTime(utcTimeStamp, DateTimeZone.forID(INDIA_TIME_ZONE));

		retval = new DateTime(utcTimeStamp, DateTimeZone.forID(itsTimeZone));
		return retval.withTimeAtStartOfDay().toDate();
	}

	/**
	 * Gets the end of day.
	 *
	 * @param itsTimeZone
	 *            the its time zone
	 * @return the startof day
	 */
	public static Date getEndofDay(Date utcTimeStamp, String itsTimeZone) {
		DateTime retval;
		if (itsTimeZone == null)
			itsTimeZone = INDIA_TIME_ZONE;

		retval = new DateTime(utcTimeStamp, DateTimeZone.forID(itsTimeZone));
		retval = retval.plusDays(1);
		return retval.withTimeAtStartOfDay().minusMillis(1).toDate();
	}

	/**
	 * Gets the address.
	 *
	 * @param latitude
	 *            the latitude
	 * @param longitude
	 *            the longitude
	 * @return the address
	 */
	@SuppressWarnings("rawtypes")
	private static String getAddress(String latitude, String longitude) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		String uri = "https://maps.google.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude
				+ "&sensor=false";
		ETLHelper.logger.debug("Getting Location: " + uri);
		HashMap map = new HashMap();
		map = restTemplate.getForObject(uri, HashMap.class);
		List<Object> maplist = (List<Object>) map.get("results");
		// if no results found, return raw Latitude, Longitude
		if (maplist == null || maplist.isEmpty()) {
			Utils.logAnomaly(ETLHelper.logger, "ADDRESS_NOT_FOUND", 0,
					"status:" + map.get("status") + " latitude:" + latitude + " longitude:" + longitude);
			return getLocationJSON(latitude, longitude);
		}

		HashMap mapAddress = new HashMap();
		mapAddress = (HashMap) maplist.get(0);
		String address = (String) mapAddress.get("formatted_address");
		return address;
	}

	/**
	 * Gets the address.
	 *
	 * @param latitude
	 *            the latitude
	 * @param longitude
	 *            the longitude
	 * @return the address
	 */
	public static String getLocationJSON(String latitude, String longitude) {
		GenericJSON json = new GenericJSON();
		json.put("latitude", latitude);
		json.put("longitude", longitude);
		return getJSonString(json);
	}

	public static String getDetailedAddress(Double latitude, Double longitude) {
		if (latitude == null || longitude == null)
			return "";

		return getAddress(latitude.toString(), longitude.toString());
	}

	/**
	 * Gets the address.
	 *
	 * @param latitude
	 *            the latitude
	 * @param longitude
	 *            the longitude
	 * @return the address
	 */
	public static String getAddress(Double latitude, Double longitude) {
		if (latitude == null || longitude == null)
			return "";

		return getLocationJSON(latitude.toString(), longitude.toString());
	}

	/**
	 * Gets the rack space token and time stamp.
	 *
	 * @return the rack space token and time stamp
	 */
	@SuppressWarnings("rawtypes")
	public static String getRackSpaceTokenAndTimeStamp() {
		RestTemplate restTemplate = new RestTemplate();
		String str = "{ \"auth\": { \"RAX-KSKEY:apiKeyCredentials\": {\"username\": \"mycariq\",\"apiKey\": \"c3cab78c76d3475b97e0b6c65abb3f13\"} }}";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Accept", "application/json");
		headers.set("Content-Length", "54");
		HttpEntity<String> entity = new HttpEntity<String>(str, headers);
		String url = "https://identity.api.rackspacecloud.com/v2.0/tokens";
		HashMap map = new HashMap();
		map = restTemplate.postForObject(url, entity, HashMap.class);
		HashMap map1 = new HashMap();
		map1 = (HashMap) map.get("access");
		HashMap map2 = new HashMap();
		map2 = (HashMap) map1.get("token");
		return map2.get("id").toString();
	}

	// public String writeToConfigFile(String token) {
	// conProperties.setProperty("rackspacetoken", token);
	// try {
	// //conProperties.store(new FileWriter(configFile), "Token Config file");
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// return "done";
	// }
	//
	// public String readFromConfigFile() {
	// Properties prop = new Properties();
	// String token =null;
	// try {
	// FileReader reader = new FileReader(configFile);
	// prop.load(reader);
	// token = prop.getProperty("rackspacetoken");
	// prop.clear();
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return token;
	// }

	// Get base url depending on server
	/**
	 * Gets the base url.
	 *
	 * @return the base url
	 */
	public static String getbaseUrl() {
		//		String baseUrl = null;
		//		try {
		//			InetAddress inetAddr = InetAddress.getLocalHost();
		//			String hostname = inetAddr.getHostName();
		//			if (hostname.equals("ecu-dev.mycariq.com")) {
		//				baseUrl = baseUrlOnDev;
		//			} else if (hostname.equals("ecu.mycariq.com")) {
		//				baseUrl = baseUrlOnEcu;
		//			} else {
		//				baseUrl = baseUrlOnEcu;
		//			}
		//		} catch (UnknownHostException e) {
		//			System.out.println("Host not found: " + e.getMessage());
		//		}
		return baseUrl;
	}

	/**
	 * Get baseUrl For VRM
	 * 
	 * @return
	 */
	public static String getbaseUrlForVRM() {
		//		String baseUrl = null;
		//		try {
		//			InetAddress inetAddr = InetAddress.getLocalHost();
		//			String hostname = inetAddr.getHostName();
		//			if (hostname.equals("ecu-dev.mycariq.com")) {
		//				baseUrl = VRMbaseUrlOnDev;
		//			} else if (hostname.equals("ecu.mycariq.com")) {
		//				baseUrl = VRMbaseUrlOnEcu;
		//			} else {
		//				baseUrl = VRMbaseUrlOnEcu;
		//			}
		//		} catch (UnknownHostException e) {
		//			System.out.println("Host not found: " + e.getMessage());
		//		}
		return baseUrl;
	}

	/**
	 * Quote.
	 *
	 * @param str
	 *            the str
	 * @return the string
	 */
	public static String quote(String str) {
		return "<" + str + ">";
	}

	/**
	 * Calcaulate months on days.
	 *
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the int
	 */
	public static int calcaulateMonthsOnDays(Date fromDate, Date toDate) {
		DateTime ToDate = new DateTime(toDate);
		DateTime FromDate = new DateTime(fromDate);

		Period p = new Period(FromDate, ToDate, PeriodType.yearMonthDayTime());
		int monthCount = Months.monthsBetween(FromDate, ToDate).getMonths();
		final int dayCount = p.getDays();

		return dayCount > 0 ? ++monthCount : monthCount;

	}

	/**
	 * Check packet is back log or not.
	 *
	 * @param pidTime
	 *            the pid time
	 * @param rangeTime
	 *            the range time
	 * @return true, if successful
	 */
	public static boolean checkPacketIsBackLogOrNot(Date pidTime, Date rangeTime) {
		Boolean isBackLog = false;
		if (pidTime.before(rangeTime)) {
			isBackLog = true;
		}
		return isBackLog;
	}

	/**
	 * Days between date.
	 *
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the int
	 */
	public static int daysBetweenDate(Date fromDate, Date toDate) {
		// find number of days
		int noOfDays = Days.daysBetween(new LocalDateTime(fromDate), new LocalDateTime(toDate)).getDays();
		if (noOfDays == 0) {
			noOfDays = 1;
		}
		return noOfDays;
	}

	/**
	 * CalculateDistance with the help of longitude and latitude.
	 *
	 * @param lat1
	 *            the lat1
	 * @param lon1
	 *            the lon1
	 * @param lat2
	 *            the lat2
	 * @param lon2
	 *            the lon2
	 * @return double
	 */
	public static double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
		try {
			lat1 = Math.toRadians(lat1);
			lat2 = Math.toRadians(lat2);
			lon1 = Math.toRadians(lon1);
			lon2 = Math.toRadians(lon2);
			double finalLat = Math.toRadians(lat2 - lat1);
			double finalLon = Math.toRadians(lon2 - lon1);
			double cal = Math.sin(finalLat / 2) * Math.sin(finalLat / 2)
					+ Math.cos(lat1) * Math.cos(lat1) * Math.sin(finalLon / 2) * Math.sin(finalLon / 2);
			double cal1 = 2 * Math.atan2(Math.sqrt(cal), Math.sqrt(1 - cal));
			double d = RADIUS * cal1;
			return Math.toDegrees(d);
		} catch (Exception e) {
			return 0.0;
		}
	}

	/**
	 * Gets the start of yesterday.
	 *
	 * @return yesterdays_Start_Time
	 */
	public static Date getStartOfYesterday() {
		DateTime now = new DateTime(DateTimeZone.forID(INDIA_TIME_ZONE));
		now = now.withTimeAtStartOfDay();
		DateTime yesterdayStart = now.minusDays(1);
		return yesterdayStart.toDate();
	}

	/**
	 * Gets the end of yester day.
	 *
	 * @return yesterdays_end_time
	 */
	public static Date getEndOfYesterDay() {
		DateTime now = new DateTime(DateTimeZone.forID(INDIA_TIME_ZONE));
		now = now.withTimeAtStartOfDay();
		DateTime endOfYesterDay = now.minusMinutes(1);
		return endOfYesterDay.toDate();
	}

	/**
	 * Gets the date out of date time.
	 *
	 * @param date
	 *            the date
	 * @return parseDate
	 */
	public static Date getDateOutOfDateTime(Date date) {
		Date parseDate = null;
		String dateStr = convertToyyyymmdd_hhmmssFormat(date);
		String dateOnly[] = dateStr.split(" ");
		parseDate = parseDate(dateOnly[0]);
		return parseDate;
	}

	/**
	 * getEndOfYesterDayMinusSevenDays.
	 *
	 * @param startDate
	 *            the start date
	 * @return Date
	 */
	public static Date getEndOfYesterDayMinusSevenDays(Date startDate) {
		DateTime date = new DateTime(startDate);
		date = date.minusDays(7);
		return date.toDate();
	}

	/**
	 * getTowDigitsAfterFloatingPoint.
	 *
	 * @param timeStamp
	 *            the time stamp
	 * @param car
	 *            the car
	 * @return double
	 */
	// public static double getTowDigitsAfterFloatingPoint(Double number) {
	// return Double.parseDouble(new DecimalFormat("#.##").format(number));
	// }

	/**
	 * splitErrorCodeString.
	 *
	 * @param errorString
	 *            the error string
	 * @return List<String>
	 */
	private static List<String> splitErrorCodeString(String errorString) {
		return Arrays.asList(errorString.split("\\|"));
	}

	/**
	 * convertToyyyy_mm_dd_Format.
	 *
	 * @param date
	 *            the date
	 * @return String dateString
	 */
	public static String convertToyyyy_mm_dd_Format(Date date) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
		DateTime dateTime = new DateTime(date);
		String dateString = fmt.print(dateTime);
		return dateString;
	}

	public static String convertToyyyy_mm_dd_Format(Date date, String timeZone) {
		if (timeZone == null)
			timeZone = INDIA_TIME_ZONE;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		return sdf.format(date);
	}

	/**
	 * getTodayStartTimeStamp.
	 *
	 * @param endDate
	 *            the end date
	 * @return Date
	 */
	public static Date getTodayStartTimeStamp(Date endDate) {
		DateTime now = new DateTime(endDate);
		return now.withTimeAtStartOfDay().toDate();
	}

	//	/**
	//	 * Checks if is backlog.
	//	 *
	//	 * @param timeStamp the time stamp
	//	 * @return true, if is backlog
	//	 */
	//	public static boolean isBacklog(Date timeStamp) {
	//		DateTime pidTime = new DateTime(timeStamp);
	//		DateTime now = DateTime.now();
	//		return pidTime.isBefore(now.minusMinutes(BACKLOG_DURATION_THRESHOLD_MINUTES));
	//	}
	//	
	/**
	 * Checks if is backlog.
	 *
	 * @param timeStamp
	 *            the time stamp
	 * @return true, if is backlog
	 */
	public static boolean isBacklog(Date packetTimeStamp, Date serverTimeStamp) {
		if (packetTimeStamp == null) // no timestamp?
			return true;

		DateTime pidTime = new DateTime(packetTimeStamp);
		DateTime srvrTime = serverTimeStamp == null ? DateTime.now() : new DateTime(serverTimeStamp);
		return pidTime.isBefore(srvrTime.minusMinutes(BACKLOG_DURATION_THRESHOLD_MINUTES));
	}

	/**
	 * Split error codes.
	 *
	 * @param errorString
	 *            the error string
	 * @return the list
	 */
	public static List<String> splitErrorCodes(String errorString) {
		List<String> errors = new ArrayList<String>();
		if (errorString != null) {
			String[] arr = errorString.split("\\|");
			for (String s : arr) {
				errors.add(s);
			}
			return errors;
		} else {
			return null;
		}
	}

	/**
	 * Gets the date formate for email.
	 *
	 * @param date
	 *            the date
	 * @return the date formate for email
	 */
	public static String getDateFormateForEmail(Date date) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("dd MMMM yyyy ");
		DateTime dateTime = new DateTime(date);
		String dateString = fmt.print(dateTime);
		return dateString;
	}

	/**
	 * Gets the date formate for email.
	 *
	 * @param date
	 *            the date
	 * @return the date formate for email
	 */
	public static String getDateFormateForEmail(Date date, TimeZone zone) {
		if (zone == null) // assume India
			zone = TimeZone.getTimeZone(INDIA_TIME_ZONE);

		DateTimeFormatter fmt = DateTimeFormat.forPattern("dd MMMM yyyy ");
		DateTime dateTime = new DateTime(date, DateTimeZone.forTimeZone(zone));
		return fmt.print(dateTime);
	}

	/**
	 * Gets the now time stamp.
	 *
	 * @param itsTimeZone
	 *            the its time zone
	 * @return the now time stamp
	 */
	public static Date getNowTimeStamp(String itsTimeZone) {
		if (itsTimeZone == null) {
			return new DateTime(DateTimeZone.forID(INDIA_TIME_ZONE)).toDate();
		}
		return new DateTime(DateTimeZone.forID(itsTimeZone)).toDate();
	}

	/**
	 * Gets the now time stamp.
	 *
	 * @param itsTimeZone
	 *            the its time zone
	 * @return the now time stamp
	 */
	public static Date getUTCToLocalTimeStamp(Date utcTime, String localTimeZone) {
		if (localTimeZone == null) {
			return new DateTime(utcTime, DateTimeZone.forID(INDIA_TIME_ZONE)).toDate();
		}
		return new DateTime(utcTime, DateTimeZone.forID(localTimeZone)).toDate();
	}

	/**
	 * Gets the day start time by its time zone.
	 *
	 * @param startDate
	 *            the start date
	 * @return the day start time by its time zone
	 */
	public static Date getDayStartTimeByItsTime(Date startDate, String localTimeZone) {

		return getStartofDay(startDate, localTimeZone);
	}

	/**
	 * Gets the day end time by its time zone.
	 *
	 * @param startDate
	 *            the start date
	 * @return the day end time by its time zone
	 */
	public static Date getDayEndTimeByItsTime(Date startDate, String localTimeZone) {

		return getEndofDay(startDate, localTimeZone);

	}

	/**
	 * Gets the night start time by its time zone.
	 *
	 * @param zoneName
	 *            the zone name
	 * @return the night start time by its time zone
	 */
	public static Date getNightStartTimeByItsTimeZone(String zoneName) {
		if (null == zoneName) {
			return new DateTime(DateTimeZone.forID(INDIA_TIME_ZONE)).plusHours(18).plusMinutes(1).toDate();
		}
		return new DateTime(DateTimeZone.forID(zoneName)).plusHours(18).plusMinutes(1).toDate();
	}

	/**
	 * Gets the night end time by its time zone.
	 *
	 * @param zoneName
	 *            the zone name
	 * @return the night end time by its time zone
	 */
	public static Date getNightEndTimeByItsTimeZone(String zoneName) {
		if (null == zoneName) {
			return new DateTime(DateTimeZone.forID(INDIA_TIME_ZONE)).plusHours(30).toDate();
		}
		return new DateTime(DateTimeZone.forID(zoneName)).plusHours(30).toDate();
	}

	/**
	 * Gets the today date out of date time by its zone.
	 *
	 * @param itsTimeZone
	 *            the its time zone
	 * @return the today date out of date time by its zone
	 */
	public static Date getTodayDateOutOfDateTimeByItsZone(String itsTimeZone) {
		String dateStr = null;
		if (itsTimeZone == null) {
			dateStr = convertToyyyymmdd_hhmmssFormat(new DateTime(DateTimeZone.forID(INDIA_TIME_ZONE)).toDate());
		}
		dateStr = convertToyyyymmdd_hhmmssFormat(new DateTime(DateTimeZone.forID(itsTimeZone)).toDate());
		String dateOnly[] = dateStr.split(" ");
		return parseDate(dateOnly[0]);
	}

	/**
	 * Gets the now time stamp in string.
	 *
	 * @return the now time stamp in string
	 */
	public static String getTodayDateInString() {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("dd MMM yyyy");
		DateTime dateTime = new DateTime();
		String dateString = fmt.print(dateTime);
		return dateString.replace(" ", "-");
	}

	/**
	 * Parse double value till two digit after decimal
	 */
	public static double parseDoubleValueFraction(double val) {
		return Double.parseDouble(new DecimalFormat("##.##").format(val));
	}

	/**
	 * Create image url
	 * 
	 * @param imageName
	 * @return
	 */
	public static String getImageUrl(String imageName) {
		if (StringUtils.isEmpty(imageName))
			return "";
		return rootPath + imageName;
	}

	/**
	 * getRoundValue
	 * 
	 * @param obj
	 *            which is double
	 * @return
	 */
	public static String getRoundValue(Object obj) {
		if (obj == null)
			return "";

		DecimalFormat df = new DecimalFormat("##");
		return df.format(obj);
	}

	/**
	 * getTime
	 * 
	 * @param itsTimeStamp
	 * @return
	 */
	public static String getTimeOnly(Date itsTimeStamp) {
		return getTimeOnly(itsTimeStamp, null);

	}

	public static String getTimeOnly(Date itsTimeStamp, String timezone) {
		return toTimeString(itsTimeStamp, timezone, "hh:mm a");
	}

	/**
	 * getDateOnly - based on its TimeStamp
	 * 
	 * @param itsTimeStamp
	 * @return
	 */
	public static String getDateOnly(Date timeStamp) {
		return getDateOnly(timeStamp, null);
	}

	/**
	 * getDateOnly - based on its TimeStamp
	 * 
	 * @param itsTimeStamp
	 * @return
	 */
	public static String getDateOnly(Date timeStamp, String timeZone) {
		return toTimeString(timeStamp, timeZone, "dd MMMM, yyyy ");
	}

	public static String getNewEmail() {
		return "deactivate+" + 10 + new Random().nextInt(99999) + "@mycariq.com";
	}

	/**
	 * Gets the comma seperated string from list .
	 *
	 * @param resultList
	 *            the result list
	 * @return the comma seperated string
	 */
	public static String getCommaSeperatedString(List<Long> resultList) {
		return resultList.toString().replaceAll("\\[", "").replaceAll("\\]", "");
	}

	public static int BIT(int shift) {
		return (1 << shift);
	}

	/**
	 * get Page Number from the start Index e.g. if page size is 10 and
	 * startIndex is 1, pageNumber =
	 * 
	 * @param pageSize
	 * @param start
	 * @return
	 */
	public static int validatePagination(int pageSize, int pageNumber) {
		int startPage = 0;
		if (pageSize == 0 || pageNumber == 0) {
			handleException("Invalid value for page number: " + pageNumber + "and/or page size: " + pageSize);
		} else if (pageSize < 0 || pageNumber < 0) {
			handleException("Invalid index values, page number: " + pageNumber + "and/or page size: " + pageSize);
		} else {
			startPage = (pageNumber - 1) * pageSize;
		}
		return startPage;
	}

	private static void concatExceptionMessage(StringBuilder bldr, Throwable e) {
		bldr.append(" ++++ ").append(e.getClass().getName()).append(":").append(e.getMessage());
		Throwable cause = e.getCause();
		if (cause != null)
			concatExceptionMessage(bldr, cause);
	}

	public static void handleException(Throwable e) throws CariqException {
		StackTraceElement[] elements = e.getStackTrace();
		StringBuilder bldr = new StringBuilder();
		bldr.append("EXCEPTION STACK:").append(elements[0]).append(elements[1]);
		concatExceptionMessage(bldr, e);
		handleException(bldr.toString());
	}

	public static void handleException(final String msgString) {
		CariqException toThrow = new CariqException() {

			@Override
			public List<String> getMsg() {
				List<String> msg = new ArrayList<String>();
				msg.add(msgString);
				return msg;
			}
		};

		logException(APIHelper.logger, toThrow, "Unknown Operation");

		throw toThrow;
	}

	public static void handleException(final String msgString, final String operation) {
		CariqException toThrow = new CariqException() {

			@Override
			public List<String> getMsg() {
				List<String> msg = new ArrayList<String>();
				msg.add(msgString);
				return msg;
			}
		};

		logException(APIHelper.logger, toThrow, operation);

		throw toThrow;

	}

	public static int getFromDate(Date scheduledTime, int dateFragment) {
		Calendar c = Calendar.getInstance();
		c.setTime(scheduledTime);

		return c.get(dateFragment);
	}

	/**
	 * Returns true randomly 1 time out of given frequency
	 * 
	 * @param frequency
	 * @return
	 */
	public static boolean randomMatch(int frequency) {
		return RandomHelper.randomMatch(frequency);
	}

	public static void logException(CarIQLogger logger, Throwable e, String operation) {
		if (null == e)
			return;

		Utils.logAnomaly(logger, "EXCEPTION_" + operation, null, e.getMessage());
		logger.error(e.getMessage());
		StringWriter stack = new StringWriter();
		e.printStackTrace(new PrintWriter(stack));
		logger.error("Caught exception during Operation: " + operation + ":\n" + stack.toString());

		logException(logger, e.getCause(), operation);
	}

	/**
	 * Check input is null or not, Then Parse date.
	 *
	 * @param dateStr
	 *            the date str
	 * @return the date
	 */
	public static Date setDateTime(String dateStr) {
		if (dateStr != null)
			return parseDateFormat(dateStr);
		return null;
	}

	public static void validateNotNull(String context, Object obj, CarIQLogger logger) {
		if (obj != null)
			return;

		handleException("Object is Null in Context: " + context);
	}

	/**
	 * Checks if bit on.
	 *
	 * @param value
	 *            the value
	 * @param bitNumber
	 *            the bit number
	 * @return true, if bit on
	 */
	public static boolean isBitOn(int value, int bitNumber) {
		return (value & bitNumber) > 0;
	}

	public static Date getFirstDateOfCurrentMonth() {
		DateTime today = new DateTime();
		int month = today.getMonthOfYear() - 1; // GregorianCalendar month start
												// from 0 as jan.
		Calendar cal = new GregorianCalendar(today.getYear(), month, 1);
		return cal.getTime();
	}

	/**
	 * Check if the give class exists in the package/classpath so that it can be
	 * loaded by reflection
	 * 
	 * @param clazz
	 * @return
	 */
	public static boolean checkClassExists(String clazz) {
		try {
			Class.forName(clazz, false, Utils.class.getClassLoader());
		} catch (ClassNotFoundException e) {
			return false;
		}
		return true;
	}

	/**
	 * Get Exception truncated to 4000 characters to store in the DB
	 * 
	 * @param e
	 * @return
	 */
	public static String getExceptionMessage(Exception e) {
		if (null == e)
			return "";

		StringBuilder str = new StringBuilder("");
		// Get the message
		str.append("Message: ").append(e.getMessage());

		// get the Stack
		StringWriter stack = new StringWriter();
		e.printStackTrace(new PrintWriter(stack));
		str.append("; StackTrace: ").append(stack.toString());

		// Trim more than 4000
		String returnVal = str.toString();
		returnVal = returnVal.substring(0, Math.min(returnVal.length(), 4000));

		return returnVal;
	}

	/**
	 * Get date - years in future
	 * 
	 * @param currentDate
	 * @param years
	 * @return
	 */
	public static Date dateYearsAhead(Date currentDate, int years) {
		DateTime date = new DateTime(currentDate);
		DateTime rangeDate = date.plusYears(years);
		return rangeDate.toDate();
	}

	/**
	 * Get date - seconds in future compared to currentDate
	 * 
	 * @param currentDate
	 * @param seconds
	 * @return
	 */
	public static Date getDateSecondsAfter(Date currentDate, int seconds) {
		DateTime date = new DateTime(currentDate);
		DateTime rangeDate = date.plusSeconds(seconds);
		return rangeDate.toDate();
	}

	/**
	 * Get date - seconds in past compared to currentDate
	 * 
	 * @param currentDate
	 * @param seconds
	 * @return
	 */
	public static Date getDateSecondsBefore(Date currentDate, int seconds) {
		DateTime date = new DateTime(currentDate);
		DateTime rangeDate = date.minusSeconds(seconds);
		return rangeDate.toDate();
	}

	/**
	 * Get Difference between two times in seconds
	 * 
	 * @param after
	 * @param before
	 * @return
	 */
	public static long getDiffInSeconds(Date after, Date before) {
		DateTime ToDate = new DateTime(after);
		DateTime FromDate = new DateTime(before);
		return Seconds.secondsBetween(FromDate, ToDate).getSeconds();
	}

	public static List<Comparable> getLocalMaximas(List<Comparable> inputList) {
		int prevDifference = 0;
		List<Comparable> retval = new ArrayList<Comparable>();
		Comparable previous = null;
		for (Comparable obj : inputList) {
			if (previous == null) {
				previous = obj;
				continue;
			}
			int val = obj.compareTo(previous);
			if (val < 0 && prevDifference >= 0)
				retval.add(previous);

			prevDifference = val;
			previous = obj;
		}

		return retval;
	}

	public static <T> List<ProximityComparable<T>> getTop(List<ProximityComparable<T>> ratios, int count,
			int differenceThreshold) {
		TopItems<T> topItems = new TopItems<T>(count, differenceThreshold);
		if (ratios.size() <= count) // List is already small
			return ratios;

		if (differenceThreshold < 0)
			differenceThreshold = 0;

		for (ProximityComparable<T> proximityComparable : ratios)
			topItems.add(proximityComparable);

		return topItems.build().getTopItems();
	}

	public static List<Comparable> getTopOld(List<Comparable> inputList, int count, int differenceThreshold) {
		if (inputList.size() <= count) // List is already small
			return inputList;

		if (differenceThreshold < 0)
			differenceThreshold = 0;

		// Do count number of passes over the list and add to output if
		// percentSeparation is good
		List<Comparable> retVal = new ArrayList<Comparable>();
		for (int i = 0; i < count; i++) {
			Comparable max = null;
			for (Comparable obj : inputList) {
				if (existsInList(retVal, obj) || existsInList(retVal, obj, differenceThreshold))
					continue;

				if (max == null) {
					max = obj;
					continue;
				}

				if (obj.compareTo(max) > 0 && !existsInList(retVal, obj, differenceThreshold))
					max = obj;
			}

			retVal.add(max);
		}

		return retVal;
	}

	/**
	 * Check if Item exists in given proximity in the list
	 * 
	 * @param retVal
	 * @param obj
	 * @param differenceThreshold
	 * @return
	 */
	private static boolean existsInList(List<Comparable> lst, Comparable obj, int differenceThreshold) {
		if (existsInList(lst, obj))
			return true;

		for (Comparable comparable : lst) {
			if (Math.abs(comparable.compareTo(obj)) < differenceThreshold)
				return true;
		}

		return false;

	}

	/**
	 * Check if the item exists in the list
	 * 
	 * @param lst
	 * @param obj
	 * @return
	 */
	private static boolean existsInList(List<Comparable> lst, Comparable obj) {
		for (Comparable comparable : lst) {
			if (comparable.equals(obj))
				return true;
		}

		return false;
	}

	public static String removeDoubleQuotation(String val) {
		try (ProfilePoint _removeDoubleQuotation = ProfilePoint.profileAction("ProfAction_removeDoubleQuotation")) {
			if (null == val || !val.contains("\""))
				return val;

			return val.replaceAll("^\"|\"$", "");
		}
	}

	/**
	 * Convert input from boolean to int.
	 * 
	 * @param boolean
	 *            value
	 * @return int value
	 */
	public static int booleanToInt(boolean value) {
		return value ? 1 : 0;
	}

	public static double formatDouble(double val) {
		return formatDouble(val, 2); // format to 2 digits
	}

	public static double formatDouble(double val, int digits) {
		double multiplier = Math.pow(10, digits);
		val = val * multiplier;
		val = Math.round(val);
		return val / multiplier;
	}

	public static boolean sameDate(Date date1, Date date2) {
		if (date1 == null || date2 == null)
			return false;

		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);
		return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
				&& cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);

	}

	public static boolean sameDate(Date date1, Date date2, TimeZone timeZone) {
		if (date1 == null || date2 == null)
			return false;

		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);

		cal1.setTimeZone(timeZone);
		cal2.setTimeZone(timeZone);

		return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
				&& cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);

	}

	public static boolean isDayTime(Date timeStamp, DateTimeZone zone) {
		if (zone == null) // assume India
			zone = DateTimeZone.forID(INDIA_TIME_ZONE);

		Date startTime = new DateTime(timeStamp, zone).withTimeAtStartOfDay().toDate();

		int hours = getDiffInHours(timeStamp, startTime);
		return (hours > 6 && hours < 18);
	}

	public static String getDateTimeString(Date date, TimeZone timeZone) {
		if (timeZone == null) // assume India
			timeZone = TimeZone.getTimeZone(INDIA_TIME_ZONE);
		DateTimeFormatter fmt = DateTimeFormat.mediumDateTime();
		DateTime time = new DateTime(date, DateTimeZone.forTimeZone(timeZone));
		return fmt.print(time);
	}

	/**
	 * Get current timeStamp in utc
	 * 
	 * @return
	 */
	public static Date getCurrentTimeInUTC() {
		DateTime dateTime = new DateTime(DateTimeZone.UTC);
		return dateTime.toDate();
	}

	/**
	 * 
	 * @param hours
	 * @return
	 */
	public static Date getCurrentTimeMinusHoursInUTC(int hours) {
		DateTime dateTime = new DateTime(DateTimeZone.UTC);
		return dateTime.minusHours(hours).toDate();
	}

	public static String getHostName() {
		try {
			InetAddress host = InetAddress.getLocalHost();
			return host.getCanonicalHostName() + ":" + host.getHostAddress();
		} catch (UnknownHostException e) {
			return "UNKNOWN";
		}
	}

	/**
	 * Get errorcount
	 * 
	 * @param errorCodes
	 * @return
	 */
	public static int getErrorcount(String errorCodes) {
		if (errorCodes == null || errorCodes.equals("P0000"))
			return 0;

		return errorCodes.split("\\|").length;
	}

	public static String convertToEEE_dd_MMM_yyyy(Date date) {
		return new SimpleDateFormat("EEE, dd MMM yyyy").format(date);
	}

	public static long getTimeEpoch(Date date) {
		return date.getTime();
	}

	/**
	 * Create String from Json Object
	 * 
	 * @param json
	 * @return
	 */
	public static String getJSonString(Object json) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writer().writeValueAsString(json);
		} catch (Exception e) {
			Utils.handleException(e.getMessage());
		}
		return null;
	}

	/**
	 * Create JSon Object of given class from String (reverse of above)
	 * 
	 * @param jsonString
	 * @param clz
	 * @return
	 */

	public static <T> T getJSonObject(String jsonString, Class<T> clz) {
		try {

			ObjectMapper mapper = new ObjectMapper();

			// JSON from String to Object
			return mapper.readValue(jsonString, clz);
		} catch (Exception e) {
			Utils.handleException(e.getMessage());
		}
		return null;
	}

	/**
	 * Gets the j son object list.
	 *
	 * @param <T>
	 *            the generic type
	 * @param jsonString
	 *            the json string
	 * @param clz
	 *            the clz
	 * @return the j son object list
	 */
	public static <T> List<T> getJSonObjectList(String jsonString, Class<T> clz) {

		List<T> list = new ArrayList<T>();
		try {
			JSONArray array = new JSONArray(jsonString);

			for (int i = 0; i < array.length(); i++) {
				JSONObject object = array.getJSONObject(i);

				// JSON from String to Object and add to list	
				list.add(new ObjectMapper().readValue(object.toString(), clz));
			}
		} catch (Exception e) {
			Utils.handleException(e.getMessage());
		}
		return list;
	}

	public static String getTimeZone(String timeZone) {
		if ((null == timeZone))
			return INDIA_TIME_ZONE;

		// The timezone can be misspelled - and the will result in GMT - but let's live with that for now
		return timeZone;
	}

	public static DateTimeZone getJodaTimeZone(String timeZone) {
		if ((null == timeZone))
			return DateTimeZone.forID(INDIA_TIME_ZONE);

		// The timezone can be misspelled - and the will result in GMT - but let's live with that for now
		return DateTimeZone.forID(timeZone);
	}

	@SuppressWarnings("unchecked")
	public static <T> T downCast(Class<T> cls, Object obj) {
		try {
			if (obj == null)
				return null;
			if (cls.isAssignableFrom(obj.getClass()))
				return (T) obj;
			String objStr = obj.toString();

			// special treatment for Date - 
			if (cls.isAssignableFrom(Date.class))
				return (T) Utils.parseDateTime(objStr);

			// Try using static ValueOf method of the class using reflection
			return BeanHelper.valueOf(cls, objStr);
		} catch (Exception e) {
			return null;
		}
	}

	public static void checkNotNull(Object obj, String msg) {
		if (obj == null)
			throw new RuntimeException(msg);
	}

	/**
	 * Campare 2 dates
	 * 
	 * @param FirstDate
	 * @param SecondDate
	 * @return true if matches
	 */
	public static boolean isOnlyDateEquals(Date FirstDate, Date SecondDate) {
		Date first = Utils.getDateOutOfDateTime(FirstDate);
		Date second = Utils.getDateOutOfDateTime(SecondDate);
		return first.equals(second);
	}

	/**
	 * Create date based identifier to be used as ID at some places instead of
	 * UUID Need not be absolutely unique, but mostly unique
	 * 
	 * @return
	 */

	public static String createDateId() {
		return createDateId(false);
	}

	/**
	 * Create date based identifier to be used as ID at some places instead of
	 * UUID Need not be absolutely unique, but mostly unique
	 * 
	 * @return
	 */
	public static String createDateId(boolean longId) {
		Date now = new Date();

		SimpleDateFormat ft = new SimpleDateFormat("ddss");
		if (longId)
			ft = new SimpleDateFormat("yyMMddhhmmssMs");

		return ft.format(now);
	}

	public static boolean isToday(Date endDate) {
		Day nowDay = new Day(new Date(), null);
		Day endDay = new Day(endDate, null);

		return nowDay.equals(endDay);
	}

	/**
	 * From url, prepare a string that will be easy to use in logging
	 * 
	 * @param xformedUrl
	 * @return
	 */
	public static String getLoggableUrl(String url) {
		return url.replaceAll("[\\/:]", "_");
	}

	public static boolean intToBoolean(int boolInt) {
		// Non zero value means true, else false
		return boolInt != 0;
	}

	public static long getTimeOffset(String timeZone) {
		TimeZone tz;
		if (timeZone == null) {
			tz = TimeZone.getTimeZone(INDIA_TIME_ZONE);
		} else {
			tz = TimeZone.getTimeZone(timeZone);
		}

		return tz.getOffset(Calendar.ZONE_OFFSET);
	}

	public static Date getTimeZoneDate(Date dt, String timeZone) {
		// Problematic bug on TimeZone - for now bring back the calculation
		return dt;

		//		// Date offset = UTC - dt
		//		long dtOffset = dt.getTimezoneOffset()*60*1000;
		//
		//		if (timeZone == null)
		//			timeZone = INDIA_TIME_ZONE;
		//
		//		// timezone offset = europeDate - UTC 
		//		long tzOffset = Utils.getTimeOffset(timeZone);
		//		
		//		// Europe time = UTC Time + Europe Offset
		//		// Europe time = (dt + offset) - Europe Offset
		//		long timeZoneTime = dt.getTime() + dtOffset + tzOffset;
		//		return  new Date(timeZoneTime);
	}

	public static Date getTimeZoneDateForGivenDate(Date dt, String timeZone) {
		// Date offset = UTC - dt
		long dtOffset = dt.getTimezoneOffset() * 60 * 1000;

		if (timeZone == null)
			timeZone = INDIA_TIME_ZONE;

		// timezone offset = europeDate - UTC 
		long tzOffset = Utils.getTimeOffset(timeZone);

		// Europe time = UTC Time + Europe Offset
		// Europe time = (dt + offset) - Europe Offset
		long timeZoneTime = dt.getTime() + dtOffset + tzOffset;
		return new Date(timeZoneTime);
	}

	// generate image name
	public static String generateImageName(String originalFilename) {
		String extension = originalFilename.substring(originalFilename.lastIndexOf("."));
		//if (imageName != null)
		//return imageName.substring(0, imageName.lastIndexOf(".")) + extension;
		return UUID.randomUUID() + extension;
	}

	public static String getValue(GenericJSON params, String key) {
		if (params.get(key) != null && (!params.get(key).toString().trim().isEmpty()))
			return params.get(key).toString();
		return null;
	}

	public static <T> T getValue(Class<T> cls, GenericJSON params, String key) {
		return Utils.downCast(cls, params.get(key));
	}

	public static Date parseDate(String dateString, String datePatterns) {
		return fromTimeString(dateString, UTC, new String[] { datePatterns });
		//		if (dateString != null) {
		//			try {
		//				if (dateString.equalsIgnoreCase("NOW"))
		//					return new Date();
		//
		//				String[] pattern = new String[] { datePatterns };
		//				return DateUtils.parseDate(dateString, pattern);
		//			} catch (ParseException e) {
		//				Utils.handleException("Invalid date format");
		//			}
		//		}
		//		return null;
	}

	public static Integer getInteger(String value) {
		if (value != null && !value.trim().isEmpty())
			return Integer.parseInt(value);
		return 0;
	}

	/**
	 * @param <T>
	 * @param singleResult
	 * @param i
	 * @return
	 * @throws Exception
	 */
	public static <T> T getValue(Object singleResult, Class<?> cls, T defaultValue) {
		try {
			if (singleResult == null)
				return defaultValue;

			return (T) downCast(cls, singleResult);
		} catch (Exception e) {
			handleException(e);
		}

		return null;
	}

	public static double getDouble(String value) {
		if (value != null && !value.trim().isEmpty())
			return Double.parseDouble(value);
		return 0.0;
	}

	/**
	 * Remove occurrence of special char from input string
	 * 
	 * @param string
	 * @return new string
	 */
	public static String removeSpecialChar(String string) {
		if (string != null && !string.isEmpty())
			return string.replaceAll("[^\\w\\s]", "");

		return string;
	}

	/**
	 * @param result
	 * @param i
	 * @return
	 */
	public static List<GenericJSON> truncateDoubles(List<GenericJSON> input, int rounding) {
		if (input == null || input.isEmpty())
			return input;

		List<GenericJSON> retval = new ArrayList<GenericJSON>();
		for (GenericJSON inputJSON : input) {
			GenericJSON outputJSON = new GenericJSON();
			for (String key : inputJSON.keySet()) {
				Object val = inputJSON.get(key);
				if (val instanceof Double) {
					val = Utils.getRoundValue((Double) val);
				}
				outputJSON.put(key, val);
			}

			retval.add(outputJSON);
		}
		return retval;
	}

	public static String getCurrentUserName() {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		if (userName == null)
			return "UNKNOWN";

		return userName;
	}

	public static <T> boolean addToList(List<T> list, T item) {
		if (list.contains(item))
			return false;

		list.add(item);
		return true;
	}

	// If String is already doublequoted, return true
	public static boolean isInDoubleQuotes(String str) {
		return (str.startsWith("\"") && str.endsWith("\""));
	}

	public static String handleSeparator(String original, String separator) {
		if (original == null || original.isEmpty() || Utils.isInDoubleQuotes(original))
			return original;

		if (original.contains(separator))
			return "\"" + original + "\"";

		return original;
	}

	public static <T> String toCSV(Set<T> objects) {
		final String COMMA = ",";
		StringBuffer buf = new StringBuffer();
		boolean first = true;
		for (T obj : objects) {
			if (first) {
				buf.append(handleSeparator(obj.toString(), COMMA));
				first = false;
			} else {
				buf.append(COMMA).append(handleSeparator(obj.toString(), COMMA));
			}
		}

		return buf.toString();
	}

	/**
	 * @param trackingId
	 * @param configProperties
	 * @return
	 */
	public static String getTrackingUrl(Object awb, Properties configProperties) {
		String trackingNo = "";
		final String XPRESSBEE_TOKEN_PATTERN = "128516";
		final String INDIA_POST_TOKEN_PATTERN = "EM";
		if (awb == null || awb.toString().isEmpty())
			return trackingNo;

		trackingNo = awb.toString();

		if (configProperties == null || configProperties.isEmpty())
			return trackingNo;

		// if Urls are specified, determine Tracking Id
		if (configProperties.containsKey(XPRESSBEE_URL_KEY) && configProperties.containsKey(INDIA_POST_URL_KEY)) {
			String xpressBeeTrackingUrl = configProperties.getProperty(XPRESSBEE_URL_KEY);
			String indiaPostTrackingUrl = configProperties.getProperty(INDIA_POST_URL_KEY);

			if (trackingNo.contains(XPRESSBEE_TOKEN_PATTERN))
				return createLink("Track Shipment", xpressBeeTrackingUrl + trackingNo);

			if (trackingNo.contains(INDIA_POST_TOKEN_PATTERN))
				return createLink("Track Shipment", indiaPostTrackingUrl + trackingNo);

		}

		return trackingNo;
	}

	/**
	 * @param trackingNo
	 * @param string
	 * @return
	 */
	private static String createLink(String value, String url) {
		//		<a href="#TRACKINGURL" style="color:#0079C2;text-decoration: none" >Track Shipment</a>
		return createLink(value, url, "#0079C2");
	}

	private static String createLink(String value, String url, String color) {
		//		<a href="#TRACKINGURL" style="color:#0079C2;text-decoration: none" >Track Shipment</a>

		return "<a href=\"" + url + "\" style=\"color:" + color + ";text-decoration: none\" >" + value + "</a>";
	}

	/**
	 * @param json
	 * @return
	 */
	public static GenericJSON trimStringValues(GenericJSON inputJSON) {
		if (inputJSON == null || inputJSON.isEmpty())
			return inputJSON;

		GenericJSON outputJSON = new GenericJSON();
		for (String key : inputJSON.keySet()) {
			Object val = inputJSON.get(key);
			if (val instanceof String) {
				val = ((String) val).trim();
			}
			outputJSON.put(key, val);
		}

		return outputJSON;
	}

	// Bypass ssl certificate
	public static ClientHttpRequestFactory getRequestFactory() {
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		DefaultHttpClient httpClient = (DefaultHttpClient) requestFactory.getHttpClient();
		TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
			@Override
			public boolean isTrusted(X509Certificate[] certificate, String authType) {
				return true;
			}
		};

		SSLSocketFactory sf = null;
		try {
			sf = new SSLSocketFactory(acceptingTrustStrategy, ALLOW_ALL_HOSTNAME_VERIFIER);
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", 8443, sf));

		// Set high read timeout - 30 minutes
		requestFactory.setReadTimeout(30 * Utils.minuteMiliSeconds);
		//		requestFactory.setConnectTimeout(1 * Utils.minuteMiliSeconds);
		// Set high connection timeout - 10 minutes
		requestFactory.setConnectTimeout(10 * Utils.minuteMiliSeconds);
		return requestFactory;
	}

	public static Date getDaysBefore(Date inputDate, int days) {
		DateTime dateTime = new DateTime(inputDate);
		return dateTime.minusDays(days).toDate();
	}

	public static Date getWeeksBefore(Date inputDate, int weeks) {
		DateTime dateTime = new DateTime(inputDate);
		return dateTime.minusDays(weeks).toDate();
	}

	/**
	 * Parse dateString into date
	 * 
	 * @param dateString
	 * @return date
	 */
	public static Date getDate(String dateStr) {
		Date date = null;
		try {
			date = parseDate(dateStr);
		} catch (Exception e) {
			try {
				date = parseDateFormat(dateStr);
			} catch (Exception ex) {
				Utils.handleException("Date must be in (YYYY-MM-DD hh:mm:ss) or (YYYY-MM-DD) format");
			}
		}
		return date;
	}

	/**
	 * Sort the list of Maps based on values of sortable parameters. Rearrange
	 * the contents
	 * 
	 * @param input
	 * @param params
	 * @return
	 */
	public static List<? extends Map<String, Object>> sortMap(List<? extends Map<String, Object>> input,
			List<SortableParameter> sortableParams) {
		Collections.sort(input, new SortableParameterComparator(sortableParams));
		return input;
	}

	/**
	 * Get date after given days
	 * 
	 * @param inputDate
	 * @param days
	 * @return new date
	 */
	public static Date getDaysAfter(Date inputDate, int days) {
		DateTime dateTime = new DateTime(inputDate);
		return dateTime.plusDays(days).toDate();
	}

	/**
	 * Log anomoly condition in a specific format
	 * 
	 * @param logger
	 * @param anomalyType
	 * @param objectid
	 * @param value
	 */
	public static void logAnomaly(CarIQLogger logger, String anomalyType, Object objectid, Object value) {
		String valueString = value == null ? "null" : value.toString();
		String objectIdStr = objectid == null ? "null" : objectid.toString();
		logger.warn("ANOMALY," + anomalyType + "," + objectIdStr + "," + valueString);
	}

	/**
	 * Log Action in a specific format
	 * 
	 * @param logger
	 * @param anomalyType
	 * @param objectid
	 * @param value
	 */
	public static void logAction(CarIQLogger logger, String actionType, Object objectid, Object value) {
		String valueString = value == null ? "null" : value.toString();
		String objectIdStr = objectid == null ? "null" : objectid.toString();
		logger.info("ACTION," + actionType + "," + objectIdStr + "," + valueString);
	}

	/**
	 * @param string
	 * @param e
	 */
	public static void handleException(String operation, Exception e) {
		StackTraceElement[] elements = e.getStackTrace();
		StringBuilder bldr = new StringBuilder();
		bldr.append("EXCEPTION STACK:").append(elements[0]).append(elements[1]);
		concatExceptionMessage(bldr, e);
		handleException(bldr.toString(), operation);
	}

	/**
	 * Parses the.
	 *
	 * @param <T>
	 *            the generic type
	 * @param cls
	 *            the cls
	 * @param obj
	 *            the obj
	 * @return the t
	 */
	public static <T> T parse(Class<T> cls, Object obj) {
		try {
			return downCast(cls, obj);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Convert String into date based on given list of pattern
	 * 
	 * @param dateString
	 * @param datePatterns
	 * @return
	 */
	public static Date parseDate(String dateString, String[] datePatterns) {
		return fromTimeString(dateString, UTC, datePatterns);
		//
		//		if (dateString != null) {
		//			try {
		//				if (dateString.equalsIgnoreCase("NOW"))
		//					return new Date();
		//				return DateUtils.parseDate(dateString, datePatterns);
		//			} catch (ParseException e) {
		//				Utils.handleException("Invalid date format");
		//			}
		//		}
		//		return null;
	}

	public static int ZERO = 0;

	public static <T> T ZERO(Class<T> cls) {
		try {
			return Utils.downCast(cls, ZERO);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @param cls
	 *            - Double/Integer/Float or even String!
	 * @param avgSpeed
	 * @return
	 */
	public static <T> boolean isNullOrZero(Class<T> cls, Object numeric) {
		if (numeric == null)
			return false;

		return ZERO(cls).equals(downCast(cls, numeric));
	}

	/**
	 * @param string
	 * @return
	 */
	public static String[] getCommaSeperatedString(String commaSeparatedString) {
		String[] retval = commaSeparatedString.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
		return retval;
	}

	/**
	 * Email exception.
	 *
	 * @param e
	 *            the e
	 */
	public static void emailException(final Throwable e) {
		emailException(e.toString(), e.getStackTrace());
	}

	/**
	 * Email exception.
	 *
	 * @param message
	 *            the message
	 */
	public static void emailException(final String message) {
		emailException(message, Thread.currentThread().getStackTrace());
	}

	/**
	 * Email exception.
	 *
	 * @param message
	 *            the message
	 * @param stackTrace
	 *            the stack trace
	 */
	public static void emailException(final String message, final StackTraceElement[] stackTrace) {
		final Map<String, Object> model = new HashMap<String, Object>();
		try {
			model.put("hostName", InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		model.put("buildDate", "-");
		model.put("generatedDate", Utils.convertToyyyymmdd_hhmmssFormat(new Date()));
		model.put("message", message);
		model.put("stack", stackTraceToString(stackTrace));

		final VelocityEngine velocityEngine = new VelocityEngine();
		velocityEngine.addProperty("resource.loader", "class");
		velocityEngine.addProperty("class.resource.loader.class",
				"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

		final String emailText = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "velocity/exception.vm",
				"UTF-8", model);

		LogManager.getLogger("ErrorEmail").error(emailText);
	}

	/**
	 * Stack trace to string.
	 *
	 * @param stackTrace
	 *            the stack trace
	 * @return the string
	 */
	public static String stackTraceToString(final StackTraceElement[] stackTrace) {
		final StringBuilder sb = new StringBuilder();
		for (StackTraceElement ste : stackTrace) {
			sb.append(ste).append("\n");
		}
		sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	/*
	 * Get dummy registration number.
	 * 
	 * @return
	 */
	public static String getDummyRegistrationNumber() {
		return Utils.DUMMY_REG_NO + UUID.randomUUID();
	}

	/**
	 * @param i
	 * @param j
	 * @param k
	 * @param l
	 * @param speed
	 * @return
	 */
	public static double evaluateRangeInterpolation(double minX, double maxX, double minY, double maxY, double x) {
		if (minX == maxX)
			return minY;

		if (minX > maxX)
			return evaluateRangeInterpolation(maxX, minX, maxY, minY, x);

		if (x < minX)
			return minY;

		if (x >= maxX)
			return maxY;

		// Interpolate
		// y = x1 + x*(y2-y1)/(x2-x1)
		return minY + x * (maxY - minY) / (maxX - minX);
	}

	/**
	 * @param i
	 * @param j
	 * @return
	 */
	public static double getRandomInRange(int min, int max) {
		return (double) RandomHelper.InRange(min, max);
	}

	/**
	 * Validate location
	 * 
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	public static boolean validateLocation(Double latitude, Double longitude) {
		if (latitude == null || longitude == null)
			return false;

		// If (lat,long) is (0,0) then return false.
		if ((latitude < 1 && latitude > -1) && (longitude < 1 && latitude > -1))
			return false;

		// if(lat, long) is (190,80) then return false
		// if(lat, long) is (90,190) then return false
		// if(lat, long) is (190,190) then return false
		// if(lat, long) is (-190,80) then return false
		// if(lat, long) is (90,-190) then return false
		// if(lat, long) is (-190,-190) then return false
		if (Math.abs(latitude) > MAX_LAT_THRESHOLD || Math.abs(longitude) > MAX_LONG_THRESHOLD)
			return false;

		return true;
	}

	/**
	 * Validate alert value
	 * 
	 * @param alertValue
	 * @return true if value is valid
	 */
	public static boolean validateAlertValue(Object alertValue) {
		if (alertValue == null)
			return true;

		if (alertValue.toString().equalsIgnoreCase(INFINITY))
			return false;

		return true;
	}

	/*
	 * Return string representation of given timestamp in given timezone
	 * 
	 * @param startTime
	 * 
	 * @param string
	 * 
	 * @return
	 */
	public static String getTimeStamp(Date timeStamp, String timeZone) {
		return toTimeString(timeStamp, timeZone, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * Return string representation of given timestamp in given timezone
	 * 
	 * @param startTime
	 * @param string
	 * @return
	 */
	public static String toTimeString(Date timeStamp, String timeZone, String format) {
		if (Strings.isNullOrEmpty(timeZone))
			timeZone = INDIA_TIME_ZONE;

		DateFormat fmt = new SimpleDateFormat(format);
		fmt.setTimeZone(TimeZone.getTimeZone(timeZone));

		String dateString = fmt.format(timeStamp);
		return dateString;
	}

	/**
	 * Return string representation of given timestamp in given timezone
	 * 
	 * @param startTime
	 * @param string
	 * @return
	 */
	public static Date fromTimeString(String timeString, String timeZone, String[] formats) {
		// for NOW - or null - return the current time
		if (timeString == null || timeString.equalsIgnoreCase("NOW"))
			return new Date();

		// if timezone is not privided it is UTC by default - because, short reason: server is in UTC
		if (Strings.isNullOrEmpty(timeZone))
			timeZone = UTC;

		for (String format : formats) {
			try {
				DateFormat fmt = new SimpleDateFormat(format);
				fmt.setTimeZone(TimeZone.getTimeZone(timeZone));
				Date dt = fmt.parse(timeString);
				if (dt != null && dt.getTime() > 0)
					return dt;

			} catch (ParseException e) {
				continue;
			}
		}

		return null;
	}

	/**
	 * @param timeZone
	 */
	public static String decodeTimeZone(String timeZone) {
		// if timezone contains . - replace it with /
		return timeZone.replace(".", "/");
	}

	/**
	 * @param utcDate
	 * @return
	 */
	public static Date portDate(Date date) {
		return portDate(date, null);
	}

	/**
	 * @param utcDate
	 * @return
	 */
	public static Date portDate(Date date, String timeZone) {
		if (Strings.isNullOrEmpty(timeZone))
			timeZone = INDIA_TIME_ZONE;

		String dateStr = Utils.toTimeString(date, timeZone, "YYYY-MM-dd");
		Date portedDate = Utils.parseDate(dateStr);

		return portedDate;
	}

	public static boolean validateInput(Object input) {
		if (input == null)
			return false;

		if (input.toString().trim().length() == 0)
			return false;

		return true;
	}

	public static String stringifyIds(Object input) {
		return "(" + input.toString().trim() + ")";
	}

	/**
	 * @param latitude
	 * @return
	 */
	public static String formatLatLong(Double latlon) {
		if (latlon == null)
			return null;

		DecimalFormat df = new DecimalFormat("#.####");
		return (df.format(latlon));
	}

	/**
	 * @param gearAndSpeedDataPoints
	 * @return
	 */
	public static boolean isNullOrEmpty(Collection<?> gearAndSpeedDataPoints) {
		return (gearAndSpeedDataPoints == null || gearAndSpeedDataPoints.isEmpty());
	}

	// -------------------------------------------------------
	// Specific to Toolkit
	// -------------------------------------------------------
	public static String getUserNameForCarIQApi() {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		if (userName == null)
			return "UNKNOWN";
		return userName;
	}

	public static <T> String getString(T input, String output) {
		if (output != null && !output.isEmpty())
			output += "," + input;
		else
			output = input + "";
		return output;
	}

	public static <T> String getStringfyIds(List<T> ids) {
		String string = "";
		int i = 0;
		for (Object id : ids) {
			if (i < (ids.size() - 1))
				string += id + ",";
			else
				string += id;
			i++;
		}

		return string;
	}
	
	/**
	 * Get strings by comma seperated.
	 * @param newString
	 * @param oldString
	 * @return
	 */
	public static <T> String appendString(T newString, String oldString) {
		if (oldString != null && !oldString.isEmpty())
			oldString += "," + newString;
		else
			oldString += newString;
		return oldString;
	}	
	

	public static String createContext(String baseString) {
		return baseString + "-" + Utils.getDateTimeString(new Date(), null);
	}

	/**
	 * Convert date string as it is to date format
	 * 
	 * @param dateString
	 * @return
	 */
	public static Date convertToDate(String dateString, String pattern) {
		try {
			return new SimpleDateFormat(pattern).parse(dateString);
		} catch (ParseException e) {
			Utils.handleException("Date should be in " + pattern);
		}
		return null;
	}

	/**
	 * Get smallest number
	 * 
	 * @param firstNum
	 * @param secNum
	 * @return
	 */
	public static Integer getSmallNum(Integer firstNum, Integer secNum) {
		if (firstNum > secNum)
			return secNum;
		return firstNum;
	}

	/**
	 * Sets http headers
	 *
	 * @param basicAuth
	 *            the basic auth
	 * @return the http headers
	 */
	public static HttpHeaders setHeaders(String basicAuth) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		headers.set("Authorization", basicAuth);
		return headers;
	}

	/**
	 * Get securityToken
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public static String getSecurityToken(String username, String password) {
		String basicAuth = createEncodedText(username, password);
		String securityToken = "Basic " + basicAuth;
		return securityToken;
	}

	/**
	 * Creates the encoded text.
	 *
	 * @param username
	 *            the username
	 * @param password
	 *            the password
	 * @return the string
	 */
	public static String createEncodedText(final String username, final String password) {

		final String pair = username + ":" + password;
		final byte[] encodedBytes = Base64.encodeBase64(pair.getBytes());

		return new String(encodedBytes);
	}

	/**
	 * @param apiName
	 * @return
	 */
	public static String makeApi(String apiName) {
		return apiName.replace("|", "/");
	}

	public static Double formatDouble(Object value) {
		if (value != null) {
			Double val = Utils.downCast(Double.class, value);
			if (val != null) {
				DecimalFormat format = new DecimalFormat(".##");
				return Double.parseDouble(format.format(val));
			}
		}
		return 0.0;
	}
	
	/**
	 * @param <IN,
	 *            OUT, OUT_LIGHT>
	 * @param resultList
	 * @param class1
	 * @return
	 */
	public static <OUT> List<OUT> convertList(List resultList, Class<OUT> clzz) {
		List<OUT> retval = new ArrayList<OUT>();
		for (Object obj : resultList) {
			if (obj instanceof JSONable) {
				JSONable jsonable = Utils.downCast(JSONable.class, obj);
				retval.add(Utils.downCast(clzz, jsonable.toJSON()));

			}
		}

		return retval;
	}
	
	public static String getMD5(String data) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");

        messageDigest.update(data.getBytes());
        byte[] digest = messageDigest.digest();
        StringBuffer sb = new StringBuffer();
        for (byte b : digest) {
            sb.append(String.format("%02x", b & 0xff));
        }
        return sb.toString();
    }
	
}