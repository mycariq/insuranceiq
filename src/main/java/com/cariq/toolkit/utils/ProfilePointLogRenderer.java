package com.cariq.toolkit.utils;

import java.util.Date;
import java.util.UUID;

import org.jboss.logging.Logger;

import com.cariq.toolkit.utils.profile.ProfileRendererIfc;
import com.cariq.toolkit.utils.profile.ThroughputRendererIfc;

public class ProfilePointLogRenderer implements ProfileRendererIfc, ThroughputRendererIfc {
	private static Logger logger = Logger.getLogger("ProfilePoint");	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ptc.profilepoint.ProfileRendererIfc#WriteActivityDetails(java.util
	 * .UUID, java.lang.String, int)
	 */
	@Override
	public void writeActivityDetails(UUID activityId, String activityName,
			int totalTimeMsec) {
		logger.debug(String.format("ProfilePoint,%s,%s,%d", activityId.toString(),
				activityName, totalTimeMsec));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ptc.profilepoint.ProfileRendererIfc#WriteActionDetails(java.util.
	 * UUID, java.lang.String, int, int, double)
	 */
	@Override
	public void writeActionDetails(UUID activityId, String actionName,
			int callCount, int totalTimeMsec, double percentage, long size) {
		if (size == 0)
			logger.debug(String.format("Profilepoint,%s,%s,%d,%d,%f",
					activityId.toString(), actionName, callCount,
					totalTimeMsec, percentage));
		else
			logger.debug(String.format("Profilepoint,%s,%s,%d,%d,%f,%s",
					activityId.toString(), actionName, callCount,
					totalTimeMsec, percentage, Long.toString(size)));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ptc.profilepoint.ProfileRendererIfc#Flush()
	 */
	@Override
	public void Flush() {
		// TODO Auto-generated method stub

	}

	@Override
	public void writeMessage(String message) {
		logger.debug(String.format("Profilepoint,Message,%s", message));

	}

	@Override
	public void writeHeader(String activityId, String activityName,
			String actionName, String callCount, String totalTimeMsec,
			String percentage) {
		logger.debug(String.format("Profilepoint,%s,%s,%s,%s,%s",
				activityId.toString(), actionName, callCount, totalTimeMsec,
				percentage));

	}

	@Override
	public void render(String instrument, String... args) {
		StringBuffer buf = new StringBuffer();
		buf.append(instrument);
		for (String arg : args) {
			buf.append(", " + arg);
		}

		logger.debug(buf.toString());
	}

	@Override
	public void render(String prefix, String info, Date startTime, Date endTime, long packetCount, long incremental,
			Long throughputPPM) {
		logger.debug(String.format("ThroughputRecord,%s,%s,%s,%s,%d,%d,%d",
				prefix, info, startTime, endTime, packetCount, incremental, throughputPPM));		
	}
}
