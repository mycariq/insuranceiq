package com.cariq.toolkit.utils;

import java.io.IOException;
import java.util.List;

public class SimpleCSVRenderer implements ReportRenderer {
	CarIQCSVFileWriter writer;
	

	public SimpleCSVRenderer(String fileName) {
		super();
		writer = new CarIQCSVFileWriter(fileName);
	}

	@Override
	public void writeHeader(List<String> header) throws IOException {
		writer.writeHeader(header);
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.workitem.ReportRenderer#writeData(java.util.List)
	 */
	@Override
	public void writeData(List<String> row) throws IOException {
		writer.write(row);
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.workitem.ReportRenderer#open()
	 */
	@Override
	public void open() throws IOException {

	}

	/* (non-Javadoc)
	 * @see java.lang.AutoCloseable#close()
	 */
	@Override
	public void close() throws IOException {
		writer.close();
	}

}
