package com.cariq.toolkit.utils;


public interface ObjectBuilder<T> {

	ObjectBuilder<T> configure(String setting, Object value);

	ObjectBuilder<T> add(T value);
	ObjectBuilder<T> add(String parameter, T value);

	ObjectBuilder<T> build();
}
