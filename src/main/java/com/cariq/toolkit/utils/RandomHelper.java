package com.cariq.toolkit.utils;

import java.util.Date;
import java.util.Random;

public class RandomHelper {
	public static int InRange(int i, int j) {
		Random r = new Random(new Date().getTime());
		return r.nextInt(j - i) + i;
	}

	public static boolean randomMatch(int frequency) {
		Random r = new Random(new Date().getTime());
		return ((r.nextInt() % frequency) == 0);	}

}
