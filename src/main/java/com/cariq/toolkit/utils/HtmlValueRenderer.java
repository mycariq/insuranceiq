package com.cariq.toolkit.utils;

/**
 * Create HTML value based on input raw value
 * Based on rules, apply changes on the value based on the key
 * @author hrishi
 *
 */
public interface HtmlValueRenderer {
	String toHtmlString(String key, String value);
}
