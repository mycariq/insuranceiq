package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class BeanAttributeBag {
	Class<?> beanClass;
	Map<String, Object> attributeMap = null;

	/**
	 * @return the attributeMap
	 */
	public Map<String, Object> getAttributeMap() {
		return attributeMap;
	}

	public BeanAttributeBag(Class<?> beanClass) {
		this(beanClass, new HashMap<String, Object>());
	}
	
	public BeanAttributeBag(Class<?> beanClass, Map<String, Object> attrMap) {
		super();
		this.beanClass = beanClass;
		attributeMap = attrMap;
	}


	/**
	 * @return the beanClass
	 */
	public Class<?> getBeanClass() {
		return beanClass;
	}

	public List<String> getAttributeList() {
		List<String> list = new ArrayList<String>();
		list.addAll(this.attributeMap.keySet());
		return list;
	}
	
	
	

}
