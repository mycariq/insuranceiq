package com.cariq.toolkit.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.profile.ProfilePoint;

public class CarIQCSVFileWriter {
	List<List<String>> lines = new ArrayList<List<String>>();
	static final int BATCH_SIZE = 500;
	String fileName;

	public CarIQCSVFileWriter(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Do Buffered write - flush only when BATCH is full
	 */
	public void write(List<String> line) throws IOException {
		lines.add(line);
		if (lines.size() >= BATCH_SIZE)
			flush();
	}

	/**
	 * Flush the records into the file and close the handle
	 */
	public void flush() throws IOException {
		try (ProfilePoint _CSVTableWriter_Flush = ProfilePoint
				.profileAction("ProfAction_CSVFileWriter_Flush")) {
			// open file in append mode and write all Records
			File fout = new File(fileName);
			FileOutputStream fos = new FileOutputStream(fout, true); // always
																		// append
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new OutputStreamWriter(fos));

				for (List<String> record : lines) {
					bw.write(toCSVString(record));
					bw.newLine();
				}
			} finally {
				// close the handle
				if (null != bw)
					bw.close();

				lines.clear();
			}
		}
	}

	public void close() throws IOException {
		flush();
	}

	/**
	 * Write Header line of the file
	 * 
	 * @TODO avoid code duplication
	 */
	public void writeHeader(List<String> header) throws IOException {
		// open file in append mode and write all Records
		File fout = new File(fileName);
		FileOutputStream fos = new FileOutputStream(fout);

		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			bw.write(toCSVString(header));
			bw.newLine();
		} finally {
			// close the handle
			if (null != bw)
				bw.close();
		}

	}

	/**
	 * Construct the String from List of columns
	 * 
	 * @param columns
	 * @return
	 */
	private String toCSVString(List<String> columns) {
		StringBuilder bldr = new StringBuilder();
		for (String column : columns) {
			bldr.append(column).append(",");
		}
		return bldr.toString();
	}
}
