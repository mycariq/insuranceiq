package com.cariq.toolkit.utils;

/**
 * @author rohini
 * It is a conversion mechanism. 
 * This Interface will convert any object to GenericJSON and VISE VERSA. 
 */
public interface GenericJsonable {
	
	/**
	 * Convert any object to GenericJSON type.
	 * @return
	 */
	public GenericJSON toGenericJSON();
	
	/**
	 * Convert or map GenericJson to any object.
	 * @param json
	 */
	public void fromGenricJSON(GenericJSON json);

}
