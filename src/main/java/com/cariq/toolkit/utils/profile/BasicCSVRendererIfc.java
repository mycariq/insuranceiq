package com.cariq.toolkit.utils.profile;

/**
 * Renderer interface to output the Profiler/Instrument data
 * @author HVN
 *
 */
public interface BasicCSVRendererIfc {
	void render(String instrument, String... args);
}
