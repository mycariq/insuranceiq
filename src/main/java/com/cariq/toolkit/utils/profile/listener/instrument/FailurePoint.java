package com.cariq.toolkit.utils.profile.listener.instrument;

import java.util.Properties;

import javax.annotation.Resource;

import com.cariq.toolkit.utils.profile.BasicCSVRendererIfc;
import com.cariq.toolkit.utils.profile.ProfActivity;
import com.cariq.toolkit.utils.profile.listener.DiagnosticInstrument;
import com.cariq.toolkit.utils.profile.listener.DiagnosticInstrumentFactory;
import com.cariq.toolkit.utils.profile.listener.FailurePointException;

/**
 * Instrument to define artificial failure points - from where exception would
 * be artificially thrown
 * 
 * @author HVN
 *
 */
public class FailurePoint extends DiagnosticInstrumentFactory {
	public enum FailureTime {
		BEFORE, AFTER
	};

	// HVN TODO - do it formally - separate it into a different project.
	@Resource(name = "cariqProfileProperties")
	private static Properties cariqProfileProperties;

	static String[] before_failurePoints;
	static String[] after_failurePoints;
	static {
		try {
			String beforeFailures = cariqProfileProperties
					.getProperty("wt.facade.ixb.profile.FailurePoint.Before");
			if (beforeFailures != null) {
				before_failurePoints = beforeFailures
						.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
			}
			String afterFailures = cariqProfileProperties
					.getProperty("wt.facade.ixb.profile.FailurePoint.After");
			if (afterFailures != null) {
				after_failurePoints = afterFailures
						.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
			}
		} catch (Exception e) {
			before_failurePoints = null;
			after_failurePoints = null;
		}
	}

	public FailurePoint(BasicCSVRendererIfc renderer) {
		super(renderer);
	}

	@Override
	public DiagnosticInstrument getDiagnosticInstrument(
			ProfActivity rootActivity, String newAction) {
		if (before_failurePoints != null) {
			for (String failPoint : before_failurePoints) {
				if (newAction.contains(failPoint))
					return new FailurePointInstrument(newAction, getRenderer(),
							FailureTime.BEFORE);
			}
		}

		if (after_failurePoints != null) {
			for (String failPoint : after_failurePoints) {
				if (newAction.contains(failPoint))
					return new FailurePointInstrument(newAction, getRenderer(),
							FailureTime.AFTER);
			}
		}

		return null;
	}

	static class FailurePointInstrument extends DiagnosticInstrument {
		String action;
		FailureTime failureTime;

		public FailurePointInstrument(String action,
				BasicCSVRendererIfc renderer, FailureTime failureTime) {
			super(renderer);
			if (failureTime == FailureTime.BEFORE) {
				try {
					throw new FailurePointException(FailureTime.BEFORE, action);
				} catch (FailurePointException e) {
					getRenderer().render("FailurePoint", "BEFORE", action,
							e.getMessage());
					printStackTrace(getRenderer(), e);
					throw e;
				}
			}

			this.action = action;
			this.failureTime = failureTime;
		}

		@Override
		public void close() {
			if (failureTime == FailureTime.AFTER) {
				try {
					throw new RuntimeException(
							"FailurePoint!!! - Exception AFTER doing operation: "
									+ action);
				} catch (FailurePointException e) {
					getRenderer().render("FailurePoint", "AFTER", action,
							e.getMessage());
					printStackTrace(getRenderer(), e);
					throw e;
				}
			}

		}

		private void printStackTrace(BasicCSVRendererIfc renderer, Exception e) {
			// TODO Auto-generated method stub
			for (StackTraceElement element : e.getStackTrace()) {
				renderer.render("FailurePoint", "Exception", element.toString());
			}

		}

		public String getDescription() {
			return "Can throw Pre or Post Exception";
		}

	}

}
