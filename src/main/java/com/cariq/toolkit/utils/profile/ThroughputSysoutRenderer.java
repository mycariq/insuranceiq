package com.cariq.toolkit.utils.profile;

import java.util.Date;

public class ThroughputSysoutRenderer implements ThroughputRendererIfc {

	@Override
	public void render(String name, String info, Date startTime, Date endTime, long packetCount, long increment, Long throughputPPM) {
		System.out.println("ThroughputRecord" + "," + name + "," + info + ","  + startTime + "," + endTime + "," + packetCount + "," + increment + "," + throughputPPM);	
	}
}
