package com.cariq.toolkit.utils.profile;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

import com.cariq.toolkit.utils.profile.listener.DiagnosticInstrumentFactory;
import com.cariq.toolkit.utils.profile.listener.DiagnosticInstrumentStack;
import com.cariq.toolkit.utils.profile.listener.FailurePointException;

/**
 * Renderer to write profile data in to log file Add following lines to
 * log4j.properties to activate log # ProfilePoint log messages #ProfilePoint
 * log4j.appender.PP=org.apache.log4j.RollingFileAppender
 * log4j.appender.PP.File=d:/logs/cq_profilepoint.csv
 * log4j.appender.PP.DatePattern='.'yyyy-MM-dd
 * log4j.appender.PP.layout=org.apache.log4j.PatternLayout
 * log4j.appender.PP.layout.ConversionPattern=%d %-5p,%m%n
 * log4j.logger.ProfilePoint=DEBUG, PP
 * 
 * @author hnene
 *
 */
// class IxbProfileLogRenderer implements ProfileRendererIfc {
// private static final Logger profilepointLogger =
// LogR.getLogger("ProfilePoint");
// /* (non-Javadoc)
// * @see
// com.ptc.profilepoint.ProfileRendererIfc#WriteActivityDetails(java.util.UUID,
// java.lang.String, int)
// */
// @Override
// public void writeActivityDetails(UUID activityId, String activityName, int
// totalTimeMsec) {
// if(profilepointLogger !=null && profilepointLogger.isDebugEnabled())
// render("ProfilePoint", activityId.toString(), activityName,
// String.valueOf(totalTimeMsec));
// }
//
// /* (non-Javadoc)
// * @see
// com.ptc.profilepoint.ProfileRendererIfc#WriteActionDetails(java.util.UUID,
// java.lang.String, int, int, double)
// */
// @Override
// public void writeActionDetails(UUID activityId, String actionName, int
// callCount, int totalTimeMsec, double percentage, long size) {
// if(profilepointLogger !=null && profilepointLogger.isDebugEnabled()){
// if(size == 0)
// render("ProfilePoint", activityId.toString(), actionName,
// String.valueOf(callCount), String.valueOf(totalTimeMsec),
// String.valueOf(percentage));
// else
// render("ProfilePoint", activityId.toString(), actionName,
// String.valueOf(callCount), String.valueOf(totalTimeMsec),
// String.valueOf(percentage), String.valueOf(size));
// }
//
//
// }
//
// /* (non-Javadoc)
// * @see com.ptc.profilepoint.ProfileRendererIfc#Flush()
// */
// @Override
// public void Flush() {
// // TODO Auto-generated method stub
//
// }
//
// @Override
// public void writeMessage(String message) {
// if(profilepointLogger !=null && profilepointLogger.isDebugEnabled())
// profilepointLogger.debug(String.format("Message,%s", message));
//
// }
//
// @Override
// public void writeHeader(String activityId, String activityName, String
// actionName, String callCount, String totalTimeMsec,
// String percentage) {
// if(profilepointLogger !=null && profilepointLogger.isDebugEnabled())
// profilepointLogger.debug(String.format("%s,%s,%s,%s,%s",
// activityId.toString(), actionName, callCount, totalTimeMsec, percentage));
//
// }
//
// @Override
// public void render(String instrument, String... args) {
// if (profilepointLogger != null || profilepointLogger.isDebugEnabled()) {
// StringBuffer buf = new StringBuffer(instrument);
// for(String arg: args) {
// buf.append(", " + arg);
// }
// profilepointLogger.debug(buf.toString());
// }
// }
// }

class SysoutRenderer implements ProfileRendererIfc {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ptc.profilepoint.ProfileRendererIfc#WriteActivityDetails(java.util
	 * .UUID, java.lang.String, int)
	 */
	@Override
	public void writeActivityDetails(UUID activityId, String activityName,
			int totalTimeMsec) {
		System.out.format("==Profilepoint==,%s,%s,%d", activityId.toString(),
				activityName, totalTimeMsec);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ptc.profilepoint.ProfileRendererIfc#WriteActionDetails(java.util.
	 * UUID, java.lang.String, int, int, double)
	 */
	@Override
	public void writeActionDetails(UUID activityId, String actionName,
			int callCount, int totalTimeMsec, double percentage, long size) {
		if (size == 0)
			System.out.format("==Profilepoint==,%s,%s,%d,%d,%f\n",
					activityId.toString(), actionName, callCount,
					totalTimeMsec, percentage);
		else
			System.out.format("==Profilepoint==,%s,%s,%d,%d,%f,%s\n",
					activityId.toString(), actionName, callCount,
					totalTimeMsec, percentage, Long.toString(size));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ptc.profilepoint.ProfileRendererIfc#Flush()
	 */
	@Override
	public void Flush() {
		// TODO Auto-generated method stub

	}

	@Override
	public void writeMessage(String message) {
		System.out.format("==Profilepoint==,Message,%s", message);

	}

	@Override
	public void writeHeader(String activityId, String activityName,
			String actionName, String callCount, String totalTimeMsec,
			String percentage) {
		System.out.format("==Profilepoint==,%s,%s,%s,%s,%s\n",
				activityId.toString(), actionName, callCount, totalTimeMsec,
				percentage);

	}

	@Override
	public void render(String instrument, String... args) {
		System.out.print(instrument);
		for (String arg : args) {
			System.out.print(", " + arg);
		}
		System.out.print("\n");
	}
}

class CarIQDefaultProfileConfigurator implements ProfileConfiguratorIfc {

	ProfileRendererIfc renderer = new SysoutRenderer();

	@Override
	public ProfileRendererIfc getRenderer() {
		return renderer;
	}

	@Override
	public void setRenderer(ProfileRendererIfc renderer) {
		this.renderer = renderer;
		// Print the header
		renderer.render("ProfilePoint",
				"Activity Id", "Activity Name", "Call Count",
				"Time (msec)", "Percentage", "Size");
	}

	@Override
	public boolean isProfilingEnabled() {
		return true;
	}

	@Override
	public boolean isProfilingEnabled(String activityName) {
		return true;
	}

	@Override
	public List<DiagnosticInstrumentFactory> getInstrumentFactories() {

		return new ArrayList<DiagnosticInstrumentFactory>();
	}

}

// class DefaultProfileConfigurator implements ProfileConfiguratorIfc {
// static HashMap<String, String> activities = null;
// // ProfileRendererIfc renderer = new SysoutRenderer();
// ProfileRendererIfc renderer = new IxbProfileLogRenderer();
//
// static {
// try {
// String profActivities =
// WTProperties.getLocalProperties().getProperty("wt.facade.ixb.profile.Activities");
// if (profActivities != null)
// {
// activities = new HashMap<String, String>();
// String[] activitiesArr =
// profActivities.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
// for (int i = 0; i < activitiesArr.length; i++)
// activities.put(activitiesArr[i], activitiesArr[i]);
//
// // initialize renderer from logR
// }
//
// } catch (Exception e)
// {
// activities = null;
// }
// }
//
// @Override
// public ProfileRendererIfc getRenderer() {
// // TODO return logger based renderer
// return renderer;
// }
//
// @Override
// public boolean isProfilingEnabled() {
// if (activities == null || activities.isEmpty())
// return false;
//
// return true;
// }
//
// @Override
// public boolean isProfilingEnabled(String activityName) {
// if (!isProfilingEnabled())
// return false;
//
// // read based on properties file
// return activities.containsKey(activityName);
// }
//
// @Override
// public void setRenderer(ProfileRendererIfc renderer) {
// this.renderer = renderer;
// }
//
// @Override
// public List<DiagnosticInstrumentFactory> getInstrumentFactories() {
// // read the instruments from the properties file
// String instrumentsStr;
// try {
// instrumentsStr =
// WTProperties.getLocalProperties().getProperty("wt.facade.ixb.profile.Instruments");
//
// if (instrumentsStr != null)
// {
// List<DiagnosticInstrumentFactory> factories = new
// ArrayList<DiagnosticInstrumentFactory>();
//
// String[] instrumentsArr =
// instrumentsStr.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
// for (String instrumentType : instrumentsArr) {
// try {
// Constructor<?> c =
// Class.forName(instrumentType).getConstructor(BasicCSVRendererIfc.class);
// factories.add((DiagnosticInstrumentFactory) c.newInstance(renderer));
// } catch (Exception e) {
// e.printStackTrace();
// }
// }
//
// return factories.isEmpty() ? null:factories;
// }
// } catch (IOException e1) {
// // TODO Auto-generated catch block
// e1.printStackTrace();
// }
//
// return null;
// }
// }

/**
 * class: Profilepoint class. Main RAII class for capturing profile information
 * of a code block Use eclipse macro to quickly create profilepoints in/around
 * the function Edit wt.properties file to profile an activity Edit
 * %WT_HOME%\codebase\WEB-INF\log4jmethodServer.properties to configure logger
 * 
 * @author hnene
 *
 *
 *         == Eclipse Action macro == try (ProfilePoint _${myvar} =
 *         ProfilePoint.profileAction("ProfAction_${myvar}")) {
 *         ${line_selection}${cursor} }
 * 
 *         == Eclipse Activity macro == try (ProfilePoint _${myvar} =
 *         ProfilePoint.profileActivity("ProfActivity_${myvar}")) {
 *         ${line_selection}${cursor} }
 *
 *         To profile activities, add activities - comma separated - as follows
 *         to %WT_HOME%\codebase\wt.properties:
 *         wt.facade.ixb.profile.Activities=
 *         ProfActivity_preview,ProfActivity_import
 *         ,ProfActivity_StandardRDService_previewReceivedDelivery
 */
public class ProfilePoint extends Instrument implements AutoCloseable {
	private ProfActivity rootActivity = null;
	private static ProfileConfiguratorIfc configurator = new CarIQDefaultProfileConfigurator();
	private ProfAction action;
	DiagnosticInstrumentStack instruments = null;
	private static List<DiagnosticInstrumentFactory> instrumentFactoryList;

	static {
		// Enclosing in try..catch as our instrument should never throw
		// exception.
		try {
			// set rendered by reading from properties file
//			if (configurator.isProfilingEnabled())
//				configurator.getRenderer().render("ProfilePoint",
//						"Activity Id", "Activity Name", "Call Count",
//						"Time (msec)", "Percentage", "Size");

			// load the listener factories
			instrumentFactoryList = configurator.getInstrumentFactories();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	/**
	 * * Function to create action profile point. Should be called as follows:
	 * try (ProfilePoint _activity = ProfilePoint.profileActivity("MyActivity"))
	 * { ... ... }
	 * 
	 * @param activity
	 * @return
	 */
	public static ProfilePoint profileActivity(String activityName) {
		try {
			return configurator.isProfilingEnabled() ? new ProfilePoint(
					activityName,
					configurator.isProfilingEnabled(activityName), 0) : null;
		} catch (FailurePointException e) { // failurepoint is artificial
											// exception - throw it
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Report Exception coming through the instrument
	 * 
	 * @param e
	 */
	public static void reportException(Throwable e) {
		if (!IsEnabled() || (ProfActivity.getCurrentActivity() == null))
			return;

		while (e != null) {
			for (StackTraceElement element : e.getStackTrace()) {
				configurator.getRenderer().render("ExceptionPoint",
						ProfActivity.getCurrentActivity().getName(),
						ProfActivity.getCurrentActivity().getUUID().toString(),
						element.toString());
			}
			e = e.getCause();
		}
	}

	/**
	 * * Function to create Activity profile point taking size. Should be called
	 * as follows: try (ProfilePoint _activity =
	 * ProfilePoint.profileActivity("MyActivity", size)) { ... ... }
	 * 
	 * @param activity
	 * @return
	 */
	public static ProfilePoint profileActivity(String activityName, long size) {
		try {
			return configurator.isProfilingEnabled() ? new ProfilePoint(
					activityName,
					configurator.isProfilingEnabled(activityName), size) : null;
		} catch (FailurePointException e) { // failurepoint is artificial
											// exception - throw it
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * * Function to create activity profile point with time threshold. Should
	 * be called as follows: try (ProfilePoint _activity =
	 * ProfilePoint.profileActivity("MyActivity", 500)) { ... ... }
	 * 
	 * @param activity
	 * @return
	 */
	public static ProfilePoint profileActivityWithTimeThreshold(
			String activityName, int thresholdInMS) {
		try {
			return configurator.isProfilingEnabled() ? new ProfilePoint(
					activityName,
					configurator.isProfilingEnabled(activityName), 0,
					thresholdInMS) : null;
		} catch (FailurePointException e) { // failurepoint is artificial
											// exception - throw it
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Function to create action profile point. Should be called as follows: try
	 * (ProfilePoint _action = ProfilePoint.profileAction("MyAction")) { ... ...
	 * }
	 * 
	 * @param action
	 * @return
	 */
	public static ProfilePoint profileAction(String actionName) {
		try {
			return configurator.isProfilingEnabled() ? new ProfilePoint(
					actionName, false, 0) : null;
		} catch (FailurePointException e) { // failurepoint is artificial
											// exception - throw it
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * For recursive call, have a constructor that is created without
	 * Action/Activity name
	 */
	ProfilePoint() {
		this(UUID.randomUUID().toString(), false, 0);
	}

	/**
	 * Constructor of ProfilePoint, profiling given Action/Activity
	 * 
	 * @param actionName
	 * @param isNewActivity
	 * @param size
	 */
	ProfilePoint(String actionName, boolean isNewActivity, long size) {
		// We enclose this in try..catch so , in any condition no exception will
		// be thrown
		this(actionName, isNewActivity, size,
				ProfActivity.DEFAULT_THRESHOLD_TIME);
	}

	/**
	 * Constructor of ProfilePoint, profiling given Action/Activity
	 * 
	 * @param actionName
	 * @param isNewActivity
	 * @param size
	 */
	ProfilePoint(String actionName, boolean isNewActivity, long size,
			int thresholdTimeMS) {
		// We enclose this in try..catch so , in any condition no exception will
		// be thrown
		try {
			begin(actionName, isNewActivity, size);
			if (ProfActivity.getCurrentActivity() != null)
				createInstruments(actionName);

			if (isNewActivity && rootActivity != null)
				rootActivity.setThresholdInMS(thresholdTimeMS);
		} catch (FailurePointException e) { // failurepoint is artificial
											// exception - throw it
			throw e;
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private void begin(String name, boolean startNewActivity, long size) {
		if (!IsEnabled())
			return;

		AtomicReference<ProfActivity> ref = new AtomicReference<ProfActivity>(
				rootActivity);
		action = ProfAction.CreateAndStart(name, startNewActivity, ref);
		rootActivity = ref.get();
		if (size == 0)
			return;

		ProfActivity current = ProfActivity.getCurrentActivity();
		if (current == null || current.getSize() != 0)
			return;

		current.setSize(size);
	}

	private void createInstruments(String actionName) {
		if (instrumentFactoryList == null)
			return;

		for (DiagnosticInstrumentFactory factory : instrumentFactoryList) {
			if (instruments == null)
				instruments = new DiagnosticInstrumentStack();

			instruments.push(factory.getDiagnosticInstrument(
					ProfActivity.getCurrentActivity(), actionName));
		}
	}

	/**
	 * Destructor - i.e. close of AutoClosable - time to calculate time
	 */
	@Override
	public void close() {
		try {
			if (!IsEnabled() || action == null)
				return;

			action.stop();

			if (rootActivity != null) {
				rootActivity.report(configurator.getRenderer());
				rootActivity.dispose();
			}

			if (instruments != null)
				instruments.close();
		} catch (FailurePointException e) { // failurepoint is artificial
											// exception - throw it
			throw e;
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	/**
	 * Static function to return the configurator to the ProfilePoint system
	 */
	public static ProfileConfiguratorIfc getConfigurator() {
		return configurator;
	}

	/**
	 * Static function to attach a configurator to ProfilePoint system
	 * 
	 * @param _configurator
	 */
	public static void setConfigurator(ProfileConfiguratorIfc _configurator) {
		configurator = _configurator;
		// load the listener factories
		instrumentFactoryList = configurator.getInstrumentFactories();
	}

	/**
	 * Static function to connect rendered with ProfilePoint - should be done
	 * using delegate mechanism from properties ideally
	 * 
	 * @param renderer
	 */
	public static void setRenderer(ProfileRendererIfc renderer) {
		configurator.setRenderer(renderer);
	}
}
