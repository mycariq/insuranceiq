package com.cariq.toolkit.utils.profile;

import java.util.Collection;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

/**
 * Class to capture the activity in the code flow. Activity is the high level
 * functionality. Activity encounters various actions during the code flow.
 * Profile report of activity is profile report of its actions Activity
 * profiling can be turned off or on using the properties files Every Activity
 * has a unique Id. It helps in comprehending the report in multi-threaded
 * environment
 * 
 * @author hnene
 *
 */
public class ProfActivity extends Instrument {
	private UUID id;
	private String name;
	private ProfAction rootAction;
	HashMap<String, ProfAction> actionDir = new HashMap<String, ProfAction>();
	long size = 0;

	// Threshold time - below which ACTIVITY should not be printed
	static final int DEFAULT_THRESHOLD_TIME = 500; // don't report if it is less
													// than 500 MS
	int thresholdInMS = DEFAULT_THRESHOLD_TIME;

	public int getThresholdInMS() {
		return thresholdInMS;
	}

	public void setThresholdInMS(int thresholdInMS) {
		this.thresholdInMS = thresholdInMS;
	}

	private static ThreadLocal<ProfActivity> Current = new ThreadLocal<ProfActivity>() {
		protected synchronized ProfActivity initialValue() {
			return null;
		}
	};

	static ProfActivity getCurrentActivity() {
		return Current.get();
	}

	static ProfActivity setCurrentActivity(ProfActivity activity) {
		Current.set(activity);
		return Current.get();
	}

	public UUID getUUID() {
		return id;
	}

	public String getName() {
		return name;
	}

	ProfActivity(ProfAction rootAction) {
		if (Current.get() != null)
			throw new RuntimeException("Current Activity is not null!");

		id = new UUID(new Random().nextInt(), new Random().nextInt());
		// id = UUID.randomUUID();
		name = rootAction.getName();
		Current.set(this);
		add(rootAction);
		this.rootAction = rootAction;
	}

	void add(ProfAction action) {
		action.setActivityId(id);
		actionDir.put(action.getName(), action);

	}

	Collection<ProfAction> getActions() {
		return actionDir.values();
	}

	// / <summary>
	// / Report the activity by calling renderer functions
	// / </summary>
	// / <param name="renderer"></param>
	void report(ProfileRendererIfc renderer) {
		// Don't report activities that have little impact
		if (null != getFirst() && getFirst().getElapsedTimeMsec() < thresholdInMS)
			return;

		// Just before reporting, evaluate the times taken by each action in the
		// stack
		evaluateTime();
		// Action rootAction = First;
		// renderer.WriteActivityDetails(id, name, rootAction.ElapsedTimeMsec);
		for (ProfAction action : getActions()) {
			renderer.writeActionDetails(id, action.getName(), action.getCallCount(), action.getElapsedTimeMsec(),
					action.getPecentageTotalTime(), size);
		}
	}

	private void evaluateTime() {
		int totalTime = getFirst().getElapsedTimeMsec();
		for (ProfAction action : getActions()) {
			action.evaluateTime(totalTime);
		}
	}

	ProfAction get(String name) {
		return actionDir.get(name);
	}

	// / <summary>
	// / First or Root action - the outermost action
	// / </summary>
	ProfAction getFirst() {
		return rootAction;
	}

	void dispose() {
		Current.set(null);
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getSize() {
		return size;
	}
}
