package com.cariq.toolkit.utils.profile;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Minutes;

import com.cariq.toolkit.utils.ProfilePointLogRenderer;

/**
 * Throughput Renderer to take count and log of throughput numbers.
 * It uses Logger to print the output.
 * It's a stateful but very light component that keeps track of Throughput over time
 * 
 * @author hrishi
 *
 */
public class ThroughputCalculator extends Instrument {
	/**
	 * Throughput Record indicates a Record in the block of time
	 * It is maintained for reference.
	 * @author hrishi
	 *
	 */
	static class ThroughputRecord implements Comparable<ThroughputRecord> {
		Date startTime, endTime;
		long startCount, packetCount, threshold;
		
		ThroughputRecord(Date startTime, long startCount, long threshold) {
			this.startCount = this.packetCount = startCount;
			this.startTime = startTime;
			this.threshold = threshold;
			this.endTime = null; // will be set only after it is sealed
		}

		/**
		 * Get the throughput speed of processing jobs/packets
		 * @return
		 */
		Long getThroughputPPM() {
			long milisec = (endTime.getTime() - startTime.getTime());
			if (milisec <= 0) // divide by 0 condition!
				return -1L;
			
			return (1000*60*(packetCount - startCount))/milisec;
		}
		
		@Override
		public int compareTo(ThroughputRecord other) {
			return getThroughputPPM().compareTo(other.getThroughputPPM());
		}
		
		/**
		 * Render output to renderer
		 * @param renderer
		 * @param prefix
		 */
		void render(ThroughputRendererIfc renderer, String name, String info) {
			// Should be something like this:
			// ThroughputRecorder, <name>, <start Time>, <end Time>, <total packets>, <packets in time>, <Throughput PPM>
			// ThroughputRecorder, Current Throughput, 2016-09-15 13:11:02, 2016-09-15 13:13:02, 1121345, 5000, 2473
			renderer.render(name, info, startTime, endTime, packetCount, (packetCount - startCount), getThroughputPPM());
		}
		
		/**
		 * How old is this record
		 * @return
		 */
		int getAgeInMinutes() {
			return getDiffInMinutes(new Date(), this.endTime);
		}
		
		private int getDiffInMinutes(Date after, Date before) {
			return Minutes.minutesBetween(new DateTime(before), new DateTime(after)).getMinutes();
		}

		boolean increment() {
			return increment(1);
		}
		/**
		 * Add Packets till it is complete. Once complete, seal it and return false
		 * @param count
		 * @return
		 */
		synchronized boolean increment(long count) {
			if (isSealed()) {
				//throw new RuntimeException("Throughput Packet is already sealed!");
				return false;
			}			
			
			packetCount += count;
			// System.out.println("Threshold = " + threshold + "PacketCount = " + packetCount + " startCount = " + startCount + "Difference = " + (packetCount - startCount));
			if (packetCount < (startCount + threshold))
				return false;
			
			sealRecord();
			return true;
		}

		private void sealRecord() {
			if (isSealed()) {
				//throw new RuntimeException("Throughput Packet is already sealed!");
				return;
			}

			endTime = new Date();		
		}

		private boolean isSealed() {
			return (endTime != null);
		}

		public long getPacketCount() {
			return packetCount;
		}
		
	}
	
	static ThroughputRendererIfc defaultRenderer = new ProfilePointLogRenderer();
	static final long DEFAULT_THROUGHPUT_THRESHOLD = 5000;
	private static final int RecentMaxRecordThresholdInMinutes = 60; // 1 hour threshold
	
	String name;
	long packetThreshold;
	ThroughputRendererIfc renderer;

	ThroughputRecord maxThroughputRecord, recentMaxThroughputRecord, lastThroughputRecord, currentThroughputRecord;
	
	public ThroughputCalculator(String name, long packetThreshold, ThroughputRendererIfc renderer) {
		this.name = name;
		this.packetThreshold = packetThreshold < 0 ? DEFAULT_THROUGHPUT_THRESHOLD : packetThreshold;
		this.renderer = renderer == null ? defaultRenderer : renderer;
		reset();
	}
	
	public ThroughputCalculator(String name, long packetThreshold) {
		this(name, packetThreshold, null);
	}
	
	public ThroughputCalculator(String name) {
		this(name, -1, null);
	}
	
	public long getPacketThreshold() {
		return packetThreshold;
	}


	public ThroughputRecord getMaxThroughputRecord() {
		return maxThroughputRecord;
	}


	public ThroughputRecord getRecentMaxThroughputRecord() {
		return recentMaxThroughputRecord;
	}


	public ThroughputRecord getLastThroughputRecord() {
		return lastThroughputRecord;
	}


	public ThroughputRecord getCurrentThroughputRecord() {
		if (null == currentThroughputRecord) {
			long startPacketCount = lastThroughputRecord == null ? 0 : lastThroughputRecord.getPacketCount();
			currentThroughputRecord = new ThroughputRecord(new Date(), startPacketCount, this.packetThreshold);
		}
		
		return currentThroughputRecord;
	}


	public ThroughputCalculator increment() {
		return increment(1);
	}
	
	synchronized public ThroughputCalculator increment(long count) {
		//System.out.println("Count = " + count + "Threshold = " + packetThreshold);
		if (!getCurrentThroughputRecord().increment(count)) // may be initialized
			return this;
		
		// update existing records and render it
		updateRecords();
		
		// finally set current record empty
		currentThroughputRecord = null;
		
		return this;
	}
	
	private void updateRecords() {
		// update last with current
		lastThroughputRecord = currentThroughputRecord;
		if (recentMaxThroughputRecord != null && recentMaxThroughputRecord.getAgeInMinutes() > RecentMaxRecordThresholdInMinutes)
			recentMaxThroughputRecord = null;
		
		// Update Max and Recent Max Records
		maxThroughputRecord = compareAndAssign(maxThroughputRecord, lastThroughputRecord);
		recentMaxThroughputRecord = compareAndAssign(recentMaxThroughputRecord, lastThroughputRecord);
		
		// Render it
		lastThroughputRecord.render(renderer, name, "Current");
		recentMaxThroughputRecord.render(renderer, name, "RecentMax");
		maxThroughputRecord.render(renderer, name, "Maximum");
	}


	// Check for the record and assign
	private ThroughputRecord compareAndAssign(ThroughputRecord refRecord, ThroughputRecord newRecord) {
		if (refRecord == null || refRecord.compareTo(newRecord) < 0)
			return newRecord;
		
		return refRecord;
	}

	public void reset() {
		maxThroughputRecord = recentMaxThroughputRecord = lastThroughputRecord = currentThroughputRecord = null;
	}
	
	public static void setRenderer(ThroughputRendererIfc renderer) {
		defaultRenderer = renderer;
	}
}
