package com.cariq.toolkit.utils.profile;

import java.util.Date;

public interface ThroughputRendererIfc {

	void render(String prefix, String info, Date startTime, Date endTime, long packetCount, long l, Long throughputPPM);

}
