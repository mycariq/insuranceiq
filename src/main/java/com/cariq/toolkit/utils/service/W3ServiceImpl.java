/**
 * 
 */
package com.cariq.toolkit.utils.service;

import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.wwwservice.api.W3Query.W3QueryJson;
import com.cariq.toolkit.utils.wwwservice.api.W3QueryAPIImpl;
import com.cariq.toolkit.utils.wwwservice.api.W3QueryInterface;
import com.cariq.toolkit.utils.wwwservice.api.W3QueryResult.W3QueryResultJson;

/**
 * @author hrishi
 *
 */
public class W3ServiceImpl extends W3Service {
	/**
	 * Execute w3 query.
	 *
	 * @param query
	 *            the query
	 * @param user
	 *            the user
	 * @return the object
	 */
	@Transactional
	@Override
	public Object executeW3Query(W3QueryJson query) {
		// Change implementation of this from Sync to Async
		// Create a Job derivative to house following code so that it will be
		// executed async
		// Convert JSon to String
	
		W3QueryInterface w3queryIf = new W3QueryAPIImpl();
		
		return w3queryIf.executeQuery(query);
	}

}
