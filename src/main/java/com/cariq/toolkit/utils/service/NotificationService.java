package com.cariq.toolkit.utils.service;

import java.util.Map;

import com.cariq.toolkit.model.User;


/**
 * The Interface NotificationService.
 */
public interface NotificationService {

	/**
	 * The Enum CarIQNotificationType.
	 */
	public enum CarIQNotificationType {

		/** The Welcome to fleet IQ. */
		WelcomeToFleetIQ,
		/** The Forgot password request. */
		ForgotPasswordRequest,
		/** The Hello. */
		Hello,
		/** The Alert notification. */
		AlertNotification, ExporterAsyncCompletion;
	}

	/** The Constant emailFrom. */
	public static final String emailFrom = "CarIQ";//@TMPL public static final String emailFrom = "[% ROOT.servers.email.from %]"; @TMPL

	/** The Constant mailReplyTo. */
	public static final String mailReplyTo = "support@mycariq.com"; //@TMPL public static final String mailReplyTo = "[% ROOT.servers.email.replyTo %]"; @TMPL


	/** The Constant mailToSupport. */
	public static final String mailToSupport = "support@mycariq.com"; // @TMPL public static final String mailToSupport = "[% ROOT.servers.email.egales %]"; @TMPL


	/**
	 * The Class NotificationMethod.
	 */
	public static class NotificationMethod {

		/** The Constant SMS. */
		public static final String SMS = "SMS";

		/** The Constant EMAIL. */
		public static final String EMAIL = "EMAIL";

		/** The Constant PUSH. */
		public static final String PUSH = "PUSH";

		/** The Constant CALLBACK. */
		public static final String CALLBACK = "CALLBACK";
	}

	/**
	 * Send message.
	 *
	 * @param mailTo
	 *            the mail to
	 * @param message
	 *            the message
	 */
	public void sendMessage(String mailTo, String message);

	/**
	 * Gets the notification text.
	 *
	 * @param type
	 *            the type
	 * @param model
	 *            the model
	 * @return the notification text
	 */
	String getNotificationText(CarIQNotificationType type, Map<String, Object> model);

	/**
	 * Send wel come email notification.
	 *
	 * @param admin
	 *            the admin
	 */
	public void sendWelComeEmailNotification(User user);

	/**
	 * Send notification email.
	 *
	 * @param emailTo
	 *            the email to
	 * @param subject
	 *            the subject
	 * @param type
	 *            the type
	 * @param model
	 *            the model
	 */
	public void sendNotificationEmail(String emailTo, String subject, CarIQNotificationType type,
			Map<String, Object> model);
}