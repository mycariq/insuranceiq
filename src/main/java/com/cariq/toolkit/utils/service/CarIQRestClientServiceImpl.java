package com.cariq.toolkit.utils.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class W4IQRestClientServiceImpl.
 */
@Service
public class CarIQRestClientServiceImpl implements CarIQRestClientService {

	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIQRestClientServiceImpl");
	
	/**
	 * Instantiates a new w 4 IQ rest client service impl.
	 */
	public CarIQRestClientServiceImpl() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.w4iq.W4IQRestClientService#post(java.lang.Object,
	 * java.lang.String, java.lang.String, java.lang.Class)
	 */
	@Override
	public <R, I> R post(I inputJson, String url, String basicAuth, Class<R> responseClass) {
		try (ProfilePoint _RestClientPost = ProfilePoint.profileAction("ProfAction_RestClientPost")) {
			// set basic auth headers
			HttpHeaders requestHeaders = Utils.setHeaders(basicAuth);
			logger.debug("Inside Post call with url: " + url + " and inputJson = " + inputJson);
			RestTemplate restTemplate = new RestTemplate(Utils.getRequestFactory());
			ResponseEntity<R> response = restTemplate.exchange(url, HttpMethod.POST,
					new HttpEntity<I>(inputJson, requestHeaders), responseClass);
			logger.debug("Response : " + response.getBody());
			return response.getBody();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.w4iq.W4IQRestClientService#get(java.lang.String,
	 * java.lang.String, java.lang.Class)
	 */
	@Override
	public <R> R get(String url, String basicAuth, Class<R> responseClass) {
		try (ProfilePoint _RestClientService_Get = ProfilePoint.profileAction("ProfAction_RestClientService_Get")) {
			// set basic auth headers
			HttpHeaders requestHeaders = Utils.setHeaders(basicAuth);
			RestTemplate restTemplate = new RestTemplate(Utils.getRequestFactory());
			ResponseEntity<R> response = restTemplate.exchange(url, HttpMethod.GET,
					new HttpEntity<String>(requestHeaders), responseClass);
			return response.getBody();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.w4iq.W4IQRestClientService#post(java.lang.Object,
	 * java.lang.String, java.lang.String, java.lang.Class)
	 */
	@Override
	public <R> R delete(String url, String basicAuth, Class<R> responseClass) {
		try (ProfilePoint _RestClientDelete = ProfilePoint.profileAction("ProfAction_RestClientDelete")) {
			// set basic auth headers
			HttpHeaders requestHeaders = Utils.setHeaders(basicAuth);
			RestTemplate restTemplate = new RestTemplate(Utils.getRequestFactory());
			ResponseEntity<R> response = restTemplate.exchange(url, HttpMethod.DELETE,
					new HttpEntity<String>(requestHeaders), responseClass);
			return response.getBody();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.utils.service.CarIQRestClientService#put(java.lang.
	 * Object, java.lang.String, java.lang.String, java.lang.Class)
	 */
	@Override
	public <R, I> R put(I inputJson, String url, String basicAuth, Class<R> responseClass) {
		try (ProfilePoint _RestClientPut = ProfilePoint.profileAction("ProfAction_RestClientService_Put")) {
			// set basic auth headers
			HttpHeaders requestHeaders = Utils.setHeaders(basicAuth);
			RestTemplate restTemplate = new RestTemplate(Utils.getRequestFactory());
			ResponseEntity<R> response = restTemplate.exchange(url, HttpMethod.PUT,
					new HttpEntity<I>(inputJson, requestHeaders), responseClass);
			return response.getBody();
		}
	}
}
