package com.cariq.toolkit.utils.scoring;

import com.cariq.toolkit.model.ParamDefinition;

public class Parameter {
	ParamDefinition definition;
	double value;
	
	public Parameter(ParamDefinition definition, double value) {
		super();
		this.definition = definition;
		this.value = definition.validateValue(value);
	}

	public Parameter(ParamDefinition def) {
		this(def, def.getDefaultValue());
	}

	public ParamDefinition getDefinition() {
		return definition;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double inputvalue) {
		
		if(definition==null){
			this.value = inputvalue; 
			return;
		}
		// eliminate outliers
		double val = Math.min(inputvalue, definition.getMaxValue());
		val = Math.max(val, definition.getMinValue());
		this.value = val;
	}	
	
	public double evaluateValue() {
		return value * definition.getWeight();
	}

	public String getName() {
		return definition.getName();
	}
}
