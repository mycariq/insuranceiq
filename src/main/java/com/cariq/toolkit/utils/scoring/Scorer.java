package com.cariq.toolkit.utils.scoring;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.cariq.toolkit.model.ParamDefinition;
import com.cariq.toolkit.model.ParamDefinition.ParamDefinition_Out;
import com.cariq.toolkit.model.ScoreDefinition;
import com.cariq.toolkit.service.ScoreParamService;
import com.cariq.toolkit.utils.SimpleAttributeMap;
import com.cariq.toolkit.utils.Utils;

@Configurable
public class Scorer {
//	static CarIQLogger logger = CarIQToolkitHelper.getLogger("Scorer");
	
	@Autowired 
	ScoreParamService scoreParamService;

	// Name of the scorer, same as scoreDef
	String name;
	
	// Actual Parameters list.
	Map<String, Parameter> parameters = new HashMap<String, Parameter>();
	
	// Scorer is built for a specific scoreDefinition
	ScoreDefinition scoreDefinition;
	
	public Scorer () {
		super();
	}
	
	public void configure(ScoreDefinition scoreDefinition) {
		if (null != parameters) 
			parameters.clear();
		this.scoreDefinition = scoreDefinition;
		this.name = scoreDefinition.getName();
		prePopulateParameters();
	}
	
	/**
	 * Populate with default values
	 */
	public void prePopulateParameters() {
		if (null == scoreParamService) {
			System.out.println("scoreParamService null !!!");
			return;
		}
		List<ParamDefinition_Out> paramDefList = scoreParamService.geParamDefsForScoreDef(name);
		for (ParamDefinition_Out paramDef : paramDefList) {
			Parameter parameter = new Parameter(ParamDefinition.findObjectById(paramDef.getId()));
			parameters.put(parameter.getName().toLowerCase(), parameter);
		}
	}

	public void addParameters(SimpleAttributeMap paramMap, boolean includeZero) {
		for (String name : paramMap.keySet()) {
			try {
				Double value = Utils.downCast(Double.class, paramMap.get(name));
				if (value == null)
					continue;
				if (includeZero == false && value == 0.0)
					continue;
				
				addParameter(name, value);
			} catch (Exception e) {
				continue;
			}
		}
	}	
	
	public void addParameters(SimpleAttributeMap paramMap) {
		addParameters(paramMap, true);
	}
	
	public void addParameter(String name, double value) {
		Parameter parameter = parameters.get(name.toLowerCase());
		if (null == parameter) {
			System.out.println("Parameter Definition not found: " + name);
			return;
		}
		
		// logger.debug("Scorer.addParamater: " + name + ": " + value);
		parameter.setValue(value);
	}
	
	public double normalize (double score) {
		Double minRange = 0.0, maxRange = 0.0;
		Double minScore = this.scoreDefinition.getMinValue();
		Double maxScore = this.scoreDefinition.getMaxValue();
		
		// Calculate minRange, maxRange
		for (Parameter parameter : parameters.values()) {
			ParamDefinition paramDef = parameter.getDefinition();
			double weight = paramDef.getWeight();
			if (weight < 0) {
				minRange += weight * paramDef.getMaxValue();
				maxRange += weight * paramDef.getMinValue();
			}
			else {
				minRange += weight * paramDef.getMinValue();
				maxRange += weight * paramDef.getMaxValue();
			}
		}
		
		// normalize the final score
		if (score <= minRange)
			return minScore;
		if (score >= maxRange)
			return maxScore;
		
		// y = (x-x1)*(y2-y1)/(x2-x1) + y1
		return minScore + (score - minRange)*(maxScore - minScore)/(maxRange - minRange);
	}
	
	public double evaluate() {
		double score = 0;
		
		for (Parameter param : parameters.values()) {
			score = score + param.evaluateValue();
		}
		
		return normalize(score);
		
	}

	/**
	 * Calculate the factor within the given range provided
	 * based on actual score and range of possible score
	 * @param score
	 * @param scoreBasedRange
	 * @return
	 */
	public double evaluateOverlayFactor(double score, double[] range) {
		if (range == null || range.length != 2)
			Utils.handleException("Range for Overlay is incorrect!!");
		
		double minV = range[0];
		double maxV = range[1];
		double minS = this.scoreDefinition.getMinValue();
		double maxS = this.scoreDefinition.getMaxValue();
		
//		logger.debug("Range of Score: { "+ minS + " - " + maxS + " }");
//		logger.debug("Range of Overlay Values: { "+ minV + " - " + maxV + " }");
//		logger.debug("Score: { "+ score + " }");

		
		// 2 point equation of line
		return ((score - minS)*(maxV - minV)/(maxS - minS)) + minV;
	}

}
