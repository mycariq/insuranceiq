package com.cariq.toolkit.utils.wwwservice.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.cariq.toolkit.utils.wwwservice.core.ArithmeticCondition.ArithmeticConditionJson;
import com.cariq.toolkit.utils.wwwservice.core.LiteralCondition.LiteralConditionJson;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef.ParameterDefJson;
import com.cariq.toolkit.utils.wwwservice.core.UnaryCondition.UnaryConditionJson;
import com.cariq.toolkit.utils.wwwservice.core.condition.And;

public abstract class Condition {
	public enum ConditionType {
		Equal, NotEqual, GreaterThan, GreaterThanEqual, LessThan, LessThanEqual, Contains, NotContains, IsNull, IsNotNull, Or, And
	}

	public abstract ConditionType getType();

	public abstract boolean isTrue(DataPoint datapoint);

	public abstract void collectParameters(Set<String> params);

	public abstract ConditionJson2 toJson();

	/*******************************************
	 * Base class for Conditions
	 * 
	 * @author Abhijit
	 *
	 */
	public static class ConditionJson2 {
		String conditionStr;

		/**
		 * 
		 */
		public ConditionJson2() {
			super();
			// TODO Auto-generated constructor stub
		}

		/**
		 * @return the conditionStr
		 */
		public String getConditionStr() {
			return conditionStr;
		}

		/**
		 * @param conditionStr
		 *            the conditionStr to set
		 */
		public void setConditionStr(String conditionStr) {
			this.conditionStr = conditionStr;
		}
	}

	public static abstract class ConditionJson {

		public ConditionJson() {
			super();
		}

		public ConditionJson(ConditionType conditionType) {
			super();
			this.conditionType = conditionType;
		}

		/**
		 * @return the conditionType
		 */
		public ConditionType getConditionType() {
			return conditionType;
		}

		/**
		 * @param conditionType
		 *            the conditionType to set
		 */
		public void setConditionType(ConditionType conditionType) {
			this.conditionType = conditionType;
		}

		ConditionType conditionType;
	}

	private static String[] splitOn(String conditionString, String[] delimiters) {
		List<String> retval = new ArrayList<String>();
		for (String delimiter : delimiters) {
			if (!conditionString.contains(delimiter))
				continue;

			String[] splitString = conditionString.split(delimiter);
			for (String string : splitString) {
				retval.add(string);
			}
		}

		if (retval.isEmpty())
			retval.add(conditionString);

		return retval.toArray(new String[retval.size()]);
	}

	static class ConditionProcessor {
		ConditionJson json;
		ConditionType conditionType;

		/**
		 * @param json
		 * @param conditionType
		 */
		public ConditionProcessor(ConditionJson json,
				ConditionType conditionType) {
			super();
			this.json = json;
			this.conditionType = conditionType;
		}

		/**
		 * @param conditionString
		 */
		public static ConditionProcessor createArithmeticCondition(
				ConditionType type, String conditionString) {
			String[] str = conditionString.split("[ \t]+");
			if (str.length != 3)
				return null;

			ArithmeticConditionJson arithJson = new ArithmeticConditionJson();
			arithJson.setConditionType(type);
			ParameterDefJson pdefjson = new ParameterDefJson();
			pdefjson.setName(str[0].trim());
			pdefjson.setInternalValue(str[0].trim());
			arithJson.setParamDef(pdefjson);
			arithJson.setValue(str[2].trim());
			arithJson.setValueTypeName(Double.class.getName());

			return new ConditionProcessor(arithJson, type);
		}

		/**
		 * @param conditionString
		 */
		public static ConditionProcessor createLiteralCondition(
				ConditionType type, String conditionString) {
			String[] str = conditionString.split("[ \t]+");
			if (str.length != 3)
				return null;

			LiteralConditionJson literalJson = new LiteralConditionJson();
			literalJson.setConditionType(type);
			ParameterDefJson pdefjson = new ParameterDefJson();
			pdefjson.setName(str[0]);
			pdefjson.setInternalValue(str[0]);
			literalJson.setParamDef(pdefjson);

			literalJson.setValue(str[2]);

			return new ConditionProcessor(literalJson, type);
		}

		Condition getCondition() {
			// TODO Not desirable to use instanceof
			if (json instanceof ArithmeticConditionJson)
				return ArithmeticCondition.fromJson(conditionType, json);
			if (json instanceof LiteralConditionJson)
				LiteralCondition.fromJson(conditionType, json);
			if (json instanceof UnaryConditionJson)
				UnaryCondition.fromJson(conditionType, json);

			return null;
		}

		public static ConditionProcessor createUnaryCondition(
				ConditionType type, String conditionString) {
			String[] str = conditionString.split("[ \t]+");
			if (str.length != 2)
				return null;

			UnaryConditionJson unaryJson = new UnaryConditionJson();
			unaryJson.setConditionType(type);
			ParameterDefJson pdefjson = new ParameterDefJson();
			pdefjson.setName(str[0]);
			pdefjson.setInternalValue(str[0]);
			unaryJson.setParamDef(pdefjson);

			return new ConditionProcessor(unaryJson, type);
		}
	}

	private static Condition getCondition(String conditionStr) {
		ConditionProcessor processor = null;
		if (conditionStr.contains("==") || conditionStr.contains("=")
				|| conditionStr.contains("EQ"))
			processor = ConditionProcessor.createLiteralCondition(
					ConditionType.Equal, conditionStr);

		else if (conditionStr.contains("!=") || conditionStr.contains("NE")
				|| conditionStr.contains("><") || conditionStr.contains("<>"))
			processor = ConditionProcessor.createArithmeticCondition(
					ConditionType.NotEqual, conditionStr);

		else if (conditionStr.contains(">") || conditionStr.contains("GT")
				|| conditionStr.contains("GreaterThan"))
			processor = ConditionProcessor.createArithmeticCondition(
					ConditionType.GreaterThan, conditionStr);

		else if (conditionStr.contains(">=") || conditionStr.contains("GE"))
			processor = ConditionProcessor.createArithmeticCondition(
					ConditionType.GreaterThanEqual, conditionStr);

		else if (conditionStr.contains("<") || conditionStr.contains("LT")
				|| conditionStr.contains("LessThan"))
			processor = ConditionProcessor.createArithmeticCondition(
					ConditionType.LessThan, conditionStr);

		else if (conditionStr.contains("<=") || conditionStr.contains("LE"))
			processor = ConditionProcessor.createArithmeticCondition(
					ConditionType.LessThanEqual, conditionStr);

		else if (conditionStr.contains("contains")
				|| conditionStr.contains("CONTAINS"))
			processor = ConditionProcessor.createLiteralCondition(
					ConditionType.Contains, conditionStr);

		else if (conditionStr.contains("notcontains")
				|| conditionStr.contains("NOTCONTAINS"))
			processor = ConditionProcessor.createLiteralCondition(
					ConditionType.NotContains, conditionStr);

		else if (conditionStr.contains("notnull")
				|| conditionStr.contains("IsNotNull")
				|| conditionStr.contains("NOTNULL")
				|| conditionStr.contains("NotNull"))
			processor = ConditionProcessor.createUnaryCondition(
					ConditionType.IsNotNull, conditionStr);

		else if (conditionStr.contains("isnull")
				|| conditionStr.contains("IsNULL")
				|| conditionStr.contains("IsNull"))
			processor = ConditionProcessor.createUnaryCondition(
					ConditionType.IsNotNull, conditionStr);

		if (processor == null)
			return null;

		return processor.getCondition();
	}

	public static Condition fromString(String conditionString) {
		// split on And/&/&&
		String[] parts = splitOn(conditionString, new String[] { " and ",
				" && ", " & ", " AND " });

		Condition condition = null;
		for (String conditionStr : parts) {
			Condition con = getCondition(conditionStr.trim());
			if (condition == null)
				condition = con;
			else
				condition = new And(condition, con);
		}

		return condition;
	}

	public static Condition fromJson(ConditionJson2 conditionJson) {
		if (conditionJson == null)
			return null;

		return fromString(conditionJson.getConditionStr());
	}

	public static Condition fromJson(ConditionJson conditionJson) {
		if (conditionJson == null)
			return null;

		ConditionType type = conditionJson.getConditionType();

		switch (type) {
		case GreaterThan:
		case GreaterThanEqual:
		case LessThan:
		case LessThanEqual:
			return ArithmeticCondition.fromJson(type, conditionJson);

		case Equal:
		case NotEqual:
		case Contains:
		case NotContains:
			return LiteralCondition.fromJson(type, conditionJson);

		case IsNull:
		case IsNotNull:
			return UnaryCondition.fromJson(type, conditionJson);

		case Or:
		case And:
			return CompositeCondition.fromJson(type, conditionJson);

		default:
			break;
		}
		return null;
	}

	static public void collectParameterStrings(ParameterDef paramDef,
			Set<String> params) {
		for (String param : paramDef.getParams()) {
			params.add(param);
		}
	}
}
