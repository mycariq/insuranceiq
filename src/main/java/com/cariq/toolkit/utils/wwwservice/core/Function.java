package com.cariq.toolkit.utils.wwwservice.core;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.wwwservice.api.W3FunctionOutput;
import com.cariq.toolkit.utils.wwwservice.api.W3FunctionOutput.W3FunctionOutputJson;
import com.cariq.toolkit.utils.wwwservice.core.Condition.ConditionJson2;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef.ParameterDefJson;
import com.cariq.toolkit.utils.wwwservice.core.condition.And;
import com.cariq.toolkit.utils.wwwservice.core.function.Average;
import com.cariq.toolkit.utils.wwwservice.core.function.Count;
import com.cariq.toolkit.utils.wwwservice.core.function.Max;
import com.cariq.toolkit.utils.wwwservice.core.function.Min;
import com.cariq.toolkit.utils.wwwservice.core.function.Sum;
import com.cariq.toolkit.utils.wwwservice.core.function.Value;

/**
 * Function is a base class of all the Arithmetic and Literal functions. Execute
 * method
 * 
 * @author HVN
 *
 */
public abstract class Function {
	public enum FunctionType {
		VALUE, AVERAGE, SUM, COUNT, MAX, MIN, MODE
	}

	List<ParameterDef> params = new ArrayList<ParameterDef>();
	protected ArrayList<W3FunctionOutput> output = new ArrayList<W3FunctionOutput>();
	Condition condition = null;

	/**
	 * @return the condition
	 */
	public Condition getCondition() {
		return condition;
	}

	public abstract FunctionType getType();

	/**
	 * @return the params
	 */
	public List<ParameterDef> getParams() {
		return params;
	}

	public Function addParam(ParameterDef def) {
		params.add(def);
		return this;
	}

	/**
	 * Add condition to function. All added conditions will be "And"ed. Support
	 * for Or is easy to implement but difficult to express
	 * 
	 * @param condition
	 * @return
	 */
	public Function addCondition(Condition newCondition) {
		// This is the first condition
		if (null == condition) {
			condition = newCondition;
			return this;
		}

		// Do Anding with previous condition
		condition = new And(condition, newCondition);
		return this;
	}

	/**
	 * Process the datapoint to accumulate into Function Output
	 * 
	 * @param dataPoint
	 */
	public void process(DataPoint dataPoint) {
		try (ProfilePoint _Function_process = ProfilePoint
				.profileAction("ProfAction_Function_process")) {
			// If condition exists but is evaluated not as true, don't process
			if (condition != null && !condition.isTrue(dataPoint))
				return;

			W3FunctionOutput out = doProcess(dataPoint);
			if (out != null)
				output.add(out);
		}
	}

	/**
	 * Do the actual work of processing
	 * 
	 * @param dataPoint
	 * @return
	 */
	protected abstract W3FunctionOutput doProcess(DataPoint dataPoint);

	/**
	 * get Output Array
	 * 
	 * @return
	 */
	public List<W3FunctionOutput> getOutputBase() {
		return output;
	}

	/**
	 * Get the output of the function - this can be single output or list
	 * 
	 * @return
	 */
	public List<W3FunctionOutput> getOutput() {
		return doGetOutput();
	}

	protected abstract List<W3FunctionOutput> doGetOutput();

	public static Function get(FunctionType functionType) {
		switch (functionType) {
		case AVERAGE:
			return new Average();

		case MAX:
			return new Max();

		case MIN:
			return new Min();

		case SUM:
			return new Sum();

		case VALUE:
			return new Value();

		case COUNT:
			return new Count();

		default:
			break;
		}
		return null;
	}

	public static Function fromJson(FunctionInputJson functionJson) {
		Function function = get(functionJson.functionType);
		for (ParameterDefJson parameterDefJson : functionJson.parameters) {
			function.addParam(ParameterDef.fromJson(parameterDefJson));
		}

		Condition condition = Condition.fromJson(functionJson.getConditionJson());
		function.addCondition(condition);
		return function;
	}

	public FunctionInputJson toJson() {
		FunctionInputJson functionInputJson = new FunctionInputJson();
		functionInputJson.functionType = this.getType();
		for (ParameterDef parameterDef : params) {
			functionInputJson.parameters.add(parameterDef.toJson());
		}
		
		if (condition != null)
			functionInputJson.setConditionJson(condition.toJson());
		
		return functionInputJson;
	}
	

	/********************************************************
	 * Function Json for Input
	 * @author Abhijit
	 *
	 */
	public static class FunctionInputJson {
		public Function.FunctionType getFunctionType() {
			return functionType;
		}

		public void setFunctionType(Function.FunctionType functionType) {
			this.functionType = functionType;
		}

		public List<ParameterDefJson> getParameters() {
			return parameters;
		}

		/**
		 * @return the conditionJson
		 */
		public ConditionJson2 getConditionJson() {
			return conditionJson;
		}

		/**
		 * @param conditionJson the conditionJson to set
		 */
//		@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.EXTERNAL_PROPERTY, property="type")
		public void setConditionJson(ConditionJson2 conditionJson) {
			this.conditionJson = conditionJson;
		}

		public void setParameters(List<ParameterDefJson> parameters) {
			this.parameters = parameters;
		}

		public Function.FunctionType functionType;
		public List<ParameterDefJson> parameters = new ArrayList<ParameterDef.ParameterDefJson>();
		// ConditionJson conditionJson = null;
		ConditionJson2 conditionJson = null;
	}
	
	/********************************************************
	 * Function Output Json
	 * @author Abhijit
	 *
	 */

	public static class FunctionOutputJson {
		public Function.FunctionType getFunctionType() {
			return functionType;
		}

		public void setFunctionType(Function.FunctionType functionType) {
			this.functionType = functionType;
		}

		public List<W3FunctionOutputJson> getFunctionOutput() {
			return functionOutput;
		}

		public void setFunctionOutput(List<W3FunctionOutputJson> functionOutput) {
			this.functionOutput = functionOutput;
		}

		public Function.FunctionType functionType;
		public List<W3FunctionOutputJson> functionOutput;
	}
}
