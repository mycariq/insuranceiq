package com.cariq.toolkit.utils.wwwservice.core;

/**
 * Helper class with static methods to do bit operations
 * @author HVN
 *
 */
public class BitOperations {
	/**
	 * Basic left shift operation
	 * @param number
	 * @param numBits
	 * @return
	 */
	public static long shiftLeft(long number, int numBits) {
		// TODO: Check if number shifted becomes greater than MAX long size
		return (number << numBits);
	}

	/**
	 * Basic right shift operation
	 * @param number
	 * @param numBits
	 * @return
	 */
	public static long shiftRight(long number, int numBits) {
		// TODO: Check if number shifted becomes greater than MAX long size
		return (number >> numBits);
	}

	/**
	 * Get value of number upon masking start-end bit and shifting to right by
	 * startBit
	 * 
	 * @param number
	 * @param startBit
	 * @param endBit
	 * @return
	 */
	public static long valueOf(long number, int startBit, int endBit) {
		int bit1 = startBit;
		int bit2 = endBit;
		if (startBit > endBit) {
			bit1 = endBit;
			bit2 = startBit;
		}

		// prepare mask
		long mask = prepareMask(bit1, bit2);
		long retval = number & mask;
		return shiftRight(retval, bit1);
	}

	/**
	 * Create a BitMask by setting Start to End Bit values as 1
	 * 
	 * @param startBit
	 * @param endBit
	 * @return
	 */
	public static long prepareMask(int startBit, int endBit) {
		int bit1 = startBit;
		int bit2 = endBit;
		if (startBit > endBit) {
			bit1 = endBit;
			bit2 = startBit;
		}
		long retval = 0;

		for (int i = bit1; i < bit2; i++) {
			retval += shiftLeft(1, i);
		}

		return retval;
	}

}
