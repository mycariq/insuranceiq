package com.cariq.toolkit.utils.wwwservice.core.function;

import com.cariq.toolkit.utils.wwwservice.core.ArithmeticFunction;
import com.cariq.toolkit.utils.wwwservice.core.ArithmeticFunction.FunctionProcessingInfo;
import com.cariq.toolkit.utils.wwwservice.core.Function.FunctionType;
import com.cariq.toolkit.utils.wwwservice.core.Parameter;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;
import com.cariq.toolkit.utils.wwwservice.core.Tuple;

public class Count extends ArithmeticFunction {
	// TODO: converge to baseclass - repeating in all Double calculations
	static class CountProcessingInfo extends FunctionProcessingInfo {
		public CountProcessingInfo(ParameterDef def) {
			super(def);
			value = 0;
		}

		@Override
		public Parameter addValue(Tuple tuple, double paramValue) {
			value++;
			return null;
		}
	}

	@Override
	public FunctionType getType() {
		return FunctionType.COUNT;
	}

	@Override
	protected FunctionProcessingInfo getProcessingInfo(ParameterDef paramDef) {
		return new CountProcessingInfo(paramDef);
	}
}
