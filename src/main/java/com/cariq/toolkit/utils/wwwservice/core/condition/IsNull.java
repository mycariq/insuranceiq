/**
 * 
 */
package com.cariq.toolkit.utils.wwwservice.core.condition;

import com.cariq.toolkit.utils.wwwservice.core.Condition.ConditionType;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;
import com.cariq.toolkit.utils.wwwservice.core.UnaryCondition;

/**
 * @author Abhijit
 *
 */
public class IsNull extends UnaryCondition {

	public IsNull(ParameterDef paramDef) {
		super(paramDef);
	}

	@Override
	public ConditionType getType() {
		return ConditionType.IsNull;
	}

	@Override
	protected boolean isReallyTrue(Object value) {
		return (value == null);
	}

}
