package com.cariq.toolkit.utils.wwwservice.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A Data point is a set of parameter values at a tuple
 * 
 * @author HVN
 *
 */
public class DataPoint {
//	private Set<ParameterDef> paramDefs = new HashSet<ParameterDef>();

	Tuple tuple = null;
	Map<String, Parameter> parameters = new HashMap<String, Parameter>();

	/**
	 * Basic Constructor
	 * 
	 * @param tuple
	 * @param parameters
	 */
	public DataPoint(Tuple tuple, Set<Parameter> parameters) {
		super();
		this.tuple = tuple;
		for (Parameter parameter : parameters) {
			parameters.add(parameter);
		}
	}

	/**
	 * Simplified Constructor
	 * 
	 * @param tuple
	 */
	public DataPoint(Tuple tuple) {
		this(tuple, new HashSet<Parameter>());
	}

	
	/**
	 * @return the tuple
	 */
	public Tuple getTuple() {
		return tuple;
	}

	/**
	 * Add Parameter to existing set
	 * 
	 * @param param
	 * @return
	 */
	public DataPoint addParameter(Parameter param) {
		// Param already present
		if (hasParameter(param.getDef())) {
			System.out
					.println("WARNING! - Parameter added twice to DataPoint: "
							+ param.getName());
			return this;
		}

		parameters.put(param.getName(), param);
		return this;
	}
	
	/**
	 * Check if DataPoint contains given ParameterDefinition
	 * @param paramDef
	 * @return
	 */
	public boolean hasParameter(ParameterDef paramDef) {
		return hasParameter(paramDef.getParam());
	}
	
	/**
	 * Check if DataPoint contains given ParameterDefinition
	 * @param paramDef
	 * @return
	 */
	public boolean hasParameter(String paramDef) {
		return parameters.containsKey(paramDef);
	}
	
	public Parameter get(String name) {
		return parameters.get(name);		
	}

	/**
	 * @return the parameters
	 */
	public Map<String, Parameter> getParameters() {
		return parameters;
	}
	
	
}
