/**
 * 
 */
package com.cariq.toolkit.utils.wwwservice.core;

import java.util.List;

/**
 * @author hrishi
 *
 */
public class PolygonRegion implements RegionInterface {
	Region boundingRegion;
	List<GeoTile> points;
	/**
	 * @param center
	 * @param size
	 */
	public PolygonRegion(List<GeoTile> points) {
		this.points = points;
		
		// manufacture the Rectangular region
		Double lowerLeftLat = null, lowerLeftLong = null;
		Double upperRightLat = null, upperRightLong = null;
		
		boolean firstPoint = true;
		for (GeoTile point : points) {
			if (firstPoint) {
				// initialize
				lowerLeftLat = point.getLatitude();
				lowerLeftLong = point.getLongitude();
				upperRightLat = point.getLatitude();
				upperRightLong = point.getLongitude();
				
				firstPoint = false;
				continue;
			}
			
			if (point.getLatitude() < lowerLeftLat)
				lowerLeftLat = point.getLatitude();

			if (point.getLongitude() < lowerLeftLong)
				lowerLeftLong = point.getLongitude();

			if (point.getLatitude() > upperRightLat)
				upperRightLat = point.getLatitude();

			if (point.getLongitude() > upperRightLong)
				upperRightLong = point.getLongitude();
		}
		
		boundingRegion = new Region(new GeoTile(lowerLeftLat, lowerLeftLong), new GeoTile(upperRightLat, upperRightLong));
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.www.utils.wwwservice.core.RegionInterface#getLowerLeft()
	 */
	@Override
	public GeoTile getLowerLeft() {
		return boundingRegion.getLowerLeft();
	}
	/* (non-Javadoc)
	 * @see com.cariq.www.utils.wwwservice.core.RegionInterface#getUpperRight()
	 */
	@Override
	public GeoTile getUpperRight() {
		return boundingRegion.getUpperRight();
	}
	/* (non-Javadoc)
	 * @see com.cariq.www.utils.wwwservice.core.RegionInterface#isInside(com.cariq.www.utils.wwwservice.core.GeoTile)
	 */
	@Override
	public boolean isInside(GeoTile tile) {
		if (boundingRegion.isInside(tile)) {
			return isReallyInside(tile);
		}
		
		return false;
	}
	/**
	 * @param tile
	 * @return
	 */
	private boolean isReallyInside(GeoTile tile) {
		// Do series of vector cross products and see if sign changes
		// will work for convex polygons.
		GeoTile first = points.get(0);
		GeoTile last = points.get(points.size() - 1);
		GeoTile previousPoint = first;
		Double cross = null;
		// start from 2nd point
		for (int i = 1; i < points.size(); i++) {
			GeoTile currentPoint = points.get(i);
			double newCross = getCrossProduct(previousPoint, currentPoint, tile);
			if (cross != null) {
				// check if sign changed - both negative or both positive
				boolean signChange = (cross * newCross) < 0;
				if (signChange)
					return false;
			}

			cross = newCross;			
			previousPoint = currentPoint;
		}
		
		// Finally check for last to first.
		double newCross = getCrossProduct(last, first, tile);
		return (cross * newCross < 0) ? false : true;
	}

	/**
	 * @param previousPoint
	 * @param currentPoint
	 * @param point
	 * @return
	 */
	private static double getCrossProduct(GeoTile previousPoint, GeoTile currentPoint, GeoTile point) {
		GeoVector polygonBoundaryVector = new GeoVector(previousPoint, currentPoint);
		GeoVector insideVector = new GeoVector(previousPoint, point);
		return polygonBoundaryVector.cross(insideVector);
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.wwwservice.core.RegionInterface#getArea()
	 */
	@Override
	public double getArea() {
		// Approx area of bounding rectangle
		return boundingRegion.getArea();
	}
	/* (non-Javadoc)
	 * @see com.cariq.www.utils.wwwservice.core.RegionInterface#getEastWestSpan()
	 */
	@Override
	public double getEastWestSpan() {
		return boundingRegion.getEastWestSpan();
	}
	/* (non-Javadoc)
	 * @see com.cariq.www.utils.wwwservice.core.RegionInterface#getNorthSouthSpan()
	 */
	@Override
	public double getNorthSouthSpan() {
		return boundingRegion.getNorthSouthSpan();
	}
}
