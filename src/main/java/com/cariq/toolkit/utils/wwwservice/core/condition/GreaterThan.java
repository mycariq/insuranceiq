package com.cariq.toolkit.utils.wwwservice.core.condition;

import com.cariq.toolkit.utils.wwwservice.core.ArithmeticCondition;
import com.cariq.toolkit.utils.wwwservice.core.Condition.ConditionType;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;

public class GreaterThan extends ArithmeticCondition {
	public GreaterThan(ParameterDef paramdef, Comparable<?> value) {
		super(paramdef, value);
	}

	@Override
	public ConditionType getType() {
		return ConditionType.GreaterThan;
	}

	/**
	 * Return if parameterValue is greater than conditionValue
	 */
	@Override
	protected boolean isReallyTrue(Comparable conditionValue,
			Comparable<?> parameterValue) {
		return (conditionValue.compareTo(parameterValue) <= 0);
	}
	
//	
//	/*************************
//	 * GreaterThan placeholder
//	 * @author Abhijit
//	 *
//	 */
//	
//	public static class GreaterThanJson extends ArithmeticConditionJson {
//		public GreaterThanJson() {
//			super();
//		}
//	}
}
