package com.cariq.toolkit.utils.wwwservice.core.function;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.wwwservice.api.W3FunctionOutput;
import com.cariq.toolkit.utils.wwwservice.core.ArithmeticFunction;
import com.cariq.toolkit.utils.wwwservice.core.ArithmeticFunction.FunctionProcessingInfo;
import com.cariq.toolkit.utils.wwwservice.core.Function.FunctionType;
import com.cariq.toolkit.utils.wwwservice.core.Parameter;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;
import com.cariq.toolkit.utils.wwwservice.core.Tuple;

// TODO: Auto-generated Javadoc
/**
 * The Class Min.
 */
public class Min extends ArithmeticFunction {
	
	/**
	 * The Class MinProcessingInfo.
	 */
	static class MinProcessingInfo extends FunctionProcessingInfo {
		
		/** The tuple. */
		Tuple tuple;
		
		/**
		 * Instantiates a new min processing info.
		 *
		 * @param def the def
		 */
		public MinProcessingInfo(ParameterDef def) {
			super(def);
			tuple = null;
			value = Double.MAX_VALUE;
		}

		/* (non-Javadoc)
		 * @see com.cariq.toolkit.utils.wwwservice.core.ArithmeticFunction.FunctionProcessingInfo#addValue(com.cariq.toolkit.utils.wwwservice.core.Tuple, double)
		 */
		@Override
		public Parameter addValue(Tuple tuple, double paramValue) {
			if (paramValue < value) {
				value = paramValue;
				this.tuple = tuple;
			}
			return null;
		}

		/**
		 * Gets the tuple.
		 *
		 * @return the tuple
		 */
		public Tuple getTuple() {
			return tuple;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.toolkit.utils.wwwservice.core.Function#getType()
	 */
	@Override
	public FunctionType getType() {
		return FunctionType.MIN;
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.utils.wwwservice.core.ArithmeticFunction#getProcessingInfo(com.cariq.toolkit.utils.wwwservice.core.ParameterDef)
	 */
	@Override
	protected FunctionProcessingInfo getProcessingInfo(ParameterDef paramDef) {
		return new MinProcessingInfo(paramDef);
	}

	/**
	 * Min and Min have special implementation. They have each parameter with a tuple attached.
	 *
	 * @return the list
	 */
	@Override
	protected List<W3FunctionOutput> doGetOutput() {
		// Single record from the AverageInfo
		ArrayList<W3FunctionOutput> output = new ArrayList<W3FunctionOutput>();

		for (FunctionProcessingInfo info : processingInfoMap.values()) {
			MinProcessingInfo minInfo = (MinProcessingInfo) info;
			W3FunctionOutput out = new W3FunctionOutput(minInfo.getTuple());
			out.addParameter(new Parameter(info.getDef(), info.getValue()));
			
			output.add(out);
		}
		
		return output;
	}
}
