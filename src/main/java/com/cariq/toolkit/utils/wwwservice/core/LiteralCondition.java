/**
 * 
 */
package com.cariq.toolkit.utils.wwwservice.core;

import java.util.Set;

import com.cariq.toolkit.utils.wwwservice.core.ParameterDef.ParameterDefJson;
import com.cariq.toolkit.utils.wwwservice.core.condition.Contains;
import com.cariq.toolkit.utils.wwwservice.core.condition.Equal;
import com.cariq.toolkit.utils.wwwservice.core.condition.NotContains;
import com.cariq.toolkit.utils.wwwservice.core.condition.NotEqual;

/**
 * @author Abhijit
 *
 */
public abstract class LiteralCondition extends Condition {
	ParameterDef paramDef;
	Object value;

	public LiteralCondition(ParameterDef parameter, Object value) {
		super();
		this.paramDef = parameter;
		this.value = value;
	}

	@Override
	public boolean isTrue(DataPoint datapoint) {
		Object paramValue = Parameter.evaluate(paramDef, datapoint);

		// return if "this" is greater than p
		if (paramValue == null)
			return false;

		return isReallyTrue(value, paramValue);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.www.utils.wwwservice.core.Condition#collectParameters(java.
	 * util.Set)
	 */
	@Override
	public void collectParameters(Set<String> params) {
		collectParameterStrings(paramDef, params);
	}

	protected abstract boolean isReallyTrue(Object conditionValue,
			Object parameterValue);

	/**
	 * Load class from json
	 * 
	 * @param type
	 * @param conditionJson
	 * @return
	 */
	public static Condition fromJson(ConditionType type,
			ConditionJson conditionJson) {
		LiteralConditionJson literalConditionJson = (LiteralConditionJson) conditionJson;
		ParameterDef paramDef = ParameterDef.fromJson(literalConditionJson
				.getParamDef());
		Object value = literalConditionJson.getValue();

		switch (type) {

		case Equal:
			return new Equal(paramDef, value);

		case NotEqual:
			return new NotEqual(paramDef, value);

		case Contains:
			return new Contains(paramDef, value);

		case NotContains:
			return new NotContains(paramDef, value);
		default:
			break;
		}

		return null;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.wwwservice.core.Condition#toJson()
	 */
	@Override
	public ConditionJson2 toJson() {
//		LiteralConditionJson retval = new LiteralConditionJson(getType());
//		retval.setParamDef(paramDef.toJson());
//		retval.setValue(value);
//
//		return retval;

		ConditionJson2 json = new ConditionJson2();
		String value = this.value.toString();
		
		String operator = null;
		switch (getType()) {
		case Equal:
			operator = " == ";
			break;

		case Contains:
			operator = " contains ";
			break;

		case NotContains:
			operator = " notcontains ";
			break;

		default:
			break;
		}


		json.setConditionStr(paramDef.getParam() + " " + operator + " " + value);
		return json;
	
	}
	
	/************************************************************
	 * 
	 * Literal Condition
	 * 
	 * @author Abhijit
	 *
	 */
	public  static class LiteralConditionJson extends ConditionJson {

		ParameterDefJson paramDef;
		Object value;

		/**
		 * @return the paramDef
		 */
		public ParameterDefJson getParamDef() {
			return paramDef;
		}

		/**
		 * @param paramDef
		 *            the paramDef to set
		 */
		public void setParamDef(ParameterDefJson paramDef) {
			this.paramDef = paramDef;
		}

		/**
		 * @return the value
		 */
		public Object getValue() {
			return value;
		}

		/**
		 * @param value
		 *            the value to set
		 */
		public void setValue(Object value) {
			this.value = value;
		}

		public LiteralConditionJson(ConditionType conditionType) {
			super(conditionType);
		}
		
		public LiteralConditionJson() {
			super();
		}

	}
}
