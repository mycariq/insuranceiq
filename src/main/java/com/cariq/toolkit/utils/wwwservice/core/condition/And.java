/**
 * 
 */
package com.cariq.toolkit.utils.wwwservice.core.condition;

import com.cariq.toolkit.utils.wwwservice.core.CompositeCondition;
import com.cariq.toolkit.utils.wwwservice.core.Condition;
import com.cariq.toolkit.utils.wwwservice.core.Condition.ConditionType;
import com.cariq.toolkit.utils.wwwservice.core.DataPoint;

/**
 * @author Abhijit
 * And Condition for parameters
 *
 */
public class And extends CompositeCondition {

	public And(Condition first, Condition second) {
		super(first, second);
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.wwwservice.core.Condition#getType()
	 */
	@Override
	public ConditionType getType() {
		return ConditionType.And;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.wwwservice.core.Condition#isTrue(com.cariq.www.utils.wwwservice.core.DataPoint)
	 */
	@Override
	public boolean isTrue(DataPoint datapoint) {
		return getFirst().isTrue(datapoint) && getSecond().isTrue(datapoint);
	}

}
