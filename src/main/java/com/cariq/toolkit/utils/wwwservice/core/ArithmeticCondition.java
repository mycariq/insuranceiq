/**
 * 
 */
package com.cariq.toolkit.utils.wwwservice.core;

import java.util.Date;
import java.util.Set;

import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef.ParameterDefJson;
import com.cariq.toolkit.utils.wwwservice.core.condition.GreaterThan;
import com.cariq.toolkit.utils.wwwservice.core.condition.GreaterThanEqual;
import com.cariq.toolkit.utils.wwwservice.core.condition.LessThan;
import com.cariq.toolkit.utils.wwwservice.core.condition.LessThanEqual;

/**
 * @author Abhijit
 *
 */
public abstract class ArithmeticCondition extends Condition {
	ParameterDef paramDef;
	Comparable<?> value;
	String valueType;

	public ArithmeticCondition(ParameterDef parameter, Comparable<?> value) {
		super();
		this.paramDef = parameter;
		this.value = value;
		valueType = value.getClass().getName();
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isTrue(DataPoint datapoint) {
		Object paramValue = Parameter.evaluate(paramDef, datapoint);
		
		// return if "this" is greater than p
		if (paramValue == null)
			return false;

		Comparable<?> v;
		try {
			v = convertValue(Class.forName(valueType), paramValue.toString());
			return isReallyTrue(value, v);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	protected abstract boolean isReallyTrue(Comparable conditionValue,
			Comparable<?> parameterValue);

	private static Comparable<?> convertValue(Class<?> type, String value) {
		try (ProfilePoint _ConvertValue = ProfilePoint
				.profileAction("ProfAction_ConvertValue")) {
			if (type.equals(Double.class))
				return Double.parseDouble(value.toString());
			if (type.equals(Integer.class))
				return Integer.parseInt(value.toString());
			if (type.equals(Long.class))
				return Long.parseLong(value.toString());
			if (type.equals(Date.class)) {
				return new Date(); // TODO construct Date from String from Util
			}
			if (type.equals(String.class)) {
				return value.toString();
			}
			// Don't know any other comparable types that we want to use
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.www.utils.wwwservice.core.Condition#collectParameters(java.
	 * util.Set)
	 */
	@Override
	public void collectParameters(Set<String> params) {
		collectParameterStrings(paramDef, params);
	}

	/**
	 * Load class from json
	 * 
	 * @param type
	 * @param conditionJson
	 * @return
	 */
	public static Condition fromJson(ConditionType type,
			ConditionJson conditionJson) {
		try {
			ArithmeticConditionJson arithmeticConditionJson = (ArithmeticConditionJson) conditionJson;
			ParameterDef paramDef = ParameterDef
					.fromJson(arithmeticConditionJson.getParamDef());
			String valueType = arithmeticConditionJson.getValueTypeName();
			Object value = arithmeticConditionJson.getValue();
			Comparable comparable = convertValue(Class.forName(valueType),
					value.toString());

			switch (type) {

			case GreaterThan:
				return new GreaterThan(paramDef, comparable);

			case GreaterThanEqual:
				return new GreaterThanEqual(paramDef, comparable);

			case LessThan:
				return new LessThan(paramDef, comparable);

			case LessThanEqual:
				return new LessThanEqual(paramDef, comparable);

			default:
				break;
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.www.utils.wwwservice.core.Condition#toJson()
	 */
	@Override
	public ConditionJson2 toJson() {
		ConditionJson2 json = new ConditionJson2();
		String value = this.value.toString();
		
		String operator = null;
		switch (getType()) {
		case NotEqual:
			operator = " != ";
			break;
		case GreaterThan:
			operator = " > ";
			break;
		case GreaterThanEqual:
			operator = " >= ";
			break;
		case LessThan:
			operator = " < ";
			break;
		case LessThanEqual:
			operator = " <= ";
			break;
		default:
			break;
		}

		json.setConditionStr(paramDef.getParam() + " " + operator + " " + value);
		return json;
	}

	/******************************************
	 * Json class for Arithmetic conditions
	 * 
	 * @author Abhijit
	 *
	 */
	public static class ArithmeticConditionJson extends ConditionJson {
		/**
		 * @return the paramDef
		 */
		public ParameterDefJson getParamDef() {
			return paramDef;
		}

		/**
		 * @param paramDef
		 *            the paramDef to set
		 */
		public void setParamDef(ParameterDefJson paramDef) {
			this.paramDef = paramDef;
		}

		/**
		 * @return the value
		 */
		public Object getValue() {
			return value;
		}

		/**
		 * @param value
		 *            the value to set
		 */
		public void setValue(Object value) {
			this.value = value;
		}

		/**
		 * @return the valueTypeName
		 */
		public String getValueTypeName() {
			return valueTypeName;
		}

		/**
		 * @param valueTypeName
		 *            the valueTypeName to set
		 */
		public void setValueTypeName(String valueTypeName) {
			this.valueTypeName = valueTypeName;
		}

		ParameterDefJson paramDef;
		Object value;
		String valueTypeName;

		public ArithmeticConditionJson(ConditionType conditionType) {
			super(conditionType);
		}
		
		public ArithmeticConditionJson() {
			super();
		}
	}
}
