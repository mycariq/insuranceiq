package com.cariq.toolkit.utils.wwwservice.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cariq.toolkit.utils.wwwservice.api.W3FunctionOutput.W3FunctionOutputJson;
import com.cariq.toolkit.utils.wwwservice.core.Function;
import com.cariq.toolkit.utils.wwwservice.core.Function.FunctionType;

public class W3QueryResult {
	static class W3FunctionOutputItem {
		public W3FunctionOutputItem(FunctionType type,
				List<W3FunctionOutput> outputList) {
			super();
			this.type = type;
			this.outputList = outputList;
		}
		
		FunctionType type;
		List<W3FunctionOutput> outputList;
		
		public W3FunctionOutputItemJson toJson() {
			W3FunctionOutputItemJson functionOutputItemJson = new W3FunctionOutputItemJson();
			functionOutputItemJson.type = type;
			for (W3FunctionOutput functionOutput : outputList) {
				functionOutputItemJson.functionOutputJsons.add(functionOutput.toJson());
			}
			
			return functionOutputItemJson;
		}
	}
	
	// Map<FunctionType, List<W3FunctionOutput>> functionOutput = new HashMap<Function.FunctionType, List<W3FunctionOutput>>();
	Map<FunctionType, W3FunctionOutputItem> functionOutput = new HashMap<Function.FunctionType, W3FunctionOutputItem>();
	W3QueryResult addFunctionOutput(Function function) {
		functionOutput.put(function.getType(), new W3FunctionOutputItem(function.getType(), function.getOutput()));
		return this;
	}
	
	public List<W3FunctionOutput> getFunctionOutput(FunctionType type) {
		return functionOutput.get(type).outputList;
	}
	
	public Set<FunctionType> getFunctionTypes() {
		return functionOutput.keySet();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		for (FunctionType type : getFunctionTypes()) {
			builder.append("Function Type = " + type + ":\n{\n");
			List<W3FunctionOutput> list = getFunctionOutput(type);
			for (W3FunctionOutput w3FunctionOutput : list) {
				builder.append(w3FunctionOutput);
				builder.append("\n");
			}
			builder.append("}\n==\n");
		}
		
		return builder.toString();
	}
	
	public static class W3FunctionOutputItemJson {
		public FunctionType getType() {
			return type;
		}
		public void setType(FunctionType type) {
			this.type = type;
		}
		public List<W3FunctionOutputJson> getFunctionOutputJsons() {
			return functionOutputJsons;
		}
		public void setFunctionOutputJsons(
				List<W3FunctionOutputJson> functionOutputJsons) {
			this.functionOutputJsons = functionOutputJsons;
		}
		FunctionType type;
		List<W3FunctionOutputJson> functionOutputJsons = new ArrayList<W3FunctionOutput.W3FunctionOutputJson>();
	}
	
	public static class W3QueryResultJson {
		public List<W3FunctionOutputItemJson> getFunctionOutputItemJsons() {
			return functionOutputItemJsons;
		}

		public void setFunctionOutputItemJsons(
				List<W3FunctionOutputItemJson> functionOutputItemJsons) {
			this.functionOutputItemJsons = functionOutputItemJsons;
		}

		List<W3FunctionOutputItemJson> functionOutputItemJsons = new ArrayList<W3QueryResult.W3FunctionOutputItemJson>();
	}

	public W3QueryResultJson toJson() {
		W3QueryResultJson retval = new W3QueryResultJson();
		for (W3FunctionOutputItem w3FunctionOutputItem : functionOutput.values()) {
			retval.functionOutputItemJsons.add(w3FunctionOutputItem.toJson());
		}
		
		return retval;
	}
}
