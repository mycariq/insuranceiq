package com.cariq.toolkit.utils.wwwservice.core;



/**
 * Simple Identity represented as Long
 * @author HVN
 *
 */
public class Identity {
	long id;

	
	public Identity(long id) {
		super();
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Identity other = (Identity) obj;
		if (id != other.id)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Identity [id=" + id + "]";
	}
	
	public static class IdentityJson {
		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		long id;
	}

	public static Identity fromJson(IdentityJson id) {
		return new Identity(id.id);
	}

	public IdentityJson toJson() {
		IdentityJson identityJson = new IdentityJson();
		identityJson.id = id;
		return identityJson;
	}

}
