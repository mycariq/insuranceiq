package com.cariq.toolkit.utils.wwwservice.core.condition;

import com.cariq.toolkit.utils.wwwservice.core.LiteralCondition;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;

public class NotContains extends LiteralCondition {
	public NotContains(ParameterDef paramdef, Object value) {
		super(paramdef, value);
	}

	@Override
	public ConditionType getType() {
		return ConditionType.NotContains;
	}

	@Override
	protected boolean isReallyTrue(Object conditionValue, Object parameterValue) {
		String paramStr = parameterValue.toString();
		return (paramStr.contains(conditionValue.toString()) == false);
	}
	
}
