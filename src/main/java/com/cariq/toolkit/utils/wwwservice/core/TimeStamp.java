package com.cariq.toolkit.utils.wwwservice.core;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeStamp {
	long epoch;

	public TimeStamp(Date date) {
		epoch = date.getTime()/1000;
	}
	

	public TimeStamp(long epoch) {
		super();
		this.epoch = epoch;
	}
	
	public TimeStamp(TimeStamp other) {
		epoch = other.epoch;
	}
	
	public TimeStamp(String timeString) throws ParseException {
		// Timestamp in  "yyyy-mm-dd hh-mi-ss" format
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		Date date = format.parse(timeString);
		epoch = date.getTime()/1000;
	}
	 
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (epoch ^ (epoch >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeStamp other = (TimeStamp) obj;
		if (epoch != other.epoch)
			return false;
		return true;
	}
	
	public static final TimeStamp MAX_TIME_STAMP = new TimeStamp(32991278065L); // 3015
	public static final TimeStamp MIN_TIME_STAMP = new TimeStamp(0L); // 1971

	/**
	 * Get internal Epoch value
	 * 
	 * @return
	 */
	public long getEpoch() {
		return epoch;
	}


	/**
	 * If this timestamp is before other timestamp
	 * @param end
	 * @return
	 */
	public boolean isBefore(TimeStamp end) {
		return epoch < end.epoch;
	}
	
	/**
	 * If this timestamp is after other timestamp
	 * @param end
	 * @return
	 */
	public boolean isAfter(TimeStamp end) {
		return epoch > end.epoch;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.format(getDate());
	}


	private Date getDate() {
		return new Date(epoch * 1000);
	}
	
	public static Date convertDateTime(java.sql.Date date, Time time) {
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        Calendar calendar1=Calendar.getInstance();
        calendar1.setTime(time);
        calendar.set(Calendar.MINUTE, calendar1.get(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendar1.get(Calendar.SECOND));
        calendar.set(Calendar.HOUR_OF_DAY, calendar1.get(Calendar.HOUR_OF_DAY));
        return calendar.getTime();
    }
	
	public static class TimeStampJson {
		public Date getDt() {
			return dt;
		}

		public void setDt(Date dt) {
			this.dt = dt;
		}

		Date dt;
	}

	public static TimeStamp fromJson(TimeStampJson start) {
		return new TimeStamp(start.dt);
	}

	public TimeStampJson toJson() {
		TimeStampJson timeStampJson = new TimeStampJson();
		timeStampJson.dt = this.getDate();
		return timeStampJson;
	}
}

