/**
 * 
 */
package com.cariq.toolkit.utils.wwwservice.core;

import java.util.Set;

import com.cariq.toolkit.utils.wwwservice.core.ParameterDef.ParameterDefJson;
import com.cariq.toolkit.utils.wwwservice.core.condition.IsNotNull;
import com.cariq.toolkit.utils.wwwservice.core.condition.IsNull;

/**
 * @author Abhijit
 *
 */
public abstract class UnaryCondition extends Condition {
	ParameterDef paramDef;

	/**
	 * Condition dependent on only parameter
	 * 
	 * @param paramDef
	 */
	public UnaryCondition(ParameterDef paramDef) {
		super();
		this.paramDef = paramDef;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.www.utils.wwwservice.core.Condition#isTrue(com.cariq.www.utils
	 * .wwwservice.core.DataPoint)
	 */
	@Override
	public boolean isTrue(DataPoint datapoint) {
		Object value = Parameter.evaluate(paramDef, datapoint);

		return isReallyTrue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.www.utils.wwwservice.core.Condition#collectParameters(java.
	 * util.Set)
	 */
	@Override
	public void collectParameters(Set<String> params) {
		collectParameterStrings(paramDef, params);
	}

	protected abstract boolean isReallyTrue(Object value);

	/**
	 * Load class from json
	 * 
	 * @param type
	 * @param conditionJson
	 * @return
	 */
	public static Condition fromJson(ConditionType type,
			ConditionJson conditionJson) {
		UnaryConditionJson unaryConditionJson = (UnaryConditionJson) conditionJson;
		ParameterDef paramDef = ParameterDef.fromJson(unaryConditionJson
				.getParamDef());

		switch (type) {

		case IsNull:
			return new IsNull(paramDef);

		case IsNotNull:
			return new IsNotNull(paramDef);

		default:
			break;
		}

		return null;
	}


	/* (non-Javadoc)
	 * @see com.cariq.www.utils.wwwservice.core.Condition#toJson()
	 */
	@Override
	public ConditionJson2 toJson() {
//		UnaryConditionJson retval = new UnaryConditionJson(getType());
//		retval.setParamDef(paramDef.toJson());
//
//		return retval;
		ConditionJson2 json = new ConditionJson2();
		
		String operator = null;
		switch (getType()) {
		case IsNotNull:
			operator = " NOTNULL ";
			break;
		case IsNull:
			operator = " ISNULL ";
			break;
	
		default:
			break;
		}

		json.setConditionStr(paramDef.getParam() + " " + operator);
		return json;
	}
	
	/************************************************************
	 * 
	 * Unary Condition Json
	 * 
	 * @author Abhijit
	 *
	 */
	public static class UnaryConditionJson extends ConditionJson {
		ParameterDefJson paramDef;

		/**
		 * @return the paramDef
		 */
		public ParameterDefJson getParamDef() {
			return paramDef;
		}

		/**
		 * @param paramDef
		 *            the paramDef to set
		 */
		public void setParamDef(ParameterDefJson paramDef) {
			this.paramDef = paramDef;
		}

		public UnaryConditionJson(ConditionType conditionType) {
			super(conditionType);
		}
		
		public UnaryConditionJson() {
			super();
		}
	}
}
