package com.cariq.toolkit.utils.wwwservice.core;

public class EnormousResultException extends RuntimeException {

	public EnormousResultException(long resultSize, long allowableSize) {
		super("Datasize too big to process. Size of Data:" + resultSize + " Max Allowable Size: " + allowableSize);
	}

}
