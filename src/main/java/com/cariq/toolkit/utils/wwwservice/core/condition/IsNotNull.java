/**
 * 
 */
package com.cariq.toolkit.utils.wwwservice.core.condition;

import com.cariq.toolkit.utils.wwwservice.core.Condition.ConditionType;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;
import com.cariq.toolkit.utils.wwwservice.core.UnaryCondition;

/**
 * @author Abhijit
 *
 */
public class IsNotNull extends UnaryCondition {

	public IsNotNull(ParameterDef paramDef) {
		super(paramDef);
	}

	@Override
	public ConditionType getType() {
		return ConditionType.IsNotNull;
	}

	@Override
	protected boolean isReallyTrue(Object value) {
		return (value != null);
	}

}
