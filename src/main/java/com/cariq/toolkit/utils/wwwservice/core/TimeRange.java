package com.cariq.toolkit.utils.wwwservice.core;

import com.cariq.toolkit.utils.wwwservice.core.TimeStamp.TimeStampJson;

/**
 * Range of time - of 0 or bigger 
 * @author HVN
 *
 */
public class TimeRange {
	TimeStamp start, end;

	/**
	 * Simple constructor
	 * @param start
	 * @param end
	 */
	public TimeRange(TimeStamp start, TimeStamp end) {
		super();
		if (start.isBefore(end)) {
		this.start = start;
		this.end = end;
		}
		else {
			this.start = end;
			this.end = start;
		}
	}

	public TimeStamp getStart() {
		return start;
	}

	public TimeStamp getEnd() {
		return end;
	}
	
	/**
	 * Total time duration in the TimeRange in seconds
	 * @return
	 */
	public long getDurationInSeconds() {
		return end.getEpoch() - start.getEpoch();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TimeRange [start=" + start + ", end=" + end + "]";
	}
	
	public static class TimeRangeJson {
		public TimeStampJson getStart() {
			return start;
		}
		public void setStart(TimeStampJson start) {
			this.start = start;
		}
		public TimeStampJson getEnd() {
			return end;
		}
		public void setEnd(TimeStampJson end) {
			this.end = end;
		}
		TimeStampJson start;
		TimeStampJson end;
	}

	public static TimeRange fromJson(TimeRangeJson timeRange) {
		// TimeStampJson start;
		// TimeStampJson end;
		return new TimeRange(TimeStamp.fromJson(timeRange.start), TimeStamp.fromJson(timeRange.end));

	}

	public TimeRangeJson toJson() {
		TimeRangeJson timeRangeJson = new TimeRangeJson();
		timeRangeJson.start = start.toJson();
		timeRangeJson.end = end.toJson();
		return timeRangeJson;
	}
	
}
