package com.cariq.toolkit.utils.wwwservice.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.cariq.toolkit.utils.profile.ProfilePoint;

public class RegexpHelper {
	// Singleton for lazy loading
	static class JavaScriptEngine {
		static JavaScriptEngine instance = null;
		ScriptEngine engine = null;

		public JavaScriptEngine(ScriptEngine e) {
			this.engine = e;
		}

		static JavaScriptEngine getInstance() {
			if (null == instance) {
				ScriptEngineManager mgr = new ScriptEngineManager();
				ScriptEngine e = mgr.getEngineByName("JavaScript");
				instance = new JavaScriptEngine(e);
			}
			
			return instance;
		}

		/**
		 * @return the e
		 */
		public ScriptEngine getEngine() {
			return engine;
		}
	}

	public static Object evaluateExpression(String expression,
			Map<String, Parameter> valueMap) throws ScriptException {
		String[] tokens = tokenize(expression);
		return evaluateExpression(expression, tokens, valueMap);
	}

	// TODO: - doing this for every value will kill performance
	public static Object evaluateExpression(String expression, String[] tokens,
			Map<String, Parameter> valueMap) throws ScriptException {
		try (ProfilePoint _regexp_replace = ProfilePoint
				.profileAction("ProfAction_regexp_replace")) {
			for (String token : tokens) {
				String wholeWord = "\\b" + token + "\\b";
				Pattern replacePattern = Pattern.compile(wholeWord);
				// get a matcher object
				Matcher m = replacePattern.matcher(expression);
				Parameter param = valueMap.get(token);
				if (null == param)
					continue;

				Object val = param.getValue();
				if (null == val)
					continue;

				expression = m.replaceAll(val.toString());
			}
		}
		Object val;
		try (ProfilePoint _EvaluateUsingJavaScriptEngine = ProfilePoint
				.profileAction("ProfAction_EvaluateUsingJavaScriptEngine")) {
			try (ProfilePoint _EvaluateExpression = ProfilePoint
					.profileAction("ProfAction_EvaluateExpression")) {
				val = JavaScriptEngine.getInstance().getEngine().eval(expression);
			}
		}

		// System.out.println(expression + " = " + val);

		if (!validateDouble(val))
			return null;

		return val;
	}

	private static boolean validateDouble(Object val) {
		try {
			Double value = Double.parseDouble(val.toString());
			if (Double.isNaN(value) || Double.isInfinite(value))
				return false;

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	public static String[] tokenize(String expression) {
		List<String> tokens = new ArrayList<String>();

		String pattern = "([a-zA-Z][a-zA-Z0-9_:]+)";

		// Create a Pattern object
		Pattern r = Pattern.compile(pattern);

		// Now create matcher object.
		Matcher m = r.matcher(expression);

		while (m.find()) {
			// System.out.println("Found value: " + m.group(0));
			tokens.add(m.group(0));
		}

		return tokens.toArray(new String[tokens.size()]);
	}
}
