package com.cariq.toolkit.utils.wwwservice.core.condition;

import com.cariq.toolkit.utils.wwwservice.core.Condition.ConditionType;
import com.cariq.toolkit.utils.wwwservice.core.LiteralCondition;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;

public class Equal extends LiteralCondition {
	public Equal(ParameterDef paramdef, Object value) {
		super(paramdef, value);
	}

	@Override
	public ConditionType getType() {
		return ConditionType.Equal;
	}

	@Override
	protected boolean isReallyTrue(Object conditionValue, Object parameterValue) {
		return conditionValue.equals(parameterValue);
	}
	
}
