package com.cariq.toolkit.utils.wwwservice.core;

public class MapUtil {

	// base angle in degree
	public static final double BaseAngle = 180.0;

	// earth radius in kilometer
	private static int earthRadius = 6371;

	// size in kilometer
	public static double Size = 0.100;

	/**
	 * calculate distance between two points
	 * 
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @return
	 */
	public static double findDistance(double lat1, double lon1, double lat2,
			double lon2) {
		try {
			lat1 = Math.toRadians(lat1);
			lat2 = Math.toRadians(lat2);
			lon1 = Math.toRadians(lon1);
			lon2 = Math.toRadians(lon2);
			double finalLat = Math.toRadians(lat2 - lat1);
			double finalLon = Math.toRadians(lon2 - lon1);
			double cal = Math.sin(finalLat / 2) * Math.sin(finalLat / 2)
					+ Math.cos(lat1) * Math.cos(lat1) * Math.sin(finalLon / 2)
					* Math.sin(finalLon / 2);
			double cal1 = 2 * Math.atan2(Math.sqrt(cal), Math.sqrt(1 - cal));
			double d = earthRadius * cal1;
			return Math.toDegrees(d);
		} catch (Exception e) {
			return 0.0;
		}
	}

	/**
	 * Calculate latitude
	 * 
	 * @param lat
	 * @param angle
	 * @param diagonal
	 * @return
	 */
	public static double calculateLatitude(double lat, double angle,
			double diagonal) {
		double latitude = 0.0;
		lat = Math.toRadians(lat);
		double bearing = Math.toRadians(angle);
		latitude = Math.asin(Math.sin(lat) * Math.cos(diagonal / earthRadius)
				+ Math.cos(lat) * Math.sin(diagonal / earthRadius)
				* Math.cos(bearing));
		return Math.toDegrees(latitude);
	}

	/**
	 * Calculate longitude
	 * 
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param angle
	 * @param diagonal
	 * @return
	 */
	public static double calculateLongitude(double lat1, double lon1,
			double lat2, double angle, double diagonal) {
		double longitude = 0.0;
		lat1 = Math.toRadians(lat1);
		lon1 = Math.toRadians(lon1);
		lat2 = Math.toRadians(lat2);
		double bearing = Math.toRadians(angle);
		longitude = lon1
				+ Math.atan2(
						Math.sin(bearing) * Math.sin(diagonal / earthRadius)
								* Math.cos(lat1),
						Math.cos(diagonal / earthRadius) - Math.sin(lat1)
								* Math.sin(lat2));
		return Math.toDegrees(longitude);
	}

	/**
	 * Calculate diagonal of square
	 * 
	 * @param height
	 * @param width
	 * @return
	 */
	public static double calculateDiagonal(double height, double width) {
		double result = (height * height) + (width * width);
		return Math.sqrt(result);
	}
}
