package com.cariq.toolkit.utils;

import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * All CarIQ APIs must be called through this class. It's a RAII class to
 * capture entry and exit conditions. It will be enhanced over time. To start
 * with, it contains logging and ProfilePoint
 * 
 * @author HVN
 *
 *         == Eclipse CarIQAPI macro == try (CarIQAPI _${myvar} = new
 *         CarIQAPI("CARIQ_${myvar}")) { ${line_selection}${cursor} }
 */

public class CarIQAPI implements AutoCloseable {
	String apiName;
	ProfilePoint profilePoint;
	String userName;
	long startTime;

	static {
		ProfilePoint.setRenderer(new ProfilePointLogRenderer());
	}
	
	public CarIQAPI(String apiName) {
		this(apiName, Utils.getUserNameForCarIQApi());
	}

	public CarIQAPI(String apiName, String userName) {
		this.apiName = apiName;
		this.userName = userName;
		this.startTime = System.currentTimeMillis();
		
		try {
			if (apiName != null)
				profilePoint = ProfilePoint.profileActivity(apiName);

			if (apiName != null && userName != null)
				CarIQToolkitHelper.logger.getLogger("API START").debug(apiName + ",Caller," + userName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close() {
		try {			
			if (profilePoint != null)
				profilePoint.close();
			
			if (apiName != null && userName != null) {
				CarIQToolkitHelper.logger.getLogger("API END").debug(apiName + ",Caller," + userName);
				CarIQToolkitHelper.logger.getLogger("CarIQAPI").debug(apiName + "," + userName + "," + (System.currentTimeMillis() - startTime));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getUserName() {
		return userName;
	}
}
