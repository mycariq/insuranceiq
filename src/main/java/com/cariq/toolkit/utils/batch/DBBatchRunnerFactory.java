/**
 * 
 */
package com.cariq.toolkit.utils.batch;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * @author Abhijit
 *
 */
public class DBBatchRunnerFactory extends BatchRunnerFactory {

	@Override
	BatchRunner getBatchRunner(List<BatchObject> batchContents) {
		return new DBBatchRunner(batchContents);
	}

	private static class DBBatchRunner extends BatchRunner {
		static CarIQLogger logger = BatchHelper.getLogger().getLogger("DBBatchRunner");

		/**
		 * Instantiates a new batch persister.
		 *
		 * @param batchContents
		 *            the batch contents
		 */
		public DBBatchRunner(List<BatchObject> batchContents) {
			super(batchContents);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		@Override
		@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = Exception.class)
		public void run() {
			try (ProfilePoint _BatchPersister_run = ProfilePoint.profileActivity("ProfAction_DBBatchRunner_run")) {
				logger.debug("[CALLED],[run],run()");
				List<BatchObject> batchContents = getBatchContents();
				if (batchContents == null || batchContents.isEmpty())
					return;
				logger.info("[INFO],[run],Persisting models by calling persist  or Merge DB()");
				for (BatchObject model : batchContents) {
					try (ProfilePoint _persistDB = ProfilePoint.profileAction("ProfAction_persistDB")) {
						logger.info("[INFO],[run],Persisting " + model.getClass().getCanonicalName());
						if (model.getMethod().equals(BatchObject.ProcessMethod.Insert))
							model.getObj().persistDB();
						else if (model.getMethod().equals(BatchObject.ProcessMethod.Update))
							model.getObj().mergeDB();
						else
							Utils.handleException("Invalid Processing method to process Batch");
					}
				}

				logger.info("[INFO],[run],Batch processed!");
			}
		}
	}
}
