package com.cariq.toolkit.utils.batch;

public class BatchObject {
	public enum ProcessMethod {
		Insert, Update, Unknown
	}

	ProcessMethod method;
	BatchInsertable obj;

	public BatchObject(BatchInsertable obj, ProcessMethod method) {
		super();
		this.method = method;
		this.obj = obj;
	}

	/**
	 * @return the method
	 */
	public ProcessMethod getMethod() {
		return method;
	}

	/**
	 * @return the obj
	 */
	public BatchInsertable getObj() {
		return obj;
	}
}
