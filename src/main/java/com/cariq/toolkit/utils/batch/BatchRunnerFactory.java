package com.cariq.toolkit.utils.batch;

import java.util.List;

public abstract class BatchRunnerFactory {
	abstract BatchRunner getBatchRunner(List<BatchObject> batchContents);

	static abstract class BatchRunner implements Runnable {
		/** The batch contents. */
		private List<BatchObject> batchContents;

		/**
		 * Instantiates a new batch persister.
		 *
		 * @param batchContents
		 *            the batch contents
		 */
		public BatchRunner(List<BatchObject> batchContents) {
			this.batchContents = batchContents;
		}

		/**
		 * @return the batchContents
		 */
		public List<BatchObject> getBatchContents() {
			return batchContents;
		}

	}
}
