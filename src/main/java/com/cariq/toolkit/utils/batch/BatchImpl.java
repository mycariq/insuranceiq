package com.cariq.toolkit.utils.batch;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.batch.BatchObject.ProcessMethod;
import com.cariq.toolkit.utils.profile.ProfilePoint;

// TODO: Auto-generated Javadoc
/**
 * The Class BatchImpl.
 */
public class BatchImpl implements Batch {

	/** The batch size. */
	private int batchSize;

	/** The batch. */
	private List<BatchObject> batch = null;

	/** The batch processor. */
	private BatchProcessor batchProcessor;

	private boolean isBatchInsertable;

	private String name;

	/** The logger. */
	private CarIQLogger logger;

	// Batch is threadsafe with this lock
	Object syncLock = new Object();

	/**
	 * Instantiates a new batch impl.
	 *
	 * @param batchSize
	 *            the batch size
	 * @param processor
	 *            the processor
	 */
	public BatchImpl(String name, int batchSize, BatchProcessor processor,
			boolean isBatchInsertable) {
		super();
		this.batchSize = batchSize;
		this.batchProcessor = processor;
		this.isBatchInsertable = isBatchInsertable;
		this.name = name;
		logger = BatchHelper.getLogger().getLogger("Batch-" + name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.batch.Batch#getCurrentSize()
	 */
	@Override
	public int getCurrentSize() {
		if (batch != null)
			return batch.size();

		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.batch.Batch#getSize()
	 */
	@Override
	public int getSize() {
		return batchSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.batch.Batch#getBatchProcessor()
	 */
	@Override
	public BatchProcessor getBatchProcessor() {
		return batchProcessor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.batch.Batch#add(com.cariq.www.utils.batch.
	 * BatchInsertable)
	 */
	private synchronized Batch add(BatchInsertable obj, ProcessMethod method) {
		if (null == batch)
			batch = new ArrayList<BatchObject>();

		batch.add(new BatchObject(obj, method));

		return this;
	}

	@Override
	public synchronized void processBatch() {
		if (batch != null) {
			batchProcessor.execute(batch);
			batch = null;
		}
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public synchronized Batch insert(BatchInsertable obj) {
		try (ProfilePoint _ProfAction_batchimpl_add = ProfilePoint
				.profileAction("ProfAction_ProfAction_batchimpl_insert")) {
			if (!isBatchInsertable) {
				logger.info("[INFO],[insert], Batching is off so persisting "
						+ obj.getClass().getCanonicalName());
				obj.persistDB();
				return this;
			}
			if (getCurrentSize() >= batchSize) {
				logger.info("[INFO],[insert],Batch is full. Executing batchProcessor");
				processBatch();
			}
			logger.info("[INFO],[adinsertd], Batching is on so inserting "
					+ obj.getClass().getCanonicalName() + " in batch");
			add(obj, ProcessMethod.Insert);
			return this;
		}
	}

	@Override
	public synchronized Batch update(BatchInsertable obj) {
		try (ProfilePoint _ProfAction_batchimpl_add = ProfilePoint
				.profileAction("ProfAction_ProfAction_batchimpl_update")) {
			if (!isBatchInsertable) {
				logger.info("[INFO],[update], Batching is off so persisting "
						+ obj.getClass().getCanonicalName());
				obj.mergeDB();
				return this;
			}
			if (getCurrentSize() >= batchSize) {
				logger.info("[INFO],[update],Batch is full. Executing batchProcessor");
				processBatch();
			}
			logger.info("[INFO],[update], Batching is on so updating "
					+ obj.getClass().getCanonicalName() + " in batch");
			add(obj, ProcessMethod.Update);
			return this;
		}
	}
}
