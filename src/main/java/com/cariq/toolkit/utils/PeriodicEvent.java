package com.cariq.toolkit.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Simple POJO to identify Periodically repeating Calendar event
 * @author HVN
 *
 */
public class PeriodicEvent {
	int frequencyUnit;
	int frequency;
	int scheduledDay = -1; // can be day of the week/month or year
	int scheduledHour = -1; // if not specified 0
	int scheduledMinute = -1; // if not specified, 0

	public PeriodicEvent(int frequencyUnit, int frequency, int scheduledDay,
			Date scheduledTime) {
		super();
		this.frequencyUnit = frequencyUnit;
		this.frequency = frequency;
		this.scheduledDay = scheduledDay;
		
		if (scheduledTime != null) {
			this.scheduledHour = Utils.getFromDate(scheduledTime, Calendar.HOUR_OF_DAY);
			this.scheduledMinute = Utils.getFromDate(scheduledTime, Calendar.MINUTE);
		}
	}

	private Date addTime(Date lastRunTime) {
		switch (frequencyUnit) {
		case Calendar.SECOND:
			return add(Calendar.SECOND, frequency, lastRunTime);
		case Calendar.MINUTE:
			return add(Calendar.MINUTE, frequency, lastRunTime);
		case Calendar.HOUR:
			return add(Calendar.HOUR, frequency, lastRunTime);

		default:
			break;
		}
		
		Utils.handleException("Invalid Frequency Unit: " + frequencyUnit);
		
		return null;
	}
	
	private Date add(int unit, int value, Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(unit, value);
		return cal.getTime();
	}

	/**
	 * Give last run time and based on frequency, give next time
	 * @param lastRunTime
	 * @return
	 */
	public Date getNextOccuranceTime(Date lastRunTime) {
		if (frequency <= 0)
			return Utils.dateYearsAhead(new Date(), 100);
		if (lastRunTime == null)
			return new Date();
		
		EventMarker marker = EventMarker.getInstance(frequencyUnit, scheduledDay, scheduledHour, scheduledMinute);
		if (null != marker) {
			Date snappedEventMarker = marker.snapToNearestEventMarker(lastRunTime);
			return marker.getEventMarkerAfter(frequency, snappedEventMarker);
		}
		return addTime(lastRunTime);
	}

	public static String translateFrequencyUnit(int unit) {
		switch (unit) {
		case Calendar.SECOND:
			return "SECOND";
		case Calendar.MINUTE:
			return "MINUTE";
		case Calendar.HOUR:
			return "HOUR";
		case Calendar.HOUR_OF_DAY: // on a fixed hour per day
			return "DAY";
		case Calendar.DAY_OF_WEEK: // fixed Day per week
			return "WEEK";		
		case Calendar.DAY_OF_MONTH: // fixed Day per month
			return "MONTH";		
		case Calendar.DAY_OF_YEAR: // fixed day per year
			return "YEAR";
		default:
			break;
		}
		
		return "INVALID";
	}
}
