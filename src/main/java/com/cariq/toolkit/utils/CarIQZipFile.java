package com.cariq.toolkit.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import com.cariq.toolkit.utils.analytics.core.CarIQFile;

public class CarIQZipFile implements ObjectBuilder<String>, CarIQFile {
	// filePath is local path, serverPath is path inside endpoint of blob
	// storage
	String filePath, serverPath;
	ZipOutputStream writer;
	ZipInputStream reader;

	Mode mode;

	public enum Mode {
		COMPRESS, EXTRACT
	}

	public CarIQZipFile(String localPath, String serverPath) throws FileNotFoundException {
		this(localPath, serverPath, Mode.COMPRESS);
	}

	public CarIQZipFile(String filePath) throws FileNotFoundException {
		this(filePath, filePath, Mode.COMPRESS);
	}

	public CarIQZipFile(String filePath, String serverPath, Mode mode) throws FileNotFoundException {
		super();
		this.mode = mode;
		if (mode == Mode.COMPRESS) {
			this.filePath = filePath;
			this.serverPath = serverPath;
			writer = new ZipOutputStream(new FileOutputStream(filePath));
		} else {
			reader = new ZipInputStream(new FileInputStream(new File(filePath)));
		}
	}

	/**
	 * @return the writer
	 */
	public ZipOutputStream getWriter() {
		return writer;
	}

	@Override
	public CarIQZipFile configure(String setting, Object value) {
		return this;
	}

	@Override
	public CarIQZipFile add(String zipEntryName, String filePath) {
		if (mode == Mode.EXTRACT)
			throw new RuntimeException("Add file operation is not permitted for Extract mode");
		if (new File(filePath).exists() == false) // can't add nonexistant file,
													// no point in failing
			return this;

		// Open the file add it to Zip stream
		try {
			writer.putNextEntry(new ZipEntry(zipEntryName));
			// copy contents of one file to other
			CarIQFileUtils.copyContents(writer, filePath);
		} catch (IOException e) {
			Utils.handleException(e.getMessage());
		}
		return this;
	}

	@Override
	public CarIQZipFile build() {
		try {
			writer.flush();
			writer.close();
		} catch (IOException e) {
			Utils.handleException(e.getMessage());
		}
		return this;
	}

	public long getSize() {
		return CarIQFileUtils.getFileSize(filePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CarIQZipFile [filePath=" + filePath + " fileSize=" + getSize() + "]";
	}

	public void delete() {
		File f = new File(filePath);
		f.delete();
	}

	@Override
	public CarIQZipFile add(String value) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("ObjectBuilder<String>.add() is not implemented");
	}

	@Override
	public String getName() {
		return serverPath;
	}

	@Override
	public String getPath() {
		return filePath;
	}

	@Override
	public File getFile() {
		return new File(getPath());
	}

	@Override
	public boolean exists() {
		return getFile().exists();
	}

	public File extractFirst() throws IOException {
		if (mode == Mode.COMPRESS)
			throw new RuntimeException("Extract operation is not permitted for Compress mode");
		ZipEntry zipEntry = reader.getNextEntry();
		if (zipEntry == null)
			return null;
		File newFile = new File(CarIQFileUtils.getTempFile(zipEntry.getName()));
		FileOutputStream fos = new FileOutputStream(newFile);
		byte[] buffer = new byte[1024];
		int len;

		while ((len = reader.read(buffer)) > 0) {
			fos.write(buffer, 0, len);
		}
		fos.close();
		reader.closeEntry();
		reader.close();
		return newFile;
	}

	public List<File> extractAll() {
		throw new UnsupportedOperationException("CarIQZipFile.extractAll() is not implemented");
	}
}
