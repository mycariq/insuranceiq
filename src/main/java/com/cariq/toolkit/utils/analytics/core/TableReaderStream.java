package com.cariq.toolkit.utils.analytics.core;

public abstract class TableReaderStream extends TableStream  implements AutoCloseable {
	public TableReaderStream(String fileName) {
		super(fileName);
	}
	
	public abstract boolean hasNext();
	
	public abstract  Record getNext();
}
