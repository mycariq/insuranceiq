package com.cariq.toolkit.utils.analytics.core;

import java.io.File;

public interface CarIQFile {
	String getName();
	String getPath();
	long getSize();
	File getFile();
	boolean exists();
}
