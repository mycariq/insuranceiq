package com.cariq.toolkit.utils.analytics.core;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * General Purpose Helper for Analytics Engine Also contains constants for the
 * purpose
 * 
 * @author HVN
 *
 */
public class AnalyticsHelper {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("AnalyticsHelper");
	
	public static final String Function_MEAN = "MEAN";
	public static final String Function_STDDEV = "STDDEV";
	public static final String TABLE_READER_STREAM = "TABLE_READER_STREAM";
	public static final String TABLE_WRITER_STREAM = "TABLE_WRITER_STREAM";
	public static final String INFRACTIONS = "INFRACTIONS";
	public static final String INFORMATIONS = "INFORMATIONS";

	// Field Names
	// Default columns of WHO WHAT WHERE WHEN
	public static final String CAR_ID = "itsCar";
	public static final String TIME_STAMP = "itsTimeStamp";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";

	public static final String SERVER_TIME_STAMP = "serverTimeStamp";
	public static final String ERROR_CODES = "errorCodes";

	public static final Object TEMPLATE_FILE = "templateFile";
	public static final String RECORD_PROCESSOR = "RECORD_PROCESSOR";
	public static final String ACCELERATION_X = "accelerometerX";
	public static final String ACCELERATION_Y = "accelerometerY";
	public static final String ACCELERATION_Z = "accelerometerZ";
	public static final String ACCELERATION_G = "accelerometerG";

	public static String getFunctionKey(String name, String function) {
		return name + "." + function;
	}

	public static Object getValue(AnalyticsContext ctx, String column, String function) {
		return ctx.get(getFunctionKey(column, function));
	}

	public static String getFilePath(String identifier, int iteration) {
		return CarIQFileUtils.getTempFile(identifier + "_" + iteration + ".csv");
	}
//
//	public static void addInfraction(AnalyticsContext ctx, Infraction infraction) {
//		InfractionsMap map = (InfractionsMap) ctx.get(INFRACTIONS);
//		if (null == map) {
//			map = new InfractionsMap();
//			ctx.put(INFRACTIONS, map);
//		}
//
//		map.add(infraction);
//	}
//
//	public static Infraction logInfraction(AnalyticsContext context, String type, Record record, Double value,
//			String unit) {
//		Infraction infra = new Infraction(type, record.getTimeStamp(), record.getTimeStamp(), record.getLatitude(),
//				record.getLongitude(), value, unit, "", "Value crossing Threshold");
//		addInfraction(context, infra);
//		return infra;
//	}
//
//	public static void addInformation(AnalyticsContext ctx, Information information) {
//		InformationMap map = (InformationMap) ctx.get(INFORMATIONS);
//		if (null == map) {
//			map = new InformationMap();
//			ctx.put(INFORMATIONS, map);
//		}
//
//		map.add(information);
//	}
//
//	public static Information logInformation(AnalyticsContext context, String type, Double value, Object additional,
//			String unit) {
//		if (value == null)
//			value = 0.0;
//
//		Information info = new Information(type, value, additional, unit, "", "Value crossing Threshold");
//		addInformation(context, info);
//		return info;
//	}

	public static boolean isValidRecord(Record record) {
		if (record == null)
			return false;
		if (record.getTimeStamp() == null || record.getLatitude() == null || record.getLongitude() == null)
			return false;

		return true;
	}

	public static boolean isValidLocationRecord(Record record) {
		if (!isValidRecord(record))
			return false;

		if (record.getTimeStamp() == null || record.getLatitude() == null || record.getLongitude() == null)
			return false;

		return true;
	}

	public static void incrementValue(AnalyticsContext ctx, String key, double increment) {
		Double val = (Double) ctx.get(key);
		if (null == val)
			val = increment;
		else
			val = Double.parseDouble(val.toString()) + increment;

		ctx.put(key, val);
	}

	public static void incrementValue(AnalyticsContext ctx, String key) {
		incrementValue(ctx, key, 1.0);
	}

	// write to csv file
	public static void writeToCSVFile(String filePath, Iterator<GenericJSON> feeder) throws Exception {
		try (ProfilePoint _writeToCsvFile = ProfilePoint.profileAction("ProfAction_writeToCsvFile")) {
			try (TableWriterStream writer = new CSVTableWriterStream(filePath)) {
				boolean first = true;
				while (feeder.hasNext()) {
					GenericJSON json = feeder.next();
					if (json == null)
						break;
					Record record = recordify(json);
					if (first) {
						writeHeader(writer, record);
						first = false;
					}
					writer.write(record);
				}
			} 
		}
	}

	private static void writeHeader(TableWriterStream writer, Record record) throws IOException {
		for (Column c : record.getColumns()) {
			writer.addColumn(c);
		}
		
		writer.writeHeader();
	}

	public static Record recordify(GenericJSON outputJSON) {
		Record rec = new Record();

		for (String key : outputJSON.keySet()) {
			Object val = outputJSON.get(key);
			rec.append(key, val == null ? "null" : val.toString());
		}
		return rec;
	}
	
	static final String OUTPUT_STRING = "OUTPUT_STRING";
	static final String HEADER_RENDERED = "HEADER_RENDERED";

	/**
	 * @param tmpExportFile
	 * @return
	 * @throws Exception 
	 */
	public static String StringifyCSV(String csvFile) throws Exception {
		StringBuilder stringBuilder = new StringBuilder();
		
		AnalyticsContext ctx = new AnalyticsContext();
		// put the Renderer in the ctx
		ctx.put(OUTPUT_STRING, stringBuilder);
		
		// Process the File using simple Analytics processor
		TableProcessor processor = new TableProcessorImpl(ctx, "CSVStringify", csvFile);

		processor.setForgiveException(false);
		RecordProcessor recordProcessor = new RecordProcessorTemplate(ctx) {
			@Override
			protected Record doProcess(Record record) {
				List<String> columns = record.getColumnKeys();
				StringBuilder stringBuilder = (StringBuilder) getProcessorContext().get(OUTPUT_STRING);
				boolean headerExists = getProcessorContext().containsKey(HEADER_RENDERED);
				if (!headerExists) {
					// add header row to String
					addCSVString(stringBuilder, columns);
					getProcessorContext().put(HEADER_RENDERED, true);
				}
	
				boolean first = true;
				for (String col : columns) {
					if (col.isEmpty())
						continue;
					
					if (first) {
						first = false;
						stringBuilder.append(record.getStringValue(col));
					} else {
						stringBuilder.append(",").append(record.getStringValue(col));
					}
				}
				
				stringBuilder.append("\n");

				return record;
			}
		};
		processor.add(recordProcessor);
		processor.init();
		processor.process();
		processor.close();
		return (String) processor.getProcessorContext().get(OUTPUT_STRING).toString();
	}

	/**
	 * @param stringBuilder
	 * @param columns
	 */
	protected static void addCSVString(StringBuilder stringBuilder, List<String> columns) {
		boolean first = true;
		for (String col : columns) {
			if (col.isEmpty())
				continue;
			
			if (first) {
				first = false;
				stringBuilder.append(col);
			} else {
				stringBuilder.append(",").append(col);
			}
		}
		stringBuilder.append("\n");
	}
}
