package com.cariq.toolkit.utils.analytics.core;

import java.io.IOException;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;

public class AnalyticsProcessIteration implements AutoCloseable {
	private TableProcessor dataProcessor;
	private String description;
	private long id;
	static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("AnalyticsProcessorIteration");
	
	public AnalyticsProcessIteration(TableProcessor dataProcessor, String description, long id) {
		super();
		this.dataProcessor = dataProcessor;
		this.description = description;
		this.id = id;
	}

	public TableProcessor getProcessor() {
		return dataProcessor;
	}
	
	public void start() throws IOException {
		logger.debug("Start Processing: DataProcessorIteration id:" + id + ", Description:" + description);
		dataProcessor.init();
		dataProcessor.process();
	}

	@Override
	public void close() {
		try {
			dataProcessor.close();
			logger.debug("Completed Processing: DataProcessorIteration id:" + id + ", Description:" + description);
		} catch (Exception e) {
			Utils.logException(logger, e, "Closing Analytics DataProcessor");
		}
	}

	public String getOutputFile() {
		return dataProcessor.getOutputFile();
	}
}
