package com.cariq.toolkit.utils.analytics.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Record represents one row in the analytics table It has ordered list of
 * columns and each column having value in the form of String
 * 
 * @author HVN
 *
 */
public class Record {
	static final String NULL_VALUE = "null";
	LinkedHashMap<Column, String> columnValueMap = new LinkedHashMap<Column, String>();
	public Set<Column> getColumns() {
		return columnValueMap.keySet();
	}

	public List<String> getColumnKeys() {
		List<String> retval = new ArrayList<String>();
		for (Column col : columnValueMap.keySet()) {
			if (col == null || col.getName() == null && col.getName().isEmpty())
				continue;
				
			retval.add(col.getName());
		}

		return retval;
	}

	/**
	 * Create record - append values later
	 */
	public Record() {
	}

	Record(List<Column> columns, String[] values) {
		//		try (ProfilePoint _Record_Constructor = ProfilePoint.profileAction("ProfAction_Record_Constructor")) {
		if (columns.size() < values.length)
			throw new RuntimeException(
					"Column and Cell sizes don't match. Columns: " + columns.size() + " Values:" + values.length);
		for (int i = 0; i < values.length; i++) {
			columnValueMap.put(columns.get(i), Utils.removeDoubleQuotation(values[i]));
		}
		//		}
	}

	public Record(List<Column> columns, List<String> values) {
		if (columns.size() < values.size())
			throw new RuntimeException(
					"Column and Cell sizes don't match. Columns: " + columns.size() + " Values:" + values.size());

		for (int i = 0; i < values.size(); i++) {
			columnValueMap.put(columns.get(i), Utils.removeDoubleQuotation(values.get(i)));
		}
	}

	public Record append(List<Column> columns, List<String> values) {
		if (columns.size() < values.size())
			throw new RuntimeException(
					"Column and Cell sizes don't match. Columns: " + columns.size() + " Values:" + values.size());

		for (int i = 0; i < values.size(); i++) {
			columnValueMap.put(columns.get(i), Utils.removeDoubleQuotation(values.get(i)));
		}

		return this;
	}

	public Record append(Column column, String value) {
		if (column == null || value == null)
			throw new RuntimeException("Column and Value should not be null!");

		columnValueMap.put(column, value);

		return this;
	}

	private boolean isValueNull(Column col) {
		try (ProfilePoint _isValueNull = ProfilePoint.profileAction("ProfAction_isValueNull")) {
			String val = columnValueMap.get(col);
			if (val == null || val.equals(NULL_VALUE))
				return true;
			return false;
		}
	}

	public Double getDoubleValue(Column col) {
		try (ProfilePoint _getDoubleValue = ProfilePoint.profileAction("ProfAction_getDoubleValue")) {
			if (isValueNull(col))
				return null;
			try {
				return Double.parseDouble(columnValueMap.get(col));
			} catch (Exception e) { // not a double  - or value does not exist
				return -1.0; // Error but no crash
			}
		}
	}

	public Double getDoubleValue(String col) {
		return getDoubleValue(new Column(col));
	}

	public Date getDateValue(Column col) {
		try (ProfilePoint _getDateValue = ProfilePoint.profileAction("ProfAction_getDateValue")) {
			if (isValueNull(col))
				return null;
			String dateStr = null;
			try {
				dateStr = columnValueMap.get(col);
				return Utils.parseDateFormatWithMiliSecond(dateStr);
			} catch (Exception e) { // not a double  - or value does not exist
				System.out.println("Unable to Parse " + dateStr + " Exception : " + e.getMessage());

				return null;
			}
		}
	}

	public Date getDateValue(String col) {
		return getDateValue(new Column(col));
	}

	public Long getLongValue(Column col) {
		try (ProfilePoint _getLongValue = ProfilePoint.profileAction("ProfAction_getLongValue")) {
			if (isValueNull(col))
				return null;
			try {
				return Long.parseLong(columnValueMap.get(col));
			} catch (Exception e) { // not a double  - or value does not exist
				System.out.println("!!! Unable to Parse : Exception : " + e.getMessage());

				return (long) -1; // Error but no crash
			}
		}
	}

	public String getStringValue(String col) {
		return getStringValue(new Column(col));
	}

	public String getStringValue(Column col) {
		if (isValueNull(col))
			return null;

		return columnValueMap.get(col);
	}

	public Long getLongValue(String col) {
		return getLongValue(new Column(col));
	}

	public Integer getIntegerValue(Column col) {
		if (isValueNull(col))
			return null;
		try {
			return Integer.parseInt(columnValueMap.get(col));
		} catch (Exception e) { // not a double  - or value does not exist
			System.out.println("!!!Unable to Parse : Exception : " + e.getMessage());

			return -1; // Error but no crash
		}
	}

	public Integer getIntegerValue(String col) {
		return getIntegerValue(new Column(col));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder("{Record:");

		for (Column col : columnValueMap.keySet()) {
			str.append(col.getName()).append(":").append(columnValueMap.get(col)).append(",");
		}

		str.append("}");
		return str.toString();
	}

	public String toCSVString() {
		return toOutputString(",");
	}

	public String toTSVString() {
		return toOutputString("\t");
	}
	
	private String toOutputString(String separator) {
		StringBuilder str = new StringBuilder();

		for (Column col : columnValueMap.keySet()) {
			str.append(Utils.handleSeparator(columnValueMap.get(col), separator)).append(separator);
		}
		return str.substring(0, str.length() - 1).toString();
	}

	public List<String> getValues(List<Column> columns) {
		List<String> values = new ArrayList<String>();
		for (Column column : columns) {
			String val = columnValueMap.get(column);
			values.add((val == null || val.isEmpty()) ? NULL_VALUE : val);
		}
		return values;
	}

	public List<String> getValues() {
		List<String> values = new ArrayList<String>();
		for (Column col : columnValueMap.keySet()) {
			String val = columnValueMap.get(col);
			values.add((val == null || val.isEmpty()) ? NULL_VALUE : val);
		}
		return values;
	}
	
	// Convenience functions to fetch the relevant values required by all in WWWW world
	public Date getTimeStamp() {
		return getDateValue(AnalyticsHelper.TIME_STAMP);
	}

	public Double getLatitude() {
		return getDoubleValue(AnalyticsHelper.LATITUDE);
	}

	public Double getLongitude() {
		return getDoubleValue(AnalyticsHelper.LONGITUDE);
	}

	public Long getCarId() {
		return getLongValue(AnalyticsHelper.CAR_ID);
	}

	/**
	 * Make json from the record
	 * 
	 * @return
	 */
	public Map<String, Object> jsonize() {
		Map<String, Object> retval = new HashMap<String, Object>();
		for (Column col : columnValueMap.keySet()) {
			retval.put(col.getName(), columnValueMap.get(col));
		}

		return retval;
	}

	public void append(String key, String val) {
		append(new Column(key), val);
	}

	public boolean isEmpty() {
		return columnValueMap.size() == 0;
	}
}
