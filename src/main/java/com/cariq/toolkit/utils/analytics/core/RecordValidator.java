package com.cariq.toolkit.utils.analytics.core;

/**
 * Validate the Record for sanity of all the values
 * @author HVN
 *
 */
public interface RecordValidator {
	boolean validate(Record record);
}
