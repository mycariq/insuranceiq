package com.cariq.toolkit.utils.analytics.core;

import java.io.IOException;
import java.util.List;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Base class for the Analytics Operation using the input csv file
 * Encapsulates entire Analytics Operation.
 * @author Abhijit
 *
 */
public abstract class AnalyticsWorker {
	static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("AnalyticsWorker");
	String id;
	String description;
	String inputFile;
	
	// same context for entire worker process
	AnalyticsContext context = new AnalyticsContext();
	
//	@Resource(name = "configProperties")
//	private Properties configProperties;
	
	protected void init() {
		// override by derived class if required to initialize
	}
	
	protected void close() {
		// override by derived class if required to cleanup
	}
	
	public AnalyticsWorker(String id, String description, String inputFile) {
		super();
		this.id = id;
		this.description = description;
		this.inputFile = inputFile;
	}
	
	/**
	 * Template method. invokes the derived method to execute Analytics Engine
	 * Information should get stored in Map format in the Context.
	 * Context is the Common bag containing all the data for reporting
	 */
	public void execute() {
		try {
			try (ProfilePoint _AnalyticsWorker = ProfilePoint
					.profileActivity("ProfActivity_AnalyticsWorker_Execute")) {
				init();
				doExecute();
			}
		} catch (Exception e) {
			Utils.logException(logger, e, "AnalyticsWorker.execute");
		} finally {
			close();
		}
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	

	/**
	 * @return the context
	 */
	public AnalyticsContext getContext() {
		return context;
	}
	
	/**
	 * @return the inputFile
	 */
	public String getInputFile() {
		return inputFile;
	}

	/**
	 * @return the reportFile
	 */
	public abstract List<CarIQFileDesc> getReportFiles();

	/**
	 * Real execution - must be overridden by derived class
	 * All the information should be filled in the AnalyticsContext
	 * @throws IOException
	 */
	protected abstract void doExecute()  throws Exception;
}
