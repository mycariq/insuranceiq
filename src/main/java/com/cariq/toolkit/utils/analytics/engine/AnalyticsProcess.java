package com.cariq.toolkit.utils.analytics.engine;

import java.io.File;
import java.util.List;

import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.core.AnalyticsProcessIteration;
import com.cariq.toolkit.utils.analytics.core.RecordProcessor;
import com.cariq.toolkit.utils.analytics.core.TableProcessor;
import com.cariq.toolkit.utils.analytics.core.TableProcessorImpl;
import com.cariq.toolkit.utils.profile.ProfilePoint;

public class AnalyticsProcess implements AutoCloseable {
	// Logger for AnalyticsProcess
	static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("AnalyticsProcess");

	public static final String INTERMEDIATE_FILES = "INTERMEDIATE_FILES"; // Key
																			// that
																			// stores
																			// list
																			// of
																			// intermediate
																			// files
	public static final String OUTPUT_FILES = "OUTPUT_FILES"; // Key that stores
																// Map of
																// OutputFiles
	AnalyticsRecipe recipe;
	AnalyticsContext context;

	String inputFilePath;

	public AnalyticsProcess(AnalyticsRecipe recipe, AnalyticsContext context, String inputFilePath) {
		super();
		this.recipe = recipe;
		this.context = context;
		this.inputFilePath = inputFilePath;
	}

	public AnalyticsRecipe getRecipe() {
		return recipe;
	}

	public AnalyticsContext getContext() {
		return context;
	}

	public String getInputFilePath() {
		return inputFilePath;
	}

	@Override
	public void close() {
		// delete all the intermediate files - assuming handles are closed
		for (String file : context.getList(INTERMEDIATE_FILES, String.class)) {
			File f = new File(file);
			if (f.exists())
				f.delete();
		}
	}

	public void execute() throws Exception {
		try (ProfilePoint _ProfAction_AnalyticsProcess_execute = ProfilePoint.profileActivity("ProfAction_AnalyticsProcess_execute")) {

			// Perform iterations of the Table processor and run those
			List<TableProcessorRecipe> tableProcessors = recipe.getTableProcessorRecipes();
			String processName = recipe.getName();
			String workId = processName + "-" + Utils.getCurrentTimeInUTC();
			String analyticsInpuFile = this.inputFilePath;
			int iterationNo = 0;
			for (TableProcessorRecipe tableProcessorRecipe : tableProcessors) {
				// Prepare output file path based on the Iteration Number
				String outputFilePath = CarIQFileUtils.getTempFile(processName + "-" + iterationNo + ".csv");
				// create Table Processor based on the context and the Record
				// Processors specified inside
				TableProcessor tableProcessor = createTableProcessor(context, analyticsInpuFile, outputFilePath, workId,
						tableProcessorRecipe.getRecordProcessorRecipes(), tableProcessorRecipe.getDescription());
				// run the iteration
				try (AnalyticsProcessIteration iteration = new AnalyticsProcessIteration(tableProcessor,
						tableProcessorRecipe.getDescription(), iterationNo++)) {
					iteration.start();
					context.putInList(INTERMEDIATE_FILES, iteration.getOutputFile());
					context.put(OUTPUT_FILES, iteration.getOutputFile());
				}
				analyticsInpuFile = outputFilePath; // copy output file to the
													// new iteration input file
			}
		}
	}

	private TableProcessor createTableProcessor(AnalyticsContext ctx, String inputFile, String outputFile,
			String workId, List<RecordProcessorRecipe> recordProcessorRecipes, String description) throws Exception {
		TableProcessor retval = new TableProcessorImpl(ctx, workId, inputFile, outputFile);
		for (RecordProcessorRecipe recordProcessorRecipe : recordProcessorRecipes) {
			retval.add(createRecordProcessor(recordProcessorRecipe.getClassName(),
					recordProcessorRecipe.getDescription(), ctx));
		}

		return retval;
	}

	private RecordProcessor createRecordProcessor(String className, String description, AnalyticsContext ctx)
			throws Exception {
		@SuppressWarnings("unchecked")
		Class<? extends RecordProcessor> processorClass = (Class<? extends RecordProcessor>) Class.forName(className);
		return processorClass.getDeclaredConstructor(AnalyticsContext.class).newInstance(ctx);
	}

	public List<String> getIntermediateFiles() {
		return context.getList(INTERMEDIATE_FILES, String.class);
	}

	public String getOutputFile() {
		return (String) context.get(OUTPUT_FILES);
	}
}
