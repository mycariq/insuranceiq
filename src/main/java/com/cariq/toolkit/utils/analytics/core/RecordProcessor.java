package com.cariq.toolkit.utils.analytics.core;

import java.io.IOException;

public interface RecordProcessor {
	void init() throws IOException;
	AnalyticsContext getProcessorContext();
	Record process(Record record) throws IOException;
	void close();
}
