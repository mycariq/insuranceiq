package com.cariq.toolkit.utils.analytics.core;

import com.cariq.toolkit.utils.Utils;

public class Column {
	String name;
	int index = -1;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Column other = (Column) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	public Column(String name) {
		super();
		this.name = Utils.removeDoubleQuotation(name);
	}

	
	public Column(String name, int index) {
		super();
		this.name = name;
		this.index = index;
	}

	String getName() {
		return name;
	}
	
}
