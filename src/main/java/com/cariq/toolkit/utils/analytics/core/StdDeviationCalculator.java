package com.cariq.toolkit.utils.analytics.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cariq.toolkit.utils.profile.ProfilePoint;

class ProcessorCacheForStdDev {
	static class ValueRecord {
		double total = 0.0;
		int count = 0;
		double meanValue;

		ValueRecord(double meanValue) {
			this.meanValue = meanValue;
		}

		void add(double value) {
			if (value == 0)
				return;

			double deviation = value - meanValue;
			total = total + deviation * deviation;
			// System.out.println("Value = " + value + " Mean = " + meanValue +
			// " Total = " + total);

			count++;
		}

		double getStdDev() {
			return count < 2 ? 0.0 : (Math.sqrt(total / (count - 1)));
		}
	}

	Map<Column, ValueRecord> stdDevValues = new HashMap<Column, ValueRecord>();

	Set<Column> getColumns() {
		return stdDevValues.keySet();
	}

	ProcessorCacheForStdDev(List<Column> columns, AnalyticsContext ctx) {
		for (Column column : columns) {
			stdDevValues.put(column, new ValueRecord(
					(Double) AnalyticsHelper.getValue(ctx, column.getName(), AnalyticsHelper.Function_MEAN)));
		}
	}

	public ProcessorCacheForStdDev(Column column, AnalyticsContext ctx) {
		stdDevValues.put(column, new ValueRecord(
				(Double) AnalyticsHelper.getValue(ctx, column.getName(), AnalyticsHelper.Function_MEAN)));
	}

	void add(Column col, Double value) {
		if (value == null)
			return;

		ValueRecord record = stdDevValues.get(col);
		record.add(value);
		stdDevValues.put(col, record); // this should be unnecessary
	}

	Double getResult(Column col) {
		return stdDevValues.get(col).getStdDev();
	}
}

public class StdDeviationCalculator extends RecordProcessorTemplate {
	ProcessorCacheForStdDev cache;
	public static final String COLUMN_LIST = "COLUMN_LIST";
	public static final String COLUMN = "COLUMN";

	public StdDeviationCalculator(AnalyticsContext processorContext) {
		super(processorContext);
		{
			Object columnList = processorContext.getParamValue(this.getClass(), COLUMN_LIST);
			if (columnList != null) {
				cache = new ProcessorCacheForStdDev((List<Column>) columnList, processorContext);
				return;
			}
		}
		{
			Object columnObj = processorContext.getParamValue(this.getClass(), COLUMN);
			if (columnObj != null) {
				cache = new ProcessorCacheForStdDev((Column) columnObj, processorContext);
				return;
			}
		}
	}

	public StdDeviationCalculator(AnalyticsContext processorContext, Column column) {
		super(processorContext);

		if (column != null) {
			cache = new ProcessorCacheForStdDev(column, processorContext);
			return;
		}

	}

	@Override
	protected Record doProcess(Record record) {
		try (ProfilePoint _StdDeviationCalculator_doProcess = ProfilePoint.profileAction("ProfAction_StdDeviationCalculator_doProcess")) {
			Set<Column> columns = cache.getColumns();
			for (Column col : columns) {
				Double val = record.getDoubleValue(col);
				cache.add(col, val);
			}
			return record;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.analytics.core.RecordProcessorTemplate#close()
	 */
	@Override
	public void close() {
		Set<Column> columns = cache.getColumns();
		for (Column col : columns) {
			Double stddev = cache.getResult(col);
			String key = AnalyticsHelper.getFunctionKey(col.getName(), (AnalyticsHelper.Function_STDDEV));
			getProcessorContext().put(key, stddev);
		}
	}
}
