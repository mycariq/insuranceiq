package com.cariq.toolkit.utils.analytics.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cariq.toolkit.utils.profile.ProfilePoint;

class ProcessorCacheForMean {
	static class ValueRecord {
		Double total = new Double(0.0);
		int count = 0;

		void add(double value) {
			if (value == 0)
				return;

			total += value;
			count++;
		}

		double getMean() {
			return count == 0 ? 0.0 : total / count;
		}
	}

	Map<Column, ValueRecord> meanValues = new HashMap<Column, ValueRecord>();

	Set<Column> getColumns() {
		return meanValues.keySet();
	}

	ProcessorCacheForMean(Column column) {
		meanValues.put(column, new ValueRecord());
	}

	ProcessorCacheForMean(List<Column> columns) {
		for (Column column : columns) {
			meanValues.put(column, new ValueRecord());
		}
	}

	void add(Column col, Double value) {
		if (value == null)
			return;

		ValueRecord record = meanValues.get(col);
		record.add(value);
		meanValues.put(col, record); // this should be unnecessary
	}

	Double getResult(Column col) {
		return meanValues.get(col).getMean();
	}
}

public class MeanCalculator extends RecordProcessorTemplate {
	ProcessorCacheForMean cache;
	public static final String COLUMN_LIST = "COLUMN_LIST";
	public static final String COLUMN = "COLUMN";

	// public MeanCalculator(AnalyticsContext processorContext,
	// List<Column> columns) {
	// super(processorContext);
	// cache = new ProcessorCacheForMean(columns);
	// }

	public MeanCalculator(AnalyticsContext processorContext) {
		super(processorContext);
		{
			Object columnList = processorContext.getParamValue(this.getClass(), COLUMN_LIST);
			if (columnList != null) {
				cache = new ProcessorCacheForMean((List<Column>) columnList);
				return;
			}
		}
		{ // isolate variable space

			Object column = processorContext.getParamValue(this.getClass(), COLUMN);
			if (column != null) {
				cache = new ProcessorCacheForMean((Column) column);
				return;
			}
		}
	}

	public MeanCalculator(AnalyticsContext processorContext, Column column) {
		super(processorContext);

		if (column != null)
			cache = new ProcessorCacheForMean(column);
	}

	@Override
	protected Record doProcess(Record record) {
		try (ProfilePoint _MeanCalculator_doProcess = ProfilePoint.profileAction("ProfAction_MeanCalculator_doProcess")) {
			Set<Column> columns = cache.getColumns();
			for (Column col : columns) {
				Double val = record.getDoubleValue(col);
				cache.add(col, val);
			}
			return record;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.analytics.core.RecordProcessorTemplate#close()
	 */
	@Override
	public void close() {
		Set<Column> columns = cache.getColumns();
		for (Column col : columns) {
			Double mean = cache.getResult(col);

			String key = AnalyticsHelper.getFunctionKey(col.getName(), (AnalyticsHelper.Function_MEAN));
			getProcessorContext().put(key, mean);
		}
	}
}
