package com.cariq.toolkit.utils.analytics.core;

import java.io.IOException;
import java.util.List;

import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

public class SkimData extends RecordProcessorTemplate {
	public static final String COLUMN_LIST = "COLUMN_LIST";
	public static final String VALIDATOR = "VALIDATOR";

	private List columns;
	private RecordValidator validator;

	public SkimData(AnalyticsContext context) {
		super(context);

		columns = processorContext.getParamValue(this.getClass(), COLUMN_LIST, List.class);
		validator = processorContext.getParamValue(this.getClass(), VALIDATOR, RecordValidator.class);

		Utils.checkNotNull(columns, this.getClass().getName() + ": columns should not be null");
		Utils.checkNotNull(validator, this.getClass().getName() + ": validator should not be null");
	}

	public SkimData(AnalyticsContext context, List<Column> columns, RecordValidator validator) {
		super(context);

		this.columns = columns;
		this.validator = validator;

		Utils.checkNotNull(columns, this.getClass().getName() + ": columns should not be null");
		Utils.checkNotNull(validator, this.getClass().getName() + ": validator should not be null");
	}
	/**
	 * Keep only the relevant columns - don't get rest
	 */
	@Override
	public void init() throws IOException {
		// remove existing columns from Output Table
		TableWriterStream stream = (TableWriterStream) getProcessorContext().get(AnalyticsHelper.TABLE_WRITER_STREAM);
		if (stream != null)
			stream.removeAllColumns();

		for (Object col : columns) {
			super.addColumn((Column) col);
		}
	}

	@Override
	public void close() {
	}

	/**
	 * Copy the contents of the record for relevant columns in the output Later,
	 * investigate specific values (e.g. speed etc.)
	 */
	@Override
	protected Record doProcess(Record record) {
		try (ProfilePoint _skimdata_doProcess = ProfilePoint.profileAction("ProfAction_skimdata_doProcess")) {
			if (!validator.validate(record))
				return null;
			Record r = new Record(columns, record.getValues(columns));
			// Check for invalid values - if invalid value, throw exception
			// InvalidRecordException
			return r;
		}
	}
}
