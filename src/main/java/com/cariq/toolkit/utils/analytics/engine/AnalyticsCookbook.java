package com.cariq.toolkit.utils.analytics.engine;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.core.io.ClassPathResource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cariq.toolkit.utils.ObjectBuilder;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * AnalyticsEngine supplies Pre-cooked Analytics Recipes.
 * 
 * @author Abhijit
 *
 */

public class AnalyticsCookbook implements ObjectBuilder<AnalyticsRecipe> {
	static String ANALYTICS_COOKBOOK_FILE = "AnalyticsCookbook.xml";

	private static AnalyticsCookbook instance = null;

	Map<String, AnalyticsRecipe> recipeMap = new HashMap<String, AnalyticsRecipe>();

	private AnalyticsCookbook() throws Exception {
		try (ProfilePoint _AnalyticsCookbook = ProfilePoint.profileActivity("ProfAction_AnalyticsCookbook")) {
			// load ScoringEngie from resource
			File analyticsCookbookFile = new ClassPathResource(ANALYTICS_COOKBOOK_FILE).getFile();
			// populate ScoringDefinitions
			load(analyticsCookbookFile);
		}
	}

	private void validateName(Node element, String name) throws Exception {
		if (element.getNodeName().equals("name"))
			throw new Exception("Invalid Root Element of AnalyticsCookbook: " + element.getNodeName());
	}

	private void load(File cookbookFile) throws Exception {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(cookbookFile);

		// optional, but recommended
		// read this -
		// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
		doc.getDocumentElement().normalize();

		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		Element cookbook = doc.getDocumentElement();
		validateName(cookbook, "AnalyticsCookbook");

		// Load Recipes
		NodeList recipies = cookbook.getElementsByTagName("AnalyticsRecipe");
		for (int nodeNo = 0; nodeNo < recipies.getLength(); nodeNo++) {
			Node def = recipies.item(nodeNo);
			AnalyticsRecipe recipe = new AnalyticsRecipe().configure("XML_NODE", def).build();
			add(recipe.getName(), recipe);
		}
		
		this.build();
	}

	public static AnalyticsCookbook getInstance() throws Exception {
		if (null == instance)
			instance = new AnalyticsCookbook();

		return instance;
	}

	public AnalyticsRecipe getRecipe(String name) {
		return recipeMap.get(name);
	}

	@Override
	public AnalyticsCookbook configure(String setting, Object value) {
		throw new NotImplementedException("Method AnalyticsCookbook.configure is not implemented!");
	}

	@Override
	public AnalyticsCookbook add(AnalyticsRecipe value) {
		recipeMap.put(value.getName(), value);
		return this;	}

	@Override
	public AnalyticsCookbook add(String parameter, AnalyticsRecipe value) {
		recipeMap.put(parameter, value);
		return this;
	}

	@Override
	public AnalyticsCookbook build() {
		return this;
	}
}
