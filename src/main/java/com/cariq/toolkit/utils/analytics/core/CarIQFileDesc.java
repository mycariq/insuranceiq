package com.cariq.toolkit.utils.analytics.core;

import java.io.File;

public class CarIQFileDesc implements CarIQFile {
	String fileName, filePath;

	public CarIQFileDesc(String fileName, String filePath) {
		super();
		this.fileName = fileName;
		this.filePath = filePath;
	}

	/**
	 * @return the fileName
	 */
	public String getName() {
		return fileName;
	}

	/**
	 * @return the filePath
	 */
	public String getPath() {
		return filePath;
	}

	@Override
	public long getSize() {
		return getFile().length();
	}

	@Override
	public File getFile() {
		return new File(getPath());
	}

	@Override
	public boolean exists() {
		return getFile().exists();
	}
}
