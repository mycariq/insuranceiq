package com.cariq.toolkit.utils.analytics.core;

import java.util.Date;

import com.cariq.toolkit.utils.AverageAggregator;
import com.cariq.toolkit.utils.CIQOrderedMap;
import com.cariq.toolkit.utils.Utils;

/**
 * This processor calculates
 * Max Speed
 * Average Speed
 * Total Distance
 * percentage Time spent in speed ranges of 10, 35, 70, 100, ..
 * percentage Time spent in gears 1, 2, 3, 4, 5, 6 
 *
 */
public class SummaryDataCalculatorSample extends RecordProcessorTemplate {
	static class TimePercentage {
		double timeSeconds;
		double percentage;
		public TimePercentage(double timeSeconds, double percentage) {
			super();
			this.timeSeconds = timeSeconds;
			this.percentage = percentage;
		}
	}
	
	/**
	 * This class captures the TimeSpent ranges
	 * @author Abhijit
	 *
	 */
	static class TimeSpentInRangeMap {
		CIQOrderedMap<Integer, Integer> timeMap = new CIQOrderedMap<Integer, Integer>();

		public TimeSpentInRangeMap(Integer[] ranges) {
			super();
			for (Integer range : ranges) {
				timeMap.put(range, 0);
			}
		}
		
		public void add(int value, int timeSpent) {
			boolean added = false;
			for (Integer key : timeMap.keyList()) {
				if (value <= key) {
					int newValue = timeMap.get(key) + timeSpent;
					timeMap.put(key, newValue);
					added = true;
					break;
				}
			}
			
			// No Range found? Add to last one
			if (!added) {
				Integer key = timeMap.lastKey();
				int newValue = timeMap.get(key) + timeSpent;
				timeMap.put(key, newValue);
			}
		}
		
		int getTotalValue() {
			int retval = 0;
			for (Integer key : timeMap.keyList())
				retval += timeMap.get(key);
			
			return retval;
		}
		
		CIQOrderedMap<Integer, TimePercentage> getPecentages() {
			CIQOrderedMap<Integer, TimePercentage> retval = new CIQOrderedMap<Integer, TimePercentage>();
			double total = getTotalValue();
			for (Integer key : timeMap.keyList()) {
				double timeVal = timeMap.get(key);
				retval.put(key, new TimePercentage(timeVal, (100*timeVal/total)));
			}
				
			return retval;
		}
	}

	private static final double PERCENT_THRESHOLD = 3;
	
	// speed Ranges
	TimeSpentInRangeMap speedRangeMap = new TimeSpentInRangeMap(new Integer[] {0, 10, 35, 70, 100, 500});
	TimeSpentInRangeMap geardRangeMap = new TimeSpentInRangeMap(new Integer[] {1, 2, 3, 4, 5, 6});
	double totalDistance = 0.0;
	Record previous;
	AverageAggregator batteryAggregator = new AverageAggregator();
	
	public SummaryDataCalculatorSample(AnalyticsContext processorContext) {
		super(processorContext);
	}

	@Override
	protected Record doProcess(Record record) {
		calculateDistance(record);
		checkSpeedRange(record);
	//	checkGearRange(record);
		
		aggregateBatteryVoltage(record);

		previous = record;
		
		return record;
	}
	
	private void aggregateBatteryVoltage(Record record) {
		Double batteryVoltage = record.getDoubleValue("voltage");
		if (batteryVoltage != null && batteryVoltage > 0)
			batteryAggregator.add(null, batteryVoltage);
	}

//	private void checkGearRange(Record record) {
//		if (previous == null)
//			return;
//		
//		Double speed = record.getDoubleValue(DriveReportHelper.SPEED);
//		Integer gear = record.getIntegerValue(DriveReportHelper.GEAR);
//		Double rpm = record.getDoubleValue(DriveReportHelper.RPM);
//		if (gear == null || speed == null || speed <= 0 || rpm == null || rpm < DriveReportHelper.MINIMUM_RPM_THRESHOLD)
//			return;
//
//		int timeDurationDiff = getTimeDurationSincePrevious(record);
//		// if there is a big gap in the records, don't use it
//		if (timeDurationDiff > DriveReportHelper.MINIMUM_TIME_THRESHOLD_FOR_CONTINUATION)
//			return;
//		
//		geardRangeMap.add(gear, getTimeDurationSincePrevious(record));
//	}
	
	int getTimeDurationSincePrevious(Record record) {
		// Check timestamps are not identical but close enough
		Date currentTime = record.getTimeStamp();
		Date previousTime = previous.getTimeStamp();;
		return (int) Utils.getDiffInSeconds(currentTime, previousTime);
	}

	private void checkSpeedRange(Record record) {
//		if (previous == null)
//			return;
//		
//		Double speed = record.getDoubleValue("speed");
//		Double rpm = record.getDoubleValue("rpm");
//		if (rpm == null || rpm < DriveReportHelper.MINIMUM_RPM_THRESHOLD) // car is off
//			return;
//			
//		if (speed == null || speed < 0.0) // invalid
//			return;
//		
//		int duration = (int)getTimeDurationSincePrevious(record);
//		if (duration < DriveReportHelper.MINIMUM_TIME_THRESHOLD_FOR_CONTINUATION) // if within 15 seconds, add it - else, skip it
//			speedRangeMap.add((int)(double)speed, duration);
	}

	private void calculateDistance(Record record) {
		Double dist = record.getDoubleValue("distance");
		if (dist != null && dist > 0)
			this.totalDistance += dist;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.analytics.core.RecordProcessorTemplate#close()
	 */
	@Override
	public void close() {
//		CIQOrderedMap<Integer, TimePercentage> speedRanges = speedRangeMap.getPecentages();
//		CIQOrderedMap<Integer, TimePercentage> gearRanges = geardRangeMap.getPecentages();
//		
////		public static final String GEAR1_PERCENT = "gear1_percent";
////		public static final String GEAR2_MINUTES = "gear2_minutes";
////		public static final String GEAR2_PERCENT = "gear2_percent";
////		public static final String SPEED30_MINUTES = "speed30_minutes";
////		public static final String SPEED30_PERCENT = "speed30_percent";
//
//		
//		for (Integer speedRange : speedRanges.keyList()) {
//			TimePercentage tp = speedRanges.get(speedRange);
//			DriveReportHelper.logInformation(getProcessorContext(), "speed"+speedRange+"_minutes", (int)tp.timeSeconds/60);
//			DriveReportHelper.logInformation(getProcessorContext(), "speed"+speedRange+"_percent", (int)tp.percentage);
//		}
//	
//		for (Integer gearRange : gearRanges.keyList()) {
//			TimePercentage tp = gearRanges.get(gearRange);
//
//			if (tp.percentage < PERCENT_THRESHOLD)
//				tp.percentage = 0.0;
//			
//			DriveReportHelper.logInformation(getProcessorContext(), "gear"+gearRange+"_minutes", (int)tp.timeSeconds/60);
//			DriveReportHelper.logInformation(getProcessorContext(), "gear"+gearRange+"_percent", (int)tp.percentage);
//		}
//		
//		DriveReportHelper.getDriveReportModel(getProcessorContext()).add(DriveReportModel.AVG_BATTERY, String.valueOf(Utils.formatDouble(batteryAggregator.getValue())));
	}
}
