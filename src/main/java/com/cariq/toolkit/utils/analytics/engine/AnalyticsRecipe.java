package com.cariq.toolkit.utils.analytics.engine;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cariq.toolkit.utils.ObjectBuilder;

class RecordProcessorRecipe implements ObjectBuilder {
	static final String NODE_NAME = "RecordProcessor";
	String className;
	String description;

	public String getClassName() {
		return className;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public RecordProcessorRecipe configure(String setting, Object value) {
		// check the name
		if (!setting.equals("XML_NODE"))
			return this;

		Node recordProcessorNode = (Node) value;
		// check the name
		if (!recordProcessorNode.getNodeName().equals(NODE_NAME))
			throw new RuntimeException(
					"Invalid XML! Nodename = " + recordProcessorNode.getNodeName() + ", Expected =  " + NODE_NAME);

		Element recordProcessorElement = (Element) recordProcessorNode;

		className = recordProcessorElement.getAttribute("Class");
		description = recordProcessorElement.getAttribute("Description");
		return this;
	}

	@Override
	public RecordProcessorRecipe add(Object value) {
		throw new NotImplementedException("Method RecordProcessorRecipe.add is not implemented!");
	}

	@Override
	public RecordProcessorRecipe add(String parameter, Object value) {
		throw new NotImplementedException("Method RecordProcessorRecipe.add is not implemented!");
	}

	@Override
	public RecordProcessorRecipe build() {
		return this;
	}
}

/**
 * Table Processor for analytics
 * @author Abhijit
 *
 */
class TableProcessorRecipe implements ObjectBuilder<RecordProcessorRecipe> {
	final static String NODE_NAME = "TableProcessor";
	String description;
	List<RecordProcessorRecipe> recordProcessorRecipes = new ArrayList<RecordProcessorRecipe>();

	public String getDescription() {
		return description;
	}

	public List<RecordProcessorRecipe> getRecordProcessorRecipes() {
		return recordProcessorRecipes;
	}

	@Override
	public TableProcessorRecipe configure(String setting, Object value) {
		if (!setting.equals("XML_NODE"))
			return this;

		Node tableProcessorNode = (Node) value;
		// check the name
		if (!tableProcessorNode.getNodeName().equals(NODE_NAME))
			throw new RuntimeException(
					"Invalid XML! Nodename = " + tableProcessorNode.getNodeName() + ", Expected =  " + NODE_NAME);

		Element tableProcessorElement = (Element) tableProcessorNode;

		description = tableProcessorElement.getAttribute("Description");

		// Load Record Processors
		NodeList recordProcessors = tableProcessorElement.getElementsByTagName("RecordProcessor");
		for (int nodeNo = 0; nodeNo < recordProcessors.getLength(); nodeNo++) {
			Node def = recordProcessors.item(nodeNo);
			add(new RecordProcessorRecipe().configure("XML_NODE", def).build());
		}

		return this;
	}

	@Override
	public TableProcessorRecipe add(RecordProcessorRecipe value) {
		recordProcessorRecipes.add(value);
		return this;
	}

	@Override
	public TableProcessorRecipe add(String parameter, RecordProcessorRecipe value) {
		recordProcessorRecipes.add(value);
		return this;
	}

	@Override
	public TableProcessorRecipe build() {
		return this;
	}
}

public class AnalyticsRecipe implements ObjectBuilder<TableProcessorRecipe> {
	String NODE_NAME = "AnalyticsRecipe";

	public String getName() {
		return name;
	}

	public List<TableProcessorRecipe> getTableProcessorRecipes() {
		return tableProcessorRecipes;
	}

	String name;
	List<TableProcessorRecipe> tableProcessorRecipes = new ArrayList<TableProcessorRecipe>();

	@Override
	public AnalyticsRecipe configure(String setting, Object value) {
		if (!setting.equals("XML_NODE"))
			return this;

		Node recipeNode = (Node) value;
		// check the name
		if (!recipeNode.getNodeName().equals(NODE_NAME))
			throw new RuntimeException(
					"Invalid XML! Nodename = " + recipeNode.getNodeName() + ", Expected =  " + NODE_NAME);

		Element recipeElement = (Element) recipeNode;

		name = recipeElement.getAttribute("Name");

		// Load Table Processors
		NodeList tableProcessors = recipeElement.getElementsByTagName("TableProcessor");
		for (int nodeNo = 0; nodeNo < tableProcessors.getLength(); nodeNo++) {
			Node def = tableProcessors.item(nodeNo);
			add(new TableProcessorRecipe().configure("XML_NODE", def).build());
		}

		return this;
	}

	@Override
	public AnalyticsRecipe add(TableProcessorRecipe value) {
		tableProcessorRecipes.add(value);
		return this;
	}

	@Override
	public AnalyticsRecipe add(String parameter, TableProcessorRecipe value) {
		tableProcessorRecipes.add(value);
		return this;
	}

	@Override
	public AnalyticsRecipe build() {
		return this;
	}
}

