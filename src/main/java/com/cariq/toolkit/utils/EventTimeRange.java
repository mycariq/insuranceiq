package com.cariq.toolkit.utils;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.springframework.format.annotation.DateTimeFormat;

public class EventTimeRange {

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private DateTime eventDate;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private DateTime currentDate;

	public EventTimeRange(DateTime eventDate, DateTime currentDate) {
		this.eventDate = eventDate;
		this.currentDate = currentDate;
	}

	public EventTimeRange(Date expiryDate, DateTime currentDate) {
		this(new DateTime(expiryDate), new DateTime(currentDate));
	}

	/**
	 * Check for input date is inside daysRange.
	 * 
	 * @param daysRange
	 * @return true if yes
	 */
	public boolean isDateInRange(int daysRange) {
		return Math.abs(Days.daysBetween(currentDate, eventDate).getDays()) <= daysRange;
	}

	/**
	 * Check eventDate and currentDate difference is equal to input days.
	 * 
	 * @param days
	 * @return true if yes.
	 */
	public boolean isDaysDiffEqual(int days) {
		return Math.abs(Days.daysBetween(currentDate, eventDate).getDays()) == days;
	}

	/**
	 * Check for input date is inside hours range.
	 * 
	 * @param hours
	 * @return true if yes
	 */
	public boolean isHourInRange(int hours) {
		return Math.abs(Hours.hoursBetween(currentDate, eventDate).getHours()) <= hours;
	}
}
