package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
public class ResponseJson {

	/**
     */
	private List<String> messages;

	public ResponseJson(String message) {
		this.messages = new ArrayList<String>();
		this.messages.add(message);
	}

	public ResponseJson(List<String> messages) {
		this.messages = messages;
	}

	public ResponseJson() {
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResponseJson [messages=" + messages + "]";
	}
}
