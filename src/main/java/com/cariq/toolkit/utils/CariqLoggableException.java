package com.cariq.toolkit.utils;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.jboss.logging.Logger;

/**
 * This class represents Loggable Exceptions from CarIQ
 * Level of logging can be controlled through Log4j
 * @author HVN
 *
 */
public abstract class CariqLoggableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public CariqLoggableException() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CariqLoggableException(String message, Throwable cause) {
		super(message, cause);
	}

	/** 
	 * Return logger for the current Exception
	 * @return
	 */
	protected abstract Logger getLogger();
	
	/**
	 * @param message
	 */
	public CariqLoggableException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
	private void doLog(Logger logger, Throwable e) {
		logger.error(e.getMessage());
		if (e.getCause() != null)
			doLog(logger, e.getCause());
	}
	
	public void logException() {
		doLog(getLogger(), this);
		getLogger().error(ExceptionUtils.getFullStackTrace(this));
	}

}
