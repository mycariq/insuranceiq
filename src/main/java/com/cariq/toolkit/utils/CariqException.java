package com.cariq.toolkit.utils;

import java.util.List;

public abstract class CariqException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public abstract List<String> getMsg();

	/* (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		List<String> msgs = getMsg();
		if (null == msgs || msgs.isEmpty())
			return "Internal Error!";
		
		StringBuilder message = new StringBuilder();
		for (String msg : msgs) {
			message.append(msg).append(";");
		}
		
		return message.toString();
	}
}
