package com.cariq.toolkit.utils;

import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class SimpleAttributeMap extends HashMap<String, Object> {
	public <T> T getValue(String attr, Class<T> cls) {
		try {
			return Utils.downCast(cls, get(attr));
		} catch (Exception e) {
			return null; // callers beware
		}
	}
	
	public SimpleAttributeMap(String jsonString) throws Exception {
	     JSONObject jObject = new JSONObject(jsonString);
	        Iterator<?> keys = jObject.keys();

	        while( keys.hasNext() ){
	            String key = (String)keys.next();
	            String value = jObject.getString(key); 
	            put(key, value);
	        }
	}

	public SimpleAttributeMap() {
		super();
	}
}
