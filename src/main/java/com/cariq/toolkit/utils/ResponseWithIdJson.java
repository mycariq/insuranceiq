package com.cariq.toolkit.utils;

import java.util.Arrays;
import java.util.List;

import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
public class ResponseWithIdJson {
	/**
	 * @param messages
	 * @param id
	 */
	public ResponseWithIdJson(String id, List<String> messages) {
		this.messages = messages;
		this.id = id;
	}

	/**
	 * @param messages
	 * @param id
	 */

	public ResponseWithIdJson(String id, String message) {
		this.messages = Arrays.asList(message);
		this.id = id;
	}

	/**
	 * 
	 */
	public ResponseWithIdJson() {
		this("0", "Unknown");
	}

	/**
     */
    private List<String> messages;

    /**
     */
    private String id;
 
    
}
