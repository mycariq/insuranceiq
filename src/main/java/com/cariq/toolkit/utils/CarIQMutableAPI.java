package com.cariq.toolkit.utils;

/**
 * All CarIQ APIs must be called through this class. It's a RAII class to
 * capture entry and exit conditions. It will be enhanced over time. To start
 * with, it contains logging and ProfilePoint
 * 
 * @author HVN
 *
 *         == Eclipse CarIQMutableAPI macro == 
 *         try (CarIQMutableAPI _${myvar} = new
 *         CarIQMutableAPI("CARIQMutable_${myvar}")) { ${line_selection}${cursor} }
 */

public class CarIQMutableAPI implements AutoCloseable {
	CarIQAPI api;

	public CarIQMutableAPI(String apiName, Object inputJSON) {
		api = new CarIQAPI(apiName);

		if (null != apiName)
			Utils.logAction(CarIQToolkitHelper.logger, apiName, api.getUserName(), inputJSON);

		if (null != inputJSON)
			CarIQToolkitHelper.logger.getLogger("CarIQMutableAPI").debug(apiName + ", inputJSON:" + Utils.getJSonString(inputJSON));

	}

	@Override
	public void close() {
		try {
			if (api != null)
				api.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
