package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.List;

public class ResponseHelper {
	
	/**
	 * Set response message
	 * @param msg
	 * @return
	 */
	public static ResponseJson setResponse(String msg) {
		ResponseJson responseJson = new ResponseJson();
		List<String> responseStrings = new ArrayList<String>();
		responseStrings .add(msg);
		responseJson .setMessages(responseStrings);
		return responseJson;
	}
	
	/**
	 * Set response message with Id
	 * @param id
	 * @param msg
	 * @return
	 */
	public static ResponseWithIdJson setIdInResponse(long id, String msg){
		ResponseWithIdJson responseWithIdJson = new ResponseWithIdJson();
		List<String> response = new ArrayList<String>(1);
		response.add(msg);
		responseWithIdJson.setId(id+"");
		responseWithIdJson.setMessages(response);
		return responseWithIdJson;
	}
	
}