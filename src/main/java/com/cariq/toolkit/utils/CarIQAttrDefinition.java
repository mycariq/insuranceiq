package com.cariq.toolkit.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.cariq.toolkit.utils.profile.ProfilePoint;

public class CarIQAttrDefinition {
	String name;
	String description;
	boolean isOptional;
	public CarIQAttrDefinition(String name, String description, boolean isOptional) {
		super();
		this.name = name;
		this.description = description;
		this.isOptional = isOptional;
	}
	public CarIQAttrDefinition(String name, boolean isOptional) {
		this(name, name, isOptional);
	}
	
	public CarIQAttrDefinition(String name, String description) {
		this(name, description, false);
	}
	
	public CarIQAttrDefinition(String name) {
		this(name, name, false);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isOptional() {
		return isOptional;
	}
	public void setOptional(boolean isOptional) {
		this.isOptional = isOptional;
	}
	
	/**
	 * Check whether passed json has all the required attributes
	 * @param json
	 */
	public static void validateJSON(GenericJSON json, List<CarIQAttrDefinition> attributeDefs) {
		try (ProfilePoint _validateJSON = ProfilePoint.profileAction("ProfAction_validateJSON")) {
			Set<String> expectedKeys = new HashSet<String>();
			for (CarIQAttrDefinition attr : attributeDefs) {
				if (attr.isOptional() == false)
					expectedKeys.add(attr.getName());
			}
			Set<String> inputKeys = new HashSet<String>();
			inputKeys.addAll(json.keySet());
			expectedKeys.removeAll(inputKeys);
			if (expectedKeys.isEmpty())
				return;
			
			String missingKeys = Utils.toCSV(expectedKeys);
			throw new RuntimeException("Missing Attributes: " + missingKeys);
		}
	}

}
