package com.cariq.toolkit.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;

import com.cariq.toolkit.utils.analytics.core.CarIQFile;
import com.cariq.toolkit.utils.analytics.core.CarIQFileDesc;

/**
 * The Class CompositeReport.
 * It holds files - either single file or, a zip
 * @TODO make it into an interface
 */
public class CompositeReport implements AutoCloseable, CarIQFile {

	/**
	 * The Enum RendererType.
	 */
	public enum RendererType {

		/** The csv. */
		CSV,
		/** The html. */
		HTML,
		/** The pdf. */
		PDF
	}

	/** The local file. */
	// private CarIQZipFile zipFile;
	private String zipFilePath;
	private int fileCount = 0;
	private CarIQFile cariqFile; // can be a zipFile

	/** The description. */
	private String description;

	/** The time stamp. */
	private Date timeStamp;

	/**
	 * Instantiates a new composite report.
	 *
	 * @param path
	 *            the path
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public CompositeReport(String path) throws FileNotFoundException {
		zipFilePath = path;
		this.timeStamp = new Date();
//		this.zipFile = new CarIQZipFile(path);
	} 

	/**
	 * Add file to report. If only 1 file is added, keep it as the file, 
	 * if more than one are added, make a zip file from it
	 * @param filePath
	 * @param zipEntryName
	 * @throws IOException
	 */
	public CompositeReport addFile(String filePath, String zipEntryName) throws IOException {
		if (fileCount == 0) {
			cariqFile = new CarIQFileDesc(zipEntryName, filePath);
			fileCount++;
			return this;
		}
		
		if (fileCount == 1) {
			// make a zip file
			convertToZipFile();
		}
		
		// add to zipFile
		getZipFile().add(zipEntryName, filePath);
		fileCount++;
		
		return this;
	}
	

	private CarIQZipFile getZipFile() {
		if (cariqFile instanceof CarIQZipFile) // may through cast class exception
			return (CarIQZipFile) cariqFile;
		
		return null;	
	}

	/**
	 * Make a zip file from the file
	 * @throws FileNotFoundException
	 */
	private void convertToZipFile() throws FileNotFoundException {
		if (null != getZipFile()) // already a zip file
			return;
			
		CarIQZipFile zipFile = new CarIQZipFile(zipFilePath);
		if (cariqFile != null)
			zipFile.add(cariqFile.getName(), cariqFile.getPath());
		
		cariqFile = zipFile;
	}

	public CompositeReport addFiles(List<CarIQFileDesc> files) throws IOException {
		if (files == null || files.isEmpty())
			return this;
		
		if (files.size() == 1) {
			CarIQFileDesc fileDesc = files.get(0);
			addFile(fileDesc.getPath(), fileDesc.getName());
			return  this;
		}
		
		convertToZipFile();
		for (CarIQFileDesc file : files) {
			getZipFile().add(file.getName(), file.getPath());		
		}		
		
		return this;
	}
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		File f = getFile();
		return f.getName();
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the time stamp.
	 *
	 * @return the time stamp
	 */
	public Date getTimeStamp() {
		return timeStamp;
	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public long getSize() {
		return cariqFile.getSize();
		//return zipFile.getSize();
	}

	/**
	 * Gets the local file.
	 *
	 * @return the local file
	 */
	public File getFile() {
		return cariqFile.getFile();
		// return new File(zipFile.getFilePath());
	}

	/**
	 * Delete local file.
	 */
	public void deleteLocalFile() {
		if (getZipFile() != null)
			getZipFile().delete();
	}

	/**
	 * Adds the report.
	 *
	 * @param name
	 *            the name
	 * @param type
	 *            the type
	 * @return the report renderer
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public ReportRenderer addReport(String name, RendererType type) throws IOException {
		convertToZipFile();
		getZipFile().getWriter().putNextEntry(new ZipEntry(name));
		if (RendererType.CSV == type)
			return new CSVRenderer(getZipFile().getWriter());
		if (RendererType.HTML == type)
			return new HTMLRenderer(getZipFile().getWriter());
		if (RendererType.PDF == type)
			return new PDFRenderer(getZipFile().getWriter());
		throw new RuntimeException("Renderer is not present");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.AutoCloseable#close()
	 */
	public void close() throws IOException {
		getZipFile().build();
	}

	/**
	 * Gets the path.
	 *
	 * @return the path
	 */
	public String getPath() {
		return cariqFile.getPath();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CompositeReport [localFile=" + cariqFile + ", description="
				+ description + ", timeStamp=" + timeStamp + "]";
	}
	
	/**
	 * Build, Seal the report before shipping
	 */
	public void build() {
		CarIQZipFile zipfile = getZipFile();
		
		if (zipfile != null)
			zipfile.build();
	}

	@Override
	public boolean exists() {
		return getFile().exists();
	}
}
