package com.cariq.toolkit.utils;

public interface JSONable<IN, OUT, OUT_LIGHT> {
	// Convert to JSON
	OUT toJSON();
	
	// Construct from JSON
	void fromJSON(IN json);
	
	// Output lightweight JSON
	OUT_LIGHT toLightJSON();
}