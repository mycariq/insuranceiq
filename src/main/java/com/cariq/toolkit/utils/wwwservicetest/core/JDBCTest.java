package com.cariq.toolkit.utils.wwwservicetest.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.utils.wwwservice.core.JDBCQuery.Results;
import com.cariq.toolkit.utils.wwwservice.core.JDBCQuery.SelectParams;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
public class JDBCTest {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/prod_cariq_1apr";

	// Database credentials
	static final String USER = "root";
	static final String PASS = "admin";

	@Test
	public void testConnect() {
		Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = "select first_name, last_name, email, cell_number from cariq_user";
			ResultSet rs = stmt.executeQuery(sql);

			// STEP 5: Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				String first_name = rs.getString("first_name");
				String last_name = rs.getString("last_name");
				String email = rs.getString("email");
				String cell_number = rs.getString("cell_number");

				// Display values
				System.out.print("first_name: " + first_name);
				System.out.print(", last_name: " + last_name);
				System.out.print(", email: " + email);
				System.out.println(", cell_number: " + cell_number);
			}
			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			}// nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}// end finally try
		}// end try
		System.out.println("Goodbye!");
	}
	
	@Test
	@Transactional
	public void testLocalHibernateQuery() {
		EntityManager em = null; // Pid.entityManager(); To be resolved
		SelectParams params = new SelectParams(new String[] { "first_name",
				"last_name", "email", "cell_number" });
		String sql = "select " + params.getSelectString() + " from cariq_user";
		Query query = em.createNativeQuery(sql);

		@SuppressWarnings("unchecked")
		List<Object[]> res = query.getResultList();
		// STEP 5: Extract data from result set
		for (Object[] objects : res) {
			Results rs = new Results(objects, params);
			String first_name = rs.getString("first_name");
			String last_name = rs.getString("last_name");
			String email = rs.getString("email");
			String cell_number = rs.getString("cell_number");

			// Display values
			System.out.print("first_name: " + first_name);
			System.out.print(", last_name: " + last_name);
			System.out.print(", email: " + email);
			System.out.println(", cell_number: " + cell_number);
		}

		System.out.println("Goodbye!");
	}
}
