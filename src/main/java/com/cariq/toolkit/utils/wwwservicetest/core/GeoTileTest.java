package com.cariq.toolkit.utils.wwwservicetest.core;

import org.junit.Test;

import com.cariq.toolkit.utils.wwwservice.core.GeoTile;

public class GeoTileTest {

	@Test
	public void testLatLongPackUnpack() {
		double deccanGymLat = 18.5184782;
		double deccanGymLong = 73.8406212;
		GeoTile deccan = new GeoTile(deccanGymLat, deccanGymLong);
		System.out.println("Deccan Lat = " + deccan.getLatitude() + " Long = "
				+ deccan.getLongitude());
	}

	@Test
	public void testDistance() {
		double deccanGymLat = 18.5184782, deccanGymLong = 73.8406212;
		double iitLat = 19.133611, iitLong = 72.9154213;

		GeoTile deccan = new GeoTile(deccanGymLat, deccanGymLong);
		GeoTile iit = new GeoTile(iitLat, iitLong);
		
		System.out.println("Areal Distance from Deccan Gymkhana to IIT = " + deccan.getDistanceFrom(iit) + "km.");
	}
	
	@Test
	public void testAmericanDistance() {
		double nashVilleLat = 36.1667, nashVilleLong = -86.7833;
		double bostonLat = 42.3601, bostonLong = -71.0589;

		GeoTile nashVille = new GeoTile(nashVilleLat, nashVilleLong);
		GeoTile boston = new GeoTile(bostonLat, bostonLong);
		
		System.out.println("Areal Distance from Deccan Gymkhana to IIT = " + nashVille.getDistanceFrom(boston) + "km.");
	}
	

}
