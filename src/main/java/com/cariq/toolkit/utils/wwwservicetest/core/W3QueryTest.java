package com.cariq.toolkit.utils.wwwservicetest.core;

import java.io.File;
import java.io.IOException;

import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cariq.toolkit.utils.service.W3Service;
import com.cariq.toolkit.utils.service.W3ServiceImpl;
import com.cariq.toolkit.utils.wwwservice.api.W3Query;
import com.cariq.toolkit.utils.wwwservice.api.W3Query.W3QueryJson;
import com.cariq.toolkit.utils.wwwservice.core.Function;
import com.cariq.toolkit.utils.wwwservice.core.GeoTile;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;
import com.cariq.toolkit.utils.wwwservice.core.TimeStamp;
import com.cariq.toolkit.utils.wwwservice.core.function.Average;
import com.cariq.toolkit.utils.wwwservice.core.function.Count;
import com.cariq.toolkit.utils.wwwservice.core.function.Max;
import com.cariq.toolkit.utils.wwwservice.core.function.Min;
import com.cariq.toolkit.utils.wwwservice.core.function.Value;
import com.fasterxml.jackson.core.JsonGenerationException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
public class W3QueryTest {
	static void outputJSons(String testName, Object requestJson, Object responseJson) {
		final String testPath = "D:\\temp\\w3\\testresults\\";
		String requestFile = testPath + testName + "_" + "RequestJson.txt";
		String responseFile = testPath + testName + "_" + "ResponseJson.txt";
		
		ObjectMapper mapper = new ObjectMapper();
		//mapper.setVisibility(JsonMethod.FIELD, Visibility.ANY);
		mapper.setVisibility(JsonMethod.FIELD, Visibility.ANY);
		// mapper.enable(SerializationFeature.INDENT_OUTPUT);

		 try
	      {
	         mapper.writeValue(new File(requestFile), requestJson);
	         mapper.writeValue(new File(responseFile), responseJson);
	      } catch (JsonGenerationException e)
	      {
	         e.printStackTrace();
	      } catch (JsonMappingException e)
	      {
	         e.printStackTrace();
	      } catch (IOException e)
	      {
	         e.printStackTrace();
	      }		// W3QueryResult result = query.execute();		
	}
	
	static void setQueryInterface(W3Query query) {
//		query.setQueryInterface(new JDBCQueryRaw());
	}
	
	/**
	 * Execute the query using JSON conversion to and from
	 * Output the jsons to file
	 * @param testName
	 * @param query
	 */
	static void executeQuery(String testName, W3Query query) {
		W3Service testService = new W3ServiceImpl();

		W3QueryJson request = query.toJson();
		Object returnJson =  testService.executeW3Query(request);
		System.out.println(returnJson);

//		Without workitem - execute directly
//		 W3QueryInterface queryif = new W3QueryAPIImpl();
//		 W3QueryResultJson response = queryif.executeQuery(request);
//			
//		 outputJSons(testName, request, response);
	}
	
	/**
	 * Simple Test to get average speed of car with id=40 We'll enhance with
	 * LatLong etc.
	 */
	@Test
	public void testAverageSpeedOfCarSimple() throws Exception {
		// select avg(speed) from pid where its_car = 40;
		final String testname = "AverageSpeedOfCarSimple";
		
		// Query the raw data
		W3Query query = new W3Query("raw");
		setQueryInterface(query);
		
		Function f = new Average(); // get Average
		f.addParam(new ParameterDef("speed")); // Average of speed
		f.addParam(new ParameterDef("rpm")); // Average of speed
		query.addFunction(f).addId(40);
		
		executeQuery(testname, query);
		
		// to make sure that the work item execution is complete fine
		Thread.sleep(2000000);
	}

	/**
	 * Get Max Speed and RPM for cars 11 and 6
	 */
	@Test
	public void testMaxSpeedOfCarsSimple() throws Exception{
		final String testname = "MaxSpeedOfCarsSimple";

		// select MAX(speed) from pid where its_car = 11 and 6;
		W3Query query = new W3Query("raw");
		setQueryInterface(query);
		Function f = new Max(); // get Max
		// Max Speed and rpm
		f.addParam(new ParameterDef("speed")).addParam(new ParameterDef("rpm"));

		// Add cars 11 and 6
		query.addFunction(f).addId(11);
		query.addFunction(f).addId(6);		
		
		executeQuery(testname, query);
		
		// to make sure that the work item execution is complete fine
		Thread.sleep(2000000);
	}

	@Test
	public void testAvgAndMaxSpeedPuneCityMarch()
			throws Exception {
		final String testname = "AvgAndMaxSpeedPuneCityMarch";

		
		W3Query query = new W3Query("raw");
		setQueryInterface(query);
		
		// City - no Highway - Pune city co-ordinates
		query.setRegion(new GeoTile(18.467684, 73.772458), new GeoTile(18.553144, 73.880948)); 
		query.setTimeRange(new TimeStamp("2015-03-01 00:00:00"), new TimeStamp(
				"2015-03-31 23:59:59")); // Month of March
		// No Specific cars - but all cars
		// query.addFunction(f).addId(11);
		// query.addFunction(f).addId(6);

		Function maxFunc = new Max(); // get Max
		Function avgFunc = new Average(); // get Average
		// Max Speed and rpm
		maxFunc.addParam(new ParameterDef("speed")).addParam(new ParameterDef("rpm")); // Max Speed
		avgFunc.addParam(new ParameterDef("speed")).addParam(new ParameterDef("rpm")); // AverageSpeed
		query.addFunction(maxFunc).addFunction(avgFunc);
		
		executeQuery(testname, query);
		
		// to make sure that the work item execution is complete fine
		Thread.sleep(2000000);
	}

	@Test
	public void testAvgAndMaxSpeedHighwayMarch()
			throws Exception {
		final String testname = "AvgAndMaxSpeedHighwayMarch";

		W3Query query = new W3Query("raw");
		setQueryInterface(query);
		
		// City - no Highway - Pune city co-ordinates
		query.setRegion(new GeoTile(18.699624, 73.648310), new GeoTile(18.715721, 73.705988)); 
		query.setTimeRange(new TimeStamp("2015-03-01 00:00:00"), new TimeStamp(
				"2015-03-31 23:59:59")); // Month of March
		// No Specific cars - but all cars
		// query.addFunction(f).addId(11);
		// query.addFunction(f).addId(6);

		Function maxFunc = new Max(); // get Max
		Function avgFunc = new Average(); // get Average
		// Max Speed and rpm
		maxFunc.addParam(new ParameterDef("speed")).addParam(new ParameterDef("rpm")); // Max Speed
		avgFunc.addParam(new ParameterDef("speed")).addParam(new ParameterDef("rpm")); // AverageSpeed
		query.addFunction(maxFunc).addFunction(avgFunc);
		
		executeQuery(testname, query);
		
		// to make sure that the work item execution is complete fine
		Thread.sleep(2000000);
	}

	
	@Test
	public void testGetLatLongForTripForCarid87March10()
			throws Exception {
		final String testname = "GetLatLongForTripForCarid87March10";

		W3Query query = new W3Query("raw");
		setQueryInterface(query);
		
		// Specific car
		query.addId(87); // Carid 87
		query.setTimeRange(new TimeStamp("2015-03-10 16:01:00"), new TimeStamp(
				"2015-03-10 16:02:59")); // 10th April

		Function valueFun = new Value(); // get Values of latlong
		
		
		// All LAT LONG values
		// valueFun.addParam(new ParameterDef("latitude")).addParam(new ParameterDef("longitude")); // Max Speed
		valueFun.addParam(new ParameterDef("Geotile", "(latitude*10000) << 32 + (longitude*10000)")); //geotile
		valueFun.addParam(new ParameterDef("Latitude", "latitude")); //geotile
		valueFun.addParam(new ParameterDef("Longitude", "longitude")); //geotile
		query.addFunction(valueFun);
		
		executeQuery(testname, query);
		
		
		// to make sure that the work item execution is complete fine
		Thread.sleep(2000000);
	}
	
	@Test
	public void testGetLatLongForTripForCarid87March10Long()
			throws Exception {
		final String testname = "GetLatLongForTripForCarid87March10Long";

		W3Query query = new W3Query("raw");
		setQueryInterface(query);
		
		// Specific car
		query.addId(87); // Carid 87
		query.setTimeRange(new TimeStamp("2015-03-10 00:00:00"), new TimeStamp(
				"2015-03-10 23:59:59")); // 10th April

		Function countFun = new Count(); // get Values of latlong
		
		
		// All LAT LONG values
		countFun.addParam(new ParameterDef("latitude")).addParam(new ParameterDef("longitude")); // Max Speed
		query.addFunction(countFun);
		
		executeQuery(testname, query);
		
		
		// to make sure that the work item execution is complete fine
		Thread.sleep(2000000);
	}
	
	@Test
	public void testAverageGearUsingExpressionParser() throws Exception {
		final String testname = "AverageGearUsingExpressionParser";

		// select avg(speed) from pid where its_car = 40;
		W3Query query = new W3Query("raw");
		setQueryInterface(query);
		Function f = new Average(); // get Average
		f.addParam(new ParameterDef("Gear Ratio", "3.123*rpm/speed")); // Average of speed
		f.addParam(new ParameterDef("Car Speed", "speed")); // Average of speed
		f.addParam(new ParameterDef("Car RPM", "rpm")); // Average of speed
		query.addFunction(f).addId(87);
		query.setTimeRange(new TimeStamp("2015-03-10 16:00:00"), new TimeStamp(
				"2015-03-10 16:04:59")); // 10th April
		
		executeQuery(testname, query);
		
		
		// to make sure that the work item execution is complete fine
		Thread.sleep(2000000);
	}
	
	
	@Test
	public void testEmptySet() throws Exception {
		final String testname = "EmptySet";

		// select avg(speed) from pid where its_car = 40;
		W3Query query = new W3Query("raw");
		setQueryInterface(query);
		Function f = new Average(); // get Average
		f.addParam(new ParameterDef("Gear Ratio", "rpm/speed")); // Average of speed
		f.addParam(new ParameterDef("Car Speed", "speed")); // Average of speed
		f.addParam(new ParameterDef("Car RPM", "rpm")); // Average of speed
		query.addFunction(f).addId(150);
		query.setTimeRange(new TimeStamp("2015-03-10 16:00:00"), new TimeStamp(
				"2015-03-10 16:04:59")); // 10th April
		
		executeQuery(testname, query);
		
		
		// to make sure that the work item execution is complete fine
		Thread.sleep(2000000);
	}

	@Test
	public void testMinMaxCombined() throws Exception {
		final String testname = "MinMaxCombined";

		// select avg(speed) from pid where its_car = 40;
		W3Query query = new W3Query("raw");
		setQueryInterface(query);
		Function maxFn = new Max(); // get Average
		Function minFn = new Min(); // get Average
		maxFn.addParam(new ParameterDef("Car Speed", "speed")); // Average of speed
		minFn.addParam(new ParameterDef("Car RPM", "rpm")); // Average of speed

		query.addFunction(maxFn).addFunction(minFn);
		query.addId(6);
		query.setTimeRange(new TimeStamp("2015-01-01 16:00:00"), new TimeStamp(
				"2015-03-31 16:04:59")); // 31st March
		
		executeQuery(testname, query);
		
		// to make sure that the work item execution is complete fine
		Thread.sleep(2000000);
	}

}
