/**
 * 
 */
package com.cariq.toolkit.utils.wwwservicetest.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import com.cariq.toolkit.utils.wwwservice.core.DataPoint;
import com.cariq.toolkit.utils.wwwservice.core.GeoTile;
import com.cariq.toolkit.utils.wwwservice.core.Identity;
import com.cariq.toolkit.utils.wwwservice.core.JDBCQuery.SelectParams;
import com.cariq.toolkit.utils.wwwservice.core.Parameter;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;
import com.cariq.toolkit.utils.wwwservice.core.QueryInterface;
import com.cariq.toolkit.utils.wwwservice.core.QueryRangeRestriction;
import com.cariq.toolkit.utils.wwwservice.core.QueryResult;
import com.cariq.toolkit.utils.wwwservice.core.Region;
import com.cariq.toolkit.utils.wwwservice.core.TimeRange;
import com.cariq.toolkit.utils.wwwservice.core.TimeStamp;
import com.cariq.toolkit.utils.wwwservice.core.Tuple;

/**
 * @author HVN
 *
 */
public class JDBCQueryTest implements QueryInterface {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/prod_cariq_1apr";

	// Database credentials
	static final String USER = "root";
	static final String PASS = "admin";

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hvn.wwwservice.core.QueryInterface#execute(java.util.List,
	 * com.hvn.wwwservice.core.Region, com.hvn.wwwservice.core.TimeRange,
	 * java.util.Set)
	 */
	@Override
	public QueryResult execute(EntityManager em, String table, List<Identity> ids, Region region,
			TimeRange timeRange, QueryRangeRestriction range, Set<String> params) {
		SelectParams selectParameters = new SelectParams();

		// Add Tuple and then select params
		// <HVN> Add id by default - comes handy - also for rowId based query
		selectParameters.add(
				new String[] { "id", "its_car", "its_time_stamp", "latitude",
						"longitude" }).add(params);

		// Populate queries for the Tuple of Datapoint
		String idQuery = populateIDQuery("its_car", ids);
		String regionQuery = populateRegionQuery("latitude", "longitude",
				region);
		String timeQuery = populateTimeQuery("its_time_stamp", timeRange);

		String rowIdQuery = populateRowIdQuery("id", range);
		// Build the Query
		StringBuilder query = new StringBuilder("SELECT "
				+ selectParameters.getSelectString()).append(" FROM " + table);

		if (idQuery != null || timeQuery != null || regionQuery != null
				|| rowIdQuery != null)
			query.append(" WHERE ");

		boolean addAnd = false;
		if (idQuery != null) {
			if (addAnd)
				query.append(" AND ");

			query.append(idQuery);
			addAnd = true;
		}

		if (regionQuery != null) {
			if (addAnd)
				query.append(" AND ");

			query.append(regionQuery);
			addAnd = true;
		}

		if (timeQuery != null) {
			if (addAnd)
				query.append(" AND ");

			query.append(timeQuery);
			addAnd = true;
		}

		if (rowIdQuery != null) {
			if (addAnd)
				query.append(" AND ");

			query.append(rowIdQuery);
			addAnd = true;
		}

		return executeQuery(query.toString(), selectParameters, params, range);
	}

	/**
	 * Based on Primary Key Id
	 * 
	 * @param columnName
	 * @param range
	 * @return
	 */
	private String populateRowIdQuery(String columnName,
			QueryRangeRestriction range) {
		// return early
		if (range == null)
			return null;

		// return early
		if (range.getMinRowId() == QueryRangeRestriction.UNKNOWN_ROW_ID
				&& range.getMaxRowId() == QueryRangeRestriction.UNKNOWN_ROW_ID)
			return null;

		long startId = range.getMinRowId();
		long endId = range.getMaxRowId();

		StringBuilder str = new StringBuilder();
		if (startId != QueryRangeRestriction.UNKNOWN_ROW_ID)
			str.append(columnName + " >= " + startId);

		if (endId != QueryRangeRestriction.UNKNOWN_ROW_ID) {
			if (str.length() > 0)
				str.append(" AND ");

			str.append(columnName + " <= " + endId);
		}

		return str.toString();
	}

	private String populateTupleParams(String idColumn, String timeStampColumn,
			String latitudeColumn, String longitudeColumn) {
		return idColumn + ", " + timeStampColumn + ", " + latitudeColumn + ", "
				+ longitudeColumn;
	}

	static class JDBCQueryResultTest implements QueryResult {
		ResultSet rs = null;
		Connection conn = null;
		Statement stmt = null;
		Set<String> params = null;
		QueryRangeRestriction range;
		int rownum = 0;

		private boolean initResultSet() {
			if (range == null)
				return true;
			
			long minRow = range.getMinRowNum();
			
			try {
				while (rownum < minRow) {
					if (!rs.next())
						return false; // resultset empty
					
					rownum++;
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
			
			return true;
		}
		
		public JDBCQueryResultTest(ResultSet rs, Connection conn, Statement stmt,
				Set<String> params, QueryRangeRestriction range) {
			super();
			this.rs = rs;
			this.conn = conn;
			this.stmt = stmt;
			this.params = params;
			this.range = range;
			
			if (!initResultSet())
				throw new RuntimeException("ResultSet is smaller than start row number!");
		}

		@Override
		public boolean next() {
			try {

				if (range != null && range.getMaxRowNum() != QueryRangeRestriction.UNKNOWN_ROW_NUM && range.getMaxRowNum() <= rownum)
					return false;
				
				rownum++;
				
				return rs.next();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return false;
		}

		@Override
		public DataPoint getDataPoint() {
			try {
				Tuple t = makeTuple(rs);
				DataPoint dataPoint = new DataPoint(t);

				for (String param : params) {
					Object value = rs.getObject(param);
					dataPoint.addParameter(new Parameter(
							new ParameterDef(param), value));
				}

				return dataPoint;

			} catch (SQLException e) {
				e.printStackTrace();
			}
			return null;
		}


		private Tuple makeTuple(ResultSet rs) throws SQLException {
			long carId = rs.getLong("its_car");
			Date itsTimeStamp = TimeStamp.convertDateTime(rs.getDate("its_time_stamp"), rs.getTime("its_time_stamp"));			
			double latitude = rs.getDouble("latitude");
			double longitude = rs.getDouble("longitude");

			return new Tuple(new Identity(carId), new TimeStamp(itsTimeStamp),
					new GeoTile(latitude, longitude));

		}

		@Override
		public boolean isEmpty() {
			try {
				return rs.first() == false;
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return true;
		}

		@Override
		public void close() {
			try {
				rs.close();
				stmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	private static QueryResult executeQuery(String query, SelectParams selectParameters, Set<String> params, QueryRangeRestriction range) {
		System.out.println("Query: " + query);

		Connection conn = null;
		Statement stmt = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			return new JDBCQueryResultTest(rs, conn, stmt, params, range);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String populateTimeQuery(String columnName, TimeRange range) {
		if (range == null)
			return null;

		String startDate = range.getStart().toString();
		String endDate = range.getEnd().toString();
		return columnName + " >= '" + startDate + "' AND " + columnName + " <= '"
				+ endDate + "'";
	}

	private String populateRegionQuery(String latColumn, String longColumn,
			Region region) {
		if (region == null)
			return null;

		String lowerLeftLat = String.valueOf(region.getLowerLeft()
				.getLatitude());
		String lowerLeftLong = String.valueOf(region.getLowerLeft()
				.getLongitude());
		String upperRightLat = String.valueOf(region.getUpperRight()
				.getLatitude());
		String upperRightLong = String.valueOf(region.getUpperRight()
				.getLongitude());

		return latColumn + " >= " + lowerLeftLat + " AND " + latColumn + " <= "
				+ upperRightLat + " AND " + longColumn + " >= " + lowerLeftLong
				+ " AND " + longColumn + " <= " + upperRightLong;
	}

	private String populateIDQuery(String idColumn, List<Identity> ids) {
		if (ids == null)
			return null;

		List<String> strList = new ArrayList<String>();
		for (Identity id : ids)
			strList.add(String.valueOf(id.getId()));

		return idColumn + " IN (" + makeCommaSeparatedString(strList) + ")";
	}

	private String populateParamString(Set<String> params) {
		return makeCommaSeparatedString(params);
	}

	public static String makeCommaSeparatedString(Collection<String> params) {
		StringBuilder builder = new StringBuilder();
		boolean first = true;
		for (String param : params) {
			if (first) {
				builder.append(param);
				first = false;
				continue;
			}

			builder.append(",").append(param);
		}

		return builder.toString();
	}
}
