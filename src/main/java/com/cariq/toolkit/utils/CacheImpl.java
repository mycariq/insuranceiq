package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Basic cache implementation.. Basically a Hashmap with expiry date on entry
 * based on time.
 * 
 * @author HVN
 *
 * @param <KEY_TYPE>
 * @param <VALUE_TYPE>
 */
public class CacheImpl<KEY_TYPE, VALUE_TYPE> implements Cache<KEY_TYPE, VALUE_TYPE> {

	Map<KEY_TYPE, VALUE_TYPE> cache = new LinkedHashMap<KEY_TYPE, VALUE_TYPE>();

	int size;

	/**
	 * Age of an entry kept separate from main data structure
	 * 
	 * @author HVN
	 *
	 * @param <KEY_TYPE>
	 */
	// TODO - uncomment after testing aging module - we don't have millions of
	// cars.. lets go ahead with cache without aging
	// static class CacheAge<KEY_TYPE, VALUE_TYPE> {
	// Map<KEY_TYPE, Long> ageCache = new Hashtable<KEY_TYPE, Long>();
	// long cache_timeout_milisec;
	//
	// /**
	// * @param cache_timeout_milisec
	// */
	// public CacheAge(long cache_timeout_milisec) {
	// super();
	// this.cache_timeout_milisec = cache_timeout_milisec;
	// }
	//
	// public void add(KEY_TYPE key) {
	// ageCache.put(key, (long) System.currentTimeMillis());
	// }
	//
	// public void reset(KEY_TYPE key) {
	// ageCache.put(key, (long) System.currentTimeMillis());
	// }
	//
	// public void removeOldEntries(Map<KEY_TYPE, VALUE_TYPE> cache) {
	// for (KEY_TYPE key : ageCache.keySet()) {
	// if ((System.currentTimeMillis() - ageCache.get(key)) >
	// cache_timeout_milisec) {
	// cache.remove(key);
	// ageCache.remove(key);
	// }
	// }
	// }
	// }
	//
	// CacheAge<KEY_TYPE, VALUE_TYPE> age;

	/**
	 * @param cache_timeout_milisec
	 */
	public CacheImpl(int size) {
		// age = new CacheAge<KEY_TYPE, VALUE_TYPE>(cache_timeout_milisec);
		this.size = size;
	}

	@Override
	public VALUE_TYPE get(KEY_TYPE key) {
		// age.reset(key);
		return cache.get(key);
	}

	@Override
	public int getSize() {
		return cache.size();
	}

	@Override
	public void add(KEY_TYPE key, VALUE_TYPE value) {
		if (cache.size() >= size)
			reset();
		cache.put(key, value);

		// Cleanup cache
		// age.add(key); // TODO - Cache with Age
		// age.removeOldEntries(cache);
	}

	private void reset() {
		List<KEY_TYPE> keys = new ArrayList<KEY_TYPE>();

		for (KEY_TYPE key : cache.keySet()) {
			if (keys.size() >= (size / 2))
				break;
			keys.add(key);
		}

		for (KEY_TYPE key : keys)
			cache.remove(key);
	}
}
