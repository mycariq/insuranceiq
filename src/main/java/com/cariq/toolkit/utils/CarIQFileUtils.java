package com.cariq.toolkit.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import com.cariq.toolkit.utils.profile.ProfilePoint;

public class CarIQFileUtils {
	
	public static final String CSV = "csv";
	public static final String HTML = "htm";
	public static final String ZIP = "zip"; // zip file (containing csv file(s))
	public static final String ZIP_CSV = "zip-csv"; // zip file (containing csv file(s))
	public static final String ZIP_HTML = "zip-htm"; // zip file (containing html file(s))


	public static final String SLASH = "/";
	
	public static String saveToFile(String reportFile, String text) throws IOException {
		File fout = new File(reportFile);
		FileOutputStream fos = new FileOutputStream(fout, true);
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			bw.write(text);
		} finally {
			// close the handle
			if (null != bw) {
				bw.flush();
				bw.close();
			}

			if (null != fos)
				fos.close();
		}

		return reportFile;
	}

	public static String getTempFile(String fileName) {
		return System.getProperty("java.io.tmpdir") + File.separator + Utils.createDateId(true) + "-" + fileName;
	}

	public static void copyContents(String outputFile, String inputFile) throws IOException {
		InputStream input = new FileInputStream(inputFile);
		OutputStream output = new FileOutputStream(outputFile);
		copyContents(output, input);

	}

	public static void copyContents(OutputStream output, InputStream input) throws IOException {
		byte[] buf = new byte[1024];
		int bytesRead;

		while ((bytesRead = input.read(buf)) > 0) {
			output.write(buf, 0, bytesRead);
		}
	}

	public static void copyContents(OutputStream output, String inputFile) throws IOException {
		InputStream input = new FileInputStream(inputFile);
		try {
			copyContents(output, input);
		} finally {
			if (input != null)
				input.close();
		}
	}

	public static void copyContents(String outputFile, InputStream input) throws IOException {
		OutputStream output = new FileOutputStream(outputFile);
		try {
			copyContents(output, input);
		} finally {
			output.close();
		}
	}

	public static long getFileSize(String filePath) {
		File f = new File(filePath);
		return f.length();
	}

	public static void attachCSVFileToResponse(HttpServletResponse response, String outputFilePath) throws Exception {
		// set content description
		attachFileToResponse(response, outputFilePath, "text/csv");
	}

	public static void attachHTMLFileToResponse(HttpServletResponse response, String outputFilePath) throws Exception {
		// set content description
		attachFileToResponse(response, outputFilePath, "text/html");
	}

	public static void attachFileToResponse(HttpServletResponse response, String outputFilePath, String mimeType)
			throws Exception {
		try (ProfilePoint _attachFileToResponse = ProfilePoint.profileAction("ProfAction__attachFileToResponse")) {
			File toBeCopied = new File(outputFilePath);
			if (mimeType == null)
				mimeType = URLConnection.guessContentTypeFromName(toBeCopied.getName());

			String contentDisposition = String.format("attachment; filename=\"%s\"", toBeCopied.getName());
			int fileSize = Long.valueOf(toBeCopied.length()).intValue();

			response.setContentType(mimeType);
			response.setHeader("Content-Disposition", contentDisposition);
			response.setContentLength(fileSize);
			try (OutputStream out = response.getOutputStream()) {
				Path path = toBeCopied.toPath();
				Files.copy(path, out);
				out.flush();
			}
		}
	}

	public static void deleteLocalFile(String fileName) {
		File f = new File(fileName);
		if (f.exists()) {
			//System.out.println("Deleting file: " + fileName);
			f.delete();
		}
	}

	public static class CarIQFileHolder implements AutoCloseable {
		String localFile;

		public CarIQFileHolder() {
			super();
			localFile = CarIQFileUtils.getTempFile("_tmp_" + UUID.randomUUID());
		}

		public CarIQFileHolder(InputStream inputStream) {
			super();
			localFile = CarIQFileUtils.getTempFile("_tmp_" + UUID.randomUUID());
			try {
				CarIQFileUtils.copyContents(localFile, inputStream);
			} catch (IOException e) {
				Utils.handleException(e);
			}
		}

		public String getFile() {
			return localFile;
		}

		@Override
		public void close() {
			CarIQFileUtils.deleteLocalFile(localFile);
		}
	}

	public static boolean exists(String csvFile) {
		File f = new File(csvFile);
		return f.exists();
	}
}
