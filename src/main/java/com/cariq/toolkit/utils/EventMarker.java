package com.cariq.toolkit.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class EventMarker {
	public enum EventMarkerType {
		Daily,
		Weekly,
		Monthly,
		Yearly
	};
	
	public enum TraverseDir {
		At,
		Before,
		After
	};
	
	int day, hour, minute;
	EventMarkerType markerType;
	
	public Date snapToNearestEventMarker(Date date) {
		Date first = getEventMarker(date, TraverseDir.At);
		Date second = date.after(first) ? getEventMarker(first, TraverseDir.After) : getEventMarker(first, TraverseDir.Before);
		
		if (Math.abs(first.getTime() - date.getTime()) < Math.abs(second.getTime() - date.getTime()))
			return first;
		
		return second;
	}
	
	public Date getEventMarker(Date date, TraverseDir direction) {
		// First do for daily - Just set the time at the given date
		if(direction.equals(TraverseDir.At))
			return snapToEvent(date);
		
		return snapToAdjecentEventMarker(snapToEvent(date), direction, 1);
	}

	/**
	 * Snap to the event marker that is next or previous
	 * @param date
	 * @param direction
	 * @return
	 */
	public Date snapToAdjecentEventMarker(Date date, TraverseDir direction, int frequency) {
		if (direction.equals(TraverseDir.At))
			Utils.handleException("Invalid Direction for Time Traversal");
		
		int factor = (direction == TraverseDir.Before) ? -1 : 1;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		switch (markerType) {
		case Daily:
			cal.add(Calendar.DATE, factor * frequency);
			break;
		case Weekly:
			cal.add(Calendar.WEEK_OF_YEAR, factor * frequency);
			break;
		case Monthly:
			cal.add(Calendar.MONTH, factor * frequency);
			break;
		case Yearly:
			cal.add(Calendar.YEAR, factor * frequency);
			break;

		default:
			break;
		}
		
		return cal.getTime();
	}

	private Date snapToEvent(Date date) {
		Date timeAdjusted = adjustTime(date, this.hour, this.minute);
		switch (markerType) {
		case Daily:
			return timeAdjusted;
		case Weekly:
			return adjustDay(timeAdjusted, this.day, Calendar.DAY_OF_WEEK);
		case Monthly:
			return adjustDay(timeAdjusted, this.day, Calendar.DAY_OF_MONTH);
		case Yearly:
			return adjustDay(timeAdjusted, this.day, Calendar.DAY_OF_YEAR);

		default:
			break;
		}
			
		return timeAdjusted;
	}

	private static Date adjustTime(Date date, int hour, int minute) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		Calendar cal2 = new GregorianCalendar(cal1.get(Calendar.YEAR), cal1.get(Calendar.MONTH), cal1.get(Calendar.DATE), hour, minute);
		return cal2.getTime();
	}
	

	private static Date adjustDay(Date date, int day, int field) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		cal1.set(field, day);

		return cal1.getTime();
	}

	private EventMarker(EventMarkerType type, int day, int hour, int minute) {
		markerType = type;
		this.day = day;
		this.hour = hour;
		this.minute = minute;
	}
	
	static EventMarker getInstance(int frequencyUnit, int day, int hour, int minute) {
		if (day < 0 && hour < 0 && minute < 0)
			return null;
		
		switch (frequencyUnit) {
		case Calendar.HOUR_OF_DAY: // on a fixed hour per day
			return new EventMarker(EventMarkerType.Daily, -1, hour, minute);
		case Calendar.DAY_OF_WEEK: // fixed Day per week
			return new EventMarker(EventMarkerType.Weekly, day, hour, minute);		
		case Calendar.DAY_OF_MONTH: // fixed Day per month
			return new EventMarker(EventMarkerType.Monthly, day, hour, minute);		
		case Calendar.DAY_OF_YEAR: // fixed day per year
			return new EventMarker(EventMarkerType.Yearly, day, hour, minute);
		default:
			break;
		}
		return null;
	}	
//	
//	static EventMarker getDaily(int hour, int minute) {
//		return new EventMarker(EventMarkerType.Daily, -1, hour, minute);
//	}		
//	
//	static EventMarker getWeekly(int dayOfTheWeek, int hour, int minute) {
//		return new EventMarker(EventMarkerType.Weekly, dayOfTheWeek, hour, minute);
//	}
//	
//	static EventMarker getMonthly(int dayOfTheMonth, int hour, int minute) {
//		return new EventMarker(EventMarkerType.Monthly, dayOfTheMonth, hour, minute);
//	}
//	
//	static EventMarker getYearly(int dayOfTheYear, int hour, int minute) {
//		return new EventMarker(EventMarkerType.Yearly, dayOfTheYear, hour, minute);
//	}	

	public Date getEventMarkerAfter(int frequency, Date snappedEventMarker) {
		return snapToAdjecentEventMarker(snappedEventMarker, EventMarker.TraverseDir.After, frequency);
	}
}
