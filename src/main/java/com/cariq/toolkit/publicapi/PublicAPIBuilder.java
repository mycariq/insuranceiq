/**
 * 
 */
package com.cariq.toolkit.publicapi;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cariq.toolkit.publicapi.swagger.Contact;
import com.cariq.toolkit.publicapi.swagger.Definition;
import com.cariq.toolkit.publicapi.swagger.Delete;
import com.cariq.toolkit.publicapi.swagger.Get;
import com.cariq.toolkit.publicapi.swagger.HttpMethod;
import com.cariq.toolkit.publicapi.swagger.Info;
import com.cariq.toolkit.publicapi.swagger.License;
import com.cariq.toolkit.publicapi.swagger.Parameter;
import com.cariq.toolkit.publicapi.swagger.Post;
import com.cariq.toolkit.publicapi.swagger.PropertyAttr;
import com.cariq.toolkit.publicapi.swagger.Put;
import com.cariq.toolkit.publicapi.swagger.Response;
import com.cariq.toolkit.publicapi.swagger.Security;
import com.cariq.toolkit.publicapi.swagger.SecurityDefinition;
import com.cariq.toolkit.publicapi.swagger.SwaggerPublicAPIDefinition;
import com.cariq.toolkit.publicapi.swagger.Tag;
import com.cariq.toolkit.publicapi.swagger.Xml;
import com.cariq.toolkit.utils.BeanHelper;

/**
 * @author hrishi
 *
 */

public class PublicAPIBuilder {

	List<Class<?>> controllers = new ArrayList<Class<?>>();
    private static String SERVER_URL = "localhost"; //@TMPL private static String SERVER_URL = "[% ROOT.servers.api.endPoint %]"; @TMPL
    private static String SERVER_PORT = "8443"; //@TMPL private static String SERVER_PORT = "[% ROOT.servers.api.port %]"; @TMPL
    private static String SERVER_BASE_PATH = "/insuranceiq"; 

	SwaggerPublicAPIDefinition swaggerPublicAPIDefinition = new SwaggerPublicAPIDefinition("2.0",
			new Info("CarIQ Public API", "0.1",
					"CarIQ Platform Public API. Captures all the interesting ways you can use the Platform. (Documentation is work in Progress...)",
					"", new Contact("hrishi@mycariq.com"), new License("CarIQ", "http://www.mycariq.com")),
					SERVER_URL + ":" + SERVER_PORT, SERVER_BASE_PATH);

	SwaggerPublicAPIDefinition swaggerInternalAPIDefinition = new SwaggerPublicAPIDefinition("2.0",
			new Info("CarIQ Internal API", "0.1",
					"CarIQ Platform Internal API. Captures all the interesting ways you can use the Platform. (Documentation is work in Progress...)",
					"", new Contact("hrishi@mycariq.com"), new License("CarIQ", "http://www.mycariq.com")),
					SERVER_URL + ":" + SERVER_PORT, SERVER_BASE_PATH);

	/**
	 * @param controller
	 */
	public void add(Class<?> controller) {
		controllers.add(controller);
	}

	/**
	 * Prepare the skeleton and other details that are common
	 */
	public void init() {
		swaggerPublicAPIDefinition.add(Security.USER_AUTH, SecurityDefinition.getUserAuth());
		swaggerPublicAPIDefinition.addScheme("https");
		
		swaggerInternalAPIDefinition.add(Security.USER_AUTH, SecurityDefinition.getUserAuth());
		swaggerInternalAPIDefinition.addScheme("https");

	}

	/**
	 * 
	 */
	public void process() {
		for (Class<?> controller : controllers) {
			if (!isPublicAPI(controller))
				continue;

			processController(controller);
		}
	}

	/**
	 * @param controller
	 */
	private void processController(Class<?> controller) {
		CarIQPublicAPI publicAPI = controller.getAnnotation(CarIQPublicAPI.class);
		RequestMapping reqMapping = controller.getAnnotation(RequestMapping.class);
		// Not registered as a public API
		if (publicAPI == null || reqMapping == null)
			return;

		Tag controllerTag = new Tag(publicAPI.name(), publicAPI.description());
		swaggerPublicAPIDefinition.add(controllerTag);
		swaggerInternalAPIDefinition.add(controllerTag);

		// process all the methods and create the definitions
		Method[] allMethods = controller.getDeclaredMethods();
		for (Method method : allMethods) {
			if (!Modifier.isPublic(method.getModifiers()))
				continue;
			
			String url =  (reqMapping.value() == null || reqMapping.value().length == 0) ? "/" : reqMapping.value()[0]; 
			processMethod(controllerTag, url, method);
		}
	}

	/**
	 * @param controller
	 */
	private void processMethod(Tag controller, String controllerUrl, Method method) {
		CarIQPublicAPIMethod publicAPIMethod = method.getAnnotation(CarIQPublicAPIMethod.class);
		RequestMapping reqMapping = method.getAnnotation(RequestMapping.class);

		// Not registered as a public API Method
		if (publicAPIMethod == null || reqMapping == null)
			return;

		// Extract method details
		String methodDescription = publicAPIMethod.description();
		String methodUrl =  (reqMapping.value() == null || reqMapping.value().length == 0) ? "/" : reqMapping.value()[0]; 

		RequestMethod methodType = (reqMapping.method() != null && reqMapping.method().length > 0)
				? reqMapping.method()[0] : RequestMethod.GET;
		Class<?> methodResponse = publicAPIMethod.responseClass();

		// Get the httpMethod from Type and url
		HttpMethod httpMethod = createMethodObject(methodType, methodDescription, methodUrl, controller.getName(),
				methodResponse);
		processParameters(httpMethod, method);
		if (httpMethod == null)
			return;

		if (!publicAPIMethod.internal()) {
			swaggerPublicAPIDefinition.add(controllerUrl + methodUrl, methodType.toString().toLowerCase(), httpMethod);
			swaggerPublicAPIDefinition.add(Security.USER_AUTH, SecurityDefinition.getUserAuth());
		}
		
		swaggerInternalAPIDefinition.add(controllerUrl + methodUrl, methodType.toString().toLowerCase(), httpMethod);
		swaggerInternalAPIDefinition.add(Security.USER_AUTH, SecurityDefinition.getUserAuth());

	}

	/**
	 * @param httpMethod
	 * @param method
	 */
	private void processParameters(HttpMethod httpMethod, Method method) {
		// Method Parameters with Annotations
		Annotation[][] annotationsMatrix = method.getParameterAnnotations();
		Class<?>[] paramTypes = method.getParameterTypes();
		int i = 0;
		int numOfParameters = annotationsMatrix.length;
//		System.out.println("Parameters Count = " + numOfParameters);

		for (Annotation[] annotationsArr : annotationsMatrix) {
			int numOfAnnotations = annotationsArr.length;
//			System.out.println("Annotations Count = " + numOfAnnotations);
			for (Annotation anno : annotationsArr) {
				if (anno instanceof CarIQPublicAPIParameter) {
//					System.out.println("Path Parameter No. " + i);

					CarIQPublicAPIParameter publicAPIParam = (CarIQPublicAPIParameter) anno;
					String paramType = paramTypes[i].getSimpleName();
					Parameter param = new Parameter("path", publicAPIParam.name(), publicAPIParam.description(), true,
							paramType);

					// Add the param
					httpMethod.add(param);
					break; // go to next parameter

				}

				// if it's a body parameter, handle it differently
				if (anno instanceof RequestBody) {
//					System.out.println("Body Parameter No. " + i);

					// CarIQPublicAPIBodyParameter publicAPIBodyParam = (CarIQPublicAPIBodyParameter) anno;
					Class<?> paramType = paramTypes[i];
					String paramName = paramType.getSimpleName();
					Parameter param = null;
					if (createDefinition(paramType)) {
						param = new Parameter("body", "body", "input json", true, swaggerPublicAPIDefinition.getDefinitions().get(paramName));
						param = new Parameter("body", "body", "input json", true, swaggerInternalAPIDefinition.getDefinitions().get(paramName));
					}
					else {
						param = new Parameter("body", "body", "input json", true, paramName);
					}

					// Add the param
					httpMethod.add(param);
					break; // go to next parameter

				}

			}
			i++;

		}
	}

	/**
	 * @param meth
	 * @param methodDescription
	 * @param methodUrl
	 * @param methodResponse
	 * @return
	 */
	private HttpMethod createMethodObject(RequestMethod meth, String methodDescription, String methodUrl,
			String controller, Class<?> methodResponse) {
		HttpMethod httpMethod = null;
		switch (meth) {
		case GET:
			httpMethod = new Get(methodDescription, methodDescription, methodUrl);
			break;
		case POST:
			httpMethod = new Post(methodDescription, methodDescription, methodUrl);
			break;
		case PUT:
			httpMethod = new Put(methodDescription, methodDescription, methodUrl);
			break;
		case DELETE:
			httpMethod = new Delete(methodDescription, methodDescription, methodUrl);
			break;
		default:
			return null;
		}

		// Associate method to its controller
		httpMethod.addTag(controller);

		// Add url Parameters to method

		// set security parameter of the method.
		HashMap<String, List<String>> security = new HashMap<String, List<String>>();
		security.put(Security.USER_AUTH, new ArrayList<String>());
		httpMethod.add(security);
		httpMethod.add(Response.getDefaultResponses());

		// Add method response
		httpMethod.add("200", processResponse("Success!", methodResponse));
		httpMethod.addProduces("application/json");

		return httpMethod;
	}

	/**
	 * @param methodResponse
	 * @return
	 */
	private Response processResponse(String responseDescription, Class<?> responseType) {
		String typeDefName = responseType.getSimpleName();

		if (!createDefinition(responseType))
			return new Response(responseDescription);

		Response response = new Response(responseDescription, swaggerInternalAPIDefinition.getDefinitions().get(typeDefName));

		return response;
	}

	/**
	 * @param typeDefName
	 */
	private boolean createDefinition(Class<?> type) {
		String typeDefName = type.getSimpleName();

		// Check for definition in the cache
		if (swaggerInternalAPIDefinition.getDefinitions().containsKey(typeDefName))
			return true;

		// Try to create Definition
		Definition typeDef = makeDefinition(type);
		if (typeDef == null) // no definition to refer to
			return false;

		swaggerPublicAPIDefinition.add(typeDefName, typeDef);
		swaggerInternalAPIDefinition.add(typeDefName, typeDef);

		return true;

	}

	/**
	 * Make Definition of object for reference in the Definitions section
	 * 
	 * @param responseType
	 * @return
	 */
	private Definition makeDefinition(Class<?> responseType) {
		if (responseType.isAnonymousClass() || responseType.isArray() || responseType.equals(Object.class))
			return null; // not supported

		String typeName = responseType.getSimpleName();
		// return Definition containing Properties
		Definition def = new Definition("object", new Xml(typeName));

		Map<String, Class<?>> attrs = BeanHelper.getAttributeProperties(responseType);
		for (String attr : attrs.keySet()) {
			if (attr.equals("class"))
				continue;
			
			def.addProperty(attr, new PropertyAttr(simplifyType(attrs.get(attr).getSimpleName()), attrs.get(attr).getSimpleName()));
		}

		return def;
	}

	/**
	 * @param simpleName
	 * @return
	 */
	private String simplifyType(String typeName) {
		if (typeName.equals("Long") || typeName.equals("Integer") || typeName.equals("int") || typeName.equals("long"))
			return "integer";
	
		if (typeName.equals("Double") || typeName.equals("Float") || typeName.equals("double") || typeName.equals("float"))
			return "number";

		if (typeName.equals("Boolean") || typeName.equals("boolean"))
			return "boolean";
		
//		if (typeName.equals("List"))
//			return "array";

		if (typeName.equals("String"))
			return "string";
		
		return "object";
	}

	/**
	 * @param controller
	 * @return
	 */
	private boolean isPublicAPI(Class<?> controller) {
		return controller.getAnnotation(CarIQPublicAPI.class) != null;
	}

	/**
	 * 
	 */
	public void postProcess() {
	}

	/**
	 * Assemble everything and return the Definition Object
	 * 
	 * @return APIDefinition
	 */
	public SwaggerPublicAPIDefinition getPublicAPIDefinition() {
		return swaggerPublicAPIDefinition;
	}
	
	public SwaggerPublicAPIDefinition getInternalAPIDefinition() {
		return swaggerInternalAPIDefinition;
	}

}
