/**
 * 
 */
package com.cariq.toolkit.publicapi;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author hrishi
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CarIQPublicAPIMethod {

	String description();

	Class<?> responseClass();

	boolean internal() default false;
}
