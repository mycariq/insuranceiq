/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hrishi
 *
 */
public class SwaggerPublicAPIDefinition {

	private String swagger;
	private Info info;
	private String host;
	private String basePath;
	private List<Tag> tags = new ArrayList<Tag>();
	private List<String> schemes = new ArrayList<String>();
	// Paths is map of url and http Object
	private HashMap<String, Map<String, HttpMethod>> paths = new HashMap<String, Map<String, HttpMethod>>();
	private Map<String, SecurityDefinition> securityDefinitions = new HashMap<String, SecurityDefinition>();
	private Map<String, Definition> definitions = new HashMap<String, Definition>();

	/**
	 * @param swagger
	 * @param info
	 * @param host
	 * @param basePath
	 */
	public SwaggerPublicAPIDefinition(String swagger, Info info, String host, String basePath) {
		this.swagger = swagger;
		this.info = info;
		this.host = host;
		this.basePath = basePath;
	}

	public SwaggerPublicAPIDefinition add(Tag tag) {
		tags.add(tag);
		return this;
	}

	public SwaggerPublicAPIDefinition addScheme(String scheme) {
		schemes.add(scheme);
		return this;
	}

	public SwaggerPublicAPIDefinition add(String url, String method, HttpMethod methodObject) {
		HashMap<String, HttpMethod> httpMethod = new HashMap<String, HttpMethod>();
		httpMethod.put(method, methodObject);
		paths.put(url, httpMethod);	
		return this;
	}

	public SwaggerPublicAPIDefinition add(String securityDefName, SecurityDefinition securityDef) {
		securityDefinitions.put(securityDefName, securityDef);
		return this;
	}

	public SwaggerPublicAPIDefinition add(String defName, Definition def) {
		definitions.put(defName, def);
		return this;
	}

	public String getSwagger() {
		return swagger;
	}

	public void setSwagger(String swagger) {
		this.swagger = swagger;
	}

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(ArrayList<Tag> tags) {
		this.tags = tags;
	}

	public List<String> getSchemes() {
		return schemes;
	}

	public void setSchemes(ArrayList<String> schemes) {
		this.schemes = schemes;
	}



	public Map<String, SecurityDefinition> getSecurityDefinitions() {
		return securityDefinitions;
	}

	public void setSecurityDefinitions(Map<String, SecurityDefinition> securityDefinitions) {
		this.securityDefinitions = securityDefinitions;
	}

	public Map<String, Definition> getDefinitions() {
		return definitions;
	}

	public void setDefinitions(Map<String, Definition> definitions) {
		this.definitions = definitions;
	}

	public HashMap<String, Map<String, HttpMethod>> getPaths() {
		return paths;
	}

	public void setPaths(HashMap<String, Map<String, HttpMethod>> paths) {
		this.paths = paths;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public void setSchemes(List<String> schemes) {
		this.schemes = schemes;
	}

}
