/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hrishi
 *
 */
public abstract class HttpMethod {

	private List<String> tags = new ArrayList<String>();
	private String summary;
	private String description;
	private String operationId;
	private HashMap<String, Response> responses = new HashMap<String, Response>();
	private List<HashMap<String, List<String>>> security = new ArrayList<HashMap<String, List<String>>>();

	private List<String> produces = new ArrayList<String>();
	private List<Parameter> parameters = new ArrayList<Parameter>();

	/**
	 * @param summary
	 * @param description
	 * @param operationId
	 */
	public HttpMethod(String summary, String description, String operationId) {
		this.summary = summary;
		this.description = description;
		this.operationId = operationId;
	}

	public HttpMethod add(String responseCode, Response response) {
		responses.put(responseCode, response);
		return this;
	}
	
	/**
	 * @param defaultResponses
	 */
	public HttpMethod add(Map<String, Response> responseMap) {
		responses.putAll(responseMap);
		return this;
	}

	public HttpMethod add(HashMap<String, List<String>> securityDef) {
		security.add(securityDef);
		return this;
	}

	public HttpMethod add(Parameter param) {
		parameters.add(param);
		return this;
	}

	public HttpMethod addProduces(String prds) {
		produces.add(prds);
		return this;
	}

	public HttpMethod addTag(String tag) {
		tags.add(tag);
		return this;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public HashMap<String, Response> getResponses() {
		return responses;
	}

	public void setResponses(HashMap<String, Response> responses) {
		this.responses = responses;
	}

	public List<HashMap<String, List<String>>> getSecurity() {
		return security;
	}

	public void setSecurity(List<HashMap<String, List<String>>> security) {
		this.security = security;
	}

	public List<String> getProduces() {
		return produces;
	}

	public void setProduces(List<String> produces) {
		this.produces = produces;
	}

	public List<Parameter> getParameters() {
		return parameters;
	}

	public void setParameters(ArrayList<Parameter> parameters) {
		this.parameters = parameters;
	}

}
