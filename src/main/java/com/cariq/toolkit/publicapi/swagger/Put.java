/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hrishi
 *
 */
public class Put extends HttpMethod {

	private List<String> consumes = new ArrayList<String>();

	/**
	 * @param summary
	 * @param description
	 * @param operationId
	 */
	public Put(String summary, String description, String operationId) {
		super(summary, description, operationId);
	}

	public Put addConsumes(String consume) {
		consumes.add(consume);
		return this;
	}

	public List<String> getConsumes() {
		return consumes;
	}

	public void setConsumes(List<String> consumes) {
		this.consumes = consumes;
	}


}
