/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;

/**
 * @author hrishi
 *
 */
public class PropertyAttr {

	private String type; // integer, string etc.
	private String format; // Object by default
	private boolean required = true; // Attribute is required by default

	/**
	 * @param type
	 * @param format
	 */
	public PropertyAttr(String type, String format) {
		this.type = type;
		this.format = format;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}
	
	

}
