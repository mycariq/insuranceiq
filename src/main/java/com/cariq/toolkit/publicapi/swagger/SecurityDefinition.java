/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hrishi
 *
 */
public class SecurityDefinition {

	private String type;
	Map<String, String> scopes = new HashMap<String, String>();

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, String> getScopes() {
		return scopes;
	}

	public void setScopes(HashMap<String, String> scopes) {
		this.scopes = scopes;
	}

	/**
	 * @param type
	 */
	public SecurityDefinition(String type) {
		this.type = type;
	}

	public SecurityDefinition addScope(String name, String scope) {
		scopes.put(name, scope);
		return this;
	}

	/**
	 * @return
	 */
	public static SecurityDefinition getUserAuth() {
		SecurityDefinition basicAuth =  new SecurityDefinition("basic");
		
		return basicAuth;
	}

}
