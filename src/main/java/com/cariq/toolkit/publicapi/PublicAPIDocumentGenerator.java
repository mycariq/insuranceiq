/**
 * 
 */
package com.cariq.toolkit.publicapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import com.cariq.toolkit.publicapi.swagger.SwaggerPublicAPIDefinition;
import com.insuranceiq.www.web.DomainController;
import com.insuranceiq.www.web.ScoringEngineController;
import com.insuranceiq.www.web.SubscriptionController;
import com.insuranceiq.www.web.TagController;
import com.insuranceiq.www.web.UserController;



/**
 * @author hrishi
 *
 */
public class PublicAPIDocumentGenerator {
	static PublicAPIDocumentGenerator _instance;

	@Resource(name = "configProperties")
	private Properties configProperties;

	private SwaggerPublicAPIDefinition publicAPI;
	private SwaggerPublicAPIDefinition internalAPI;

	static List<Class<?>> controllers = new ArrayList<Class<?>>();

	static {
		
/*		controllers.add(ReportController.class);
		controllers.add(CarIQExporterController.class);
		controllers.add(CarIQCompositeAPIController.class);
		controllers.add(CarIQLoaderController.class);
		controllers.add(WorkItemController.class);
		controllers.add(JobController.class);*/
		
		controllers.add(UserController.class);
		controllers.add(DomainController.class);
		controllers.add(TagController.class);
		controllers.add(SubscriptionController.class);
		controllers.add(ScoringEngineController.class);		
	}

	/**
	 * 
	 */
	public PublicAPIDocumentGenerator() {
		PublicAPIBuilder builder = new PublicAPIBuilder();

		// from configurator, load the Controllers from comma separated list - for now, hardcoded list in static block

		for (Class<?> controller : controllers) {
			builder.add(controller);
		}

		builder.init();
		builder.process();
		builder.postProcess();

		publicAPI = builder.getPublicAPIDefinition();
		internalAPI = builder.getInternalAPIDefinition();
	}

	/**
	 * @return
	 */
	public static PublicAPIDocumentGenerator getInstance() {
		//		// Temp - for debugging
		//		return new PublicAPIDocumentGenerator();
		//		@TODO Temp
		if (_instance == null) {
			_instance = new PublicAPIDocumentGenerator();
		}

		return _instance;
	}

	/**
	 * @return
	 */
	public SwaggerPublicAPIDefinition getPublicAPI() {
		return publicAPI;
	}

	/**
	 * @return
	 */
	public SwaggerPublicAPIDefinition getInternalAPI() {
		return internalAPI;
	}

}
